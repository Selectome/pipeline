#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::EnsemblDB;
use Selectome::Utils;


############################## Options management ##############################
my ($ac, $db, $debug) = ('', 'ensembl', 0);
my %opts = ('ac=s'    => \$ac,        # Ensembl gene tree accession
            'db=s'    => \$db,        # DB host
            'debug'   => \$debug,     # Debug mode
           );


# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $ac eq '' ){
    Selectome::Utils::error("Invalid parameter(s)\n\n\te.g.: \e[1;37;42m$0 --ac=ENSGT00550000074475\e[m
\t--ac=          Ensembl gene tree accession
\t--db=...       DB host to get families: ensembl (default), genomes (ensembl genomes) or local
\t--debug        Debug mode: print extra information");
}


my $connection_param = Selectome::EnsemblDB::get_connection_parameters($db);

############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => $debug,
);


############################## Get the Adaptors ##############################
my $tree_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'GeneTree');
my $tree         = $tree_adaptor->fetch_by_stable_id($ac);

print "ac: $ac\t\tid: ", $tree->root_id(), "\n";

exit 0;

