#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::EnsemblDB;

use DBI;
use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68\n\n";


#### Get ensembl gene id / protein id for subtree table
my $dbh      = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_gene = $dbh->prepare('SELECT own_id, id, prot_id, transcript_id, gene_id FROM gene');
$sth_gene->execute()  or die $sth_gene->errstr;
my $gene_ref = $sth_gene->fetchall_arrayref;
$sth_gene->finish();


#### Tree parsing ####
if ( exists($gene_ref->[0]) ){
    for my $row (@$gene_ref){
        my ($own_id, $id, $prot_id, $transcript_id, $gene_id) = @$row;

        # Get subtree
        my $sth_id = $dbh->prepare('SELECT tree FROM subtree WHERE id=?');
        $sth_id->execute($id)  or die $sth_id->errstr;
        my $tree_ref = $sth_id->fetchall_arrayref;
        $sth_id->finish();
        my $taxid = 0;
        TREE:
        for my $trow (@$tree_ref){
            my $tree = $trow->[0];
            # Parse subtree
            my @nhx  = grep { /:PR=$prot_id/ && /:G=$gene_id/ & /:TR=$transcript_id/ }
                       split(/\[&&NHX/, $tree);
            warn "\tParsing problem for gene $gene_id, id:$id - own_id:$own_id\n"  if ( exists($nhx[1]) || !exists($nhx[0]) );

            # Get taxid for this species gene
            my ($taxID) = $nhx[0] =~ /:T=(\d+)/;
            warn "\ttaxid problem: $taxid - $taxID\n"  if ( $taxid!=0 && $taxid!=$taxID );
            $taxid = $taxID;
        }

        # Update taxid
        my $sth_tax = $dbh->prepare('UPDATE gene SET taxid=? WHERE own_id=?');
        $sth_tax->execute($taxid, $own_id)  or die $sth_tax->errstr;
        $sth_tax->finish();
    }
}

$dbh->disconnect or warn $dbh->errst;
exit 0;

