#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use lib '.';
use DB;

my $dbname = $ARGV[0]  or die "\n\tNo db provided: $0 <db name>\n\tE.g. $0 selectome_v06_ens68\n\n";
my $path   = '/var/SELECTOME';

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sthID = $dbh->prepare('SELECT i.AC, i.taxon, i.subfamily, s.tree FROM subtree s, id i WHERE i.id=s.id');
$sthID->execute()  or die $sthID->errstr;
my $ID_ref = $sthID->fetchall_arrayref;
$sthID->finish();

ID:
for my $row (@$ID_ref){
    my ($AC, $taxon, $subfamily, $tree) = @$row;

    my $file_path = "$path/$AC/$taxon/$AC.$taxon.".sprintf('%03d', $subfamily).'.nhx';
    if ( !-e "$file_path" ){
        warn "[$file_path] not there!\n";
        mkdir "$path/$AC/";
        mkdir "$path/$AC/$taxon/";
    }
    write_file("$file_path", $tree);
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;

