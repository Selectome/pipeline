#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use FindBin qw($Bin);
use lib "$Bin";

use Selectome::EnsemblDB;
use Selectome::Taxa;

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;


our $VERSION = 0.9.9;

#########################################
# Get Trees and MSA from ensembl        #
# and prepare ctl files for codeml ONLY #
# Submission by another script          #
#########################################


my ($subtaxon, $target_db, $fam_nbr) = (0, 'ensembl', 100_000_000_000);
my %opts = ("taxon=s"      => \$subtaxon,       # NCBI tax_id for expected root tree
            "db=s"         => \$target_db,      # DB host
            'fam_nbr=i'    => \$fam_nbr,        # Family number to run on
           );


# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $subtaxon eq '0' ){
    error("Taxon not found or invalid parameters\n\n\te.g.: \e[1;37;42m$0 --taxon=vertebrates\e[m
\t      \e[1;37;42m$0 --taxon=vertebrates    --db=local_e\e[m
\t      \e[1;37;42m$0 --taxon=arthropoda     --db=metazoa\e[m
\t      \e[1;37;42m$0 --taxon=arthropoda     --db=local_m\e[m
\t      \e[1;37;42m$0 --taxon=amniota        --db=annot_e\e[m
\t      \e[1;37;42m$0 --taxon=hexapoda       --db=annot_m\e[m
\t      \e[1;37;42m$0 --taxon=clupeocephala\e[m
\t--taxon=...    NCBI tax_id or name for expected root tree
\t--db=...       DB host to get families: ensembl (default), genomes (ensembl genomes), local or annotbioinfo computer
\t--fam_nbr=     Family number to run on (default: 100'000'000'000)\n");
}

error('Invalid taxon') if ( ! exists( $Selectome::Taxa::valid_taxa->{lc($subtaxon)} ) );
$subtaxon = $Selectome::Taxa::valid_taxa->{lc($subtaxon)};

my $connection_param = Selectome::EnsemblDB::get_connection_parameters($target_db);



############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => 1,  # for debug purpose !
);




############################## Get the Adaptors ##############################
my $tree_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'GeneTree');


############################## fetch all roots ###############################
# In a release database, there is a basal root that holds together all the trees.
my @children = @{$tree_adaptor->fetch_all(-tree_type     => 'tree',
                                          -member_type   => 'protein',
                                          -clusterset_id => 'default',
                                         )}; # Protein tree only !


system('rm', '-f', 'error/*');
mkdir 'error';
my $famNbr = 0;
TREE:
for my $tree (sort {$a->stable_id cmp $b->stable_id} (@children)) { #Sort to always get the same order
    my $node_id = $tree->root->node_id;

    $famNbr++;
    exit 0 if ( $famNbr > $fam_nbr );

    system("/usr/bin/time -p   ./prepare_MSA_1-to-1_ortholog_trees.pl --node_id=$node_id --taxon=$subtaxon --db=$target_db");
    $tree->root->release_tree;
}

$reg->clear();
print "\t[$famNbr] families\n";
exit 0;


#####################################################################################
sub error {
    my ($msg, $id) = @_;

    print "\n\t$msg\n\n";
    exit 1;
}

