#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;


H0MLC:
for my $mlc ( `find ENSGT* -type f -name \*.H0.mlc` ){
    chomp $mlc;
    my $h0mlc = $mlc;
    my $h1mlc = $mlc;
    $h1mlc =~ s{H0\.mlc}{H1\.mlc};

    if ( !-e "$h1mlc" || -z "$h1mlc" ){
        unlink $h0mlc, $h1mlc;
    }
    elsif ( -s "$h1mlc" && -s "$h0mlc" ){
        my $completed0 = `tail -1 $h0mlc | grep '^Time used:'`;
        my $completed1 = `tail -1 $h1mlc | grep '^Time used:'`;

        unlink $h0mlc, $h1mlc  if ( !$completed0 || !$completed1 );
    }
    else {
        print "\t\t$mlc\n";
    }
}

exit 0;

