#!/bin/sh

for all in `find . -type f -name DONE`
do
    dir=${all/%DONE/}
    find $dir -type f ! -name \*.gz -exec gzip -9 {} \;
done

echo -e "cleaning..."
find . -type f -name DONE.gz -exec gunzip {} \;

exit

