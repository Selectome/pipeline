#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;
use FindBin qw($Bin);
use lib "$Bin";

use Selectome::MSA;
use Selectome::Codeml;


my $fasta_file = $ARGV[0]  || die "\n\t$0 prot_fasta_file newick_file\n\n";
my $tree_file  = $ARGV[1]  || die "\n\t$0 prot_fasta_file newick_file\n\n";

# prot back to nt
Selectome::MSA::backTranslate_mask('HBG002065-1_dna.fasta', "$fasta_file", '');

# Convert fasta MSA to Phylip
#Selectome::MSA::convert_fasta_to_phylip("$fasta_file", "$fasta_file.phy");
Selectome::MSA::convert_fasta_to_phylip('HBG002065-1_dna.fasta.preTrimAl', "$fasta_file.phy");

# Protect seq name in Phylip format + change back in tree file
my $nwk = read_file( "$tree_file" );
$nwk    = Selectome::MSA::protect_phylip_msa($nwk, "$fasta_file");
write_file("$fasta_file.nwk", $nwk);
#print $nwk;

# Prepare ctl files
Selectome::Codeml::create_CTL_file("$fasta_file", 1, 0, 2, ''); #H0
Selectome::Codeml::create_CTL_file("$fasta_file", 1, 1, 2, ''); #H1

# Fix because do not run M0 first
system("perl -i -pe 's{fix_omega = \\S*}{fix_omega = 0};s{fix_blength = 1 }{fix_blength = 0 };s{\.1\.nwk}{.nwk}' $fasta_file*.ctl");


exit 0;

