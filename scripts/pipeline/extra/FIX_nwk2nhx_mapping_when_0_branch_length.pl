#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;
use FindBin qw($Bin);
use lib "$Bin";

use Selectome::Tree;
use Selectome::Utils;

my $branchid_length = $Selectome::Utils::branchid_length;


my $in_nhx  = $ARGV[0]  or die "\n\t$0 ori_nhx_file\n\n";

if ( $in_nhx =~ /\.gz$/ && -B "$in_nhx" ){
    system('gunzip', "$in_nhx");
    $in_nhx =~ s{\.gz$}{};
}


# Find nwk files in subdir
my $in_nwk = '';
my ($dir, $sub) = $in_nhx =~ /^(.+)\/\w+\.\w+\.(\d+)\.nhx\.ORI/;
NWK:
for my $nwk ( glob("$dir/$sub.*/*.nwk") ){
    $in_nwk = $nwk;
    last NWK;
}


# ENSGT00670000098226.Euteleostomi.001.nhx.ORI.gz
my $out_nhx = $in_nhx;
$out_nhx   =~ s{\.ORI$}{};

my $nwk     = read_file("$in_nwk", chomp => 1);
my $nhx     = read_file("$in_nhx", chomp => 1);


# Remove #1 in nwk if any
$nwk =~ s{#1}{}g;


# Label NHX branches
my $branch_nbr = 0;
while( $nhx =~ m/\)\w*:[\d\.eE\-]*\[/ ){
    $branch_nbr = sprintf("%0${branchid_length}d", ++$branch_nbr);
    $nhx =~ s{(\)\w*):([\d\.eE\-]*\[)}{$1===$branch_nbr=:$2};
}
$nhx =~ s{:[\d\.eE-]*(\[[^]]+?\]);}{:0.0$1;}; #Remove root length


# Mapping new Newick tree (from M0 model) + Label NHX tree
#    => M0 branch lengths + node labels (not seqname protected)
if ( $nhx ne '' ){
    $nhx = Selectome::Tree::map_nwk_to_nhx($nhx, $nwk);


    # Write final clean NHX file !
    #   Only the NHX file is required because easy to convert it to newick format
#    print $nwk, "\n", $nhx, "\n", $out_nhx, "\n";
    write_file("$out_nhx", $nhx);
}

exit 0;

