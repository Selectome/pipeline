#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::MSA;

my $infile  = $ARGV[0];
my $outfile = $infile;
$outfile   =~ s{\.fas}{.phy};
Selectome::MSA::convert_fasta_to_phylip("$infile", "$outfile");

exit 0;

