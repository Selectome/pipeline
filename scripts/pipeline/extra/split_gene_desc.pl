#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68\n\n";


#### Get species taxid
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_gene = $dbh->prepare('SELECT id, own_id, description FROM gene');
$sth_gene->execute()  or die $sth_gene->errstr;
my $gene_ref = $sth_gene->fetchall_arrayref;
$sth_gene->finish();


####
if ( exists($gene_ref->[0]) ){
    GENE:
    for my $row (@$gene_ref){
        my ($id, $own_id, $description) = @$row;

        print "$id\t$own_id\t$description\n";
        next GENE  if ( $description eq '' );
        next GENE  if ( $description !~ / \[Source:/ );


        my $source = '';
        ($description, $source) = $description =~ /^(.+?) \[Source:(.+?)\].*$/;
        print "$id\t$own_id\t$description\t$source\n";


        # Update gene table
        my $sth_split = $dbh->prepare('UPDATE gene SET description=?, source=? WHERE own_id=?');
        $sth_split->execute($description, $source, $own_id)  or die $sth_split->errstr;
        $sth_split->finish();
    }
}


$dbh->disconnect or warn $dbh->errst;
exit 0;

