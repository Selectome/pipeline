#!/usr/bin/env perl

## Perl embedded modules
use strict;
use warnings;
use diagnostics;

my $directory = $ARGV[0] || '';
die "\n\tInvalid directory\n\te.g. $0 directory extension1 [extension2 ...]\n\n" if ( ! -d "$directory" );

shift @ARGV;
my @extension = @ARGV;
die "\n\tMissing extension\n\te.g. $0 directory extension1 [extension2 ...]\n\n" if ( ! exists($extension[0]) );
#TODO remove . if extensions are provided as .ext


my $files_to_arch = 'files_to_archives_from.txt';
my $archive       = 'archive.tar';
unlink $files_to_arch, $archive.'.gz';

EXT:
for my $ext ( @extension ){
    system("find $directory -name \*.$ext >> $files_to_arch");
}


# Use --dereference option to follow symlinks and archive the file they point to
system("tar cvfSsp  $archive  --files-from=$files_to_arch  --dereference");
system("gzip -9 archive.tar");

unlink $files_to_arch;

exit 0;

