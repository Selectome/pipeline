#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use List::MoreUtils qw{uniq};

my @dir = uniq map { s/\.\d\d\d\/.+\.H1\.mlc$//; $_ } glob('E*/*/*/*H1.mlc');

MSA:
for my $msa ( @dir ){
    $msa =~ s{^\./}{};
    $msa =~ s{^(\w+)/(\w+)/(\d+)$}{$1/$2/$1.$2.$3.aa.fas.newmafft};

    if ( -e "$msa.gz" ){
        system("gunzip $msa.gz");
    }
    if ( -e "$msa.ScoreMerged.gz" ){
        system("gunzip $msa.ScoreMerged.gz");
    }


    # Get x char number
    my $before = `grep -v '>' $msa             | perl -pe "s{[^xX]}{}g" | wc -m`;
    chomp $before;
    my $after  = `grep -v '>' $msa.ScoreMerged | perl -pe "s{[^xX]}{}g" | wc -m`;
    chomp $after;

    print "$msa\t$before\t$after\t". ($after-$before). "\n";
}

exit 0;

