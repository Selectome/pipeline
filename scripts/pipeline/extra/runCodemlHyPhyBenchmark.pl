#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Slurp;
use File::Copy 'cp';
use File::Remove 'rm';
use File::Which;
use DateTime;
use Cwd;

use FindBin qw($Bin);
use lib "$Bin";
use lib 'Selectome/';
use Tree;
#use Node;

local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print


##################################
## HYPHY VS CODEML BENCHMARKING ##
##################################
#
# This script runs a benchmark to compare performances of codeml vs HyPhy
# It requires both codeml and HyPhy installed
# DNA alignment sequences must be placed in a 'seq' folder located in $seq_dir
# Newick format trees must be placed in the 'tree' folder located in $tree_dir
# The script must be launched from where the external scripts are (fastaToPHYLIP.sh, Node.pm, Tree.pm, compare_time.r, etc.)
#
# IMPLEMENTED
# Codeml file preparation
# Codeml running
# HyPhy file preparation
# HyPhy running
#
# NOT IMPLEMENTED YET
# Codeml multiple processor usage
# Comparison: automatic result (positive selection) comparison
# Comparison: automatic CPU time summary and usage comparison (with R?)
#
#
# -- ARGUMENTS --
#
# seq_dir               is a string telling where the dna sequence alignment files are. Default is './seq'
# tree_dir              is a string telling where the tree files are. Default is './tree'
# codeml_dir            is a string telling where the codeml files are or will be. Default is './codeml'
# hyphy_dir             is a string telling where the hyphy files are or will be. Default is './hyphy'
# comp_dir              is a string telling where the comparison files will be. Default is '.' (current directory)
# stat_dir              is a string telling where the input statistics file will be. Default is '.' (current directory)
# script_dir            is a string telling where the external scripts are and should always be '.' (current directory)
# 'v'                   is an integer for the verbosity. 0 = nothing, 1 = only summary, 2 = list all files, 3 = summary for each file,
#                           4 = all info for each file except the really junky, 5 = everything
# 'tree_v'              is an integer telling the verbosity for the Tree.pm module ( print information of newick parsing)
# 'seq_ext'             is a string telling the extension to look for sequence files. Default is 'fasta'
# 'tree_ext'            is a string telling the extension to look for tree files. Default is 'nwk'
# 'force_phy_convert'   is a boolean telling if the DNA file should be forced to be reconverted to PHYLIP format
# 'stat'                is a boolean telling to get statistics about the data to be analysed (in 'seq' and 'tree' folders)
# 'sim'                 is a boolean telling to run the script without executing codeml
# 'run_codeml'          is a boolean telling to run codeml
# 'run_hyphy'           is a boolean telling to run HyPhy
# 'prepare_codeml'      is a boolean telling to prepare codeml
# 'prepare_hyphy'       is a boolean telling to prepare HyPhy
# 'compare_time'        is a boolean telling to compare the run time (CPU usage) of codeml and HyPhy
# 'compre_res'          is a boolean telling to compare the results (positive selection detected) of codeml and HyPhy
# 'help'                is a boolean telling to display the usage/help and exit

our ($seq_dir,  $tree_dir,  $codeml_dir,    $hyphy_dir, $comp_dir,  $stat_dir,  $script_dir, $v, $tree_v,    $force_phy_convert)
  = ('./seq',   './tree',   './codeml',     './hyphy',  '.',        '.',        getcwd,      0,  0,          0,                );
our ($help,  $sim,   $seq_ext,   $tree_ext,  $run_codeml,   $run_hyphy)
  = (0,      0,      'fasta',    'nwk',      0,             0 ,       );
our ($prepare_codeml,   $prepare_hyphy, $compare_time,  $compare_res,   $stat)
  = (0,                  0,              0,             0           ,   0    );

GetOptions(
            'seqdir=s'      => \$seq_dir,
            'treedir=s'     => \$tree_dir,
            'codemldir=s'   => \$codeml_dir,
            'hyphydir=s'    => \$hyphy_dir,
            'compdir=s'     => \$comp_dir,
            'statdir=s'     => \$stat_dir,
            'v=i'           => \$v,
            'treeverb=i'    => \$tree_v,
            'seqext=s'      => \$seq_ext,
            'treeext=s'     => \$tree_ext,
            'rc!'           => \$run_codeml,
            'run_codeml!'   => \$run_codeml,
            'rh!'           => \$run_hyphy,
            'pc!'           => \$prepare_codeml,
            'ph!'           => \$prepare_hyphy,
            'ct!'           => \$compare_time,
            'cr!'           => \$compare_res,
            'fc!'           => \$force_phy_convert,
            's!'            => \$sim,
            'stat!'         => \$stat,
            'h!'            => \$help);

unless ($run_codeml || $run_hyphy || $prepare_codeml || $prepare_hyphy || $compare_time || $compare_res || $stat) {
    print "WARNING: No action requested!\n";
    $help = 1;
}

print "Usage: runCodemlHyphyBenchmark.pl [OPTIONS]
    -seqdir   \tstring telling where the dna sequence alignment files are. Default is './seq'
    -treedir  \tstring telling where the tree files are. Default is './tree'
    -codemldir\tstring telling where the codeml files are or will be. Default is './codeml'
    -hyphydir \tstring telling where the hyphy files are or will be. Default is './hyphy'
    -compdir  \tstring telling where the comparison files will be. Default is '.' (current directory)
    -statdir  \tstring telling where the input statistics file will be. Default is '.' (current directory)
    -v        \tinteger telling the verbosity, how much rubish on the screen. 0 = nothing, 1 = only summary, 2 = list all files,
              \t3 = summary for each file, 4 = all info for each file except the really junky, 5 = everything
    --treeverb\tinteger telling the verbosity for the Tree.pm module, which can print out information about the newick tree parsing
    --seq_ext \tstring telling the extension to look for sequence files. Default is 'fasta'
    --tree_ext\tstring telling the extension to look for tree files. Default is 'nwk'
    -fc       \tboolean telling if the DNA file should be forced to be converted to PHYLIP format
    -rc       \tis a boolean telling to run codeml
    -rh       \tis a boolean telling to run HyPhy
    -pc       \tis a boolean telling to prepare codeml
    -ph       \tis a boolean telling to prepare HyPhy
    -ct       \tis a boolean telling to compare the run time (CPU usage) of codeml and HyPhy
    -cr       \tis a boolean telling to compare the results (positive selection found) of codeml and HyPhy
    -s        \tsimulation, to run the script without executing codeml
    -stat     \tget statistics about the data to be analysed (in 'seq' and 'tree' folders)
    -h        \tdisplaying help and exit\n" if $help;
exit 0 if $help;

# Header of the program with date and time
my $first_line = "###################  Start at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  ###################";
print "\n".'#' x length($first_line)."\n$first_line\n\n" if $v;

# 'formating' is for ouput printing into console with OK aligned
# 'c' is a counter variable
# 'ctl_file is the template of the codeml ctl configuration file
# 'console_ouput_file' is a classical output of a codeml execution
our ($formating, $c, $ctl_file, $codeml_console_file, $hyphy_file, $hyphy_console_file) = ("%-140s", 1, '', '', '', '');
$Tree::debug = $tree_v; # set the Tree module's verbosity

# checks if given directory exists and transforms it into absolute path (for HyPhy)
sub set_check_dir {
    my ($dir_ref, $name, $must_not_exist) = @_;
    $$dir_ref .= "/" unless $$dir_ref =~ /\/$/;
    if ($must_not_exist) { # do not try to chdir or anything if must not exist
        $$dir_ref = getcwd."/$$dir_ref";
        return;
    }
    die "ERROR: $name directory '$$dir_ref' does not exist!" unless (-s $$dir_ref);
    chdir $$dir_ref or die "ERROR: Cannot chdir to '$$dir_ref'!";
    $$dir_ref = getcwd; # transform dir to absolute path
    $$dir_ref .= "/" unless $$dir_ref =~ /\/$/;
    chdir $script_dir;
}

# Check for directoriesi
set_check_dir(\$seq_dir, "Sequence");
set_check_dir(\$tree_dir, "Tree");
set_check_dir(\$codeml_dir, "Codeml", !($compare_time || $compare_res));
set_check_dir(\$hyphy_dir, "HyPhy", !($compare_time || $compare_res));
set_check_dir(\$comp_dir, "Comparison");
set_check_dir(\$stat_dir, "Input statistics");

get_stats() if $stat;

# Prepare and/or run codeml and/or codeml
my (@valid_names_codeml, @valid_runs_codeml, @valid_names_hyphy, @valid_runs_hyphy);
@valid_names_codeml = @{prepare_codeml()} if $prepare_codeml;
@valid_names_hyphy = @{prepare_hyphy()} if $prepare_hyphy;
@valid_runs_codeml = @{run_codeml(\@valid_names_codeml)} if $run_codeml;
@valid_runs_hyphy = @{run_hyphy(\@valid_names_hyphy)} if $run_hyphy;

compare_time() if $compare_time;

# do eventually something with the @valid_runs_... array

# Footer of the program with date and time
my $last_line = "###################  Done at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  ###################";
print "\n$last_line\n".'#' x length($last_line)."\n\n" if $v;

exit 0;

# Prepares all the files requieres for running codeml
# sequences to PHYLIP conversion, ctl files, etc.
sub prepare_codeml {

    print "       #########  PREPARE CODEML at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # fills in the $ctl_file and $codeml_console_file variables from the templates at the bottom of this script
    init_templates();

    # Check for codeml directory
    printf $formating, "Checking for directory $codeml_dir ... " if $v;
    mkdir "$codeml_dir" unless (-s $codeml_dir);
    print "OK\n" if $v;

    # Get all sequence files with '$seq_ext' extension
    printf $formating, "Getting all files to be processed from directory '$seq_dir' ..." if $v;
    opendir DIR, "$seq_dir";
    my @seq_files = grep {/\.$seq_ext$/} readdir DIR;
    closedir DIR;
    printf "ERROR\n" if !@seq_files && $v; # ERROR for 'Getting all...' line
    printf $formating, "ERROR : No *.$seq_ext file found! " unless @seq_files;
    printf "OK\n${formating}OK\n", "Found ".($#seq_files + 1)." file(s)" if @seq_files && $v;

    my @valid_names = (); # stores the names/prefix of the files valid to be runned (tree file ok, etc.)
    $c = 1;

    # Go through all sequence files for prepartion: file conversions, checking, creation.
    # If all files are okay and prepared, the seq file is validated for being runned by codeml
    for my $seq_file (@seq_files) {
        my $first_line = sprintf "    File number %03d/%03d (%03d%%): '$seq_file' ... ", $c, ($#seq_files + 1),
            100.0 * $c / ($#seq_files + 1);
        printf $formating, $first_line if ($v >= 2);
        printf "\n" if ($v >= 3); # end of 'File number...' line

        # Creating the common root name (or prefix)
        my ($name) = $seq_file;
        $name =~ s/\.$seq_ext$//;

        # Checking for the newick tree file
        printf $formating, "        Tree file '$tree_dir/$name.$tree_ext' ..." if ($v >= 3);
        unless (-s "$tree_dir/$name.$tree_ext") {
            print "    ERROR\n" if ($v >= 2);
            print "         ERROR: No tree file found for '$name' at '$tree_dir/$name.$tree_ext', skipping this run!\n" if $v;
            next;
        }
        print "OK\n" if ($v >= 3); # OK for "Tree file..." line

        printf $formating, "        Creating/cleaning directory to '$codeml_dir/$name/' ... " if ($v >= 3);
        rm "$codeml_dir/$name" if (-s "$codeml_dir/$name");
        mkdir "$codeml_dir/$name";
        print "OK\n" if ($v >= 3);

        # Labeling un-named internal branches
        printf $formating, "        Labeling un-named internal branches of newick tree ... " if ($v >= 3);
        open NWK_FILE, "<$tree_dir/$name.$tree_ext";
        my $ori_nwk = <NWK_FILE>;
        close NWK_FILE;
        print "\n            Newick before internal branch labeling:\n$ori_nwk" if ($v >= 5);
        my $nwk = label_newick_branches($ori_nwk);
        print "            Newick after internal branch labeling:\n$nwk" if ($v >= 5);
        if (length($ori_nwk) > length($nwk)) { # test lengths of the newick files, new on should be longer or equal length
            printf "$formating", "" if ($v == 5);
            print "ERROR\n" if ($v >= 2);
            print "        ERROR: length before labeling (".length($ori_nwk).") is longer than after (".length($nwk).
                "), skipping this run!\n" if $v;
            next;
        } else { # newick file lengths are okay
            printf "OK\n" if ($v >= 3 && $v < 5); # OK for "Labeling un-name..." line
            printf "".($v == 5? "%s" : $formating), "        Writing out the tree to ".
                "'$codeml_dir/$name/$name\_base.$tree_ext' ... " if ($v >= 3);
            # Writing out the base newick file
            open BASE_NWK, ">$codeml_dir/$name/$name\_base.$tree_ext";
            print BASE_NWK $nwk;
            close BASE_NWK;
            unless (-s "$codeml_dir/$name/$name\_base.$tree_ext") { # test if writing failed
                print "    ERROR\n" if ($v >= 2);
                print "         ERROR: Couldn't create the base tree file for '$name' at ".
                    "'$codeml_dir/$name/$name\_base.$tree_ext', skipping this run!\n" if $v;
                next;
            }
            print "OK\n" if ($v >= 3); # OK for "Writing out the tree..." line
        }

        # Converting the fasta dna sequence into PHYLIP format
        printf $formating, "        Converting fasta file to PHYLIP ... " if ($v >= 3);
        if (!$force_phy_convert && -s "$seq_dir/$name.phy") { # file already there
            print "skip (already there)\n" if ($v >= 3);
        } else {
            printf "..\n$formating", "            Calling 'system(\"$script_dir/fastaToPHYLIP.sh $seq_dir/$seq_file\")' ".
                "..." if ($v >= 4);
            unless (-s "$script_dir/fastaToPHYLIP.sh") { # test if converting script exists
                print "    ERROR\n" if ($v >= 2);
                print "         ERROR: Convering script fastaToPHYLIP.sh does not exists at '$script_dir', ".
                    "skipping this run!\n" if $v;
                next;
            }
            system("$script_dir/fastaToPHYLIP.sh $seq_dir/$seq_file");
            unless (-s "$seq_dir/$name.phy") { # test if conversion failed
                print "ERROR\n" if ($v >= 2);
                print "        ERROR: Could not convert '$seq_file' to PHYLYP format, skipping this run!\n" if $v;
                next;
            }
            print "OK\n" if ($v >= 3); # OK for "Converting fasta..." line
        }

        # Creating tree files with '#1'
        printf $formating, "        Creating newick files with '#1' ... " if ($v >= 3);
        print "..\n" if ($v >= 4 || $tree_v); # line feed for "Creating newick files..." line
        # Initialize a Tree object and get node count info out of it
        my $tree = new Tree($nwk);
        my $nodes_count = @{$tree->all_nodes};
        my $status = move_sharp1_in_tree($tree, $name);
        unless ($status) { # test if move_sharp1_in_tree subroutine failed
            print "        ERROR: Problem during creation of tree files with '#1', skipping this run!\n" if $v;
            next;
        }
        # Test if number of internal nodes and number of directories written out matches
        opendir NAME_DIR, "$codeml_dir/$name/";
        my @name_dir_files = readdir(NAME_DIR);
        closedir NAME_DIR;
        printf "$formating\n", sprintf("%-60s %s","            All files in '$codeml_dir/$name/':",
            join(', ', @name_dir_files)) if ($v >= 5);
        printf "$formating\n", sprintf("%-60s %s","            Branch files in '$codeml_dir/$name/': ",
            join(', ', (grep {/^\d+$/} @name_dir_files))) if($v >= 5);
        printf $formating, "            Total nodes number: ".@{$tree->all_nodes}.", internal nodes (without root): ".
            (@{$tree->all_nodes} - @{$tree->leaves} - 1).", number of branch directories: ".
            (grep {/^\d+$/} @name_dir_files)." ..." if ($v >= 4);

        unless ($status) { # test if number of folders in $name folder and number of internal branches (without root) are equal
            print "ERROR\n" if ($v >= 2);
            print "        ERROR: Not the same number of internal nodes (without root) and folders in ".
                "'$codeml_dir/$name/'!\n" if $v;
            next;
        }
        print "OK\n" if ($v >= 3); # OK for "Creating newick..." or "Total number..." lines
        print "OK\n" if ($v == 2); # OK for "File number..." line

        push(@valid_names, $name); # store the names that made it through the validation

        $c++;
    }

    print "Found ".($#valid_names + 1)." valid run(s) to launch!\n" if $v;

    return \@valid_names;
}

# Runs codeml, without checking if files are present.
# In case of doubt, run prepare_codeml before !
sub run_codeml {
    my $valid_names_ref = shift;

    print "       #########  RUN CODEML at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # fills in the $ctl_file and $codeml_console_file variables from the templates at the bottom of this script
    init_templates();

    # Get the files to process from the result of prepare_codeml, if it was not runned, get all files from seq folder
    my @valid_names = @$valid_names_ref;
    unless ($prepare_codeml) {
        # Get all sequence files with '$seq_ext' extension
        printf $formating, "Getting all files to be processed from sequences directory '$seq_dir' ..." if $v;
        opendir DIR, "$seq_dir";
        my @seq_files = grep {/\.$seq_ext$/} readdir DIR;
        closedir DIR;
        printf "ERROR\n" if !@seq_files && $v; # ERROR for 'Getting all...' line
        printf $formating, "ERROR : No *.$seq_ext file found! " unless @seq_files;
        printf "OK\n${formating}OK\n", "Found ".($#seq_files + 1)." file(s)" if @seq_files && $v;
        @valid_names = map{s/\.$seq_ext//;$_} @seq_files; # generate only the names/prefix by remove seq extension
    }

    unless (which('codeml')) { # check if the program codeml is installed
        print "ERROR: Cannot run codeml because the programm 'codeml' cannot be accessed\n";
        return;
    }

    printf $formating, "Running codeml ... " if $v;
    print "\n" if ($v >= 2);
    my @valid_runs = (); # store the runs that succeeded
    $c = 1;
    my $total_runs = 0;
    # Go through all the validated names and run codeml on them
    # less tests are made in this part as everything was verified above
    for my $name (@valid_names) {
        my $first_line = sprintf "    Sequence number %03d/%03d (%03d%%): running codeml for '$name' ... ", $c,
            ($#valid_names + 1), 100.0 * $c / ($#valid_names + 1);
        printf $formating, $first_line if ($v >= 2);
        opendir BRANCH_DIR, "$codeml_dir/$name/";
        my @branches = readdir(BRANCH_DIR);
        closedir BRANCH_DIR;
        @branches = grep {/^\d+$/} @branches; # keep only the directories which refere to branch numbers
        printf "\n$formating", "             ".($#branches + 1)." runs to do for '$name' ..." if ($v >= 3);
        print "..\n" if ($v >= 4); # .. for "XX runes to do..." line
        for my $branch (@branches) {
            my $status = call_codeml($name, "H0", $branch);
            printf $formating, "                Status for '$name', '$branch', H0: $status ... " if ($v >= 4);
            print "".($status? 'ERROR' : 'OK')."\n" if ($v >= 4);
            print "                ERROR: Problem while calling codeml for '$name', '$branch', 'H0', ".
                "codeml returned status '$status'!\n" if $status;
            $status = call_codeml($name, "H1", $branch) || $status;
            printf $formating, "                Status for '$name', '$branch', H1: $status ... " if ($v >= 4);
            print "".($status? 'ERROR' : 'OK')."\n" if ($v >= 4);
            print "                ERROR: Problem while calling codeml for '$name', '$branch', 'H1', ".
                "codeml returned status '$status'!\n" if $status;
            push(@valid_runs, "$name.$branch") unless $status;
            $total_runs++;
        }

        print "OK\n" if ($v == 3); # OK for "XX runs to do..." line
        print "OK\n" if ($v == 2); # OK for "Sequence number..." line

        $c++;
    }
    print "OK\n" if ($v == 1); # OK for "Running codeml..." line
    print "Runned ".($#valid_runs + 1)." valid run(s) (".($#valid_runs + 1 - $total_runs)." problems) !\n" if $v;

    return \@valid_runs;
}

# Prepares all the files requieres for running HyPhy
sub prepare_hyphy {

    print "       #########  PREPARE HYPHY at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # fills in the $ctl_file and $codeml_console_file variables from the templates at the bottom of this script
    init_templates();

    # Check for hyphy directory
    printf $formating, "Checking for directory $hyphy_dir ... " if $v;
    mkdir "$hyphy_dir" unless (-s $hyphy_dir);
    print "OK\n" if $v;

    # Get all sequence files with '$seq_ext' extension
    printf $formating, "Getting all files to be processed from sequences directory '$seq_dir' ..." if $v;
    opendir DIR, "$seq_dir";
    my @seq_files = grep {/\.$seq_ext$/} readdir DIR;
    closedir DIR;
    printf "ERROR\n" if !@seq_files && $v; # ERROR for 'Getting all...' line
    printf $formating, "ERROR : No *.$seq_ext file found! " unless @seq_files;
    printf "OK\n${formating}OK\n", "Found ".($#seq_files + 1)." file(s)" if @seq_files && $v;

    my @valid_names = (); # stores the names/prefix of the files valid to be runned (tree file ok, etc.)
    $c = 1;

    # Go through all sequence files for preparation: file conversions, checking, creation.
    # If all files are okay and prepared, the seq file is validated for being runned by hyphy
    for my $seq_file (@seq_files) {
        my $first_line = sprintf "    File number %03d/%03d (%03d%%): '$seq_file' ... ", $c, ($#seq_files + 1),
            100.0 * $c / ($#seq_files + 1);
        printf $formating, $first_line if ($v >= 2);
        printf "\n" if ($v >= 3); # end of 'File number...' line

        # Creating the common root name (or prefix)
        my ($name) = $seq_file;
        $name =~ s/\.$seq_ext$//;

        # Checking for the newick tree file
        printf $formating, "        Tree file '$tree_dir/$name.$tree_ext' ..." if ($v >= 3);
        unless (-s "$tree_dir/$name.$tree_ext") {
            print "    ERROR\n" if ($v >= 2);
            print "         ERROR: No tree file found for '$name' at '$tree_dir/$name.$tree_ext', skipping this run!\n" if $v;
            next;
        }
        print "OK\n" if ($v >= 3); # OK for "Tree file..." line

        printf $formating, "        Creating/cleaning directory to '$hyphy_dir/$name/' ... " if ($v >= 3);
        rm "$hyphy_dir/$name" if (-s "$hyphy_dir/$name");
        mkdir "$hyphy_dir/$name";
        print "OK\n" if ($v >= 3);

        # Labeling un-named internal branches
        printf $formating, "        Labeling un-named internal branches of newick tree ... " if ($v >= 3);
        open NWK_FILE, "<$tree_dir/$name.$tree_ext";
        my $ori_nwk = <NWK_FILE>;
        close NWK_FILE;
        print "\n            Newick before internal branch labeling:\n$ori_nwk" if ($v >= 5);
        my $nwk = label_newick_branches($ori_nwk, 1);
        print "            Newick after internal branch labeling:\n$nwk" if ($v >= 5);
        if (length($ori_nwk) > length($nwk)) { # test lengths of the newick files, new on should be longer or equal length
            printf "$formating", "" if ($v == 5);
            print "ERROR\n" if ($v >= 2);
            print "        ERROR: length before labeling (".length($ori_nwk).") is longer than after (".length($nwk).
                "), skipping this run!\n" if $v;
            next;
        } else { # newick file lengths are okay
            printf "OK\n" if ($v >= 3 && $v < 5); # OK for "Labeling un-name..." line
            printf "".($v == 5? "%s" : $formating), "        Writing out the tree to ".
                "'$hyphy_dir/$name/$name\_base.$tree_ext' ... " if ($v >= 3);
            # Writing out the base newick file
            open BASE_NWK, ">$hyphy_dir/$name/$name\_base.$tree_ext";
            print BASE_NWK $nwk;
            close BASE_NWK;
            unless (-s "$hyphy_dir/$name/$name\_base.$tree_ext") { # test if writing failed
                print "    ERROR\n" if ($v >= 2);
                print "         ERROR: Couldn't create the base tree file for '$name' at ".
                    "'$hyphy_dir/$name/$name\_base.$tree_ext', skipping this run!\n" if $v;
                next;
            }
            print "OK\n" if ($v >= 3); # OK for "Writing out the tree..." line
        }

        printf $formating, "        Creating .hyphy configuration file to $hyphy_dir/$name/$name.hyphy ..." if ($v >= 3);
        my $status = create_hyphy_file($name);
        unless (!$status && -s "$hyphy_dir/$name/$name.hyphy") { # test if .hyphy file is present
            print "ERROR\n" if ($v >= 2);
            print "                ERROR: Could not create.hyphy file for '$name' at ".
                "                    '$hyphy_dir/$name/$name.hyphy', skipping this run!\n" if $v;
            next;
        }
        print "OK\n" if ($v >= 3); # OK for "Creating .hyphy ..." line

        print "OK\n" if ($v == 2); # OK for "File number..." line

        push(@valid_names, $name); # store the names that made it through the validation

        $c++;
    }

    print "Found ".($#valid_names + 1)." valid run(s) to launch!\n" if $v;

    return \@valid_names;
}

# Runs hyphy, without checking if files are present.
# In case of doubt, run prepare_hyphy before !
sub run_hyphy {
    my $valid_names_ref = shift;

    print "       #########  RUN HYPHY at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # fills in the $hyphy_file and $hyphy_console_file variables from the templates at the bottom of this script
    init_templates();

    # Get the files to process from the result of prepare_hyphy, if it was not runned, get all files from seq folder
    my @valid_names = @$valid_names_ref;
    unless ($prepare_hyphy) {
        # Get all sequence files with '$seq_ext' extension
        printf $formating, "Getting all files to be processed from sequences directory '$seq_dir' ..." if $v;
        opendir DIR, "$seq_dir";
        my @seq_files = grep {/\.$seq_ext$/} readdir DIR;
        closedir DIR;
        printf "ERROR\n" if !@seq_files && $v; # ERROR for 'Getting all...' line
        printf $formating, "ERROR : No *.$seq_ext file found! " unless @seq_files;
        printf "OK\n${formating}OK\n", "Found ".($#seq_files + 1)." file(s)" if @seq_files && $v;
        @valid_names = map{s/\.$seq_ext//;$_} @seq_files; # generate only the names/prefix by remove seq extension
    }

    unless (which('HYPHYMP')) { # check if the program hyphy is installed
        print "ERROR: Cannot run HyPhy because the programm 'HYPHYMP' cannot be accessed\n";
        return;
    }

    printf $formating, "Running HyPhy ... " if $v;
    print "\n" if ($v >= 2);
    my @valid_runs = (); # store the runs that succeeded
    $c = 1;

    # Go through all the validated names and run hyphy on them
    # less tests are made in this part as everything was verified above
    for my $name (@valid_names) {
        my $first_line = sprintf "    Sequence number %03d/%03d (%03d%%): running HyPhy for '$name' ... ", $c,
            ($#valid_names + 1), 100.0 * $c / ($#valid_names + 1);
        printf $formating, $first_line if ($v >= 2);
        my $status = call_hyphy($name);
        printf $formating, "\n                Status for '$name': $status ... " if ($v >= 4);
        print "".($status? 'ERROR' : 'OK')."\n" if ($v >= 4);
        print "                ERROR: Problem while calling hyphy for '$name', hyphy returned status '$status'!\n" if $status;

        push(@valid_runs, $name) unless $status;

        print "OK\n" if ($v == 3); # OK for "XX runs to do..." line
        print "OK\n" if ($v == 2); # OK for "Sequence number..." line

        $c++;
    }
    print "OK\n" if ($v == 1); # OK for "Running hyphy..." line
    print "Runned ".($#valid_runs + 1)." valid run(s) (".($#valid_runs + 2 - $c)." problems) !\n" if $v;

    return \@valid_runs;
}

# Creates the ctl codeml configuration files
sub create_ctl_file {
    my ($name, $hyp, $branch, $status) = @_;
    unless (-s "$seq_dir/$name.phy") { # check seq file
        print "WARNING\n" if (!$status && $v >= 2);
        print "                WARNING: ctl file indicated seq file for '$name', '$hyp', '$branch' doesn't exist at ".
            "'$seq_dir/$name.phy'\n" if $v;
        $status = 1;
    }
    unless (-s "$codeml_dir/$name/$branch/$name.$branch.$tree_ext") { # check for tree file
        print "WARNING\n" if (!$status && $v >= 2);
        print "                WARNING: ctl file indicated tree file for '$name', '$hyp', '$branch' doesn't exist at ".
            "'$codeml_dir/$name/$branch/$name.$branch.$tree_ext'\n" if $v;
        $status = 1;
    }
    unless (-s "$codeml_dir/$name/$branch/$hyp/") { # check for H0/H1 folders
        print "WARNING\n" if (!$status && $v >= 2);
        print "                WARNING: ctl file indicated run folder for '$name', '$hyp', '$branch' doesn't exist at ".
            "'$codeml_dir/$name/$branch/$hyp/'\n" if $v;
        $status = 1;
    }

    print "..\n" if (!$status && $v >= 4); # .. for above "WARNINIG:..." lines
    printf $formating, "                Writing ctl file for '$name', '$hyp', '$branch' at ".
        "'$codeml_dir/$name/$branch/$hyp/$name.$branch.$hyp.ctl' ... " if ($v >= 4);
    # Read the ctl file from the global $ctl_file variable
    my $ctl = $ctl_file;
    # replace with specific information
    $ctl =~ s:__SEQ_FILE__:../../../../seq/$name.phy:;
    $ctl =~ s:__TREE_FILE__:../$name.$branch.$tree_ext:;
    $ctl =~ s:__OUT_FILE__:../../$name.$branch.$hyp.mlc:;
    $ctl =~ s/__FIX_OMEGA__/($hyp eq 'H0'? '1' : '0')/e;

    # Write out the ctl file
    open CTL, ">$codeml_dir/$name/$branch/$hyp/$name.$branch.$hyp.ctl";
    print CTL $ctl;
    close CTL;
    return $status;
}

# Runs codeml for the specified name, branch, hypothesis
sub call_codeml {
    my ($name, $hyp, $branch) = @_;
    my $status = ($sim? # do not run codeml if only simulation, instead just echo into console_out file
        system("cd $codeml_dir/$name/$branch/$hyp/; /usr/bin/time -o $codeml_dir/$name/$name.$branch.$hyp.time ".
            "echo \"SIM $name.$branch.$hyp\\n$codeml_console_file\" > $codeml_dir/$name/$name.$branch.$hyp.console_out") :
        system("cd $codeml_dir/$name/$branch/$hyp/; /usr/bin/time -o $codeml_dir/$name/$name.$branch.$hyp.time ".
            "codeml $name.$branch.$hyp.ctl > $codeml_dir/$name.$branch.$hyp.console_out"));
    return $status;
}

# Creates the .hyphy configuration file
sub create_hyphy_file {
    my ($name) = @_;
    my $status = 0;
    unless (-s "$seq_dir/$name.phy") { # check seq file
        print "WARNING\n" if (!$status && $v >= 2);
        print "                WARNING: .hyphy file indicated seq file for '$name', doesn't exist at ".
            "'$seq_dir/$name.phy'\n" if $v;
        $status = 1;
    }
    unless (-s "$hyphy_dir/$name/$name\_base.$tree_ext") { # check for tree file
        print "WARNING\n" if (!$status && $v >= 2);
        print "                WARNING: .hyphy indicated tree file for '$name', doesn't exist at ".
            "'$hyphy_dir/$name/$name\_base.$tree_ext'\n" if $v;
        $status = 1;
    }

    print "..\n" if (!$status && $v >= 4); # .. for above "WARNINIG:..." lines
    printf $formating, "                Writing .hyphy file for '$name', at ".
        "'$hyphy_dir/$name/$name.hyphy' ... " if ($v >= 4);
    # Read the .hyphy file from the global $hyphy_file variable
    my $hyphy = $hyphy_file;
    # replace with specific information
    $hyphy =~ s:__SEQ_FILE__:$seq_dir/$name.fasta:;
    $hyphy =~ s:__TREE_FILE__:$hyphy_dir/$name/$name\_base.$tree_ext:;
    $hyphy =~ s:__OUT_FILE__:$hyphy_dir/$name/$name.out:;
    $hyphy =~ s:__PATH__:$script_dir/:;

    # Write out the hyphy file
    open HYPHY, ">$hyphy_dir/$name/$name.hyphy";
    print HYPHY $hyphy;
    close HYPHY;
    return $status;
}

# Runs hyphy for the specified name/prefix
sub call_hyphy {
    my ($name) = @_;
    my $status = ($sim? # do not run hyphy if only simulation, instead just echo into console_out file
        system("cd $hyphy_dir/$name/; /usr/bin/time -o $hyphy_dir/$name/$name.time echo \"".
            "SIMU $name\\n$hyphy_console_file\" > $hyphy_dir/$name.console_out") :
        system("cd $hyphy_dir/$name/; /usr/bin/time -o $hyphy_dir/$name/$name.time HYPHYMP ".
            "$name.hyphy > $hyphy_dir/$name/$name.console_out"));
    return $status;
}

# get some statistics about the data to analyse
sub get_stats {
    print "       ######### STATS at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # Get the files to analyse
    printf $formating, "Getting all sequences files from '$seq_dir' ... " if $v;
    open SYS_CMD, "find $seq_dir -maxdepth 1 -name *.$seq_ext |";
    my @seq_files = <SYS_CMD>;
    close SYS_CMD;

    map { s/\n$// } @seq_files;

    printf "ERROR\n" if !@seq_files && $v; # ERROR for 'Getting all...' line
    print "ERROR : No sequence file found in '$seq_dir'!\n" unless @seq_files;
    printf "OK\n${formating}OK\n", "Found ".($#seq_files + 1)." file(s)" if @seq_files && $v;
    print "    Sequences files:\n        ".join("\n        ", @seq_files)."\n" if ($v >= 5);

    $c = 1;

    my @sum_lines; # stores the summary lines, will be sorted at the end

    # Go through all sequence files, convert them (if not already done) and extract sequence length and number of sequences
    for my $seq_file (@seq_files) {
        my $first_line = sprintf "    File number %03d/%03d (%03d%%): '$seq_file' ... ", $c, ($#seq_files + 1),
            100.0 * $c / ($#seq_files + 1);
        printf $formating, $first_line if ($v >= 2);
        printf "\n" if ($v >= 3); # end of 'File number...' line

        # Extracting the name of this file
        my $name = $seq_file;
        $name =~ s:.+?/([^/]+)\.$seq_ext$:$1:;

        # Converting the fasta dna sequence into PHYLIP format if needed
        printf $formating, "        Converting fasta file to PHYLIP ... " if ($v >= 3);
        if (!$force_phy_convert && -s "$seq_dir/$name.phy") { # file already there
            print "skip (already there)\n" if ($v >= 3);
        } else {
            printf "..\n$formating", "            Calling 'system(\"$script_dir/fastaToPHYLIP.sh $seq_file\")' ".
                "..." if ($v >= 4);
            unless (-s "$script_dir/fastaToPHYLIP.sh") { # test if converting script exists
                print "    ERROR\n" if ($v >= 2);
                print "         ERROR: Convering script fastaToPHYLIP.sh does not exists at '$script_dir', ".
                    "skipping this run!\n" if $v;
                next;
            }
            system("$script_dir/fastaToPHYLIP.sh $seq_file");
            unless (-s "$seq_dir/$name.phy") { # test if conversion failed
                print "ERROR\n" if ($v >= 2);
                print "        ERROR: Could not convert '$seq_file' to PHYLYP format, skipping this run!\n" if $v;
                next;
            }
            print "OK\n" if ($v >= 3); # OK for "Converting fasta..." line
        }

        # Extract the number of sequences and the sequence length from the PHYLIP file
        printf "\n$formating", "        Extracting number of seq and seq len for '$name' from ".
            "'$seq_dir/$name.phy' ... " if ($v >= 4);
        unless (-s "$seq_dir/$name.phy") {
            print "ERROR\n" if ($v >= 2);
            print "            ERROR: Couldn't extract number of seq and seq len from file at ".
                "'$seq_dir/$name.phy does not exist !\n" if $v;
            next;
        }
        open PHY_FILE, "<$seq_dir/$name.phy";
        my $first_phy_line = readline(PHY_FILE);
        close PHY_FILE;
        $first_phy_line =~ /\s+(\d+)\s+(\d+)/;
        my ($nseq, $seq_len) = ($1, $2);
        printf "\n$formating", "                For '$name' - number of sequences: '$nseq', ".
            "sequences length: '$seq_len'" if ($v >= 5);

        # Fill a line in the summary file for this run
        push(@sum_lines, "$name\t$nseq\t$seq_len\n");
        print "OK\n" if ($v >= 3); # OK for "Extracting content for..." lines
        print "OK\n" if ($v == 2); # OK for "File number..." line

        $c++;
    } # end of loop

    print "OK\n" if ($v == 1);

    printf $formating, "Writing out the summary to '$stat_dir/data_summary_.txt' ... " if $v;
    # data summary file content
    open STAT_SUM, ">$stat_dir/data_summary.txt";
    print STAT_SUM "name\tnSeq\tseqLen\n";
    print STAT_SUM sort(@sum_lines);
    close STAT_SUM;

    unless (-s "$stat_dir/data_summary.txt") {
        print "ERROR\n" if $v;
        print "ERROR: Couldn't write out the summary to '$stat_dir/data_summary.txt'! \n";
        return;
    }
    print "OK\n" if $v;
}

# Compares the run time (CPU usage) of codeml and HyPhy
sub compare_time {
    print "       #########  COMPARE TIME at ".DateTime->now->strftime('%d/%m/%Y - %H:%M:%S')." !  #########\n" if $v;

    # Get the files to process from the codeml and hyphy folder
    printf $formating, "Getting all time files from '$codeml_dir' and '$hyphy_dir' directories ... " if $v;
    open SYS_CMD, "find $codeml_dir -name *.time |";
    my @time_files = <SYS_CMD>;
    close SYS_CMD;
    open SYS_CMD, "find $hyphy_dir -name *.time |";
    push(@time_files, <SYS_CMD>);
    close SYS_CMD;

    printf "ERROR\n" if !@time_files && $v; # ERROR for 'Getting all...' line
    printf $formating, "ERROR : No time file found in '$codeml_dir' and ".
        "'$hyphy_dir' directories!! " unless @time_files;
    printf "OK\n${formating}OK\n", "Found ".($#time_files + 1)." file(s)" if @time_files && $v;

    map { s/\n$// } @time_files;
    print "    Time files:\n        ".join("\n        ", @time_files)."\n" if ($v >= 5);

    $c = 1;
    my @sum_lines; # stores the summary lines for codeml and hyphy, will be sorted at the end

    # Go through all time files and extract times
    for my $time_file (@time_files) {
        my $first_line = sprintf "    File number %03d/%03d (%03d%%): '$time_file' ... ", $c, ($#time_files + 1),
            100.0 * $c / ($#time_files + 1);
        printf $formating, $first_line if ($v >= 2);
        printf "\n" if ($v >= 3); # end of 'File number...' line

        # Extracting the name of this file
        my $name = $time_file;
        $name =~ s:.+?/([^/]+)\.time$:$1:;
        my $type = ($time_file =~ /codeml/? 'codeml' : $time_file =~ /hyphy/? 'hyphy' : 'NONE');
        unless ($type eq 'codeml' || $type eq 'hyphy') {
            print "ERROR: Time file doesn't belong neither to 'codeml' nor 'hyphy', skipping it!\n" if $v;
            next;
        }

        # Extract content and parse out the informations
        printf $formating, "        Extracting content for '$name' ('$type') ... " if ($v >= 3);
        open TIME_FILE, "<$time_file";
        my $time = <TIME_FILE>;
        close TIME_FILE;

        $time =~ s/\n$//; # remove last line break
        $time =~ /([\d\.]+)user ([\d\.]+)system ([\d:\.]+)elapsed ([\?\d]+)%CPU/;
        my ($user_time, $sys_time, $elapsed, $cpu) = ($1, $2, $3, $4);
        $cpu = 0 if ($cpu eq '?'); # defaeat simluation bug where cpu is so not used that a percent can't even be calculated
        printf "\n$formating", "            Time content: '$time'\n            Time data: user: $user_time".
            ", sys: $sys_time, elapsed: $elapsed, cpu: $cpu" if ($v >= 5);

        my ($branch, $hyp) = ('-', '-'); # used for type = 'codeml', otherwise '-'

        if ($type eq 'codeml') {
            $name =~ /(.+)\.(\d+)\.(H[01])$/;
            ($name, $branch, $hyp) = ($1, $2, $3);
        }

        # Extract the number of sequences and the sequence length from the PHYLIP file
        printf "\n$formating", "        Extracting number of seq and seq len for '$name' ('$type') from ".
            "'$seq_dir/$name.phy' ... " if ($v >= 4);
        unless (-s "$seq_dir/$name.phy") {
            print "ERROR\n" if ($v >= 2);
            print "        ERROR: Couldn't extract number of seq and seq len from file at ".
                "'$seq_dir/$name.phy does not exist !\n" if $v;
            next;
        }
        open PHY_FILE, "<$seq_dir/$name.phy";
        my $first_phy_line = readline(PHY_FILE);
        close PHY_FILE;
        $first_phy_line =~ /\s+(\d+)\s+(\d+)/;
        my ($nseq, $seq_len) = ($1, $2);
        printf "\n$formating", "            For '$name' : number of sequences: '$nseq', ".
            "sequences length: '$seq_len'" if ($v >= 5);

        # Fill a line in the summary file for this run
        push(@sum_lines, "$type\t$name\t$nseq\t$seq_len\t$branch\t$hyp\t".
            "$user_time\t$sys_time\t$cpu\t$elapsed\n");

        print "OK\n" if ($v >= 3); # OK for "Extracting content for..." lines
        print "OK\n" if ($v == 2); # OK for "File number..." line

        $c++;
    } # end of loop

    print "OK\n" if ($v == 1);

    printf $formating, "Writing out the summary to '$comp_dir/time_summary_.txt' ... " if $v;
    # summary file content for later R processing
    open TIME_SUM, ">$comp_dir/time_summary.txt";
    print TIME_SUM "type\tname\tnSeq\tseqLen\tbranch\thyp\tuser\tsys\tcpu\telapsed\n";
    print TIME_SUM sort(@sum_lines);
    close TIME_SUM;

    unless (-s "$comp_dir/time_summary.txt") {
        print "ERROR\n" if $v;
        print "ERROR: Couldn't write out the summaries to '$comp_dir/time_summary.txt'! \n";
        return;
    }
    print "OK\n" if $v;

#ADD HERE R SCRIPT RUNNING

#    printf $formating, "Summarizing ... " if $v;
#    printf "\n$formating", "  Calculating ... " if ($v >= 2);
#    print "\n" if ($v >= 5);
#    for my $type (sort(keys(%comp))) {
#        print "  Type: $type\n" if ($v >= 5);
#        for my $name (sort(keys(%{$comp{$type}}))) {
#            print "    Name: $name\n" if ($v >= 5);
#            if ($type eq 'codeml') {
#                for my $branch (sort(keys(%{$comp{$type}{$name}}))) {
#                    print "      Branch: $branch\n" if ($v >= 5);
#                    for my $hyp (sort(keys(%{$comp{$type}{$name}{$branch}}))) {
#                        print "        Hyp: $hyp\n" if ($v >= 5);
#                        for my $key (sort(keys(%{$comp{$type}{$name}{$branch}{$hyp}}))) {
#                            print "          $key: $comp{$type}{$name}{$branch}{$hyp}{$key}\n" if ($v >= 5);
#                        }
#                    }
#                }
#            } elsif ($type eq 'hyphy') {
#
#            }
#        }
#    }
#    print "OK\n" if ($v == 2); # ok for "Summarizing..." line
#    print "OK\n" if ($v == 1); # ok for "Calcularing..." line

}

# Compares the results (positive selection detection) of codeml and HyPhy
sub compare_res {

}

# Label internal branches as some internal branches might have no name
# Warning: only use newick tree with this syntax:   ):\
# if $no_equals is defined, branches are only labeld with number: )001:0.12345 (needed for HyPhy)
sub label_newick_branches {
    my ($nwk, $no_equals) = @_;
    my $branch_nbr = 0;

    while( $nwk =~ m/\):[\d\.eE\-]*/ ){ # go through all ): regions to label them
        $branch_nbr = sprintf("%03d", ++$branch_nbr); # use 3-digit encoded numbering
        $nwk =~ s{\):([\d\.eE\-]*)}{)===$branch_nbr=:$1} unless $no_equals;
        $nwk =~ s{\):([\d\.eE\-]*)}{)$branch_nbr:$1} if $no_equals;
    }

    print 'ERROR: Wrong Newick tree structure' if ($nwk =~ /(\)...)/ && $1 !~ /^\)===$/ && !$no_equals); # FIXME to test

    $nwk =~ s{:[\d\.eE-]*;}{:0.0;}; # Remove root length

    return $nwk;
}

# Prints out one newick file per branch with every time the '#1' at a different branch
# the '#1' is used to specify the foreground branch for codeml
sub move_sharp1_in_tree {
    my ($tree, $name) = @_;
    for my $node (@{$tree->all_nodes}) {
        printf $formating, "            Checking node ".$node->id." ..." if ($v >= 5);
        if ($node->is_leaf eq 'leaf') { # do not calculate selection on leaves
            print "skip (leaf)\n" if ($v >= 5); # for "Checking node..." line
            next;
        } elsif ($node->is_root) {      # do not calculate selection on root
            print "skip (root)\n" if ($v >= 5); # for "Checking node..." line
            next;
        }
        print "..\n" if ($v >= 5); # for "Checking node..." line
        my $sharp1_tree = $tree->write_tree(0, 1);
        my $branch = sprintf("%03d", $node->branch_number);
        $sharp1_tree =~ s/($branch=)/$1#1/; # add the '#1'

        printf $formating, "            Creating/cleaning directory to ".
            "'$codeml_dir/$name/$branch/' ... " if ($v >= 4);
        eval { # test the remove and directory making operations, if fail $@ will be filled with error message
            rm "$codeml_dir/$name" if (-s "$codeml_dir/$name/$branch/");
            mkdir "$codeml_dir/$name/$branch/";
            mkdir "$codeml_dir/$name/$branch/H0/";
            mkdir "$codeml_dir/$name/$branch/H1/";
        };
        if ($@) { # in case of an error in the previous eval
            print "ERROR\n" if ($v >= 2);
            print "                ERROR: Problem during cleaning/creation of directory to ".
                "'$codeml_dir/$name/$branch/', error message: $@\n" if $v;
        }
        print "OK\n" if ($v >= 4); # OK for "Creating/cleaning..."

        print "                '#1' tree for node ".($node->branch_number||'NONE').":\n$sharp1_tree\n" if ($v >= 5);
        # Write out the new '#1'-including tree
        open SHARP1_TREE, ">$codeml_dir/$name/$branch/$name.$branch.$tree_ext";
        print SHARP1_TREE $sharp1_tree;
        close SHARP1_TREE;
        unless (-s "$codeml_dir/$name/$branch/$name.$branch.$tree_ext") { # test if '#1' tree file is present
            print "ERROR\n" if ($v >= 2);
            print "                ERROR: Couldn't create '#1' tree for '$name', '$branch' at ".
                "'$codeml_dir/$name/$branch/$name.$branch.$tree_ext'\n" if $v;
        }

        printf $formating, "            Creating codeml ctl files (H0 and H1) to ".
            "'$codeml_dir/$name/$branch/' ... " if ($v >= 4);
        my $status = create_ctl_file($name, "H0", $branch);
        $status = create_ctl_file($name, "H1", $branch, $status) || $status;
        unless (!$status && -s "$codeml_dir/$name/$branch/H0/$name.$branch.H0.ctl" &&
                -s "$codeml_dir/$name/$branch/H1/$name.$branch.H1.ctl") { # test if ctl files are present
            print "ERROR\n" if ($v >= 2);
            print "                ERROR: Could not create ctl files for '$name' at \n".
                "                    '$codeml_dir/$name/$branch/H0/$name.$branch.H0.ctl' and \n".
                "                    '$codeml_dir/$name/$branch/H1/$name.$branch.H1.ctl', ".
                "skipping this run!\n" if $v;
            return 0; # return failed status
        }
        print "OK\n" if ($v >= 4); # OK for "Creating codeml..." line
    }

    return 1; # return OK status
}

# init the $ctl_file, $codeml_console_file and $hyphy_file templates
sub init_templates {
    $ctl_file = '     seqfile = __SEQ_FILE__    * sequence data file name
    treefile = __TREE_FILE__   * tree structure file name
     outfile = __OUT_FILE__    * main result file name

       noisy = 9   * 0,1,2,3,9: how much rubbish on the screen
     verbose = 1   * 1: detailed output, 0: concise output
     runmode = 0   * 0: user tree;  1: semi-automatic;  2: automatic
                   * 3: StepwiseAddition; (4,5):PerturbationNNI; -2: pairwise

     seqtype = 1   * 1:codons; 2:AAs; 3:codons-->AAs
   CodonFreq = 2   * 0:1/61 each, 1:F1X4, 2:F3X4, 3:codon table
       ndata = 1   * specifies the number of separate data sets in the file
       clock = 0   * 0: no clock, unrooted tree, 1: clock, rooted tree

      aaDist = 0   * 0:equal, +:geometric; -:linear, 1-6:G1974,Miyata,c,p,v,a
                   * 7:AAClasses

       model = 2   * models for codons:
                       * 0:one, 1:b, 2:2 or more dN/dS ratios for branches
                   * models for AAs or codon-translated AAs:
                       * 0:poisson, 1:proportional,2:Empirical,3:Empirical+F
                       * 6:FromCodon, 8:REVaa_0, 9:REVaa(nr=189)

     NSsites = 2   * 0:one w;1:neutral;2:positive selection; 3:discrete;4:freqs;
                   * 5:gamma;6:2gamma;7:beta;8:beta&w;9:beta&gamma;
                   * 10:beta&gamma+1; 11:beta&normal>1; 12:0&2normal>1;
                   * 13:3normal>0

       icode = 0   * 0:universal code; 1:mammalian mt; 2-11:see below
       Mgene = 0   * 0:rates, 1:separate;

   fix_kappa = 0   * 1: kappa fixed, 0: kappa to be estimated
       kappa = 3.53330   * initial or fixed kappa

   fix_omega = __FIX_OMEGA__   * 1: omega or omega_1 fixed, 0: estimate
       omega = 1

       getSE = 0   * 0: don\'t want them, 1: want S.E.s of estimates
RateAncestor = 0   * (0,1,2): rates (alpha>0) or ancestral states (1 or 2)
  Small_Diff = .5e-6 * small value used in the difference approximation of derivatives

   cleandata = 0   * remove sites with ambiguity data (1:yes, 0:no)?
 fix_blength = 1   * 0: ignore, -1: random, 1: initial, 2: fixed
      method = 0   * 0: simultaneous; 1: one branch at a time
';
    $codeml_console_file = '
 15         verbose | verbose                1.00
  7         runmode | runmode                0.00
  4         seqtype | seqtype                1.00
 13       CodonFreq | CodonFreq              2.00
 34           ndata | ndata                  1.00
  9           clock | clock                  0.00
 18          aaDist | aaDist                 0.00
 16           model | model                  2.00
 20         NSsites | NSsites                2.00
 22           icode | icode                  0.00
 23           Mgene | Mgene                  0.00
 24       fix_kappa | fix_kappa              0.00
 25           kappa | kappa                  3.53
 26       fix_omega | fix_omega              1.00
 27           omega | omega                  1.00
 11           getSE | getSE                  0.00
 12    RateAncestor | RateAncestor           0.00
 36      Small_Diff | Small_Diff             0.00
  6       cleandata | cleandata              0.00
 37     fix_blength | fix_blength            1.00
  8          method | method                 0.00
CODONML in paml version 4.5, December 2011

----------------------------------------------
Phe F TTT | Ser S TCT | Tyr Y TAT | Cys C TGT
      TTC |       TCC |       TAC |       TGC
Leu L TTA |       TCA | *** * TAA | *** * TGA
      TTG |       TCG |       TAG | Trp W TGG
----------------------------------------------
Leu L CTT | Pro P CCT | His H CAT | Arg R CGT
      CTC |       CCC |       CAC |       CGC
      CTA |       CCA | Gln Q CAA |       CGA
      CTG |       CCG |       CAG |       CGG
----------------------------------------------
Ile I ATT | Thr T ACT | Asn N AAT | Ser S AGT
      ATC |       ACC |       AAC |       AGC
      ATA |       ACA | Lys K AAA | Arg R AGA
Met M ATG |       ACG |       AAG |       AGG
----------------------------------------------
Val V GTT | Ala A GCT | Asp D GAT | Gly G GGT
      GTC |       GCC |       GAC |       GGC
      GTA |       GCA | Glu E GAA |       GGA
      GTG |       GCG |       GAG |       GGG
----------------------------------------------
Nice code, uuh?


Ambiguity character definition table:

T (1): T
C (1): C
A (1): A
G (1): G
U (1): T
Y (2): T C
R (2): A G
M (2): C A
K (2): T G
S (2): C G
W (2): T A
H (3): T C A
B (3): T C G
V (3): C A G
D (3): T A G
- (4): T C A G
N (4): T C A G
? (4): T C A G

ns = 4      ls = 1641
Reading sequences, sequential format..
Reading seq # 1: ORYLA19_6_PE15
Reading seq # 2: TAKRU81_1_PE12
Reading seq # 3: GASACOUPV_10_PE5
Reading seq # 4: TETNG2_6_PE71

Sequences read..
Counting site patterns..  0:00
         395 patterns at      547 /      547 sites (100.0%),  0:00
1 ambiguous codons are seen in the data:
 ---
Counting codons..

  2   1:Sites   288.3 +  929.7 = 1218.0 Diffs   178.5 +  204.5 =  383.0
  3   1:Sites   293.1 +  927.9 = 1221.0 Diffs   164.8 +  185.2 =  350.0
  3   2:Sites   309.2 +  974.8 = 1284.0 Diffs   151.5 +  191.5 =  343.0
  4   1:Sites   266.4 +  849.6 = 1116.0 Diffs   168.2 +  201.8 =  370.0
  4   2:Sites   271.4 +  865.6 = 1137.0 Diffs    83.7 +   87.3 =  171.0
  4   3:Sites   277.9 +  862.1 = 1140.0 Diffs   138.9 +  172.1 =  311.0

       48 bytes for distance
   385520 bytes for conP
    12640 bytes for fhK
  5000000 bytes for space

2 branch types are in tree. Stop if wrong.
TREE #  1

Branch lengths in tree used as initials.
(1, (3, (4, 2)));   MP score: -1
This is a rooted tree, without clock.  Check.

   578280 bytes for conP, adjusted

    0.306290    0.054920    0.229270    0.195460    0.136400    0.081450    3.533300    1.019315    0.021013    0.279170

ntime & nrate & np:     6     2    10

Bounds (np=10):
   0.000004   0.000004   0.000004   0.000004   0.000004   0.000004   0.000100 -99.000000 -99.000000   0.000001
  50.000000  50.000000  50.000000  50.000000  50.000000  50.000000 999.000000  99.000000  99.000000   1.000000
branch=0  freq=0.730725 w0 = 0.279170
branch=0  freq=0.269275 w1 = 1.000000
            Qfactor for branch 0 = 5.615039
branch=1  freq=0.578253 w0 = 0.279170
branch=1  freq=0.213089 w1 = 1.000000
branch=1  freq=0.208658 w2 = 1.000000
            Qfactor for branch 1 = 4.961344
w[0] = 0.279170
w[1] = 1.000000
w[2] = 1.000000

np =    10
lnL0 = -4598.704070

Iterating by ming2
Initial: fx=  4598.704070
x=  0.30629  0.05492  0.22927  0.19546  0.13640  0.08145  3.53330  1.01931  0.02101  0.27917

  1 h-m-p  0.0000 0.0015 967.5177 +++CCCCC  4414.979706  4 0.0007    26 | 0/10
  2 h-m-p  0.0003 0.0013 170.1737 CYCCC  4406.559593  4 0.0005    46 | 0/10
  3 h-m-p  0.0004 0.0043 199.5339 +YYCC  4390.107563  3 0.0015    64 | 0/10
  4 h-m-p  0.0069 0.0344  17.7274 YC     4389.488483  1 0.0041    78 | 0/10
  5 h-m-p  0.0059 0.2917  12.2757 +YCCC  4386.923196  3 0.0440    97 | 0/10
  6 h-m-p  0.0168 0.0991  32.2055 YCCC   4385.483393  3 0.0110   115 | 0/10
  7 h-m-p  0.0676 0.6901   5.2628 CCC    4385.220386  2 0.0243   132 | 0/10
  8 h-m-p  0.0197 0.2850   6.4909 YC     4385.116024  1 0.0100   146 | 0/10
  9 h-m-p  0.0132 3.4853   4.8987 ++YCCC  4383.084483  3 0.3467   166 | 0/10
 10 h-m-p  1.6000 8.0000   0.3533 CC     4382.530609  1 2.1067   181 | 0/10
 11 h-m-p  1.6000 8.0000   0.3596 CC     4382.290860  1 1.8531   206 | 0/10
 12 h-m-p  1.6000 8.0000   0.2918 YC     4382.258974  1 0.9960   230 | 0/10
 13 h-m-p  1.6000 8.0000   0.1111 CC     4382.248776  1 1.8705   255 | 0/10
 14 h-m-p  1.1123 8.0000   0.1868 ++     4382.203442  m 8.0000   278 | 0/10
 15 h-m-p  1.6000 8.0000   0.3120 C      4382.189128  0 1.6836   301 | 0/10
 16 h-m-p  1.3553 8.0000   0.3876 +YC    4382.181571  1 3.8744   326 | 0/10
 17 h-m-p  1.6000 8.0000   0.4152 CY     4382.178378  1 1.9643   351 | 0/10
 18 h-m-p  1.5726 8.0000   0.5186 YC     4382.176881  1 3.0735   375 | 0/10
 19 h-m-p  1.6000 8.0000   0.4746 C      4382.176341  0 1.9975   398 | 0/10
 20 h-m-p  1.6000 8.0000   0.5169 C      4382.176118  0 2.1876   421 | 0/10
 21 h-m-p  1.6000 8.0000   0.5392 C      4382.176001  0 2.0691   444 | 0/10
 22 h-m-p  1.6000 8.0000   0.5104 C      4382.175948  0 2.3681   467 | 0/10
 23 h-m-p  1.6000 8.0000   0.5053 C      4382.175925  0 2.4291   490 | 0/10
 24 h-m-p  1.6000 8.0000   0.4996 C      4382.175915  0 2.4949   513 | 0/10
 25 h-m-p  1.6000 8.0000   0.5029 C      4382.175911  0 2.4326   536 | 0/10
 26 h-m-p  1.6000 8.0000   0.5125 C      4382.175910  0 2.5050   559 | 0/10
 27 h-m-p  1.6000 8.0000   0.5161 C      4382.175909  0 2.4015   582 | 0/10
 28 h-m-p  1.6000 8.0000   0.5603 Y      4382.175909  0 2.6780   605 | 0/10
 29 h-m-p  1.6000 8.0000   0.6652 C      4382.175908  0 2.5168   628 | 0/10
 30 h-m-p  1.6000 8.0000   0.8768 Y      4382.175908  0 2.8401   651 | 0/10
 31 h-m-p  0.6883 8.0000   3.6182 C      4382.175908  0 0.6883   674 | 0/10
 32 h-m-p  1.0002 8.0000   2.4899 Y      4382.175908  0 0.5023   687 | 0/10
 33 h-m-p  0.0399 5.3380  31.3766 --------------..  | 0/10
 34 h-m-p  0.0160 8.0000   0.0017 --Y    4382.175908  0 0.0003   727 | 0/10
 35 h-m-p  0.0160 8.0000   0.0025 -Y     4382.175908  0 0.0007   751 | 0/10
 36 h-m-p  0.0160 8.0000   0.0007 C      4382.175908  0 0.0046   774 | 0/10
 37 h-m-p  0.0161 8.0000   0.0002 C      4382.175908  0 0.0044   797 | 0/10
 38 h-m-p  0.0160 8.0000   0.0002 Y      4382.175908  0 0.0160   820 | 0/10
 39 h-m-p  0.0160 8.0000   0.0003 C      4382.175908  0 0.0160   843 | 0/10
 40 h-m-p  0.1443 8.0000   0.0000 -------C  4382.175908  0 0.0000   873
Out..
lnL  = -4382.175908
874 lfun, 6992 EigenQcodon, 20976 P(t)
end of tree file.

Time used:  0:19';
    $hyphy_file = 'inputRedirect = {};
inputRedirect["01"]="Universal";
inputRedirect["02"]="__SEQ_FILE__";
inputRedirect["03"]="__TREE_FILE__";
inputRedirect["04"]="__OUT_FILE__";
ExecuteAFile ("__PATH__/BranchSiteREL.bf", inputRedirect);
';
    $hyphy_console_file = '';
}
