#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;

my $path   = $ARGV[0]  or die "\n\tNo path provided: $0 <path to mlc> <db name>\n\tE.g. $0 primates_ens70/ selectome_v06_ens68\n\n";
my $dbname = $ARGV[1]  or die "\n\tNo db provided: $0 <path to mlc> <db name>\n\tE.g. $0 primates_ens70/ selectome_v06_ens68\n\n";

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $sth = $dbh->prepare('UPDATE codeml c, id i SET c.lnLH0_2=?, c.lnLH1_2=? WHERE c.id=i.id AND i.AC=? AND i.taxon=? AND i.subfamily=? AND c.branch=?');

# $path/ENSGT00700000105095/Primates/001.002/ENSGT00700000105095.Primates.001.002.H1.mlc
MLC:
for my $mlc ( sort glob($path.'/*/*/*/*.H1.mlc') ){
    my $H0_mlc = $mlc;
    $H0_mlc   =~ s{\.H1\.mlc$}{\.H0\.mlc};
    my $H1_mlc = $mlc;
    # Exist and not empty
    die "$H1_mlc is empty"  if ( -z "$H1_mlc" );
    die "$H0_mlc is empty"  if ( !-e "$H0_mlc" || -z "$H0_mlc" );

    # Successfully finished
    if ( `tail -1 $H1_mlc | grep '^Time used:'` ){
    }
    else {
        die "$H1_mlc not successfully finished\n";
    }
    if ( `tail -1 $H0_mlc | grep '^Time used:'` ){
    }
    else {
        die "$H0_mlc not successfully finished\n";
    }


    # Parsing
    #lnL(ntime: 10  np: 15):   -319.518513      +0.000000
    #lnL(ntime:100  np:104): -12274.697682      +0.000000
    my $tmp = `grep '^lnL' $H0_mlc`;
    my ($lnLH0) = $tmp =~ /^lnL\(ntime:\s*\d+\s+np:\s*\d+\):\s*(\-[\d\.]+)\s+/;
    $tmp = `grep '^lnL' $H1_mlc`;
    my ($lnLH1) = $tmp =~ /^lnL\(ntime:\s*\d+\s+np:\s*\d+\):\s*(\-[\d\.]+)\s+/;


    # Prepare db parameters
    my ($AC, $taxon, $subfamily, $branch) = $mlc =~ /\/(\w+)\.(\w+)\.(\d+)\.(\d+)\.H1\.mlc$/;
#    print "$AC\t$taxon\t$subfamily\t$branch\t$lnLH0\t$lnLH1\n";
    $sth->execute($lnLH0, $lnLH1, $AC, $taxon, $subfamily, $branch)  or die $sth->errstr;
}

$sth->finish();
$dbh->disconnect  or warn $dbh->errst;
print "\n\tNeed to take care of pseudo-root branch duplications now!\n\n";
exit 0;

