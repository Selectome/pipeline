#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;


#FIXME Need to retest it with new ctl file names (H0.ctl & H1.ctl instead of fx.ctl & .ctl)
my @mlc_files = glob("*/*/*/*.H0.mlc");
open(my $STAT, '>', "stat.txt");

MLC:
for my $mlc (@mlc_files){
    my $mlc0 = $mlc;
    my $mlc1 = $mlc;
    $mlc1 =~ s{\.H0}{\.H1}g;


    my ($phy) = $mlc =~ /^(.+?)\.\d{1,3}.H0\.mlc/;
    $phy .= '.phy';
    if ( -s "$phy" ){
    }
    elsif ( -s "$phy.gz" ){
        system('gunzip', "$phy.gz");
    }
    else {
        next MLC;
    }
    print $phy, "\n";

    my $msa_info = `head -1 $phy`;
    chomp $msa_info;
    $msa_info =~ s{^\s+}{};
    my ($tree_length, $msa_length) = split(/\s+/, $msa_info);
    my $input_info = "$tree_length\t$msa_length\t";


    if ( -s "$mlc0" ){
        my $completed0 = `tail -1 $mlc0 | grep '^Time used:'`;
        if ( $completed0 ){
            print {$STAT} $input_info, clean_codeml_time_used($completed0);
        }
    }

    if ( -s "$mlc1" ){
        my $completed1 = `tail -1 $mlc1 | grep '^Time used:'`;
        if ( $completed1 ){
            print {$STAT} $input_info, clean_codeml_time_used($completed1);
        }
    }

}
close $STAT;

exit 0;


##### How to read columns in R #####
##  p<-read.table("stat.txt")
##  x1<-p[,1]
##  x2<-p[,2]
##  y<-p[,3]
##  png("msa_length_distrib.png")
##  hist(x2, xlab="MSA length", main="Distribution of MSA length")
##
##  png("tree_length_distrib.png")
##  hist(x1, xlab="tree length", main="Distribution of tree leaves number")
##
##  png("CPU_time_distrib.png")
##  hist(y, xlab="CPU time (sec)", main="Distribution of CPU time"
##
##  png("cpu_vs_tree.png")
##  plot(x1, y, xlab="tree length (leaf number)", ylab="CPU time (sec)")
##
##  png("cpu_vs_msa.png")
##  plot(x2, y, xlab="MSA length", ylab="CPU time (sec)")
##


sub clean_codeml_time_used {
    my ($time_used) = @_;
    chomp $time_used;
    $time_used =~ s{^Time used:\s+}{};

    my @time_fields = split(/:/, $time_used);
    my $time_in_seconds = 0;
    if ( exists $time_fields[2] ){
        $time_in_seconds = $time_fields[2] + ($time_fields[1] * 60) + ($time_fields[0] * 60 * 60);
    }
    elsif ( exists $time_fields[1] ){
        $time_in_seconds = $time_fields[1] + ($time_fields[0] * 60);
    }
    else {
        $time_in_seconds = $time_fields[0];
    }

    return $time_in_seconds, "\n";
}
