#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Copy;
use File::Slurp;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::EnsemblDB;
use Selectome::Taxa;
use Selectome::MSA;
use Selectome::Tree;
use Selectome::Codeml;
use Selectome::Utils;

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;
use Bio::AlignIO;


our $VERSION        = 0.9.9;
my $min_leaf_number = 5; #$Selectome::Utils::min_leaf_number;
my $famid_length    = $Selectome::Utils::famid_length;

#########################################
# Get Trees and MSA from ensembl        #
# and prepare ctl files for codeml ONLY #
# Submission by another script          #
#########################################



############################## FILTERING STEPS ##############################
#
#   0. Family already processed
#   1. Discard families without "targeted taxa" sequences
#   2. Discard families with less than $min_leaf_number sequences
#          * Need internal branches AND enough statistical power !
#   3. Basal re-alignment with MAFFT
#   4. Filter by sequence length: MaxAlign
#          * Replace 'X' amino acids by gaps '-' in all sequences
#          * Remove poorly aligned sequences which should disrupt global alignment (MSA) e.g.
#   5. Re-do 2.



#   DONE later on by  msa_filtering.pl
#   6. MAFFT alignment
#          * Should clean most non-orthologous exons
#   7. Compute M-Coffee scores: keep only "biologically meaningful" columns in the MSA
#          * Replace residues with low score (lower than 8, from 0 to 7; and keep from 8 to 9) by 'x'
#   8. Compute Guidance scores
#          * Replace residues with low score (lower than 9.3, out of 10) by 'x'
#   9. Merge scores
#          * + Produce mask map !!!
#  10. Apply masking on NT alignment + mask stop codons (real or selenocysteines) not handled by codeml
#  11. Filter, with TrimAl, columns with  '$min_leaf_number'  OR  '3' or less residues
#
#############################################################################


$ENV{'PATH'} .= ':./';



############################## Options management ##############################
my ($node_id, $subtaxon, $db, $filter, $debug) = (0, 0, 'ensembl', 1, 0);
my %opts = ("node_id=i"    => \$node_id,        # Root node_id of a tree
            "taxon=s"      => \$subtaxon,       # NCBI tax_id for expected root tree
            "db=s"         => \$db,             # DB host
            'filter!'      => \$filter,         # Apply or not MSA filtering
            'debug'        => \$debug,          # Debug mode
           );


# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $node_id==0 || $subtaxon eq '0' ){
    Selectome::Utils::error("Invalid parameter(s)\n\n\te.g.: \e[1;37;42m$0 --taxon=vertebrates --node_id=870377\e[m
\t--node_id=...  Node_id of a tree (its root)
\t--taxon=...    NCBI tax_id or name for expected root tree
\t--db=...       DB host to get families: ensembl (default), genomes (ensembl genomes) or local
\t--nofilter     Do not apply MSA filtering with Mafft, Guidance, MCoffee core, ...
\t--debug        Debug mode: print extra information");
}

Selectome::Utils::error('Invalid taxon') if ( ! exists( $Selectome::Taxa::valid_taxa->{lc($subtaxon)} ) );
$subtaxon = $Selectome::Taxa::valid_taxa->{lc($subtaxon)};


my $connection_param = Selectome::EnsemblDB::get_connection_parameters($db);
if ( defined($ARGV[1]) ){
    $db = lc($ARGV[1]);
    $connection_param = Selectome::EnsemblDB::get_connection_parameters($db);
}





############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => $debug,
);


############################## Get the Adaptors ##############################
my $tree_adaptor       = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'GeneTree');
my $ncbi_taxon_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'NCBITaxon');

my $limit_node  = $ncbi_taxon_adaptor->fetch_node_by_taxon_id($subtaxon);
my $limit_left  = $limit_node->left_index;
my $limit_right = $limit_node->right_index;
$subtaxon       = $limit_node->short_name();


my $tree    = $tree_adaptor->fetch_by_root_id($node_id);
my $id      = $tree->stable_id;
$tree       = $tree->root; # To use GeneTreeNode everywhere after that !!!!
my $subtree = 0; #No initial subtree(s)



############################## Check if already done ########################
### 0: Family already done
if ( -e "$id/$subtaxon/DONE" || -e "$id/$subtaxon/DONE.gz"){
    print "\n ** Job $id ($node_id) already done for $subtaxon\n";
    $tree->release_tree;
    $reg->disconnect_all();
    exit 0;
}


#############################################################################
############ START PROCESS ##################################################
#############################################################################

my $family_info = { 'id'       => $id,
                    'node_id'  => $node_id,
                    'subtaxon' => $subtaxon,
                    'debug'    => $debug,
                    'db'       => $db,
                    'registry' => $reg,
                    'multi'    => $connection_param->{'multi'},
                  };

############################## Clean and create local repositories ##########
print "\n == $id ($node_id) __ $subtaxon\n";
system('rm',    '-Rf', "$id/$subtaxon/");
system('rm',    '-f',  "$id.*");
system('rm',    '-f',  "error/$id.*");
system('mkdir', '-p',  "$id/$subtaxon");


############################## Check root node status #######################
my $tax_level = $tree->get_tagvalue('taxon_name');
if ( defined $tax_level && $tax_level ne '' ){
    my $deepest_node_tax = $ncbi_taxon_adaptor->fetch_node_by_name($tax_level);
    my $left_tax         = $deepest_node_tax->left_index;
    my $right_tax        = $deepest_node_tax->right_index;


    if (    $left_tax >= $limit_left && $right_tax <= $limit_right && $tree->get_value_for_tag('node_type') eq 'speciation' ){
        $subtree = sprintf("%0${famid_length}d", ++$subtree); # To increase $subtree before sprintf
        print "\@$subtree\n";

        my $leaf_nbr = Selectome::Tree::get_leaf_number($tree);
        ### 2: Discard families with less than $min_leaf_number sequences
        if ( $leaf_nbr < $min_leaf_number ){
            Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
            finished($id);
            $tree->release_tree;
            $reg->disconnect_all();
            exit 0;
        }
#        Selectome::Utils::warnings($family_info, "$tax_level subtree instead of $subtaxon", $subtree, 0) if ( $tax_level ne $subtaxon );

        get_MSA($tree, $id, $subtree);
        store_files($id, $subtree);
    }
    elsif ( $left_tax >= $limit_left && $right_tax <= $limit_right ){ # duplication node
        $subtree = split_tree($tree, $id, 0);
    }
    elsif ( $left_tax < $limit_left  && $right_tax >  $limit_right ){
        $subtree = split_tree($tree, $id, 0);
        ### 1: Discard families without "targeted taxa" sequences
        Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1) if ( $subtree==0 );
    }
    else {
        ### 1: Discard families without "targeted taxa" sequences
        Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1);
    }
}
else {
    Selectome::Utils::warnings($family_info, 'Taxonomic level is not defined!', '00', 1);
}

finished($id);
$tree->release_tree;
$reg->disconnect_all();
exit 0;




##########################################################################################
##########################################################################################

################################ Tree checks ################################

sub split_tree {
    my ($root, $id, $subtree) = @_;

    # retrieve all children for this node
    my @children = @{$root->sorted_children()};

    SUBTREE:
    for my $tree ( @children ){
        my $tax_level = $tree->get_tagvalue('taxon_name');
        if ( defined $tax_level && $tax_level ne '' ){
            my $deepest_node_tax = $ncbi_taxon_adaptor->fetch_node_by_name($tax_level);
            my $left_tax         = $deepest_node_tax->left_index;
            my $right_tax        = $deepest_node_tax->right_index;


            if (    $left_tax >= $limit_left && $right_tax <= $limit_right && $tree->get_value_for_tag('node_type') eq 'speciation' ){
                $subtree = sprintf("%0${famid_length}d", ++$subtree);
                print "\@$subtree\n";

                my $leaf_nbr = Selectome::Tree::get_leaf_number($tree);
                ### 2: Discard families with less than $min_leaf_number sequences
                if ( $leaf_nbr < $min_leaf_number ){
                    Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
                    $tree->release_tree;
                    next SUBTREE;
                }
#                Selectome::Utils::warnings($family_info, "$tax_level subtree instead of $subtaxon", $subtree, 0) if ( $tax_level ne $subtaxon );

                get_MSA($tree, $id, $subtree);
                store_files($id, $subtree);
            }
            elsif ( $left_tax >= $limit_left && $right_tax <= $limit_right ){ # Duplication node, goes on to get a root speciation node
                $subtree = split_tree($tree, $id, $subtree);
            }
            elsif ( $left_tax < $limit_left  && $right_tax >  $limit_right ){
                $subtree = split_tree($tree, $id, $subtree);
            }
            else {
#                Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1);
            }
        }
        else {
#            Selectome::Utils::warnings($family_info, 'Taxonomic level is not  defined!', '00', 1);
        }
        $tree->release_tree;
    }

    return $subtree;
}



################################ Get (sub-)tree MSA ###########################

sub get_MSA {
    my ($tree, $id, $subtree) = @_;

    $subtree = sprintf("%0${famid_length}d", $subtree);

    my $nhx = $tree->nhx_format('protein_id');
    if ( $nhx =~ /:D=Y/ ){
        Selectome::Utils::warnings($family_info, 'Subtree with duplication(s), so not 1-to-1 orthologs only', $subtree, 1);
        return($tree, 1);
    }



    print "\n\tGet basic prot and cds MSAs...\n" if ( $debug );
    #Protein alignment
    my $prot_align  = $tree->get_SimpleAlign;
#    print "...\n";
    #Codon alignment
    my $cds_align   = $tree->get_SimpleAlign(-cdna=>1);
    # Write alignments in fasta format
    # nt and aa.ORI.fas are original alignments before any filtering steps
    Selectome::MSA::write_MSA($cds_align,  "$id.$subtaxon.$subtree", 'nt.fasta', 'nt.ORI.fas');
    Selectome::MSA::write_MSA($prot_align, "$id.$subtaxon.$subtree", 'aa.fasta', 'aa.ORI.fas');


    # Get chromosome name
    print "\n\tGet chromosomal location...\n" if ( $debug );
    my $chr = Selectome::Codeml::get_chromosomal_localization($tree, $prot_align) || '';
    if ( $chr ne '' ){ # eq 'MT'
        write_file( "$id.$subtaxon.$subtree.CHR", $chr ) ;
        Selectome::Utils::warnings($family_info, 'Mitochondrial loci', $subtree, 0);
    }


    if ( $filter ){
        print "\n\tBasic MSA re-alignment (MAFFT)...\n" if ( $debug );
        ### 3: Basal re-alignment with MAFFT
        Selectome::MSA::align_by_MAFFT($family_info, "$id.$subtaxon.$subtree.aa.ORI.fas");


        ### 4: Filter by sequence length or with MaxAlign
        print "\n\t\tTree filtering (MaxAlign)...\n" if ( $debug );
        ($tree, undef)      = Selectome::MSA::filter_by_maxAlign($family_info, "$id.$subtaxon.$subtree.aa.ORI.fas.mafft", $tree);
        unlink "$id.$subtaxon.$subtree.aa.ORI.fas.mafft";


        my $leaf_nbr_after  = Selectome::Tree::get_leaf_number($tree);
        ### 5: Discard families with less than $min_leaf_number sequences
        if ( $leaf_nbr_after < $min_leaf_number ){
            Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
            return($tree, 1);
        }


        # Re-get clean AA & nt alignments after filtering by MaxAlign
        $prot_align = $tree->get_SimpleAlign;
        $cds_align  = $tree->get_SimpleAlign(-cdna=>1);
        # Write alignments in fasta format
        Selectome::MSA::write_MSA($prot_align, "$id.$subtaxon.$subtree", 'aa.fasta', 'aa.fas');
        Selectome::MSA::write_MSA($cds_align,  "$id.$subtaxon.$subtree", 'nt.fasta', 'nt.fas');
    }
    else { # No MSA filtering !
        copy("$id.$subtaxon.$subtree.nt.ORI.fas", "$id.$subtaxon.$subtree.nt.fas");
        copy("$id.$subtaxon.$subtree.aa.ORI.fas", "$id.$subtaxon.$subtree.aa.fas");
    }


    $nhx = $tree->nhx_format('protein_id');
    write_file("$id.$subtaxon.$subtree.nhx", $nhx);


    return ($tree);
}


sub store_files {
    my ($id, $subtree) = @_;
    my $prefix = "$id.$subtaxon.$subtree";

    $subtree = sprintf("%0${famid_length}d", $subtree);
    my @res_files = glob("$prefix.*");
    if ( exists($res_files[0]) && -e "$res_files[0]" ){
        system("mv $prefix.* $id/$subtaxon/");
    }


    #Archive useless files
    system("gzip -9 $id/$subtaxon/*.ORI.fas 2>/dev/null");

    print "\n";
    return;
}

################################ Error and warning management #################

sub finished {
    my ($id) = @_;

    system('touch', "$id/$subtaxon/DONE") if ( defined($id) );
    return;
}

