#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;

use lib './pipeline'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68 Primates\n\n";

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


# Don't do it for leaves-pseudoroot
my $sth_pseudoroot = $dbh->prepare('SELECT n.id, n.branch FROM node2gene n, id i WHERE i.id=n.id AND i.taxon=? AND n.is_root=1 AND n.is_a_leaf=0 GROUP BY n.id, n.branch');
$sth_pseudoroot->execute($phylum)  or die $sth_pseudoroot->errstr;
my $pseudoroot_ref = $sth_pseudoroot->fetchall_arrayref;
$sth_pseudoroot->finish();

if ( exists($pseudoroot_ref->[0]) ){
    my $nbr_to_be_duplicated = 0;
    my $to_duplicate;
    PSEUDOROOT:
    for my $row (@$pseudoroot_ref){
        my ($id, $branch) = @$row;

        my $sth_checkInCodeml = $dbh->prepare('SELECT * FROM codeml WHERE id = ? AND branch = ?');
        $sth_checkInCodeml->execute($id, $branch);
        my $inCodeml_ref      = $sth_checkInCodeml->fetchall_arrayref;
        if ( ! exists($inCodeml_ref->[0]) ){
            $to_duplicate->{$id}->{'TODO'} = $branch;
        }
        else { # exists($inCodeml_ref->[0])
            $to_duplicate->{$id}->{'DONE'} = $branch;
        }

        $sth_checkInCodeml->finish();
    }


    CODEML:
    for my $id (keys %$to_duplicate){
        next CODEML  if ( ! exists($to_duplicate->{$id}->{'TODO'}) || ! exists($to_duplicate->{$id}->{'DONE'}) );

        my $sth_codemlInsert = $dbh->prepare('INSERT INTO codeml (id, branch, omega0, omega2, kappa, p0, p1, p2ab, lnLH0, timeH0, lnLH1, timeH1, lrt_raw, lrt, pvalue, qvalue, selected, validity)
                                                  SELECT id, ?, omega0, omega2, kappa, p0, p1, p2ab, lnLH0, timeH0, lnLH1, timeH1, lrt_raw, lrt, pvalue, qvalue, selected, validity FROM codeml WHERE id = ? AND branch = ?
                                                  ON DUPLICATE KEY UPDATE kappa=VALUES(kappa), p0=VALUES(p0), p1=VALUES(p1), p2ab=VALUES(p2ab)');
        $sth_codemlInsert->execute($to_duplicate->{$id}->{'TODO'}, $id, $to_duplicate->{$id}->{'DONE'});
        $sth_codemlInsert->finish();
#        print "$id, $to_duplicate->{$id}->{'DONE'}, $to_duplicate->{$id}->{'TODO'}\n";
        $nbr_to_be_duplicated++;
    }

    print "\n\tPseudo-root number: 2 x ", (scalar @$pseudoroot_ref / 2), "\n";
    print "\tPseudo-root to be duplicated: ", $nbr_to_be_duplicated, "\n\n";
}


$dbh->disconnect  or warn $dbh->errst;
exit 0;

