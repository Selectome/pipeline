#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;

# List all job files
#my @jobs_to_do = `find . -type f -name \*.ctl -print`;
my @jobs_to_do = glob('E*/*/*/*.H0.ctl');

my $stats = '';
JOB:
for my $job ( @jobs_to_do ){
    my ($phy) = $job =~ /^(.+)\.\d+\.H0\.ctl$/;
    $phy     .= '.phy';

    my $stat = `head -1 $phy | sed -e 's/^ *//'`;
    chomp $stat;

    $stats .= "$job\t".join("\t", split(/\s+/, $stat))."\n";
}

write_file("fam_stat.$$", $stats);
system("sort -n -k2 -k3 fam_stat.$$ > ordered_fam_jobs");
unlink "fam_stat.$$";

exit 0;

