#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;

use lib '.';
use DB;



my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68 Primates\n\n";
my $aa_extension        = 'aa.fas.REF';
my $nt_extension        = 'nt.fas.ORI';
my $aa_masked_extension = 'aa.fas.newmafft.ScoreMerged';
my $nt_masked_extension = 'nt.fas.preTrimAl';


#### Get useful subfamilies that have passed filters
my $dbh     = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_msa = $dbh->prepare('SELECT m.id, m.msa_path, m.kept_positions FROM msa m, id i WHERE i.id=m.id AND i.taxon = ? ORDER BY m.id');
$sth_msa->execute($phylum)  or die $sth_msa->errstr;
my $msa_ref = $sth_msa->fetchall_arrayref;
$sth_msa->finish();


####
if ( exists($msa_ref->[0]) ){
    MSA:
    for my $row (@$msa_ref){
        my ($id, $msa_path, $kept) = @$row;
        next MSA  if ( $kept eq ':' );

        EXT:
        for my $ext ( qw(aa.fas nt.fas aa_masked.fas nt_masked.fas nwk nhx) ){
            if ( !-e "$DB::wwwtmp/$msa_path.$ext" || -z "$DB::wwwtmp/$msa_path.$ext" ){
                print "[$id\t$msa_path\t$ext]\n";
                last EXT;
            }
        }
    }
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;

