#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;

use lib './pipeline'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;


my $dbname = $ARGV[0] || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68 [Primates]\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $phylum = $ARGV[1] || 0;


# Get all ids
my $ids;
if ( $phylum ){
    my $sth = $dbh->prepare("SELECT m.id, m.kept_positions, m.msa_length FROM msa m, id i WHERE i.id=m.id AND i.taxon = ?");
    $sth->execute($phylum)  or die $sth->errstr;
    $ids    = $sth->fetchall_arrayref;
    $sth->finish();
}
else {
    my $sth = $dbh->prepare('SELECT m.id, m.kept_positions, m.msa_length FROM msa m');
    $sth->execute()  or die $sth->errstr;
    $ids    = $sth->fetchall_arrayref;
    $sth->finish();
}

ID:
for my $rows (@$ids){
    my ($id, $kept_positions, $msa_length) = @$rows;

    my $block_size = 0;
    if ( $kept_positions !~ /,/ ){
        my @pos = split(/:/, $kept_positions);
        $block_size = $pos[1] - $pos[0] + 1;
    }
    else {# Several blocks
        my @blocks = split(/,/, $kept_positions);
        for ( my $i=0; $i < scalar @blocks; $i++ ){
            my @pos = split(/:/, $blocks[$i]);
            $block_size += $pos[1] - $pos[0] + 1;
        }
    }

    print "$id\t$block_size\t$msa_length\n";
}


$dbh->disconnect  or warn $dbh->errst;
exit;

