#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Statistics::Basic qw(:all);
use Statistics::Distributions;
use List::Compare;

my @originals = glob('1_ens61/*/*/*/*H0.mlc');
#my @originals = ('1_ens61/ENSGT00390000003057/Primates/1.002/ENSGT00390000003057.Primates.01.002.H0.mlc',
#                 '1_ens61/ENSGT00390000003057/Primates/1.001/ENSGT00390000003057.Primates.01.001.H0.mlc',
#                 '1_ens61/ENSGT00590000082785/Primates/8.008/ENSGT00590000082785.Primates.08.008.H0.mlc',
#                 '1_ens61/ENSGT00610000085994/Primates/1.110/ENSGT00610000085994.Primates.01.110.H0.mlc',
#                 '1_ens61/ENSGT00530000063902/Primates/1.030/ENSGT00530000063902.Primates.01.030.H0.mlc',
#                 '1_ens61/ENSGT00530000063518/Primates/1.180/ENSGT00530000063518.Primates.01.180.H0.mlc',
#                );
my @other_folders = ('2_ens61', '3_ens61');

#Need to get:   lnL(ntime: 10  np: 14):  -4685.147198
#               LRT
#               tree length =
#               kappa (ts/tv) =
#               BEB sites + proba
#               Time used:

open(my $RESULTS, '>', 'results.txt');
open(my $EXTRA,   '>', 'extra_results.txt');
open(my $ALL,     '>', 'all_results.txt');
print {$ALL} "ln01\tln02\tln03\tln11\tln12\tln13\tlrt1\tlrt2\tlrt3\tpval1\tpval2\tpval3\tpsignif1_2_3\tpdiff\ttime01\ttime02\ttime03\ttimediff0\ttime11\ttime12\ttime13\ttimediff1\ttreelength01\ttreelength02\ttreelength03\ttreelengthdiff0\ttreelength11\ttreelength12\ttreelength13\ttreelengthdiff1\tkappa01\tkappa02\tkappa03\tkappadiff0\tkappa11\tkappa12\tkappa13\tkappadiff1\tbeb1\tbeb2\tbeb3\tbebdiff\n";
for my $H0mlc (@originals){
    my $H0mlc_2 = $H0mlc;
    $H0mlc_2    =~ s/^\w+/$other_folders[0]/;
    my $H0mlc_3 = $H0mlc;
    $H0mlc_3    =~ s/^\w+/$other_folders[1]/;
    die "\n\tH0 files to compare are not different: [$H0mlc] [$H0mlc_2] [$H0mlc_3]\n" if ( "$H0mlc" eq "$H0mlc_2" || "$H0mlc_2" eq "$H0mlc_3" || "$H0mlc_3" eq "$H0mlc" );

    my $H1mlc = $H0mlc;
    $H1mlc      =~ s/\.H0\./.H1./;
    my $H1mlc_2 = $H1mlc;
    $H1mlc_2    =~ s/^\w+/$other_folders[0]/;
    my $H1mlc_3 = $H1mlc;
    $H1mlc_3    =~ s/^\w+/$other_folders[1]/;
    die "\n\tH1 files to compare are not different: [$H1mlc] [$H1mlc_2] [$H1mlc_3]\n" if ( "$H1mlc" eq "$H1mlc_2" || "$H1mlc_2" eq "$H1mlc_3" || "$H1mlc_3" eq "$H1mlc" );


    # lnL_H0
    my $lnL_H0     = `grep -a 'lnL(ntime' $H0mlc`;
    my ($lnL_H0_1) = $lnL_H0 =~ /\): +(\-?\d+\.\d+)/;

    $lnL_H0        = `grep -a 'lnL(ntime' $H0mlc_2`;
    my ($lnL_H0_2) = $lnL_H0 =~ /\): +(\-?\d+\.\d+)/;

    $lnL_H0        = `grep -a 'lnL(ntime' $H0mlc_3`;
    my ($lnL_H0_3) = $lnL_H0 =~ /\): +(\-?\d+\.\d+)/;


    # lnL_H1
    my $lnL_H1     = `grep -a 'lnL(ntime' $H1mlc`;
    my ($lnL_H1_1) = $lnL_H1 =~ /\): +(\-?\d+\.\d+)/;

    $lnL_H1        = `grep -a 'lnL(ntime' $H1mlc_2`;
    my ($lnL_H1_2) = $lnL_H1 =~ /\): +(\-?\d+\.\d+)/;

    $lnL_H1        = `grep -a 'lnL(ntime' $H1mlc_3`;
    my ($lnL_H1_3) = $lnL_H1 =~ /\): +(\-?\d+\.\d+)/;


    # LRT
    my $lrt_1 = 2*($lnL_H1_1-$lnL_H0_1);
    my $lrt_2 = 2*($lnL_H1_2-$lnL_H0_2);
    my $lrt_3 = 2*($lnL_H1_3-$lnL_H0_3);


    # p-value
    my $pvalue_1 = Statistics::Distributions::chisqrprob(1, $lrt_1);
    my $pvalue_2 = Statistics::Distributions::chisqrprob(1, $lrt_2);
    my $pvalue_3 = Statistics::Distributions::chisqrprob(1, $lrt_3);


    # p-value signif
    my $psignif_1 = $pvalue_1 < 0.05 ? 1 : 0;
    my $psignif_2 = $pvalue_2 < 0.05 ? 1 : 0;
    my $psignif_3 = $pvalue_3 < 0.05 ? 1 : 0;


    # p-value diff
    my $p_diff = 0;

    if ( $pvalue_1 != $pvalue_2 || $pvalue_1 != $pvalue_3 ){
        $p_diff = 1;
    }


    # Running time
    my $threshold = 20; # 20%
    my $time1_0 = time_in_seconds(`tail -1 $H0mlc   | grep -a 'Time used:'`);
    my $time2_0 = time_in_seconds(`tail -1 $H0mlc_2 | grep -a 'Time used:'`);
    my $time3_0 = time_in_seconds(`tail -1 $H0mlc_3 | grep -a 'Time used:'`);
    my $time_diff_0 = are_different($time1_0, $time2_0, $time3_0, $threshold);

    my $time1_1 = time_in_seconds(`tail -1 $H1mlc   | grep -a 'Time used:'`);
    my $time2_1 = time_in_seconds(`tail -1 $H1mlc_2 | grep -a 'Time used:'`);
    my $time3_1 = time_in_seconds(`tail -1 $H1mlc_3 | grep -a 'Time used:'`);
    my $time_diff_1 = are_different($time1_1, $time2_1, $time3_1, $threshold);


    # tree length
    $threshold = 5; # 5%
    my ($tree_length1_0) = `grep -a '^tree length =' $H0mlc`   =~ /tree length = *([\-\d\.]+)$/;
    my ($tree_length2_0) = `grep -a '^tree length =' $H0mlc_2` =~ /tree length = *([\-\d\.]+)$/;
    my ($tree_length3_0) = `grep -a '^tree length =' $H0mlc_3` =~ /tree length = *([\-\d\.]+)$/;
    my $tree_length_diff_0 = are_different($tree_length1_0, $tree_length2_0, $tree_length3_0, $threshold);

    my ($tree_length1_1) = `grep -a '^tree length =' $H1mlc`   =~ /tree length = *([\-\d\.]+)$/;
    my ($tree_length2_1) = `grep -a '^tree length =' $H1mlc_2` =~ /tree length = *([\-\d\.]+)$/;
    my ($tree_length3_1) = `grep -a '^tree length =' $H1mlc_3` =~ /tree length = *([\-\d\.]+)$/;
    my $tree_length_diff_1 = are_different($tree_length1_1, $tree_length2_1, $tree_length3_1, $threshold);


    # kappa value
    $threshold = 5; # 5%
    my ($kappa1_0) = `grep -a '^kappa (ts/tv) =' $H0mlc`   =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my ($kappa2_0) = `grep -a '^kappa (ts/tv) =' $H0mlc_2` =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my ($kappa3_0) = `grep -a '^kappa (ts/tv) =' $H0mlc_3` =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my $kappa_diff_0 = are_different($kappa1_0, $kappa2_0, $kappa3_0, $threshold);

    my ($kappa1_1) = `grep -a '^kappa (ts/tv) =' $H1mlc`   =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my ($kappa2_1) = `grep -a '^kappa (ts/tv) =' $H1mlc_2` =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my ($kappa3_1) = `grep -a '^kappa (ts/tv) =' $H1mlc_3` =~ /kappa \(ts\/tv\) = *([\-\d\.]+)$/;
    my $kappa_diff_1 = are_different($kappa1_1, $kappa2_1, $kappa3_1, $threshold);


    # predicted sites with associated probabilities (BEB)
    my $beb_1 = get_BEB_aa($H1mlc);
    my $beb_2 = get_BEB_aa($H1mlc_2);
    my $beb_3 = get_BEB_aa($H1mlc_3);

    my $beb_diff = compare_BEBs($beb_1, $beb_2, $beb_3);


    print {$RESULTS} "$lnL_H0_1\t$lnL_H0_2\t$lnL_H0_3\t$lnL_H1_1\t$lnL_H1_2\t$lnL_H1_3\t$lrt_1\t$lrt_2\t$lrt_3\t$pvalue_1\t$pvalue_2\t$pvalue_3\t$psignif_1$psignif_2$psignif_3\t$p_diff\n";
    print {$ALL}     "$lnL_H0_1\t$lnL_H0_2\t$lnL_H0_3\t$lnL_H1_1\t$lnL_H1_2\t$lnL_H1_3\t$lrt_1\t$lrt_2\t$lrt_3\t$pvalue_1\t$pvalue_2\t$pvalue_3\t$psignif_1$psignif_2$psignif_3\t$p_diff\t";

    print {$EXTRA} "$time1_0\t$time2_0\t$time3_0\t$time_diff_0\t$time1_1\t$time2_1\t$time3_1\t$time_diff_1\t";
    print {$ALL}   "$time1_0\t$time2_0\t$time3_0\t$time_diff_0\t$time1_1\t$time2_1\t$time3_1\t$time_diff_1\t";
    print {$EXTRA} "$tree_length1_0\t$tree_length2_0\t$tree_length3_0\t$tree_length_diff_0\t$tree_length1_1\t$tree_length2_1\t$tree_length3_1\t$tree_length_diff_1\t";
    print {$ALL}   "$tree_length1_0\t$tree_length2_0\t$tree_length3_0\t$tree_length_diff_0\t$tree_length1_1\t$tree_length2_1\t$tree_length3_1\t$tree_length_diff_1\t";
    print {$EXTRA} "$kappa1_0\t$kappa2_0\t$kappa3_0\t$kappa_diff_0\t$kappa1_1\t$kappa2_1\t$kappa3_1\t$kappa_diff_1\t";
    print {$ALL}   "$kappa1_0\t$kappa2_0\t$kappa3_0\t$kappa_diff_0\t$kappa1_1\t$kappa2_1\t$kappa3_1\t$kappa_diff_1\t";
    print {$EXTRA} scalar(@$beb_1), "\t", scalar(@$beb_2), "\t", scalar(@$beb_3), "\t$beb_diff\n";
    print {$ALL}   scalar(@$beb_1), "\t", scalar(@$beb_2), "\t", scalar(@$beb_3), "\t$beb_diff\n";
}
close $EXTRA;
close $RESULTS;
close $ALL;

exit 0;


sub time_in_seconds {
    my ($time) = @_;
    my $seconds = 0;

    if ( $time =~ /^Time used: +(\d+):(\d+):(\d+) *$/ ){
        $seconds = $3 + $2*60 + $1*60*60;
    }
    elsif ( $time =~ /^Time used: +(\d+):(\d+) *$/ ){
        $seconds = $2 + $1*60;
    }

    return $seconds;
}

sub are_different {
    my ($val1, $val2, $val3, $threshold) = @_;
    my $stat = 0;

    my $mean     = mean($val1, $val2, $val3);
    my $stddev   = stddev($val1, $val2, $val3);
    my $var_coef;
    if ( $mean == 0 && $stddev == 0 ){
        $var_coef = 0;
    }
    elsif ( $mean == 0 && $stddev != 0 ){
        $var_coef = 1;
    }
    else {
        $var_coef = $stddev / $mean;
    }

    if ( $var_coef < ($threshold / 100) ){
        $stat = 1;
    }

#    if ( abs(($val1 - $val2) / ($val1 + $val2)) < 0.05 ){
#        $stat .= '1';
#    }
#    else {
#        $stat .= '0';
#    }
#    if ( abs(($val1 - $val3) / ($val1 + $val3)) < 0.05 ){
#        $stat .= '1';
#    }
#    else {
#        $stat .= '0';
#    }
#    if ( abs(($val3 - $val2) / ($val3 + $val2)) < 0.05 ){
#        $stat .= '1';
#    }
#    else {
#        $stat .= '0';
#    }

    return $stat;
}


sub get_BEB_aa {
    my ($H1_file) = @_;

    my @BEB_aa;
    my $flag = 0;
    open(my $H1, '<', "$H1_file") or die "\tCannot open '$H1_file' file\n";
    H1:
    while(<$H1>){
        last H1   if ( $_ =~ /^The grid \(see ternary graph for p0-p1\)/ );
        $flag = 1 if ( $_ =~ /^Bayes Empirical Bayes \(BEB\) analysis/ );
        next H1   if ( $flag==0 );

        # Only on significant BEB proba
        if ( $_ =~ /^\s*(\d+) [A-Z] ([01]\.\d+)\*/ ){
            push @BEB_aa, $1;
        }
    }
    close $H1;

    return \@BEB_aa;
}

sub compare_BEBs {
    my ($beb1, $beb2, $beb3) = @_;
    my $stat = 0;

    if ( scalar(@$beb1) != scalar(@$beb2) || scalar(@$beb3) != scalar(@$beb2) ){
        $stat = 0;
    }
    else {
        my $lcm = List::Compare->new($beb1, $beb2, $beb3);
        my @intersection = $lcm->get_intersection;

        if ( scalar(@$beb1) == scalar(@intersection) ){
            $stat = 1;
        }
    }

    return $stat;
}

