#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::Config;

use DBI;
my $dbname   = 'selectome_7';
my $host     = $Selectome::Config::host;
my $user     = $Selectome::Config::user;
my $port     = $Selectome::Config::port;
my $password = $Selectome::Config::pass;

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$host;port=$port", $user, $password) or die $DBI::errstr;


###  Check positions ###
my $sth_valid = $dbh->prepare('SELECT AC, subtree_number, random_number, position, proba FROM selectome_positions ORDER BY AC, subtree_number, random_number, position');
# Don't care about previous validity computed !!!
$sth_valid->execute();
my $positions_to_check = $sth_valid->fetchall_arrayref;
$sth_valid->finish();


my %count;
my $threshold  = 2; # Remove track/branch if more than X contigueous sites predicted !
my $prev_pos   = -10;
my $prev_entry = '';
my %valid_pos;
POSITION_CHECKING:
for my $rows (@$positions_to_check){
    my ($AC, $subtree_number, $random_number, $position, $proba) = @$rows;

    if ( $prev_pos==($position-1) ){
        $count{"$AC-$subtree_number-$random_number"}++;
    }
    else {
        if ( exists($count{$prev_entry}) && $count{$prev_entry} >= $threshold ){
            my $original_pos = $prev_pos - $count{"$prev_entry"};
            my @entry = split(/-/, $prev_entry);

            # A track/branch where $threshold predicted positions are found is no more valid
            my $sth_update = $dbh->prepare('UPDATE selectome_positions SET validity=0 WHERE AC=? AND subtree_number=? AND random_number=?');
            $sth_update->execute($entry[0], $entry[1], $entry[2]);
            $sth_update->finish();

            $valid_pos{"$prev_entry-$original_pos-$prev_pos"} = $count{$prev_entry} + 1;
        }
        $count{"$AC-$subtree_number-$random_number"} = 0;
    }

    $prev_entry = "$AC-$subtree_number-$random_number";
    $prev_pos   = $position;
}


for my $key ( sort keys(%valid_pos) ){
    print "[$key]\t[$valid_pos{$key}]\n";
}
print "\n\n";


#Set as selection = 0 all families that have only posterior probabilities = 0, no more 1 validity
my $sth_sum = $dbh->prepare('SELECT DISTINCT p.AC, p.subtree_number, p.validity FROM selectome_positions p WHERE p.validity=0 AND (SELECT IF((SELECT q.validity FROM selectome_positions q WHERE q.AC=p.AC AND q.subtree_number=p.subtree_number AND q.validity=1 LIMIT 1), 0, 1))');
$sth_sum->execute();
my $sum = $sth_sum->fetchall_arrayref;
$sth_sum->finish();

for my $rows (@$sum){
    my ($AC, $subtree_number, $validity) = @$rows;

    # Set selection to 0 if all branches of a subtree have validity=0
    my $sth_up = $dbh->prepare('UPDATE selectome_summary SET selection=0 WHERE AC=? AND subtree_number=?');
    $sth_up->execute($AC, $subtree_number);
    $sth_up->finish();
    print "[$AC]\t[$subtree_number]\n";
}

print "\n\n";


#Set as without selection branches where all sites have been invalidated !
my $sth_branch = $dbh->prepare('SELECT DISTINCT c.AC, c.subtree_number, c.random_number, c.boolean FROM selectome_branches b, selectome_codeml c WHERE b.AC=c.AC AND b.subtree_number=c.subtree_number AND b.random_number=c.random_number AND c.boolean=1 AND (SELECT IF((SELECT q.validity FROM selectome_positions q WHERE q.AC=c.AC AND q.subtree_number=c.subtree_number AND q.random_number=c.random_number AND q.validity=1 LIMIT 1), 0, 1))');
$sth_branch->execute();
my $branch = $sth_branch->fetchall_arrayref;
$sth_branch->finish();

for my $rows (@$branch){
    my ($AC, $subtree_number, $random_number, $boolean) = @$rows;

    # Set selection to 0 if all branches of a subtree have validity=0
    my $sth_up = $dbh->prepare('UPDATE selectome_codeml SET validity = 0 WHERE AC=? AND subtree_number=? AND random_number=? AND boolean=1');
    $sth_up->execute($AC, $subtree_number, $random_number);
    $sth_up->finish();
    print "[$AC]\t[$subtree_number]\t[$random_number]\n";
}



#Set as selection = 0 families where all branches have been invalidated
my $sth_sum2 = $dbh->prepare('SELECT DISTINCT c.AC, c.subtree_number FROM selectome_branches b, selectome_codeml c WHERE b.AC=c.AC AND b.subtree_number=c.subtree_number AND c.boolean=1 AND c.validity=0 AND (SELECT IF((SELECT q.validity FROM selectome_codeml q WHERE q.AC=c.AC AND q.subtree_number=c.subtree_number AND q.validity=1 AND q.boolean=1 LIMIT 1), 0, 1))');
$sth_sum2->execute();
my $sum2 = $sth_sum2->fetchall_arrayref;
$sth_sum2->finish();

for my $rows (@$sum2){
    my ($AC, $subtree_number) = @$rows;

    # Set selection to 0 for a subtree if all branches of a subtree have validity=0
    my $sth_up = $dbh->prepare('UPDATE selectome_summary SET selection=0 WHERE AC=? AND subtree_number=?');
    $sth_up->execute($AC, $subtree_number);
    $sth_up->finish();
    print "[$AC]\t[$subtree_number]\n";
}


$dbh->disconnect or warn $dbh->errst;

exit 0;

