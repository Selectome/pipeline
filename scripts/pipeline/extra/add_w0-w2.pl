#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use FindBin qw( $RealBin );
use lib "$RealBin/..";
use Selectome::Utils;
use DB;


my $famid_length    = $Selectome::Utils::famid_length;
my $branchid_length = $Selectome::Utils::branchid_length;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v06_ens68\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $sql_codeml = 'SELECT c.id, i.ac, i.subfamily, i.taxon, c.branch FROM id i, codeml c WHERE i.id=c.id AND i.taxon="Glires"';
my $sth_codeml = $dbh->prepare($sql_codeml);
$sth_codeml->execute()  or die $sth_codeml->errstr;


my $sth_omega = $dbh->prepare('UPDATE codeml SET omega0=?, omega2=?, kappa=?, p0=?, p1=?, p2ab=? WHERE id=? AND branch=?');
while ( my ($tree_id, $ac, $sub, $taxon, $branch) = ($sth_codeml->fetchrow_array) ){
    $sub    = sprintf("%0${famid_length}d",    $sub);
    $branch = sprintf("%0${branchid_length}d", $branch);

    # To prevent using fake duplicates from pseudoroot which have no mlc file
    next  if ( !-e "$ac/$taxon/$sub.$branch/$ac.$taxon.$sub.$branch.H1.mlc" );

    my $omegas      = `grep '^foreground w'    $ac/$taxon/$sub.$branch/*.H1.mlc`;
    chomp $omegas;

    my $kappas      = `grep '^kappa (ts/tv) =' $ac/$taxon/$sub.$branch/*.H1.mlc`;
    chomp $kappas;

    my $proportions = `grep '^proportion '     $ac/$taxon/$sub.$branch/*.H1.mlc`;
    chomp $proportions;

    my ($omega0, $omega2)     = $omegas      =~ /^foreground w\s+(-?\d+\.\d+)\s+-?\d+\.\d+\s+(-?\d+\.\d+)\s+/;
    my ($kappa)               = $kappas      =~ /^kappa\s+\(ts\/tv\)\s+=\s+(\d+\.\d+)/;
    my ($p0, $p1, $p2a, $p2b) = $proportions =~ /^proportion\s+(-?\d+\.\d+)\s+(-?\d+\.\d+)\s+(-?\d+\.\d+)\s+(-?\d+\.\d+)/;
    my $p2ab = $p2a+$p2b;
    $sth_omega->execute($omega0, $omega2, $kappa, $p0, $p1, $p2ab, $tree_id, $branch)  or die $sth_omega->errstr;
#    print "$omega0, $omega2, $kappa, $p0, $p1, $p2a, $p2b, $tree_id, $branch\n";
}
$sth_omega->finish();


$sth_codeml->finish();
$dbh->disconnect  or warn $dbh->errst;

exit 0;

