#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use Statistics::Distributions;
use lib '.';
use DB;

my $dbname = $ARGV[0]  or die "\n\tNo db provided: $0 <db name>\n\tE.g. $0 selectome_v06_ens68\n\n";

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $sthID = $dbh->prepare('SELECT c.id FROM codeml c, id i WHERE i.id=c.id AND i.taxon != "Primates"');
$sthID->execute()  or die $sthID->errstr;
my $ID_ref = $sthID->fetchall_arrayref;
$sthID->finish();

# When several runs
#my $sthLRT = $dbh->prepare('SELECT branch, IF(lnLH1>lnLH1_2, lnLH1, lnLH1_2), IF(lnLH0>lnLH0_2, lnLH0, lnLH0_2), ( SELECT MAX(IF(c.lnLH0>c.lnLH0_2, c.lnLH0, c.lnLH0_2)) FROM codeml c WHERE c.id=? ) FROM codeml WHERE id=?');
# When single run
my $sthLRT = $dbh->prepare('SELECT branch, lnLH1, lnLH0, ( SELECT MAX(c.lnLH0) FROM codeml c WHERE c.id=? ) FROM codeml WHERE id=?');
my $upLRT  = $dbh->prepare('UPDATE codeml SET lrt=?, pvalue=? WHERE id=? AND branch=?');
ID:
for my $row (@$ID_ref){
    my ($id) = @$row;

    $sthLRT->execute($id, $id)  or die $sthLRT->errstr;
    my $LRT_ref = $sthLRT->fetchall_arrayref;
    LRT:
    for my $rw (@$LRT_ref){
        my ($branch, $best_lnL1, $best_lnL0, $max_lnL0) = @$rw;
        die "Problem with max [$best_lnL0] [$max_lnL0]\n"  if ( $best_lnL0 > $max_lnL0 );

        my $corrected_lrt = 2 * ( $best_lnL1 - $max_lnL0 );
        $corrected_lrt = $corrected_lrt < 0 ? 0 : $corrected_lrt;

        my $pvalue = Statistics::Distributions::chisqrprob(1, $corrected_lrt);
        $pvalue    = 1e-200  if ( $pvalue==0 ); # To avoid NaN qvalue when pvalue too close to 0

        # Update in db
#        print "$id\t$best_lnL1\t$best_lnL0\t$max_lnL0\t$corrected_lrt\n";
        $upLRT->execute($corrected_lrt, $pvalue, $id, $branch)  or die $upLRT->errstr;
    }
}
$sthLRT->finish();
$upLRT->finish();

$dbh->disconnect  or warn $dbh->errst;
exit 0;

