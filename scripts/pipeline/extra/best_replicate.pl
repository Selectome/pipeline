#!/usr/bin/env perl

## Perl embedded modules
use strict;
use warnings;
use diagnostics;

use List::Util qw(min max);
use List::MoreUtils qw(first_index);

my $final_folder = 'Best_replicate';



my @originals = glob('2_ens61/*/*/*/*H0.mlc');
#my @originals = ('1_ens61/ENSGT00390000003057/Primates/1.002/ENSGT00390000003057.Primates.01.002.H0.mlc',
#                 '1_ens61/ENSGT00390000003057/Primates/1.001/ENSGT00390000003057.Primates.01.001.H0.mlc',
#                 '1_ens61/ENSGT00590000082785/Primates/8.008/ENSGT00590000082785.Primates.08.008.H0.mlc',
#                 '1_ens61/ENSGT00610000085994/Primates/1.110/ENSGT00610000085994.Primates.01.110.H0.mlc',
#                 '1_ens61/ENSGT00530000063902/Primates/1.030/ENSGT00530000063902.Primates.01.030.H0.mlc',
#                 '1_ens61/ENSGT00530000063518/Primates/1.180/ENSGT00530000063518.Primates.01.180.H0.mlc',
#                );
my @other_folders = ('2_ens61', '1_ens61', '3_ens61');

system("rm -Rf $final_folder");
mkdir $final_folder;

MLC:
for my $H0mlc (@originals){

    # LIST mlc files
    my $H0mlc_2 = $H0mlc;
    $H0mlc_2    =~ s/^\w+/$other_folders[1]/;
    my $H0mlc_3 = $H0mlc;
    $H0mlc_3    =~ s/^\w+/$other_folders[2]/;
    die "\n\tH0 files to compare are not different: [$H0mlc] [$H0mlc_2] [$H0mlc_3]\n" if ( "$H0mlc" eq "$H0mlc_2" || "$H0mlc_2" eq "$H0mlc_3" || "$H0mlc_3" eq "$H0mlc" );

    my $H1mlc = $H0mlc;
    $H1mlc      =~ s/\.H0\./.H1./;
    my $H1mlc_2 = $H1mlc;
    $H1mlc_2    =~ s/^\w+/$other_folders[1]/;
    my $H1mlc_3 = $H1mlc;
    $H1mlc_3    =~ s/^\w+/$other_folders[2]/;
    die "\n\tH1 files to compare are not different: [$H1mlc] [$H1mlc_2] [$H1mlc_3]\n" if ( "$H1mlc" eq "$H1mlc_2" || "$H1mlc_2" eq "$H1mlc_3" || "$H1mlc_3" eq "$H1mlc" );


    # GET best H0 AND best H1 mlc files
    # lnL_H0
        # grep -a to deal with mlc files that look like binary files and not text files, so force text usage.
    my @lnL_H0;
    ($lnL_H0[0]) = `grep -a 'lnL(ntime' $H0mlc`   =~ /\): +(\-?\d+\.\d+)/;
    ($lnL_H0[1]) = `grep -a 'lnL(ntime' $H0mlc_2` =~ /\): +(\-?\d+\.\d+)/;
    ($lnL_H0[2]) = `grep -a 'lnL(ntime' $H0mlc_3` =~ /\): +(\-?\d+\.\d+)/;
    my $lowestH0 = get_lowest_lnL(@lnL_H0);

    # lnL_H1
    my @lnL_H1;
    ($lnL_H1[0]) = `grep -a 'lnL(ntime' $H1mlc`   =~ /\): +(\-?\d+\.\d+)/;
    ($lnL_H1[1]) = `grep -a 'lnL(ntime' $H1mlc_2` =~ /\): +(\-?\d+\.\d+)/;
    ($lnL_H1[2]) = `grep -a 'lnL(ntime' $H1mlc_3` =~ /\): +(\-?\d+\.\d+)/;
    my $lowestH1 = get_lowest_lnL(@lnL_H1);

    print "[$lnL_H0[0]] [$lnL_H0[1]] [$lnL_H0[2]]\t\t[$lnL_H1[0]] [$lnL_H1[1]] [$lnL_H1[2]]\t\t\t$lowestH0 $lowestH1\n";


    # RE-BUILD folder hierarchy
    my ($original_folder) = $H0mlc =~ /^(.+)\/[^\/]+$/;
    #FIXME with symlink only ???
    my $subtree_folder = $H0mlc;
    $subtree_folder    =~ s{$other_folders[0]}{$final_folder};
    $subtree_folder    =~ s{/[^/]+$}{};
    system("mkdir -p $subtree_folder");
    link_files($subtree_folder, "$original_folder", '../../../..');

    my ($family_folder) = $subtree_folder =~ /^(.+)\/[^\/]+$/;
    link_files($family_folder, "$original_folder/..", '../../..');


    # COPY / LINK best mlc files
    if ( $lowestH0 != 0 ){ # The best mlc is not the 1st one
        link_best_mlc($lowestH0, $H0mlc, $subtree_folder, '../../../..');
        print "\t$H0mlc\n";
    }
    if ( $lowestH1 != 0 ){
        link_best_mlc($lowestH1, $H1mlc, $subtree_folder, '../../../..');
        print "\t$H1mlc\n";
    }
}

exit 0;


sub get_lowest_lnL {
    my (@lnl) = @_;

    return first_index { $_ == min(@lnl) } @lnl;
}

sub link_files {
    my ($dest_path, $source_path, $back) = @_;

    my @files = grep { !-d $_ } glob("$source_path/*");
    FILE:
    for my $file (@files){
        system("ln -fs $back/$file  $dest_path/");
    }

    return;
}

sub link_best_mlc {
    my ($indice, $mlc1, $dest_path, $back) = @_;

    my $best_mlc_path = $mlc1;
    $best_mlc_path    =~ s{^\w+}{$other_folders[$indice]};

    system("ln -fs $back/$best_mlc_path  $dest_path/");

    print "\t$best_mlc_path\n";
    return;
}

