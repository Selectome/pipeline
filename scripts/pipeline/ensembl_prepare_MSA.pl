#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Copy;
use File::Slurp;
use FindBin qw($Bin);
use lib "$Bin";

use Selectome::EnsemblDB;
use Selectome::Taxa;
use Selectome::Ensembl;
use Selectome::MSA;
use Selectome::Utils;

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;
use Bio::AlignIO;

our $VERSION           = 0.9.9;
my $min_leaf_number    = $Selectome::Utils::min_leaf_number;
my $min_species_number = $Selectome::Utils::min_species_number;
my $famid_length       = $Selectome::Utils::famid_length;

#########################################
# Get Trees and MSA from ensembl        #
# and prepare ctl files for codeml ONLY #
# Submission by another script          #
#########################################



############################## FILTERING STEPS ##############################
#
#   0. Family already processed
#   1. Discard families without "targeted taxa" sequences
#   2. Discard families with less than $min_leaf_number sequences
#          * Need internal branches AND enough statistical power !
#   3. Basal re-alignment with MAFFT
#   4. Filter by sequence length: MaxAlign
#          * Replace 'X' amino acids by gaps '-' in all sequences
#          * Remove poorly aligned sequences which should disrupt global alignment (MSA) e.g.
#   5. Re-do 2.
#   6. Discard families with less than $min_number_of_species
#
#   DONE later on by  msa_filtering.pl
#   6. Pagan alignment
#          * Should clean most non-orthologous exons
#   7. Compute M-Coffee scores: keep only "biologically meaningful" columns in the MSA
#          * Replace residues with low score (lower than 8, from 0 to 7; and keep from 8 to 9) by 'x'
#   8. Produce mask map !!!
#   9. Apply masking on NT alignment + mask stop codons (real or selenocysteines) not handled by codeml
#  10. Filter, with TrimAl, columns with  '$min_leaf_number'  OR  '3' or less residues
#
#############################################################################


$ENV{'PATH'} .= ':./';



############################## Options management ##############################
my ($node_id, $subtaxon, $db, $resultsdir, $filter, $debug) = (0, 0, 'ensembl', '.', 1, 0);
my %opts = ("node_id=i"    => \$node_id,        # Root node_id of a tree
            "taxon=s"      => \$subtaxon,       # NCBI tax_id for expected root tree
            "db=s"         => \$db,             # DB host
            "resultsdir=s" => \$resultsdir,     # Results directory
            'filter!'      => \$filter,         # Apply or not MSA filtering
            'debug'        => \$debug,          # Debug mode
           );


# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $node_id==0 || $subtaxon eq '0' ){
    Selectome::Utils::error("Invalid parameter(s)\n\n\te.g.: \e[1;37;42m$0 --taxon=vertebrates --node_id=870377\e[m
\t--node_id=...    Node_id of a tree (its root)
\t--taxon=...      NCBI tax_id or name for expected root tree
\t--db=...         DB host to get families: ensembl (default), genomes (ensembl genomes) or local
\t--resultsdir=... Results directory path (default=pwd)
\t--nofilter       Do not apply MSA filtering with MAFFT, MCoffee core, ...
\t--debug          Debug mode: print extra information");
}

Selectome::Utils::error('Invalid taxon')  if ( ! exists( $Selectome::Taxa::valid_taxa->{lc($subtaxon)} ) );
$subtaxon = $Selectome::Taxa::valid_taxa->{lc($subtaxon)};


my $connection_param = Selectome::EnsemblDB::get_connection_parameters($db);
if ( defined($ARGV[1]) ){
    $db = lc($ARGV[1]);
    $connection_param = Selectome::EnsemblDB::get_connection_parameters($db);
}



############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => $debug,
);


############################## Get the Adaptors ##############################
my $tree_adaptor       = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'GeneTree');
my $ncbi_taxon_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'NCBITaxon');

my $limit_node  = $ncbi_taxon_adaptor->fetch_node_by_taxon_id($subtaxon);
my $limit_left  = $limit_node->left_index;
my $limit_right = $limit_node->right_index;
$subtaxon       = $limit_node->get_short_name(); #TODO check new method returns what expected


my $tree    = $tree_adaptor->fetch_by_root_id($node_id);
my $id      = $tree->stable_id;
$tree       = $tree->root; # To use GeneTreeNode everywhere after that !!!!
my $subtree = 0; #No initial subtree(s)



############################## Check if already done ########################
### 0: Family already done
if ( -e "$resultsdir/$id/$subtaxon/DONE" || -e "$resultsdir/$id/$subtaxon/DONE.gz"){
    print "\n ** Job $id ($node_id) already done for $subtaxon\n";
    $tree->release_tree;
    $reg->disconnect_all();
    exit 0;
}


#############################################################################
############ START PROCESS ##################################################
#############################################################################

my $family_info = { 'id'       => $id,
                    'node_id'  => $node_id,
                    'subtaxon' => $subtaxon,
                    'debug'    => $debug,
                    'db'       => $db,
                    'registry' => $reg,
                    'multi'    => $connection_param->{'multi'},
                  };

############################## Clean and create local repositories ##########
print "** $id ($node_id) __ $subtaxon\n";
system('rm',    '-Rf', "$resultsdir/$id/$subtaxon/");
system('rm',    '-f',  "$resultsdir/$id.*");
system('rm',    '-f',  "$resultsdir/error/$id.*");
system('mkdir', '-p',  "$resultsdir/$id/$subtaxon");


############################## Check root node status #######################
my $tax_level = $tree->species_tree_node()->taxon_id();
my $tax_level_name =  $tree->species_tree_node()->node_name();
#print "$tax_level $tax_level_name\n";

if ( defined $tax_level && $tax_level ne '' ){
    my $deepest_node_tax = $ncbi_taxon_adaptor->fetch_node_by_taxon_id($tax_level);
    my $left_tax         = $deepest_node_tax->left_index;
    my $right_tax        = $deepest_node_tax->right_index;


    if (    $left_tax >= $limit_left && $right_tax <= $limit_right ){
        $subtree = sprintf("%0${famid_length}d", ++$subtree); # To increase $subtree before sprintf
        print "\@$subtree $tax_level $tax_level_name\n";

        my $leaf_nbr = scalar @{$tree->get_all_leaves};
        ### 2: Discard families with less than $min_leaf_number sequences
        if ( $leaf_nbr < $min_leaf_number ){
            Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
            finished($id);
            $tree->release_tree;
            $reg->disconnect_all();
            exit 0;
        }
#        Selectome::Utils::warnings($family_info, "$tax_level subtree instead of $subtaxon", $subtree, 0) if ( $tax_level ne $subtaxon );
        get_MSA($tree, $id, $subtree);
        store_files($id, $subtree, $resultsdir);
    }
    elsif ( $left_tax < $limit_left  && $right_tax >  $limit_right ){
        $subtree = split_tree($tree, $id, 0, $resultsdir);
        ### 1: Discard families without "targeted taxa" sequences
        Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1)  if ( $subtree==0 );
    }
    else {
        ### 1: Discard families without "targeted taxa" sequences
        Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1);
    }
}
else {
    Selectome::Utils::warnings($family_info, 'Taxonomic level is not defined!', '00', 1);
}

finished($id);
$tree->release_tree;
$reg->disconnect_all();
exit 0;




##########################################################################################
##########################################################################################

################################ Tree checks ################################

sub split_tree {
    my ($root, $id, $subtree, $resultsdir) = @_;

    # retrieve all children for this node
    my @children = @{$root->sorted_children()};

    SUBTREE:
    for my $tree ( @children ){
        my $tax_level      = $tree->species_tree_node()->taxon_id();
        my $tax_level_name = $tree->species_tree_node()->node_name();
        #print "SUB $tax_level $tax_level_name\n";
        if ( defined $tax_level && $tax_level ne '' ){
            my $deepest_node_tax = $ncbi_taxon_adaptor->fetch_node_by_taxon_id($tax_level);
            my $left_tax         = $deepest_node_tax->left_index;
            my $right_tax        = $deepest_node_tax->right_index;


            if (    $left_tax >= $limit_left && $right_tax <= $limit_right ){
                $subtree = sprintf("%0${famid_length}d", ++$subtree);
                print "\@$subtree $tax_level $tax_level_name\n";

                my $leaf_nbr = scalar @{$tree->get_all_leaves};
                ### 2: Discard families with less than $min_leaf_number sequences
                if ( $leaf_nbr < $min_leaf_number ){
                    Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
                    $tree->release_tree;
                    next SUBTREE;
                }
#                Selectome::Utils::warnings($family_info, "$tax_level subtree instead of $subtaxon", $subtree, 0) if ( $tax_level ne $subtaxon );

                get_MSA($tree, $id, $subtree);
                store_files($id, $subtree, $resultsdir);
            }
            elsif ( $left_tax < $limit_left  && $right_tax >  $limit_right ){
                $subtree = split_tree($tree, $id, $subtree, $resultsdir);
            }
            else {
#                Selectome::Utils::warnings($family_info, "No $subtaxon subtree", '00', 1);
            }
        }
        else {
#            Selectome::Utils::warnings($family_info, 'Taxonomic level is not  defined!', '00', 1);
        }
        $tree->release_tree;
    }
    return $subtree;
}



################################ Get (sub-)tree MSA ###########################

sub get_MSA {
    my ($tree, $id, $subtree) = @_;

    $subtree = sprintf("%0${famid_length}d", $subtree);


    print "\n\tGet basic prot and cds MSAs...\n"  if ( $debug );
    #Protein alignment
    my $prot_align  = $tree->get_AlignedMemberSet()->get_SimpleAlign();
    #Codon alignment
    my $cds_align   = $tree->get_AlignedMemberSet()->get_SimpleAlign(-SEQ_TYPE => 'cds');
    # Write alignments in fasta format
    # nt and aa.ORI.fas are original alignments before any filtering steps
    Selectome::Ensembl::write_MSA($cds_align,  "$id.$subtaxon.$subtree", 'nt.fasta', 'nt.ORI.fas');
    Selectome::Ensembl::write_MSA($prot_align, "$id.$subtaxon.$subtree", 'aa.fasta', 'aa.ORI.fas');


    # Get chromosome name
    print "\n\tGet chromosomal location...\n"  if ( $debug );
    my $chr = Selectome::Ensembl::get_chromosomal_localization($tree, $prot_align) || '';

    if ( $chr ne '' ){ # eq 'MT'
        write_file( "$id.$subtaxon.$subtree.CHR", $chr ) ;
        Selectome::Utils::warnings($family_info, 'Mitochondrial loci', $subtree, 0);
    }

    if ( $filter ) {
        print "\n\tBasic MSA re-alignment (MAFFT)...\n"  if ( $debug );
        ### 3: Basal re-alignment with MAFFT
        Selectome::MSA::align_by_MAFFT($family_info, "$id.$subtaxon.$subtree.aa.ORI.fas");


        ### 4: Filter by sequence length or with MaxAlign
        print "\n\t\tTree filtering (MaxAlign)...\n"  if ( $debug );
        ($tree, undef)      = Selectome::MSA::filter_by_maxAlign($family_info, "$id.$subtaxon.$subtree.aa.ORI.fas.mafft", $tree);
        #unlink "$id.$subtaxon.$subtree.aa.ORI.fas.mafft";


        my $leaf_nbr_after  = Selectome::Tree::get_leaf_number($tree);
        ### 5: Discard families with less than $min_leaf_number sequences
        if ( $leaf_nbr_after < $min_leaf_number ){
            Selectome::Utils::warnings($family_info, "Subtree with less than $min_leaf_number leaves", $subtree, 1);
            return($tree, 1);
        }

        ### 6: Discard families with less than $min_species_number species
        my $number_of_tree_species = Selectome::Ensembl::get_species_number($tree, $prot_align);
        if ( $number_of_tree_species < $min_species_number ){
            Selectome::Utils::warnings($family_info, "Subtree with less than $min_species_number species", $subtree, 1);
            return($tree, 1);
        }

        # Re-get clean AA & nt alignments after filtering by MaxAlign
        $prot_align = $tree->get_AlignedMemberSet()->get_SimpleAlign();
        $cds_align  = $tree->get_AlignedMemberSet()->get_SimpleAlign(-SEQ_TYPE => 'cds');
        # Write alignments in fasta format
        Selectome::MSA::write_MSA($prot_align, "$id.$subtaxon.$subtree", 'aa.fasta', 'aa.fas');
        Selectome::MSA::write_MSA($cds_align,  "$id.$subtaxon.$subtree", 'nt.fasta', 'nt.fas');
    }
    else { # No MSA filtering !
        copy("$id.$subtaxon.$subtree.nt.ORI.fas", "$id.$subtaxon.$subtree.nt.fas");
        copy("$id.$subtaxon.$subtree.aa.ORI.fas", "$id.$subtaxon.$subtree.aa.fas");
   }

    my $nhx = $tree->nhx_format('protein_id');
    write_file("$id.$subtaxon.$subtree.nhx", $nhx);


    return ($tree);
}


sub store_files {
    my ($id, $subtree, $resultsdir) = @_;
    my $prefix = "$id.$subtaxon.$subtree";

    $subtree = sprintf("%0${famid_length}d", $subtree);
    my @res_files = glob("$prefix.*");
    if ( exists($res_files[0]) && -e "$res_files[0]" ){
        system("mv $prefix.* $resultsdir/$id/$subtaxon/");
    }


    #Archive useless files
    #system("xz -9 $id/$subtaxon/*.ORI.fas");

    print "\n";
    return;
}

################################ Error and warning management #################

sub finished {
    my ($id) = @_;

    system('touch', "$resultsdir/$id/$subtaxon/DONE")  if ( defined($id) );
    return;
}

