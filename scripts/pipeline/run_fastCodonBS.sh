#!/bin/sh

#SBATCH --account mrobinso_selectome
#SBATCH --partition long
#SBATCH --mem 4G

# Load vital-it tools
module add Bioinformatics/Software/vital-it

## Load FastCodonBS
module add Phylogeny/FastCodonBS/20190702


PREFIX=$SLURM_JOB_NAME
XML=$PREFIX."fastCodonBS.xml"

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."fastCodonBS.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."fastCodonBS.cmd"

mpirun -np 1 $FASTCODONBS_PATH/bin/fastCodonBS -x $XML

echo -e "\nENDTIME:" >> $PREFIX."fastCodonBS.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."fastCodonBS.cmd"

touch $PREFIX."fastCodonBS_OKAY"


