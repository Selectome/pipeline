#!/usr/bin/env perl

# job_submitter.pl
# submits msa-filtering and godon jobs to cluster

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Basename;
use File::Slurp;

my ($tasks, $resultsdir, $cluster, $ac) = (0, '.', 'cpu', 'none');

my $project = 'mrobinso_selectome';
GetOptions ("tasks=s"   => \$tasks,      # List of tasks
            "dir=s"     => \$resultsdir, # Results directory
            "cluster=s" => \$cluster,    # e.g. cpu
            "ac=s"      => \$ac)         # optional ENSID (to run only one job)
or die ("Run script from selectome pipeline directory with path to input dir containing trees as parameter\n
         Usage: $0 --dir=<input dir> --tasks=\"<list of tasks e.g. msa-filtering godonBS godonBSG godonM8 fastCodonBS fastcodeml>\" --cluster=<cluster name> --ac=<ENSGTID>\n");

$resultsdir = $1  if ($resultsdir =~ /(.+)\/\z/);

my @tasks = split / /, $tasks;

my $bindir    = File::Basename::dirname($0);
my $count     = 0;
my $job_limit = 150;
my @jobs_to_do;

if ( $ac eq 'none' ){
#    @jobs_to_do = glob($resultsdir.'/*/*/*.nwk');
    @jobs_to_do = `find $resultsdir -name \*.nwk`;
}
else {
#    @jobs_to_do = glob($resultsdir.'/'.$ac.'/*/'.$ac.'*.nwk');
    @jobs_to_do = `find $resultsdir -name \*.nwk | grep '/$ac/'`;
}

JOB:
for my $job ( @jobs_to_do ){
    my ($prefix) = $job =~ /^(.+)\.nwk$/;
    my $id       = basename($prefix);
    my $dir      = dirname($prefix);
    my $running_jobs = check_running_jobs();
    while ( $running_jobs >= $job_limit ) {
        print "\t$running_jobs running on cluster. Waiting until below $job_limit jobs...\n";
        sleep 300;
        $running_jobs = check_running_jobs();
    }

    if ( grep $_ eq 'msa-filtering', @tasks ){
        next JOB if ( -e "$dir/OKAY__$id" || -e "$dir/OKAY_temp");  # been run already
        #next JOB if ( -e "$prefix.aa.mafft.newmafft.mcf" || -e "$dir/OKAY_temp");  # been run already
        print "Submitting msa-filtering Job: $prefix\n";

        my ($msa_time, $msa_mem) = choose_time($prefix, 'msa-filtering');

        my $msa_filtering_command = "sbatch --job-name \"$prefix\"  --mem $msa_mem --partition $cluster --time $msa_time --error $prefix.msa-filtering.std.err --output $prefix.msa-filtering.std.out  \"$bindir/run_msa_filtering.sh\"";
        write_file("$prefix.msa-filtering.cmd", {binmode => ':raw'}, $msa_filtering_command);
        system("$msa_filtering_command")==0 or warn "Failed to submit msa-filtering job [$id]\n";
        $count++;
    }
    else {
        #next JOB  unless (-e "$dir/OKAY__$id"); # check msa-filtering is done
        if (grep $_ eq "comap", @tasks) {
            next JOB if ( -e "$prefix.comap.statistics" || -e "$prefix.comap_SUBMITTED" ); # been run already or already submitted
            # create config bpp
            open(BPP, '>', "$prefix.comap.bpp") || die "Could not create file '$prefix.comap.bpp' $!";

            # read bpp template
            open (IN, "$bindir/template.comap.bpp") || die "Could not read $bindir/template.comap.bpp $!";
            while (my $line = <IN>) {
                $line=~ s/NAME_TREE/$prefix.nwk/;
                $line=~ s/NAME_FASTA/$prefix.nt.fas.REF/;
                $line=~ s/NAME_MAP_INFO/$prefix.comap.info/;
                $line=~ s/NAME_MAP_DND/$prefix.comap.dnd/;
                $line=~ s/NAME_MAP_TLN/$prefix.comap.tln/;
                $line=~ s/NAME_VECTOR_FILE/$prefix.comap.outputvector.txt/;
                $line=~ s/NAME_OUTPUT_STATS/$prefix.comap.statistics/;
                print BPP $line;
            }
            close IN;
            close BPP;

            print "Submitting comap Job: $prefix\n";
            # create lock file
            write_file("$prefix.comap_SUBMITTED", '');
            my $comap_command = "sbatch --job-name \"$prefix\" --partition $cluster --time 8-0 --error $prefix.comap.std.err --output $prefix.comap.std.out \"$bindir/run_comap.sh\"";

            write_file("$prefix.comap.cmd", {binmode => ':raw'}, $comap_command);
            system($comap_command)==0  or warn "Failed to submit comap job [$id]\n";
            $count++;
        }
        if (grep $_ eq "godonBSG", @tasks) {
            next  if ( -e "$prefix.godonBSG_OKAY" || -e "$prefix.godonBSG_SUBMITTED" || -e "$dir/OKAY_temp" ); # been run already or already submitted
            next  if (not (-e "$prefix.nwk"));
            my ($godon_time, $godon_mem) = choose_time($prefix, 'godonBSG');

            print "Submitting godonBSG Job: $prefix\n";
            # create lock file
            write_file("$prefix.godonBSG_SUBMITTED", '');

            my $godonBSG_command = "sbatch --job-name \"$prefix\" --mem $godon_mem --account $project --partition $cluster --time $godon_time --error $prefix.godonBSG.std.err --output $prefix.godonBSG.std.out \"$bindir/run_godonBSG.sh\"";

            write_file("$prefix.godonBSG.cmd", {binmode => ':raw'}, $godonBSG_command);
            system($godonBSG_command)==0 or warn "Failed to submit godonBS job [$id]\n";
            $count++;
        }
        if ( grep $_ eq 'godonM8', @tasks ){
            next  if ( -e "$prefix.godonM8_OKAY" || -e "$prefix.godonM8_SUBMITTED" || -e "$dir/OKAY_temp" ); # been run already or already submitted
            next  if ( !-e "$prefix.nwk" );
            my ($godon_time, $godon_mem) = choose_time($prefix, 'godonM8');

            print "Submitting godonM8 Job: $prefix\n";
            # create lock file
            write_file("$prefix.godonM8_SUBMITTED", '');

            my $godonM8_command = "sbatch --job-name \"$prefix\" --mem $godon_mem --account $project --partition $cluster --time $godon_time --error $prefix.godonM8.std.err --output $prefix.godonM8.std.out \"$bindir/run_godonM8.sh\"";

            write_file("$prefix.godonM8.cmd", {binmode => ':raw'}, $godonM8_command);
            system($godonM8_command)==0  or warn "Failed to submit godonBS job [$id]\n";
            $count++;
        }

        if (grep $_ eq "fastCodonBS", @tasks) {
            next  if ( -e "$prefix.fastCodonBS_OKAY");

            # create xml
            open(XML, '>', "$prefix.fastCodonBS.xml")  or die "Could not create file '$prefix.fastCodonBS.xml' $!";
            # read template xml
            open (IN, "$bindir/template.fastCodonBS.xml")  or die "Could not open file $bindir\/template.fastCodonBS.xml $!";
            while (my $line = <IN>) {
                $line=~ s/<filename><\/filename>/<filename>$prefix.fastCodonBS<\/filename>/;
                $line=~ s/<alignFile><\/alignFile>/<alignFile>$prefix.nt.fas.REF<\/alignFile>/;
                $line=~ s/<treeFile><\/treeFile>/<treeFile>$prefix.nwk<\/treeFile>/;
                print XML $line;
            }
            close IN;
            close XML;

            print "Submitting fastCodonBS Job: $prefix\n";

            my $fastCodonBS_command = "sbatch --job-name \"$prefix\" --error $prefix.fastCodonBS.std.err --output $prefix.fastCodonBS.std.out \"$bindir/run_fastCodonBS.sh\"";

            write_file("$prefix.fastCodonBS.cmd", {binmode => ':raw'}, $fastCodonBS_command);
            system($fastCodonBS_command)==0  or warn "Failed to submit fastCodonBS job [$id]\n";
            $count++;
        }

        if (grep $_ eq "fastcodeml", @tasks) {
            next  if ( -e "$prefix.fastcodeml_OKAY");

            print "Submitting fastcodeml Job: $prefix\n";

            my $fastcodeml_command = "sbatch --job-name \"$prefix\" --error $prefix.fastcodeml.std.err --output $prefix.fastcodeml.std.out \"$bindir/run_fastcodeml.sh\"";

            write_file("$prefix.fastcodeml.cmd", {binmode => ':raw'}, $fastcodeml_command);
            system($fastcodeml_command)==0  or warn "Failed to submit fastcodeml job [$id]\n";
            $count++;
        }
    }
}

print "\t$count jobs submitted\n";
exit 0;


sub check_running_jobs {

    my $user         = $ENV{USER};
    my $running_jobs = `squeue -u $user -t PENDING,RUNNING|wc -l` || 0;
    chomp($running_jobs);
    return $running_jobs;
}

sub choose_time {

    use Bio::Align::AlignI;
    use Bio::AlignIO;
    use Bio::TreeIO;

    my ($prefix, $task) = @_;
    my $time       = '2-0';
    my $mem        = '4GB';

    my $treeio     = new Bio::TreeIO(-file   => $prefix.'.nwk',
                                     -format => 'newick');
    my $tree       = $treeio->next_tree;

    my $alignin    = Bio::AlignIO->new(-format => 'fasta',
                                       -file   => $prefix.'.nt_masked.fas');
    my $aln        = $alignin->next_aln;
    my $tree_size  = scalar $tree->get_leaf_nodes;
    my $aln_length = $aln->length;

    if ($task eq 'msa-filtering') {
        if ($aln_length > 2500) {
            $time = '10-0';
            $mem  = '16GB';
        }
        if  ($tree_size > 700 || $aln_length > 8000) {
            $mem  = '48GB';
            $time = '10-0';
        }
    }
    elsif ( $task eq 'godonBS' || $task eq 'godonBSG' || $task eq 'godonM8' ){
        if ( $aln_length > 800*3 ){
            $time = '3-0';
        }
    }

    if ($task eq 'godonBSG' && -e $prefix.".godonBSG.std.err") {
        # been run already - check if it timed out and run on long queue if so
        if (read_file($prefix.'.godonBSG.std.err') =~ /DUE TO TIME LIMIT/) {
            $time = '10-0';
        }
    }

    return ($time, $mem);
}

