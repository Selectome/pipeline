#!/bin/sh

#SBATCH --account mrobinso_selectome
#SBATCH --mem 4G

# Load vital-it tools
module add Bioinformatics/Software/vital-it

## Load comap
module add Phylogeny/CoMap/1.5.5

PREFIX=$SLURM_JOB_NAME

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."comap.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."comap.cmd"

comap params=$PREFIX.comap.bpp

echo -e "\nENDTIME:" >> $PREFIX."comap.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."comap.cmd"

touch $PREFIX."comap_OKAY"

#remove lock file
if [ -f $PREFIX."comap_SUBMITTED" ]; then
    rm $PREFIX."comap_SUBMITTED"
fi

