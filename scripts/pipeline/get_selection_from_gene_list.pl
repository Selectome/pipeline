#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;
use File::Slurp;

my $gene_list = $ARGV[0]  or die "\n\t$0 <list_gene_mus.txt>\n\n";


if ( $gene_list =~ /_mus/ or $gene_list =~ /_ham/ ){
    print join("\t", '#gene_id', 'taxid', 'scientific_name', 'qvalue', 'lrt'), "\n";
}
else {
    print join("\t", '#gene_id', 'taxid', 'branch_length', 'scientific_name', 'qvalue', 'lrt', 'omega0', 'omega2'), "\n";
}

GENE_LIST:
for my $gene_id ( read_file("$gene_list", chomp =>1) ){
    #list_gene_mus.txt
    #mysql -u root -p  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, t.scientific_name, s.qvalue, s.lrt FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='ENSMUSG00000031181' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (10088, 39107, 10066) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch"
    #with 10088 (Mus genus), 39107 (Murinae subfamily), 10066 (Muridae family)
    #
    #list_gene_ham.txt
    #mysql -u root -p  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, t.scientific_name, s.qvalue, s.lrt FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='ENSMAUG00000000006' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (10026, 337677) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch"
    #with 10026 (Cricetinae subfamily), 337677 (Cricetidae family)

    if ( $gene_list =~ /_mus/ ){
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, t.scientific_name, s.qvalue, s.lrt FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (10088, 39107, 10066) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list =~ /_ham/ ){
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, t.scientific_name, s.qvalue, s.lrt FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (10026, 337677) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list =~ /pike_/ ){
        #with 7898 (Actinopterygii superclass), 41665 (Neopterygii subclass), 1489341 (Osteoglossocephalai clade), 186625 (Clupeocephala ...), 1489388 (Euteleosteomorpha cohort), 41705 (Protacanthopterygii clade)
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (41705, 1489388, 186625, 1489341, 41665, 7898) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list =~ /fish_gene/ ){
        #with 117571 Euteleostomi, 7898 Actinopterygii, 41665 Neopterygii, 1489341 Osteoglossocephalai, 186625 Clupeocephala, 1489388 Euteleosteomorpha, 186634 Otomorpha, 186626 Otophysi, 186628 Characiphysae, 123368 Acanthomorphata, 1489872 Percomorphaceae, 41705 Protacanthopterygii, 1489908 Ovalentaria, 1489913 Atherinomorphae
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (1489913, 1489908, 41705, 1489872, 123368, 186628, 186626, 186634, 1489388, 186625, 1489341, 41665, 7898, 117571) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list =~ /one2one\.txt/ ){
        #with 117571 Euteleostomi, 8287 Sarcopterygii, 1338369 Dipnotetrapodomorpha, 32523 Tetrapoda, 32524 Amniota, 40674 Mammalia, 32525 Theria, 9347 Eutheria, 1437010 Boreoeutheria, 314145 Laurasiatheria, 33554 Carnivora, 314146 Euarchontoglires
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (314146, 33554, 314145, 1437010, 9347, 32525, 40674, 32524, 32523, 1338369, 8287, 117571) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list =~ /one2one_4_species\.tsv/ ){
        next GENE_LIST  if ( $gene_id !~ /ENS([A-Z]{3})?G\d{11}/ );
        #Gene_name	Homo_sapiens	Cavia_porcellus	Heterocephalus_glaber	Papio_anubis
        #MT-ND1	ENSG00000198888	ENSCPOG00000017403	ENSHGLG00000000006	ENSPANG00000031403
        my (undef, $hs, $cp, $hg, $pa) = split(/\t/, $gene_id);
        for my $gene_id ( $hs, $cp, $hg, $pa ){
            #with 32524 Amniota, 10167 Bathyergidae, 1437010 Boreoeutheria, 9526 Catarrhini, 10140 Cavia, 9527 Cercopithecidae, 9528 Cercopithecinae, 314146 Euarchontoglires, 117571 Euteleostomi, 9347 Eutheria, 314147 Glires, 376913 Haplorrhini, 9604 Hominidae, 207598 Homininae, 314295 Hominoidea, 33550 Hystricomorpha, 40674 Mammalia, 9443 Primates, 9989 Rodentia, 8287 Sarcopterygii, 314293 Simiiformes, 32523 Tetrapoda, 32525 Theria
            print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (32524, 10167, 1437010, 9526, 10140, 9527, 9528, 314146, 117571, 9347, 314147, 376913, 9604, 207598, 314295, 33550, 40674, 9443, 9989, 8287, 314293, 32523, 32525) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
        }
    }
    elsif ( $gene_list =~ /Danio_rerio_full_bgd\.txt/ || $gene_list eq 'DRE_protein_coding_Ensembl_105.txt' ){
        next GENE_LIST  if ( $gene_id !~ /ENSDARG00\d{9}/ );
        #with 7742 Vertebrata, 7776 Gnathostomata, 117570 Teleostomi, 117571 Euteleostomi, 7898 Actinopterygii, 186623 Actinopteri, 41665 Neopterygii, 32443 Teleostei, 1489341 Osteoglossocephalai, 186625 Clupeocephala, 186634 Otomorpha, 32519 Ostariophysi, 186626 Otophysi, 186627 Cypriniphysae, 7952 Cypriniformes, 30727 Cyprinoidei, 2743709 Danionidae, 2743711 Danioninae, 7954 Danio
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (7742, 7776, 117570, 117571, 7898, 186623, 41665, 32443, 1489341, 186625, 186634, 32519, 186626, 186627, 7952, 30727, 2743709, 2743711, 7954) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list eq 'ELU_protein_coding_Ensembl_105.txt' ){
        next GENE_LIST  if ( $gene_id !~ /ENSELUG00\d{9}/ );
        #with 7742 Vertebrata, 7776 Gnathostomata, 117570 Teleostomi, 117571 Euteleostomi, 7898 Actinopterygii, 186623 Actinopteri, 41665 Neopterygii, 32443 Teleostei, 1489341 Osteoglossocephalai, 186625 Clupeocephala, 1489388 Euteleosteomorpha, 41705 Protacanthopterygii, 8007 Esociformes, 8008 Esocidae, 8009 Esox
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (7742, 7776, 117570, 117571, 7898, 186623, 41665, 32443, 1489341, 186625, 1489388, 41705, 8007, 8008, 8009) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
    elsif ( $gene_list eq 'LOC_protein_coding_Ensembl_105.txt' ){
        next GENE_LIST  if ( $gene_id !~ /ENSLOCG00\d{9}/ );
        #with 7742 Vertebrata, 7776 Gnathostomata, 117570 Teleostomi, 117571 Euteleostomi, 7898 Actinopterygii, 186623 Actinopteri, 41665 Neopterygii, 1489100 Holostei, 7914 Semionotiformes, 7915 Lepisosteidae, 7916 Lepisosteus
        print `mysql -u root -pBdD.pw  selectome_v07_euteleostomi98  -e "SELECT g.gene_id, b.taxid, b.length, t.scientific_name, s.qvalue, s.lrt, s.omega0, s.omega2 FROM gene g, branch b, taxonomy t, selection s, node2gene n WHERE g.gene_id='$gene_id' AND b.id=g.id AND b.id=s.id AND b.taxid=t.taxid AND t.taxid IN (7742, 7776, 117570, 117571, 7898, 186623, 41665, 1489100, 7914, 7915, 7916) AND s.branch=b.branch AND n.prot_id=g.prot_id AND n.branch=b.branch" 2>/dev/null | grep -v "^gene_id"`;
    }
}

exit 0;

