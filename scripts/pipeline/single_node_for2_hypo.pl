#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Basename;
use Cwd;


my $codeml_path = '/software/Phylogeny/paml/4.8/bin/codeml';
my @tmp_files   = ('rub', 'rst', 'rst1', '4fold.nuc', 'lnf', '2NG.t', '2NG.dS', '2NG.dN');



my $infile0 = $ARGV[0] // ''; # Safer than || for files called 0 or undef
my $infile1 = $ARGV[1] // '';

if ( $infile0 eq '' || $infile1 eq '' ){
    die "\n\tMissing ctl file(s)\n\t$0 ctl_hypo0_file ctl_hypo1_file\n\n";
}
die "Invalid $infile0 file\n"  if ( ! -e "$infile0" || -z "$infile0" );
die "Invalid $infile1 file\n"  if ( ! -e "$infile1" || -z "$infile1" );

unlink('std.err', 'std.out');


my $basename0 = basename($infile0);
my $basename1 = basename($infile1);
my $cwd       = getcwd();

chdir dirname($infile0);
unlink @tmp_files;
unlink glob('*.std.err'), glob('*.std.out');

#CTL file 1: null hypothesis
system($codeml_path, $basename0)==0  or die "Cannot run 'codeml_H0': $?";
unlink @tmp_files;


#CTL file 2: positive selection hypothesis
system($codeml_path, $basename1)==0  or die "Cannot run 'codeml_H1': $?";
unlink @tmp_files;

#TODO read input files from /data/ul/dee/selectome/...
# and write result files to /scratch/local/SANDBOX/ !!!!
# Then scp back mlc files to /data/ul/dee/selectome/...
#
# mkdir /scratch/local/weekly/__JOB_ID__
# cp /data/ul/dee/selectome/... /scratch/local/weekly/__JOB_ID__/
# cd /scratch/local/weekly/__JOB_ID__/
# codeml
# scp *.mlc /data/ul/dee/selectome/...
# cleaning


# Cleaning
chdir $cwd;

exit 0;

