#!/bin/sh

#SBATCH --account mrobinso_selectome
#SBATCH --partition long
#SBATCH --mem 4G

# Load vital-it tools
module add Bioinformatics/Software/vital-it

## Load FastCodonBS
module add Phylogeny/FastCodeML/1.3.0

PREFIX=$SLURM_JOB_NAME

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."fastcodeml.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."fastcodeml.cmd"

fast -m 22 -nt 1 $PREFIX."clean.nwk"  $PREFIX."phy"

echo -e "\nENDTIME:" >> $PREFIX."fastcodeml.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."fastcodeml.cmd"

touch $PREFIX."fastcodeml_OKAY"


