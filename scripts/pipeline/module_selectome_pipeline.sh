# Load vital-it tools
module add Bioinformatics/Software/vital-it
#Load MaxAlign
module add SequenceAnalysis/Filtering/MaxAlign/1.1 # Previous pipeline part!
# Load MCoffee
module add SequenceAnalysis/MultipleSequenceAlignment/T-Coffee/11.00.8cbe486
#NOTE MCoffee aligners are directly loaded while loading TCoffee itself!
module add SequenceAnalysis/MultipleSequenceAlignment/mafft/7.310
#module add SequenceAnalysis/MultipleSequenceAlignment/muscle/3.8.1551
#module add SequenceAnalysis/MultipleSequenceAlignment/clustal-omega/1.2.4
#module add SequenceAnalysis/MultipleSequenceAlignment/kalign/2.04

# Load Guidance
#module add SequenceAnalysis/Filtering/guidance/2.02
#NOTE Guidance aligner is directly loaded while loading Guidance itself!
#module add SequenceAnalysis/MultipleSequenceAlignment/mafft/7.310 # > 7.124 for new MAFFT

# Load TrimAl
module add SequenceAnalysis/Filtering/trimAl/1.4.1

## Load codeml
#module add Phylogeny/paml/4.9g

## Load slimcodeml
#module add Phylogeny/Slimcodeml/2014_02_11

## Load godon
module add Phylogeny/godon/20200613

## Load FastCodeML
#module add Phylogeny/FastCodeML/1.1.0

## Load newick-utils
#module add Phylogeny/newick-utils/1.6

## Load FastCodonBS
#module add Phylogeny/FastCodonBS/20190702


