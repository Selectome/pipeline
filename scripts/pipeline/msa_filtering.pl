#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Copy;
use File::Basename;
use File::Slurp;
use FindBin qw($Bin);
use lib "$Bin";

use Selectome::MSA;
use Selectome::Tree;
use Selectome::Utils;

use Bio::AlignIO;


our $VERSION        = 0.0.3;
my $min_leaf_number = $Selectome::Utils::min_leaf_number;
my $famid_length    = $Selectome::Utils::famid_length;

#########################################
# Filter MSAs                           #
# and prepare ctl files for codeml ONLY #
# Submission by another script          #
#########################################



############################## FILTERING STEPS ##############################
#
#   DONE before on by  prepare_MSA.pl       => tree filtering
#   0. Family already processed
#   1. Discard families without "targeted taxa" sequences
#   2. Discard families with less than $min_leaf_number sequences
#          * Need internal branches AND enough statistical power !
#   3. Basal re-alignment with MAFFT
#   4. Filter by sequence length: MaxAlign
#          * Replace 'X' amino acids by gaps '-' in all sequences
#          * Remove poorly aligned sequences which should disrupt global alignment (MSA) e.g.
#   5. Re-do 2.



#                                           => MSA filtering
#   6. New MAFFT alignment
#          * Should clean most non-orthologous exons
#   7. Compute M-Coffee scores: keep only "biologically meaningful" columns in the MSA
#          * Replace residues with low score (lower than 5, from 0 to 5; and keep from 6 to 9) by 'x'
#   8. Apply masking on NT alignment + mask stop codons (real or selenocysteines) not handled by codeml
#   9. Filter, with TrimAl, columns with  '$min_leaf_number'  OR  '3' or less residues
#
#############################################################################


$ENV{'PATH'} .= ':./';


############################## Options management ##############################
my ($aamsa_file, , $ntmsa_file, $tree_file) = ('', '', '');
my ($prefix)                                = ('');
my ($label_leaves, $debug)                  = (0, 0);
my %opts = ('aamsa=s'      => \$aamsa_file,     # Input MSA  AA file
            'ntmsa=s'      => \$ntmsa_file,     # Input MSA  NT file
            'tree=s'       => \$tree_file,      # Input Tree    file
            'prefix=s'     => \$prefix,         # Prefix of input files
            'label_leaves' => \$label_leaves,   # Label leaves to run codeml on leaves also
            'debug'        => \$debug,          # Debug
           );


# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || ($prefix eq '' && ($aamsa_file eq '' || $ntmsa_file eq '' || $tree_file eq '')) ){
    Selectome::Utils::error("Invalid parameter(s)\n\n\te.g.: \e[1;37;42m$0  --aamsa=AA_MSA_file  --ntmsa=NT_MSA_File  --tree=Tree_file  [options] ||  --prefix=prefix  [options]\e[m
\t--aamsa=...    Input MSA AA file
\t--ntmsa=...    Input MSA NT file
\t--tree=...     Input Tree   file (NHX format)
\t--prefix=...   Prefix of input files
\t--label_leaves Label leaves to run codeml on leaves also
\t--debug        Debugging mode");
}


# Test input files
my ($id, $subtaxon, $subtree, $resultsdir) = ('', '', '', '');
my $chr = '';
if ( $prefix ne '' ){
    $aamsa_file = $prefix.'.aa.fas';
    $ntmsa_file = $prefix.'.nt.fas';
    $tree_file  = $prefix.'.nhx';
    $resultsdir = dirname($prefix);

    ($id, $subtaxon, $subtree) = split(/\./, basename($prefix));

    if ( -e "$prefix.CHR" && -s "$prefix.CHR" ){
        $chr = read_file( "$prefix.CHR", chomp => 1 );
    }
}

if ( !-e "$aamsa_file" || -z "$aamsa_file" || !-e "$ntmsa_file" || -z "$ntmsa_file" || !-e "$tree_file" || -z "$tree_file" ){
    Selectome::Utils::error('At least one of the imput files is missing, or empty');
}

my $family_info = { 'id'       => $id,
                    'subtaxon' => $subtaxon,
                    'subtree'  => $subtree,
                    'debug'    => $debug,
                  };
mkdir 'error';


############################## MSA filtering ##############################
print "\tMSA filtering...\n\n";
# Work in the local folder, not somewhere else where paths can break something !
if ( "$aamsa_file" ne basename("$aamsa_file") ){
    copy("$aamsa_file", basename("$aamsa_file"));
    $aamsa_file = basename("$aamsa_file");
}
if ( "$ntmsa_file" ne basename("$ntmsa_file") ){
    copy("$ntmsa_file", basename("$ntmsa_file"));
    $ntmsa_file = basename("$ntmsa_file");
}
if ( "$tree_file"  ne basename("$tree_file") ){
    copy("$tree_file",  basename("$tree_file"));
    $tree_file  = basename("$tree_file");
}
$prefix = basename("$prefix");
#FIXME Problem with the previous split /\./ if path contains dots !!!
$family_info->{'id'} = basename($family_info->{'id'});


### 6: Best alignment with skipped non-homologous exons (new MAFFT)
print "\n\t\tNew MAFFT alignment building...\n"  if ( $debug );
Selectome::MSA::align_by_newMAFFT($family_info, "$aamsa_file");
copy("$aamsa_file.newmafft", "$aamsa_file.ORI");
#NOTE Now the alignment to use IS the one out of NEW MAFFT !!!!


### 7: Compute M-Coffee scores
print "\n\t\tMCoffee scoring...\n"  if ( $debug );
Selectome::MSA::score_by_MCoffee($family_info,  "$aamsa_file.newmafft");


### 8: Compute Guidance scores
#print "\n\t\tGuidance scoring...\n"  if ( $debug );
#Selectome::MSA::score_by_Guidance($family_info, "$aamsa_file.newmafft");


### 9: Merge scores + Produce mask map (for Jalview later !)
#print "\n\t\tmerging scores...\n"  if ( $debug );
#Selectome::MSA::merge_scores("$aamsa_file.newmafft.mcf", "$aamsa_file.newmafft.Guidance");


#FIXME Need to Do something when there are mostly Xs !!!
print "\n\t\tCheck overall masking... If too masked, no need to run codeml !\n"  if ( $debug );
my $status = Selectome::MSA::check_masking($family_info, "$aamsa_file.newmafft.mcf");
if ( $status ){
    Selectome::Utils::warnings($family_info, 'Nothing useful, pre-filtering steps have masked almost everything', $subtree, 1);
    system("xz -9 $prefix.nhx"); # To prevent using it later
    storage_final();
    exit 0;
}


### 8: Map mask on NT alignment + Mask selenocysteine coded by "stop codons"
print "\n\t\tNT alignment masking...\n"  if ( $debug );
Selectome::MSA::backTranslate_mask("$ntmsa_file", "$aamsa_file.ORI", $chr); # For unmasked (original) MSA
system("mv $ntmsa_file.preTrimAl  $ntmsa_file.ORI");
Selectome::MSA::backTranslate_mask("$ntmsa_file", "$aamsa_file.newmafft.mcf", $chr); # For masked MSA

### 9: Filter, with TrimAl, columns with 3 or less residues
print "\n\t\tTrimAl column filtering (less than 4 residues)...\n"  if ( $debug ); # 4 residues is the minimum to get an internal branch
Selectome::MSA::filter_by_trimal($family_info, "$ntmsa_file.preTrimAl");
copy("$ntmsa_file.trimal", "$ntmsa_file.REF");



############################## File conversion ##############################
#Convert to Phylip format
$prefix = $ntmsa_file  if ( $prefix eq '' );
Selectome::MSA::convert_fasta_to_phylip("$ntmsa_file.REF", "$prefix.phy");

#FIXME Need to do something when sequences are 100% identical
# No need to run codeml on them !


############################## Tree topology is NOW fixed, no more filtering afterward ####
print "\tTree parsing...\n\n";
$subtree = sprintf("%0${famid_length}d", $subtree);

# Read tree
my $tree = read_file( "$tree_file", chomp => 1 );
copy("$tree_file", "$tree_file.ORI");


# If this is an NHX tree:
my ($nhx, $nwk) = ('', '');
if ( $tree =~ /\[&&NHX:/ ){
    $nwk  = $tree;

    $tree =~ s{\[&&NHX:.*?\]([,\);])}{$1}g; # Remove NHX tags
    $tree =~ s{\).*?:}{\):}g;               # Remove bootstrap-like tags if any
}
$nwk = $tree;

# Write Newick for Godon
$tree_file =~ s{\.nhx$}{\.nwk};
write_file("$tree_file",$nwk);
$tree_file =~ s{\.nwk$}{\.nhx};

# Protect seq names longer than 11 char in MSA & input tree !!!
# They are now less than 11 char long and without special char
$nwk = Selectome::MSA::protect_phylip_msa($nwk, "$prefix");


## Unroot tree with nw_reroot -d from newick-utils package
#$nwk = `echo \"$nwk\" | nw_reroot -d -`;
#chomp $nwk;
#die "Problem during Newick unrooting of [$prefix]\n"  if ( !defined $nwk || $nwk eq '' );


# Label Newick tree + run M1 model
#print "\tM1 model + tree labeling...\n\n";
#my $kappa = 2;
#($nwk, $kappa) = Selectome::Tree::label_newick_branches($nwk, $family_info, $label_leaves, $subtree, $chr);


#NOTE Useless for fastcodeml!
#Place #1 at every node but the branches near the pseudo-root
#Selectome::Codeml::move_sharp1_in_tree($nwk, $family_info, $subtree, $kappa, $chr);


# Mapping new Newick tree (from M1 model) + Label NHX tree
#    => M1 branch lengths + node labels (not seqname protected)
if ( $nhx ne '' ){
    #FIXME Problem between rooted (NHX) and unrooted (Newick) trees mapping
    #NOTE fastcodeml unroots tree now! (codeml does NOT)
    $nhx = Selectome::Tree::map_nwk_to_nhx($nhx, $nwk);

    warn "\t\tNHX with ==== for '$tree_file'\n"  if ( $nhx =~ /====/ );#NOTE famous unexplained bug!

    # Write final clean NHX file !
    #   Only the NHX file is required because easy to convert it to newick format
    write_file("$tree_file", $nhx);

    # Write final clean Newick for FastCodeml
    my $clean_nwk = $nwk;
    $clean_nwk =~ s{\[&&NHX:.*?\]([,\);])}{$1}g; # Remove NHX tags
    $clean_nwk =~ s{===\d+=}{}g;                 # Remove branch labels
    $tree_file =~ s{\.nhx$}{\.clean.nwk};
    write_file("$tree_file", $clean_nwk);
}


# Store/xz unused files
storage_final();

exit 0;


END {
    unlink qw(rub rst rst1 4fold.nuc lnf 2NG.t 2NG.dS 2NG.dN);
    system('rmdir error/ 2>/dev/null'); # Clean if empty
}

sub storage_final {
    # Store/xz unused files
    my @res_files = glob("$prefix.*");
    if ( exists($res_files[0]) && -e "$res_files[0]" ){
        system("mv $prefix.* $resultsdir/");
    }

    # Create sandbox to limit job concurrency for the same output file names
    #   Problems with codeml (PAML in general) which creates several result files
    #   with the same name. E.g. rst, rst1, rub, ...
    # Both hypotheses per tree node ARE in the same directory to make things easier for bsub sheduler and to create less files
    my @ctl_files = glob("$resultsdir/$prefix.*.H0.ctl");
    CTL:
    for my $ctl_file (@ctl_files){
        my ($job_tmp_name, $node_name) = $ctl_file =~ /^.*\/.*\.(\d+\.(\d+))\.H0\.ctl$/;
        mkdir "$resultsdir/$job_tmp_name/";
        system('mv', "$ctl_file", "$resultsdir/$prefix.$node_name.H1.ctl", "$resultsdir/$prefix.$node_name.nwk", "$resultsdir/$job_tmp_name/");
        chdir "$resultsdir/$job_tmp_name/";
        system('ln', '-fs', "../$prefix.phy", "$prefix.phy");
        chdir "$Bin";
    }


    # Archive useless files
    my @files_to_store = map { $prefix.'.*.'.$_ } qw(fas score log stat ORI trimal preTrimAl mcf Guidance ScoreMerged newmafft);
    # Next files don't have this structure $prefix.*.
    push @files_to_store, "$prefix.CTL", "$prefix.MLC", "$prefix.map";
    chdir "$resultsdir";
    system("xz -f -9  @files_to_store  2>/dev/null");
    chdir '$Bin';

    system("touch $resultsdir/OKAY__$prefix");
    return;
}

