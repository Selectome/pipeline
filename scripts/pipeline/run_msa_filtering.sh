#!/bin/bash

#SBATCH --account mrobinso_selectome

# Load vital-it tools
module add Bioinformatics/Software/vital-it
#Load MaxAlign
module add SequenceAnalysis/Filtering/MaxAlign/1.1
# Load MCoffee
module add SequenceAnalysis/MultipleSequenceAlignment/T-Coffee/11.00.8cbe486
# Load Mafft
module add SequenceAnalysis/MultipleSequenceAlignment/mafft/7.310 # > 7.124 for new MAFFT
# Load TrimAl
module add SequenceAnalysis/Filtering/trimAl/1.4.1

PREFIX=$SLURM_JOB_NAME;

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\nSTARTTIME:" >> $PREFIX."msa-filtering.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."msa-filtering.cmd"

./msa_filtering.pl --prefix=$PREFIX

echo -e "\nENDTIME:" >> $PREFIX."msa-filtering.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."msa-filtering.cmd"

