#!/bin/sh

#SBATCH --account mrobinso_selectome

## Load Godon
module load godon/20200906

PREFIX=$SLURM_JOB_NAME

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."godonM8.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonM8.cmd"

godon test M8  --json=$PREFIX.godonM8.json --m0-tree --no-neb --ncat-codon-rate 4 $PREFIX."nt_masked.fas" $PREFIX."nwk"

echo -e "\nENDTIME:" >> $PREFIX."godonM8.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonM8.cmd"

touch $PREFIX."godonM8_OKAY"

#remove lock file
if [ -f $PREFIX."godonM8_SUBMITTED" ]; then
    rm -f $PREFIX."godonM8_SUBMITTED"
fi

