#!/bin/sh

#SBATCH --account mrobinso_selectome

# Load vital-it tools
module add Bioinformatics/Software/vital-it

## Load Godon
module add Phylogeny/godon/20200613

PREFIX=$SLURM_JOB_NAME

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."godonBS.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonBS.cmd"

godon test BS  --json=$PREFIX.godonBS.json --no-neb --m0-tree $PREFIX."nt.fas.REF" $PREFIX."nwk"
echo -e "\nENDTIME:" >> $PREFIX."godonBS.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonBS.cmd"

touch $PREFIX."godonBS_OKAY"


