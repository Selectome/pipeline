#!/bin/sh

#SBATCH --account mrobinso_selectome

# Load vital-it tools
module add Bioinformatics/Software/vital-it

## Load Godon
module add Phylogeny/godon/20200613

PREFIX=$SLURM_JOB_NAME

echo -e "\n\nJOBID:\n$SLURM_JOB_ID\n\nSTARTTIME:" >> $PREFIX."godonBSG.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonBSG.cmd"

godon test BSG  --json=$PREFIX.godonBSG.json --m0-tree --no-neb --all-branches  --no-leaves --ncat-codon-rate 4 $PREFIX."nt.fas.REF" $PREFIX."nwk"

echo -e "\nENDTIME:" >> $PREFIX."godonBSG.cmd"
date "+%H:%M:%S %d-%m-%Y" >> $PREFIX."godonBSG.cmd"

touch $PREFIX."godonBSG_OKAY"

#remove lock file
if [ -f $PREFIX."godonBSG_SUBMITTED" ]; then
    rm $PREFIX."godonBSG_SUBMITTED"
fi

