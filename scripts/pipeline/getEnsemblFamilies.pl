#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use FindBin qw($Bin);

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;

use lib "$Bin";
use Selectome::EnsemblDB;
use Selectome::Taxa;


our $VERSION = 0.9.9;

#########################################
# Get Trees and MSA from ensembl        #
# and prepare ctl files for codeml ONLY #
# Submission by another script          #
#########################################


my ($subtaxon, $target_db, $id, $ac, $fam_nbr, $range, $resultsdir) = (0, 'ensembl', 0, '', 100_000_000_000,'0','.');
my %opts = ('taxon=s'      => \$subtaxon,       # NCBI tax_id for expected root tree
            'db=s'         => \$target_db,      # DB host
            'id=i'         => \$id,             # Ensembl Gene Tree id to search/limit for
            'ac=s'         => \$ac,             # Ensembl Gene Tree ac to search/limit for
            'range=s'      => \$range,          # Range of families to process
            'resultsdir=s' => \$resultsdir      # Results Directory
    );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $subtaxon eq '0' ){
    error("Taxon not found or invalid parameters\n\n\te.g.: \e[1;37;42m$0 --taxon=vertebrates\e[m
\t      \e[1;37;42m$0 --taxon=vertebrates    --db=local_e\e[m
\t      \e[1;37;42m$0 --taxon=arthropoda     --db=metazoa\e[m
\t      \e[1;37;42m$0 --taxon=arthropoda     --db=local_m\e[m
\t      \e[1;37;42m$0 --taxon=Drosophila     --db=metazoa\e[m
\t      \e[1;37;42m$0 --taxon=amniota        --db=annot_e\e[m
\t      \e[1;37;42m$0 --taxon=hexapoda       --db=annot_m\e[m
\t      \e[1;37;42m$0 --taxon=primates\e[m
\t      \e[1;37;42m$0 --taxon=Glires\e[m
\t      \e[1;37;42m$0 --taxon=Sauropsida\e[m
\t      \e[1;37;42m$0 --taxon=Clupeocephala\e[m
\t--taxon=...      NCBI tax_id or name for expected root tree
\t--db=...         DB host to get families: ensembl (default), genomes (ensembl genomes), local or annotbioinfo computer
\t--id=...         Ensembl Gene Tree id to search/limit for
\t--ac=...         Ensembl Gene Tree ac to search/limit for
\t--resultsdir=... Directory for results (default is pwd)
\t--range=...      Range of families to process (e.g. 1:100 default: All)\n");
}

error('Invalid taxon')  if ( ! exists( $Selectome::Taxa::valid_taxa->{lc($subtaxon)} ) );
$subtaxon = $Selectome::Taxa::valid_taxa->{lc($subtaxon)};

my $connection_param = Selectome::EnsemblDB::get_connection_parameters($target_db);


############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => 1,  # for debug purpose !
);

############################## Get the Adaptors ##############################
my $tree_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'GeneTree');


############################## fetch all roots ###############################
# In a release database, there is a basal root that holds together all the trees.
my @all_children = @{$tree_adaptor->fetch_all(-tree_type     => 'tree',
                                              -member_type   => 'protein',
                                              -clusterset_id => 'default',
                                             )}; # Protein tree only !

if ( $id > 0 ){
    @all_children = grep { $_->root_id eq $id } @all_children;
}
elsif ( $ac ne '' && $ac =~ /^ENSGT/ ){
    @all_children = grep { $_->stable_id eq $ac } @all_children;
}

# Sort to ensure same order
@all_children = sort {$a->stable_id cmp $b->stable_id} @all_children;

# Get families in range
my ($min_range, @children) = get_subset_of_children(\@all_children, $range);

# Count families
my $total_children_count = scalar @all_children;
my $children_count       = scalar @children;

system('rm', '-f', '$resultsdir/error/*');
mkdir '$resultsdir/error';

my $famCount = 0;
my $famNbr   = $min_range;
TREE:
for my $tree ( @children ){
    my $node_id = $tree->root->node_id;

    $famCount++;

    print "\n== Job $famNbr of $total_children_count\n";

    system("/usr/bin/time -p   $Bin/ensembl_prepare_MSA.pl --node_id=$node_id --taxon=$subtaxon --db=$target_db --resultsdir=$resultsdir");

    $famNbr++;

    $tree->root->release_tree;
}

$reg->clear();
system('rmdir error/ 2>/dev/null'); # Remove if empty

print "\tprocessed [$famCount] potential families out of [$children_count]. Total Families [$total_children_count].\n";

exit 0;


#####################################################################################
sub get_subset_of_children {
    my ($children, $range) = @_;
    my @sub_children=@$children;
    my $min_range = 1;
    my $max_range = 100_000_000;


    if ( !($range eq '0') ){
        ($min_range, $max_range) = split /\:/, $range;
        if ( $max_range < $min_range ){
            print "\n\t Range format incorrect.  Usage example  --range 1:1000\n";
            exit 0;
        }
        else {
            @sub_children=@sub_children[$min_range-1..$max_range-1];
        }
    }
    return ($min_range, @sub_children);
}

sub error {
    my ($msg, $id) = @_;

    print "\n\t$msg\n\n";
    exit 1;
}

