#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use Data::Dumper;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;

use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome id]\n\n";
my $AC     = $ARGV[1]  || '';
# db connect
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $outdir = '/var/SELECTOME';
my ($db1, $db2) = ('', '');
if ( $dbname =~ /timema/ ){
    $db1 = 'timema';
    $db2 = $db1;
}
elsif ( $dbname =~ /euteleostomi/ ){
     $db1 = 'euteleostomi';
     $db2 = 'Euteleostomi';
}


# display a full family to see if reinsert the same!
# ./update_node2gene_from_NHX_files.pl selectome_v07_euteleostomi98 ENSGT00940000157298.Euteleostomi.002 | sort -k2 -n
# SQL
my $sql_id = 'SELECT i.id FROM id i WHERE i.AC=? AND i.subfamily=?';
my $sth_id = $dbh->prepare($sql_id);

#SELECT DISTINCT g.prot_id, g.branch FROM gene g, node2gene n WHERE g.id=n.id AND g.prot_id NOT IN (SELECT n.prot_id FROM node2gene n) AND g.id=11127 AND g.prot_id='MGP_CAROLIEiJ_P0037661';
#./update_node2gene_from_NHX_files.pl selectome_v07_euteleostomi98 ENSGT00940000154661.Euteleostomi.001 | sort -k2 -n
my $sql_prot   = 'SELECT DISTINCT g.prot_id, g.branch FROM gene g, node2gene n WHERE g.id=n.id AND g.prot_id NOT IN (SELECT n.prot_id FROM node2gene n) AND g.id=? AND g.gene_id=?';
#my $sql_prot   = 'SELECT DISTINCT g.prot_id, g.branch FROM gene g, node2gene n WHERE g.id=n.id AND g.id=? AND g.gene_id=?';
my $sth_prot   = $dbh->prepare($sql_prot);

my $sql_insert = 'INSERT INTO node2gene VALUES (?, ?, ?, ?, ?)';
my $insert     = $dbh->prepare($sql_insert);


# Get new NHX trees
NHX:
for my $NHX_file ( sort grep { /$AC/ } glob("$outdir/$db1/*/$db2/*.NHX") ){
    #/var/SELECTOME/euteleostomi/ENSGT00980000199298/Euteleostomi/ENSGT00980000199298.Euteleostomi.001.NHX
    my ($st_AC, $st_subfamily) = $NHX_file =~ /\/(\w+)\.\w+\.(\d+)\.NHX$/;
    # Get id
    $sth_id->execute($st_AC, $st_subfamily)  or die $sth_id->errstr;
    my @ids = $sth_id->fetchrow_array;
    my $st_id = $ids[0];

#    print "[$NHX_file] [$st_AC][$st_subfamily]\n";
    my $NHX = read_file($NHX_file, chomp =>1);

    my $forest =  Bio::Phylo::IO->parse(
        -string          => $NHX,
        -format          => 'nhx',
        -keep_whitespace => 1,
    );
    for my $tree ( @{ $forest->get_entities } ){
        # access nodes in $tree
        NODE:
        for my $node ( @{ $tree->get_entities } ){
            if ( $node->is_Leaf ){
                my $is_a_leaf = 1;
                my $is_root   = 0;
                my $NHX_gene = $node->get_meta_object('nhx:G');
                my $NHX_prot = $node->get_meta_object('nhx:PR');
                my $NHX_nd   = $node->get_meta_object('nhx:ND');

                $sth_prot->execute($st_id, $NHX_gene)  or die $sth_prot->errstr;
                my @res = $sth_prot->fetchrow_array;
                # Skip prots and branches already in node2gene!
                if ( !exists $res[0] ){
                    next NODE;
                }
                my ($prot, $branch) = ($res[0], $res[1]);
                #NOTE Test if protein id and branch number match between NHX tree and db
                if ( $prot ne $NHX_prot || $branch != $NHX_nd ){
                    die "Protein id or ND do not match: [$prot][$NHX_prot] [$branch][$NHX_nd]\n";
                }
                #NOTE Test if this leaf is also a pseudo-root
                if ( $node->get_parent->get_meta_object('nhx:ROOT') ){
                    $is_root = 1;
                }
                print join("\t", $st_id, $branch, $is_a_leaf, $is_root, $prot), "\n";
                $insert->execute($st_id, $branch, $is_a_leaf, $is_root, $prot)  or warn $insert->errstr;

                $is_a_leaf = 0;
                while ( my $parent = $node->get_parent() ){
                    my $nd = $parent->get_meta_object('nhx:ND');
                    if ( $parent->get_meta_object('nhx:ROOT') && $parent->get_meta_object('nhx:ROOT') == 1 ){
                        $nd = 0;
                        $is_root = 2;
                    }
                    elsif ( $parent->get_parent->get_meta_object('nhx:ROOT') ){
                        $is_root = 1; # pseudo-root
                    }
                    else {
                        $is_root = 0;
                    }
                    print join("\t", $st_id, $nd, $is_a_leaf, $is_root, $prot), "\n";
                    $insert->execute($st_id, $nd, $is_a_leaf, $is_root, $prot)  or warn $insert->errstr;
                    $node = $parent;
                }
            }
        }
    }
}
$sth_id->finish();
$insert->finish();
$sth_prot->finish();

$dbh->disconnect;

exit 0;

