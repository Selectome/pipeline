#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use List::Util qw(max);

use lib '.';
use DB;
#use Node;
use Selectome::Tree;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";

my $percent_step = 10;

# Shut off the print buffer to have immediat display of the progress
$| = 1;


# % complete stuff initialization
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)
                or die $DBI::errstr;

# SQL query to get all the subtrees to annotate
my $sql_trees = 'SELECT s.id, s.tree FROM subtree s, id i WHERE i.id = s.id AND i.taxon = ?';
my $sth_trees = $dbh->prepare($sql_trees);
$sth_trees->execute($phylum)  or die $sth_trees->errstr;


# File where the new subtrees will be written out
my ($totalCount, $counter, $perc, $previous_perc) = ($sth_trees->rows, 0, 0, 0);


# Go trough the NHX sequence of all subtrees and add the pvalue, the presence of selection, etc.
# Once the tree is consolidated, write-read-write it to have all the modifications updated in
# the NHX tags as well.
my $sth_upTree = $dbh->prepare('UPDATE subtree SET tree = ? WHERE id = ?');
while ( my ($st_id, $st_seq) = ($sth_trees->fetchrow_array) ){
    #NOTE Force reading of tree in the database without SEL & ASEL NHX tags to avoid
    #     passing NHX tags between tree updates!
    $st_seq =~ s{:A?SEL=[YN]}{}g;
    my $tree = Selectome::Tree->new($st_seq);

    my $pseudoroot_pvalue = 1;
    my $leaf_nbr          = 0;

    # Get max branch number to start interate on it for leaves/missing_numbers
    my $new_branch_number = max map { $_->branch_number } grep { defined $_->branch_number } @{$tree->all_nodes};
    # Assign a number to branch without number (mainly leaves)
    map { $_->branch_number(++$new_branch_number); $_->pvalue('NA'); }
        grep { ! defined $_->branch_number } @{$tree->all_nodes};

    NODE:
    for my $node ( sort { $a->branch_number <=> $b->branch_number } @{$tree->all_nodes} ) {
        # Check for selection in every node
        my $sth_sel = $dbh->prepare('SELECT c.selected, c.validity, c.pvalue FROM selection c WHERE c.id = ? and c.branch = ?');
        $sth_sel->execute($st_id, $node->branch_number);
        my ($selected, $validity, $pvalue) = $sth_sel->fetchrow_array;

        if ( $selected && $validity && $selected==1 && $validity==1 ){
            $node->has_selection(1);
        }
        elsif ( defined $node->is_pseudo_root() && $node->is_pseudo_root == 1 ){
            # node->has_selection should be already set with the pseudo_root identification later in the script !
            $pvalue = $pseudoroot_pvalue;
        }
        else {
            $node->has_selection(0);
        }
        $sth_sel->finish;


        # Add a value for the 'PVAL' NHX tag even if there is no pvalue in the  selection  table,
        # Set the NHX tag to 'NA' ('NA' stands for 'not assigned')
        if ( $tree->root_node->branch_number != $node->branch_number ){
            $pvalue = 'NA'  unless ($pvalue);
            $node->pvalue($pvalue);
        }


        # Fill the short name tag for species
        my $sth_species;

        # if the node has a taxon id of uniprot in the NHX tags {T=....}
        if ( $node->tags->{T} ){
            $sth_species = $dbh->prepare('SELECT t.uniprot_code, t.short_name FROM taxonomy t WHERE t.taxid = ?');
            $sth_species->execute($node->tags->{T});
        } # elswise if the node has an id that begins with 'ENS' (leaf/gene)
        elsif ( index($node->id, 'ENS') != -1 ){
            $sth_species = $dbh->prepare('SELECT t.uniprot_code, t.short_name FROM taxonomy t WHERE t.ensembl_code = ?');
            if ( substr($node->id, 3, 1) =~ /\d/ ){
                $sth_species->execute('ENS');
            }
            else {
                $sth_species->execute(substr($node->id, 0, 6));
            }
        } # elswise if the node has an id that doesn't begin with 'ENS' (not a leaf/gene)
        elsif ( index($node->id, 'ENS') == -1 ){
            $sth_species = $dbh->prepare('SELECT t.uniprot_code, t.short_name FROM taxonomy t WHERE t.scientific_name = ?');
            $sth_species->execute($node->id);
        } # if there is a gene NHX tag
        elsif ( $node->tags && $node->tags->{G} && index($node->tags->{G}, 'ENS') != -1 ){
            $sth_species = $dbh->prepare('SELECT t.uniprot_code, t.short_name FROM taxonomy t WHERE t.ensembl_code = ?');
            if ( substr($node->id, 3, 1) =~ /\d/ ){
                $sth_species->execute('ENS');
            }
            else {
                $sth_species->execute(substr($node->id, 0, 6));
            }
        }


        # if a species short name was found
        if ( $sth_species ){
            my ($uniprot_code, $species_short_name) = ($sth_species->fetchrow_array);
            $sth_species->finish;
            if ( $uniprot_code ){
                $node->add_tag('S', $uniprot_code);
            } # if no "uniprot code species name" was found then use the "ensembl species short name"
            elsif ( $species_short_name ){
                $node->add_tag('S', $species_short_name);
            }
        }
        else {
            print "WARNING: could'nt find a species for node with id '".$node->id."' !\n";
        }


        # Fill annotation for leaves
        if ( $node->is_leaf eq 'leaf' ){
            # Fill the gene name tag for the leaf node
            my $sth_gene_name = $dbh->prepare('SELECT g.gene_name FROM gene g WHERE g.id = ? AND g.gene_id = ?');
            $sth_gene_name->execute($st_id, $node->tags->{G});
            my ($gene_name) = $sth_gene_name->fetchrow_array;
            if ( $gene_name ){
                # Protection for not really allowed char in Newick trees
                $gene_name =~ s{\s}{_}g;
                $gene_name =~ s{\(}{}g;
                $gene_name =~ s{\)}{}g;
                $gene_name =~ s{:}{_}g; # TODO postpone this on tree viewer
                #TODO fixes for : and [] chars in gene names!
                $node->add_tag('GN', $gene_name);
            }
            $sth_gene_name->finish;


            # Fill the transcript tag for the leaf node
            my $sth_transcript = $dbh->prepare('SELECT g.transcript_id FROM gene g WHERE g.id = ? AND g.gene_id = ?');
            $sth_transcript->execute($st_id, $node->tags->{G});
            my ($transcript) = $sth_transcript->fetchrow_array;
            if ( $transcript ){
                $node->add_tag('TR', $transcript);
            }
            $sth_transcript->finish;


            # Fill the protein tag for the leaf node
            $node->add_tag('PR', $node->id);


            # Use geneName_species as tree leaf default label, if any
            if ( $gene_name ne '' ){
                my $species = $node->tags->{'S'};
                my $new_id  = $gene_name.'_'.$species;
                if ( $gene_name =~ /_$species$/ ){
                    $new_id = $gene_name;
                }
                $node->id($new_id);
            }
        }


        # Fill node2gene table
        my $is_a_leaf     = 0;
        my $is_root       = 0;

        if ( $node->is_leaf eq 'leaf' ){
            $is_a_leaf = 1;
            $leaf_nbr++;
        }

        if ( $node->is_root ){
            $is_root   = 2;
        }
        elsif ( grep { $_->branch_number == $node->branch_number } @{$tree->root_node->child_nodes} ){
            $is_root   = 1; # Is a direct child of the root / pseudo-roots
            for my $pseudoroot ( @{$tree->root_node->child_nodes} ){
                $pseudoroot->is_pseudo_root(1);
                if ( $node->has_selection() ){
                    $pseudoroot->has_selection(1);
                }
                else {
                    $pseudoroot->has_selection(0);
                }
            }
            $pseudoroot_pvalue = $pvalue; #CHECK Redundant with the following now?

            # Assign computed pvalue to the other pseudoroot node
            #TODO Check with NA! Does it happen that pseudoroot nodes have NA ???
            if ( defined ${$tree->root_node->child_nodes}[0]->{'pvalue'} && defined ${$tree->root_node->child_nodes}[1]->{'pvalue'}
                    && ${$tree->root_node->child_nodes}[0]->{'pvalue'} ne 'NA' && ${$tree->root_node->child_nodes}[1]->{'pvalue'} ne 'NA' ){ # Both passed
                my $pseudoroot_pvalue = ${$tree->root_node->child_nodes}[0]->{'pvalue'} < ${$tree->root_node->child_nodes}[1]->{'pvalue'}
                                      ? ${$tree->root_node->child_nodes}[0]->{'pvalue'}
                                      : ${$tree->root_node->child_nodes}[1]->{'pvalue'};
                ${$tree->root_node->child_nodes}[0]->pvalue($pseudoroot_pvalue);
                ${$tree->root_node->child_nodes}[1]->pvalue($pseudoroot_pvalue);
            }
        }

        # If leaves have no numbers, they cannot be processed as before, need to use gene_id instead
        my $sth_node2gene    = $dbh->prepare('INSERT INTO node2gene (id, branch, is_a_leaf, is_root, prot_id) VALUES (?, ?, ?, ?, (SELECT prot_id FROM gene WHERE id = ? AND gene_id = ?))');
        my $sth_upGeneBranch = $dbh->prepare('UPDATE gene SET branch = ? WHERE id = ? AND gene_id = ?');
        CHILD_PROT:
        for my $child ( sort map { $_->tags->{'G'} } $node->leaves ){ # Sort here for already sorted insertion in MySQL
#            print "$st_id\t", $node->branch_number, "\t$is_a_leaf\t$is_root\t$child\n";
            $sth_node2gene->execute($st_id, $node->branch_number, $is_a_leaf, $is_root, $st_id, $child);
            $sth_upGeneBranch->execute($node->branch_number, $st_id, $child)  if ($is_a_leaf == 1);
        }
        $sth_node2gene->finish;
        $sth_upGeneBranch->finish;
    }

    # print out the new tree in the standard output
    my $NHX_tree_string = (Selectome::Tree->new( $tree->write_tree(1, 0) ))->write_tree(1, 0);
#    print $st_id, "\t", $NHX_tree_string, "\t", $leaf_nbr, "\n";
    #FIXME Better/simpler this way if already in db but not up-to-date
    #FIXME maybe need to add $leaf_nbr in the update query
    $sth_upTree->execute($NHX_tree_string, $st_id)  or die $sth_upTree->errstr;


    # Update selected field in gene table based on SEL and ASEL values !
    my @nodes = grep { /&&NHX:/ } split(']', $NHX_tree_string);
    my $sth_upGeneSelected = $dbh->prepare('UPDATE gene SET selected=1 WHERE id = ? AND gene_id = ?');
    for my $node ( @nodes ){
        # Is a leaf with a gene_id and under selection directly or in the past
        if ( $node =~ /:G=(\w+)/ ){
            my $gene_id = $1;
            if ( $node =~ /:SEL=Y/ || $node =~ /:ASEL=Y/ ){
                $sth_upGeneSelected->execute($st_id, $gene_id);
            }
        }
    }
    $sth_upGeneSelected->finish;
}
$sth_upTree->finish;
$sth_trees->finish;
$dbh->disconnect;

exit 0;

