#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;
use DBI;

use lib '.';
use Selectome::Utils;
my $famid_length = $Selectome::Utils::famid_length;
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 taxon_name\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 taxon_name\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $sthID  = $dbh->prepare('SELECT id, AC, taxon, subfamily FROM id WHERE taxon=? ORDER BY id');
$sthID->execute($phylum)  or die $sthID->errstr;
my $ID_ref = $sthID->fetchall_arrayref;
$sthID->finish();
$dbh->disconnect  or warn $dbh->errst;

# Transform db results to keys
my %ids = map { my $subfamily = sprintf("%0${famid_length}d", $_->[3]); "$_->[1].$_->[2].$subfamily" => $_->[0] } @$ID_ref;


my @sorted_dumps = ('branch.txt.sorted.uniq',
                    'selection.plus.sorted.uniq',
#                    'msa.txt.sorted.uniq',
                    'position.txt.sorted.uniq',
                    'subtree.txt.sorted.uniq',
                    'warning.txt.sorted.uniq',
                    'gene.tsv',
                    'xref.tsv',
                   );
mkdir 'SAVE';
system("cp @sorted_dumps SAVE/");

DUMP:
for my $dump ( @sorted_dumps ){
    next DUMP  if ( !-e "$dump" );

    my $ready_dump = '';
    if ( exists($ID_ref->[0]) ){
        for my $line ( read_file( "$dump", chomp => 1 ) ){
            my ($id, $rest) = split(/\t/, $line, 2);

            if ( exists $ids{$id} ){
                $ready_dump .= "$ids{$id}\t$rest\n";
            }
            else {
                warn "[$id] not found in the db\n";
            }
        }
        write_file( "$dump.changed", $ready_dump );
    }
}

exit 0;

