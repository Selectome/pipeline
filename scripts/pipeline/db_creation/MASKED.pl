#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::MSA;

my $nt                  = 'nt.fas';
my $aa_extension        = 'aa.fas.REF';
my $nt_extension        = 'nt.fas.ORI';
my $aa_masked_extension = 'aa.fas.newmafft.ScoreMerged';
my $nt_masked_extension = 'nt.fas.preTrimAl';


my $prefix = $ARGV[0]  or die "Provide a prefix for MSA files: $0 path_to_prefix.nhx\n";
$prefix =~ s{\.nhx$}{};


# $ntmsa_file
if ( -e "$prefix.$nt.gz" ){
    system("gunzip --force $prefix.$nt.gz");
}
die "1\n"  if ( -z "$prefix.$nt" );
my $ntmsa_file = "$prefix.$nt";


# $ntmsa_file.preTrimAl
if ( -e "$prefix.$nt_masked_extension.gz" ){
    system("gunzip --force $prefix.$nt_masked_extension.gz");
}
die "2\n"  if ( -z "$prefix.$nt_masked_extension" );
system("mv -f $prefix.$nt_masked_extension $prefix.$nt_masked_extension.ORI");


# $aamsa_file.REF
if ( -e "$prefix.$aa_extension.gz" ){
    system("gunzip --force $prefix.$aa_extension.gz");
}
die "3\n"  if ( -z "$prefix.$aa_extension" );
my $aamsa_file = "$prefix.aa.fas";


#CHR
my $chr = '';
if ( -e "$prefix.CHR" && -s "$prefix.CHR" ){
    $chr = read_file( "$prefix.CHR", chomp => 1 );
}


if ( -e "$aamsa_file.REF.gz" ){
    system("gunzip --force $aamsa_file.REF.gz");
}
if ( -e "$aamsa_file.newmafft.ScoreMerged.gz" ){
    system("gunzip --force $aamsa_file.newmafft.ScoreMerged.gz");
}


Selectome::MSA::backTranslate_mask("$ntmsa_file", "$aamsa_file.REF", $chr); # For unmasked (original) MSA
system("mv -f $ntmsa_file.preTrimAl  $ntmsa_file.ORI");
Selectome::MSA::backTranslate_mask("$ntmsa_file", "$aamsa_file.newmafft.ScoreMerged", $chr); # For masked MSA

exit 0;

