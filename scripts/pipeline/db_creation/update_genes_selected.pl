#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
#use List::Util qw(any);

use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome id]\n\n";
my $id     = $ARGV[1]  || 0;
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

# Shut off the print buffer to have immediat display of the progress
$| = 1;


# SQL
my $sql_ids = 'SELECT s.id FROM subtree s WHERE s.selected=1'; #NOTE only on trees with selection
if ( $id && $id =~ /^\d+$/ ){
    $sql_ids = 'SELECT s.id FROM subtree s WHERE s.selected=1 AND s.id=?';
}
my $sth_get_selected_ids       = $dbh->prepare($sql_ids);
my $sth_get_genes_not_selected = $dbh->prepare('SELECT g.own_id, g.prot_id FROM gene g WHERE g.id=? AND g.selected=0');
my $sth_is_branch_selected     = $dbh->prepare('SELECT b.selected FROM branch b WHERE b.id=? AND b.branch IN (SELECT n.branch FROM node2gene n WHERE n.prot_id=? AND n.id=?) AND b.selected=1');
my $sth_update_gene_selected   = $dbh->prepare('UPDATE gene SET selected=1 WHERE own_id=?');


# Get all ids
if ( $id && $id =~ /^\d+$/ ){
    $sth_get_selected_ids->execute($id)  or die $sth_get_selected_ids->errstr;
}
else {
    $sth_get_selected_ids->execute()     or die $sth_get_selected_ids->errstr;
}
while ( my ($st_id) = ($sth_get_selected_ids->fetchrow_array) ){
    $sth_get_genes_not_selected->execute($st_id)  or die $sth_get_genes_not_selected->errstr;
    while ( my ($st_own_id, $st_prot_id) = ($sth_get_genes_not_selected->fetchrow_array) ){
#        print "[$st_own_id] [$st_prot_id]\n";
        $sth_is_branch_selected->execute($st_id, $st_prot_id, $st_id)  or die $sth_is_branch_selected->errstr;
        while( my ($st_select) = ($sth_is_branch_selected->fetchrow_array) ){
            print "[$st_prot_id]\n";
            $sth_update_gene_selected->execute($st_own_id)  or die $sth_update_gene_selected->errstr;
            last;
        }
    }
}

$sth_update_gene_selected->finish;
$sth_is_branch_selected->finish;
$sth_get_genes_not_selected->finish;
$sth_get_selected_ids->finish;

$dbh->disconnect;
exit 0;

