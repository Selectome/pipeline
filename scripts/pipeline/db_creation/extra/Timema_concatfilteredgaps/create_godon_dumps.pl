#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
#use Compress::Zlib;
use File::Slurp;
use File::Basename;
use Cpanel::JSON::XS;
use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;
use Statistics::Distributions;
use Sort::Naturally;


# Options
my ($subtaxon, $has_leaf_results, $resultsdir) = ('', 0, '.');
GetOptions('subtaxon|taxon=s'    => \$subtaxon, # Subtaxon to extract: Euteleostomi, ...
           'label_leaves'        => \$has_leaf_results,
           'results_directory=s' => \$resultsdir,
           );

# Check arguments
error("Invalid options:\n\te.g.: $0 --taxon=Euteleostomi --results_directory=<path_to_godon_data_dir> [--label_leaves]")  if ( $subtaxon !~ /[A-Z]/i );

# Get families which have finished GODON runs
my @jobs_to_do = nsort glob($resultsdir.'/*.godonBSG_OKAY');

error('No data to analyze')  if (!exists($jobs_to_do[0]));

# create mySQL dump files
print "\tCreating MySQL DUMPS\n";
open(my $TREES,      '>', 'subtree.txt');
open(my $BRANCHES,   '>', 'branch.txt');
open(my $POSITIONS,  '>', 'position.txt');
open(my $SELECTIONS, '>', 'selection_no_qvalues.txt');
open(my $MSA,        '>', 'msa.txt');

my $tree_id    = 0;
my $tool       = 'godon';
my $h0_include = 1;

open(my $PVALUES, '>', 'pvalues.txt');
JOB:
for my $job ( @jobs_to_do ){
    my ($prefix) = $job =~ /^(.+?)_godonBSG_8rate\.godonBSG_OKAY$/;
    my $id       = basename($prefix);
    my $dir      = dirname($prefix);

    print "parsing $id\n";
    # check json file is there
    my $json_file = "${prefix}_godonBSG_8rate.json";

    if ( !-e $json_file ){
        error("Problem with json file for $id\n");
        next JOB;
    }

    my $json = read_file($json_file);
    my $data = decode_json $json;

    $tree_id++;

    my $finalTree = $data->{'tests'}->[0]->{'tree'};
    $finalTree =~ s{#1}{};
    my ($leaf_number, $tree_size, $selected, $geneName, $subdesc)  = (0, 0, 0, '', '');

    # get branch lengths from godon tree and put into original nhx
    my ($dbTree, $branch_data) = create_trees($finalTree, $id, $prefix);

    # msa
    my $msa_length     = get_msa_length("$prefix.fasta");
    my $used_length    = get_msa_length("$prefix.fasta");
    my $kept_positions = 'NA';#FIXME no stat file here #extract_stat("$prefix.nt.fas.trimal.stat", $msa_length);

    # branches
    my $godon_branches = $data->{'tests'};

    my $branch_names;
    my $is_a_leaf;

    # get branch names from original nhx and add empty nhx tags
    ($leaf_number, $tree_size, $branch_names, $is_a_leaf) = get_branch_names($id, $prefix);

    #map godon branch ids to real ids
    my %godon_branch_to_real_branch;
    my $godon_branch_id = 0;
    for (my $b=1; $b < $tree_size; $b++){
        if ( $has_leaf_results == 0 ){
            if ( $is_a_leaf->{$b} == 0 ){
                $godon_branch_to_real_branch{$godon_branch_id} = $b;
                $godon_branch_id++;
            }
        }
        else {
            $godon_branch_to_real_branch{$godon_branch_id} = $b;
            $godon_branch_id++;
        }
        print {$BRANCHES} "$tree_id\t$b\t$branch_names->{$b}\t0\t0\t0\t$branch_data->{$b}\t0\n";
    }

    my $branch_id = 0; # godon output is zero based and unrooted so start at 1

    # go through the godon branches and get selection data
    for my $branch ( @$godon_branches ){
        my ($taxid, $duplication, $bootstrap, $length, $pvalue) = (0, 0, 'na', 0, 'na');
        my $real_branch_id = $godon_branch_to_real_branch{$branch_id};
        $length = $branch->{'tree'} ;

        if ( $length =~ /\#1\:(\d+\.\d+)/ ){
            $length = $1;
        }
        elsif ( $length =~ /\#1\:(1e-\d*)/ ){
            $length = $1;
        }
        elsif ( $length =~ /\#1\:(\d+)?/ ){
            $length = $1;
        }


        my ($H0_LnL, $H1_LnL, $omega_0, $omega_2, $kappa, $alphac, $p0prop, $p01sum, $h0_kappa, $h0_omega0, $h0_omega2, $h0_p0prop, $h0_p01sum, $p0, $p1, $h0_p0, $h0_p1, $lrt);
        $H0_LnL  = $branch->{'H0'}->{'maxLnL'};
        $H1_LnL  = $branch->{'H1'}->{'maxLnL'};
        $omega_0 = $branch->{'H1'}->{'maxLParameters'}->{'omega0'};
        $omega_2 = $branch->{'H1'}->{'maxLParameters'}->{'omega2'};
        $kappa   = $branch->{'H1'}->{'maxLParameters'}->{'kappa'};
        $alphac  = $branch->{'H1'}->{'maxLParameters'}->{'alphac'} || 'na';
        $p0prop  = $branch->{'H1'}->{'maxLParameters'}->{'p0prop'};
        $p01sum  = $branch->{'H1'}->{'maxLParameters'}->{'p01sum'};
        if ( $h0_include == 1 ){
            $h0_kappa  = $branch->{'H0'}->{'maxLParameters'}->{'kappa'};
            $h0_omega0 = $branch->{'H0'}->{'maxLParameters'}->{'omega0'};
            $h0_p0prop = $branch->{'H0'}->{'maxLParameters'}->{'p0prop'};
            $h0_p01sum = $branch->{'H0'}->{'maxLParameters'}->{'p01sum'};
        }

        # p01sum = p0 + p1
        # p0prop = p0 / (p0 + p1)
        $p0 = $p0prop * $p01sum;
        $p1 = $p01sum - $p0;
        if ( $h0_include == 1 ){
            $h0_p0 = $h0_p0prop * $h0_p01sum;
            $h0_p1 = $h0_p01sum - $h0_p0;
        }
        my $testOptimizations = $branch->{'testOptimizations'};

        my ($i, $H0_time, $H1_time) = (0, 0 ,0);

        for my $optimization ( @$testOptimizations ){
            if ( $i == 0 ){
                # H0 always first
                $H0_time = "$optimization->{'optimizer'}->{'optimizationTime'}";
            }
            else {
                # calculate sum of H1 times
                $H1_time = $H1_time + $optimization->{'optimizer'}->{'optimizationTime'};
            }
            $i++;
        }
        $lrt = 2 * ($H1_LnL - $H0_LnL);

        $pvalue = Statistics::Distributions::chisqrprob(1, $lrt);

        $pvalue = 1e-200  if ( $pvalue==0 ); # To avoid NaN qvalue when pvalue too close to 0
        $pvalue = remove_useless_0($pvalue);
        $H0_LnL = remove_useless_0($H0_LnL);
        $H1_LnL = remove_useless_0($H1_LnL);
        $lrt    = remove_useless_0($lrt);

        print {$PVALUES} "$pvalue\n";
        if ( $branch->{'H1'}->{'final'} ){
            # check the std.out for BEB (not in json)
            open (my $GODON, "${prefix}_godonBSG_8rate.txt");
            BRANCH:
            while ( my $line = <$GODON> ){
                if ( $line =~ /Testing branch $branch_id\// ){
                    while ( my $nextline = <$GODON> ){
                        if ( $nextline =~ /pos/ ){
                            while ( my $data = <$GODON> ){
                                if ( $data =~ /lnL0/ ){
                                    last BRANCH;
                                }
                                else {
                                    chomp $data;
                                    my ($pos, $codon, $aa, $prob) = split (/\t/, $data);
                                    print {$POSITIONS} "$tree_id\t$real_branch_id\t$pos\t$aa\t$prob\t1\n";
                                }
                            }
                        }
                    }
                }
            }
            close $GODON;
        }
        if ( $h0_include == 1 ){
            print {$SELECTIONS} "$tree_id\t$tool\t$real_branch_id\t$omega_0\t$omega_2\t$p0\t$p1\t$kappa\t$alphac\t$H0_LnL\t$H0_time\t$H1_LnL\t$H1_time\t$lrt\t$pvalue\tqvalue\t$selected\t1\t$h0_kappa\t$h0_omega0\t$h0_p0\t$h0_p1\n";
        }
        else {
            print {$SELECTIONS} "$tree_id\t$tool\t$real_branch_id\t$omega_0\t$omega_2\t$p0\t$p1\t$kappa\t$alphac\t$H0_LnL\t$H0_time\t$H1_LnL\t$H1_time\t$lrt\t$pvalue\tqvalue\t$selected\t1\n";
        }
        $branch_id++;
    }
    print {$TREES} "$tree_id\t$dbTree\t$leaf_number\t$geneName\t$subdesc\t$selected\ts\n";
    print {$MSA}   "$tree_id\t$kept_positions\t$id\t$msa_length\t$used_length\n";
}
close $POSITIONS;
close $SELECTIONS;
close $BRANCHES;
close $TREES;
close $MSA;

# calculate q values
Rscript();

# replace q values in selection.txt with those from qvalues.txt
system ("awk 'BEGIN {FS=OFS=\"\t\"}NR == FNR {a[FNR] = \$B;next}{\$A = a[FNR];print \$0}' B=2 A=16 qvalues.txt selection_no_qvalues.txt > selection.txt");
#unlink ("selection_no_qvalues.txt");
exit 0;


sub error {
    my ($msg) = @_;

    warn "\n\t$msg\n\n";
    exit 1;
}


sub remove_useless_0 {
    my ($value) = @_;

    if ( $value !~ /[eE]/ ){ #Do not touch 1-e200 nbr
        $value =~ s{([^0])0+$}{$1};
        $value =~ s{\.$}{};
    }
    return $value;
}

sub Rscript {
    open(my $R, '>', 'script.R');
    print {$R} "library(qvalue)\np<-scan(\"pvalues.txt\")\nqobj<-qvalue(p, pi0.method=\"bootstrap\", fdr.level=0.1, pfdr=TRUE)\nwrite.qvalue(qobj, file = \"qvalues.t\")\n";
    close $R;

    system("/usr/bin/env R --vanilla --quiet --slave --no-save  <script.R 2>&1 | grep -v 'Read'");
    unlink 'script.R';

    system("sed -n -e '2,\$p' qvalues.t | awk '{print \$1\"\\t\"\$2}' >qvalues.txt");
    unlink 'qvalues.t';
    return;
}

sub get_msa_length {
    my ($msa_file) = @_;

    my $alignin  = Bio::AlignIO->new(-format => 'fasta',
                                     -file   => $msa_file,
                                    );
    my $aln         = $alignin->next_aln;
    my $msa_length  = $aln->length;
    $msa_length     = $msa_length / 3;
    return $msa_length;
}

sub extract_stat {
    my ($stat_file, $msa_length) = @_;

    my $STAT;
    open($STAT, $stat_file)  or die "Cannot open the TrimAl stat file '$stat_file'\n";

    my $stat = do{local $/; <$STAT>;};
    chomp $stat;
    close $STAT;

    #NOTE Easy because stat sur nucleotide CDS !
    my @col_kept = map  { $_ / 3 }          # Back to codon position number
                   grep { ($_ % 3 )==0 }    # Keep only 1 position out of 3 to get codon
                   map  { ++$_ }            # Positions start at 0 and not 1, so ++$_ to return $_+1
                   grep { /^\d+$/ }         # Remove header + non-numeric things
                   split(/,?\s+/, $stat);

    die "Prob with MSA length: $stat_file: $col_kept[-1] > $msa_length\n"  if ( $col_kept[-1] > $msa_length ); # $msa_length is in aa/codon


    my $range = "$col_kept[0]:";             # Initialize at the 1st col_kept value
    COL:
    for (my $c = 1; $c <= $#col_kept; $c++){ # so start loop at 2nd index (#2)
        if ( ($col_kept[$c] - 1) != $col_kept[$c-1] ){
            $range .= "$col_kept[$c-1],$col_kept[$c]:";
        }
    }
    #FIXME Does cgi can deal with e.g. 1:25,26:26,100:105 (26:26 single/isolated column) ???
    $range .= $col_kept[-1];

    return $range;

}

sub get_branch_names {
    my ($id, $prefix) = @_;

    my $nwk = read_file("$prefix.nwk");

    my $input = new Bio::TreeIO(-string => $nwk,
                                -format => 'newick',
                               );
    my $bn = 0;

    my $leaf_number = 0;
    my $tree_size   = 0;
    my %branch_names;
    my %is_leaf;
    while( my $tree = $input->next_tree ){
        my $root = $tree->get_root_node;
        for my $node ( $tree->get_nodes ){
            my $name = $node->id || 'na';
            my $bl = $node->branch_length;
            # check if leaf
            my $is_a_leaf = $node->is_Leaf || 0;
            $is_leaf{$bn} = $is_a_leaf;
            $branch_names{$bn} = $name;
            $bn++;
        }
        $tree_size = $bn;
        $leaf_number = scalar $tree->get_leaf_nodes;
    }

    return ($leaf_number, $tree_size, \%branch_names, \%is_leaf);
}

sub create_trees {
    my ($finalTree, $id, $prefix) = @_;
    my $returnTree;

    my $nhx;

    # nhx for ensembl, nwk for others
    if ( $id =~ /ENS/ ){
        $nhx = read_file("$prefix.nhx");
    }
    else {
        $nhx = read_file("$prefix.nwk");
        $nhx =~ s/(,|\))/\[&&NHX\:D=N\]$1/g;
    }

    # read godon final tree
    my $godon_forest = Bio::Phylo::IO->parse(-string          => $finalTree,
                                             -format          => 'newick',
                                             -keep_whitespace => 1
                                            );

    my $forest =  Bio::Phylo::IO->parse(-string          => $nhx,
                                        -format          => 'nhx',
                                        -keep_whitespace => 1,
                                       );


    # read final tree and get branch lengths
    my %branch_data;
    $branch_data{0} = 0; #root has branch length 0;
    for my $tree ( @{ $godon_forest->get_entities } ){
        # access nodes in $tree
        my $bn = 1;

        for my $node ( @{ $tree->get_entities } ){
            my $branch_length = $node->get_branch_length;
            $branch_data{$bn-1} = $branch_length;
            $bn++;
        }
    }
    for my $tree ( @{ $forest->get_entities } ){
        # access nodes in $tree
        my $bn = 0; #branch number (rooted)
        for my $node ( @{ $tree->get_entities } ){
            my $branch_length = $branch_data{$bn};
            $node->set_branch_length($branch_length);
            $node->set_meta_object('nhx:ND'  => $bn );
            $node->set_meta_object('nhx:SEL' => 'N');
            $node->set_meta_object('nhx:PVAL'=> 'na');
            $node->set_meta_object('nhx:PR'  => 'na');
            $bn++;
        }
        $returnTree = Bio::Phylo::IO->unparse(-phylo  => $tree,
                                              -format => 'nhx',
                                             );
    }
    return($returnTree, \%branch_data);
}

