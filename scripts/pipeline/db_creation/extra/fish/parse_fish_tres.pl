#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;
use DBI;
use lib '.';
use DB;
use File::Slurp;
use File::Basename;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $tree_dir = $ARGV[1];

open(my $fh, '>', "output.tsv") or die "Could not open file 'output.tsv' $!";
# print headers

print $fh "#tree_file\tselectome_tree_ac\tbranch_number\tchild_genes_of_branch\ttaxon\ttaxon_id\tduplication\tbranch_length\tomega0\tomega2\tp0\tp1\tkappa\talphac\tlnLH0\tlnLH1\tlrt\tpvalue\tqvalue\tselected\th0_kappa\th0_omega0\th0_p0\th0_p1\n";
&parse_trees($dbh, $tree_dir);
close $fh;
$dbh->disconnect  or warn $dbh->errst;
exit 0;

sub parse_trees {
    my ($dbh, $tree_dir) = @_;

    my @trees =  glob($tree_dir.'/genetree.*');

    foreach my $tree (@trees) {
        &check_tree_in_db($dbh,$tree);
    }
}

sub check_tree_in_db {
    my ($dbh, $tree_file) = @_;

    #get gene_ids from tree
    my $nhx = read_file($tree_file);

    my $forest =  Bio::Phylo::IO->parse(
            -string => $nhx,
            -format => 'nhx',
            -keep_whitespace => 1,
        );

        foreach my $tree ( @{ $forest->get_entities } ) {
        # access nodes in $tree and get the taxons
            my @taxons;
            my @all_taxons;
            foreach my $node ( @{ $tree->get_entities } ) {
                my $node_name = $node->get_name;
                unless ($node_name =~ /^EN/) {
                    push (@all_taxons, $node_name);
                }
            }
            @taxons = uniq(@all_taxons);
            my $gene_in_tree = 0;
            my ($st_id, $st_tree);

            # check if gene is in selectome db and get tree
            NODE: foreach my $node ( @{ $tree->get_entities } ) {
                my $node_name = $node->get_name;

                if ($node_name =~ /^EN/) {
                    my $sql_trees = 'SELECT t.id, t.tree FROM gene g, subtree t WHERE g.gene_id = ? AND g.id = t.id';
                    my $sth_trees = $dbh->prepare($sql_trees);
                    $sth_trees->execute($node_name) or die $sth_trees->errstr;
                    while ( ($st_id, $st_tree) = ($sth_trees->fetchrow_array) ) {
                        $gene_in_tree = 1;
                        last NODE;  #found a match so stop comparing
                    }
                }
            }
            if ($gene_in_tree == 1) {
            # get branches from selectome tree that match taxon tags in fish tree
                foreach my $taxon (@taxons){
                    my $sql_branch = '
                                    SELECT i.AC, b.branch, b.taxid, b.duplication, b.length, s.omega0, s.omega2, s.p0, s.p1, s.kappa, s.alphac, s.lnLH0, s.lnLH1, s.lrt, s.pvalue, s.qvalue, s.selected, s.h0_kappa, s.h0_omega0, s.h0_p0, s.h0_p1
                                    FROM branch b, taxonomy t, id i, selection s
                                    WHERE b.id = ?
                                    AND t.scientific_name = ?
                                    AND t.taxid = b.taxid
                                    AND b.id = i.id
                                    AND b.id = s.id
                                    AND b.branch = s.branch';
                    my $sth_branch = $dbh->prepare($sql_branch);
                    $sth_branch->execute($st_id, $taxon) or die $sth_branch->errstr;
                    while ( my ($st_AC, $st_branch, $st_taxid, $st_duplication, $st_branch_length, $st_omega0, $st_omega2, $st_p0, $st_p1, $st_kappa, $st_alphac, $st_lnLH0, $st_lnLH1, $st_lrt, $st_pvalue, $st_qvalue, $st_selected, $st_h0_kappa, $st_h0_omega0, $st_h0_p0, $st_h0_p1) = ($sth_branch->fetchrow_array) ) {

                        # read the selectome tree and get list of genes that are children of branch
                        my $selectome_forest =  Bio::Phylo::IO->parse(
                                    -string => $st_tree,
                                    -format => 'nhx',
                                    -keep_whitespace => 1,
                            );
                        my @child_genes;
                        foreach my $selectome_tree ( @{ $selectome_forest->get_entities } ) {
                            foreach my $selectome_node ( @{ $selectome_tree->get_entities } ) {
                                my $nd = $selectome_node->get_meta_object('nhx:ND');
                                # only want to check where node is same as branch from db
                                next if (not($nd));
                                next if (not($nd == $st_branch));
                                # get descendents
                                my @children = $selectome_node->get_all_Descendents;
                                foreach my $child (@children) {
                                    if ($child->is_Leaf) {
                                        my $child_gene = $child->get_meta_object('nhx:G');
                                        push (@child_genes,$child_gene);
                                    }
                                 }
                                last;
                            }
                        }
                        print $fh basename($tree_file)."\t$st_AC\t$st_branch\t".join(',',@child_genes)."\t$taxon\t$st_taxid\t$st_duplication\t$st_branch_length\t$st_omega0\t$st_omega2\t$st_p0\t$st_p1\t$st_kappa\t$st_alphac\t$st_lnLH0\t$st_lnLH1\t$st_lrt\t$st_pvalue\t$st_qvalue\t$st_selected\t$st_h0_kappa\t$st_h0_omega0\t$st_h0_p0\t$st_h0_p1\n";
                    }
                }
            }
        }

}
sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}
