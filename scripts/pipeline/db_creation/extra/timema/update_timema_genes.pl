#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;
use DBI;
use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $genefile = 'timema_gene.txt';

open(my $FH, '<', $genefile) or die $!;

while (my $line = <$FH>) {
    chomp $line;
    my ($HOG, $id, $genename,$des) = split (/\t/,$line);

    my ($species) = $id =~ /^(\w\w\w)_.*/g;

    $species = lc($species);
    $species = ucfirst($species);

    #get db id
    my $sql_id = 'SELECT id FROM id where ac = ?';
    my $sth_id = $dbh->prepare($sql_id);
    $sth_id->execute($HOG)  or die $sth_id->errstr;

    while ( my ($st_id) = ($sth_id->fetchrow_array) ) {
        #get tax id
        my $sql_tax = 'SELECT taxid FROM taxonomy where short_name = ?';
        my $sth_tax = $dbh->prepare($sql_tax);
        $sth_tax->execute($species)  or die $sth_tax->errstr;

        while ( my ($tax_id) = ($sth_tax->fetchrow_array) ) {
            #update gene table
            my $update = 'UPDATE gene SET prot_id = ?, transcript_id = ?, gene_id = ?, gene_name = ?, description = ? WHERE id = ? AND taxid = ?';
            my $sth_update = $dbh->prepare($update);
            $sth_update->execute($id,$id,$id,$genename,$des,$st_id,$tax_id)  or die $sth_update->errstr;
        }
   }
}
close($FH);


$dbh->disconnect  or warn $dbh->errst;
exit 0;

    #get all the trees
    my $sql_trees = 'SELECT s.id, s.tree FROM subtree s order by id';
    my $sth_trees = $dbh->prepare($sql_trees);
    $sth_trees->execute()  or die $sth_trees->errstr;
    my $sth_updateTree = $dbh->prepare('UPDATE subtree SET tree = ? WHERE id = ?');
    my $gene_table_id = 1;
    while ( my ($st_id, $st_nhx) = ($sth_trees->fetchrow_array) ) {
        my $forest =  Bio::Phylo::IO->parse(
            -string => $st_nhx,
            -format => 'nhx',
            -keep_whitespace => 1,
        );
        foreach my $tree ( @{ $forest->get_entities } ) {
        # access nodes in $tree
            print "$st_id\n";
            my $returnTree;

            my $sql_ac  = 'SELECT ac FROM id WHERE id=?';
            my $sth_ac  = $dbh->prepare($sql_ac);
            $sth_ac->execute($st_id);
            my $ac = ($sth_ac->fetchrow_array);
            foreach my $node ( @{ $tree->get_entities } ) {
                my $branch = $node->get_meta_object('nhx:ND') || '-1';
                my $branch_name = $node->get_name;
                my $sql_tax   = 'SELECT t.taxid FROM taxonomy t, branch b WHERE b.branch_name=short_name AND branch=? AND id=?';
                my $sth_tax   = $dbh->prepare($sql_tax);
                $sth_tax->execute($branch, $st_id);

                while (my $taxid = ($sth_tax->fetchrow_array) ) {
                    $node->set_meta_object('nhx:T' =>$taxid);
                    $node->set_meta_object('nhx:G' =>$branch_name."_".$ac);
                    $node->set_meta_object('nhx:PR' =>$branch_name."_".$ac);

                    my $insert = "INSERT INTO gene VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    my $sth_insert = $dbh->prepare($insert) or die "Prepare failed: " . $dbh->errstr();

                    my @values = ($gene_table_id, $st_id,$branch_name."_".$ac, $branch_name."_".$ac, $branch_name."_".$ac, $ac, "Timema gene $ac", '', 0, $branch, $taxid, 'g');
                    #$sth_insert->execute(@values);
                    $gene_table_id ++;
                }
                my $sql_tax2   = 'SELECT t.taxid FROM taxonomy t, branch b WHERE b.branch_name=t.scientific_name AND branch=? AND id=?';
                my $sth_tax2   = $dbh->prepare($sql_tax2);
                $sth_tax2->execute($branch, $st_id);
                while (my $taxid = ($sth_tax2->fetchrow_array) ) {
                    $node->set_meta_object('nhx:T' =>$taxid);
                }
             }

            $returnTree = Bio::Phylo::IO->unparse(
                '-phylo'  => $tree,
                '-format' => 'nhx',
                '-keep_whiteapce' => 1,
            );
            $sth_updateTree->execute($returnTree, $st_id);

        }

    }


