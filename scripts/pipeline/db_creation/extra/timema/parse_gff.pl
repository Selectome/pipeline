#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;
use File::Slurp;
use DBI;
use LWP::UserAgent;

my $agent = LWP::UserAgent->new(agent => "libwww-perl");
push @{$agent->requests_redirectable}, 'POST';

my %id_to_hog;
my %id_to_description;
my %id_to_gi;
my %gi_to_ac;
my %ac_to_gene;

open(my $fasta, "<", "fasta_headers") or die "Can't open < fasta_headers $!";

while (my $line = <$fasta>) {
    my ($path, $id, $HOG) = split (/\s/,$line);
    $id_to_hog{$id}=$HOG;
}

open(my $fh, "<", "all_mRNA.gff") or die "Can't open < all_mRNA.gff: $!";

my $all_gis;

while (my $line = <$fh>) {
    my %all_data;

    chomp $line;
    my (@data) = split (/;/,$line);

    foreach my $data (@data) {
    my ($key, $value) = split (/=/,$data);
    $all_data{$key}=$value;
    }

    my $id =  "$all_data{'ID'}\n";
    chomp $id;

    my $HOG=$id_to_hog{$id};
    my $description = 'n/a';


    if ($HOG) {

    my $blast_hit;

    # take dros blast hit first and then arth if no dros
    if ($all_data{'droso_topblasthit'}) {
        $blast_hit = $all_data{'droso_topblasthit'};
    }
    elsif ($all_data{'arth_topblasthit'}) {
        $blast_hit = $all_data{'arth_topblasthit'};
    }
    else {
        $blast_hit = "n/a";
    }
    my $gi = 'n/a';
    if ($blast_hit =~ /gi\|(\d*)\|(ref|pir|pdb|gb|emb|dbj|sp|prf|tpg)\|(.*)\|(.*)$/) {
        $gi          = $1;
        my $db       = $2;
        my $db_id    = $3;
        $description = $4;

        $all_gis       = $all_gis."$gi ";
        $id_to_gi{$id} = $gi;
    }
    }
    $id_to_description{$id} = $description;
}
chop $all_gis; # remove trailing space

# get all the swissprot acs
my $params = {
    from => 'P_GI',
    to => 'ACC',
    format => 'tab',
    query => $all_gis
};
my $response = $agent->post("https://www.uniprot.org/uploadlists/", $params);

while (my $wait = $response->header('Retry-After')) {
  print STDERR "Waiting ($wait)...n";
  sleep $wait;
  $response = $agent->get($response->base);
}

$response->is_success ?
    my @swissprot_accs = split (/\n/, $response->content) :
    die 'Failed, got ' . $response->status_line .' for ' . $response->request->uri . "n";

my $all_swissprot_acs;

foreach my $row (@swissprot_accs) {
    next if ($row =~ /From/);

    my ($gi, $ac) = split (/\t/,$row);
    $gi_to_ac{$gi}     = $ac;
    $all_swissprot_acs = $all_swissprot_acs."$ac ";
}
chop $all_swissprot_acs; # remove trailing space

# get all the gene names from the swissprot acs
my $g_params = {
    from => 'ACC',
    to => 'GENENAME',
    format => 'tab',
    query => $all_swissprot_acs
};

$response = $agent->post("https://www.uniprot.org/uploadlists/", $g_params);

while (my $wait = $response->header('Retry-After')) {
  print STDERR "Waiting ($wait)...n";
  sleep $wait;
  $response = $agent->get($response->base);
}

$response->is_success ?
    my @genenames = split (/\n/, $response->content) :
    die 'Failed, got ' . $response->status_line .' for ' . $response->request->uri . "n";

foreach my $row (@genenames)
{
    next if ($row =~ /From/);

    my ($ac, $gene) = ('', "n/a");
    ($ac, $gene) = split (/\t/,$row);

    $ac_to_gene{$ac} = $gene;
}

foreach my $id (keys %id_to_hog)
{
    my ($gi,$ac,$genename,$description) = ('','',"n/a","n/a");

    my $HOG = $id_to_hog{$id};

    if (exists($id_to_description{$id})) {
    $description = $id_to_description{$id};
    }

    if (exists($id_to_gi{$id})) {
    $gi          = $id_to_gi{$id};
    $ac          = $gi_to_ac{$gi};

    if (exists($ac_to_gene{$ac})) {
        $genename    = $ac_to_gene{$ac};
        $genename =~ s/Dmel\\//g; # remove Dmel\ from genename
    }
    }
    print "$HOG\t$id\t$genename\t$description\n";
}
