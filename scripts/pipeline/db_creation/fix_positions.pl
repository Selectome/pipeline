#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;

use lib '.'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $sth_msa = $dbh->prepare('SELECT m.id, m.kept_positions, m.msa_length FROM msa m, id i WHERE i.id=m.id AND i.taxon = ? ORDER BY m.id');
$sth_msa->execute($phylum)  or die $sth_msa->errstr;
my $msa_ref = $sth_msa->fetchall_arrayref;
$sth_msa->finish();

if ( exists($msa_ref->[0]) ){
    MSA:
    for my $row (@$msa_ref){
        my ($id, $kept_positions, $msa_length) = @$row;
        next MSA  if ( $kept_positions eq ':' ); # TrimAl removed all sites

        if ( $kept_positions =~ /^1:\d+$/ ){
            # No column filtered that requires fixing: a single block starting at 1
            next MSA;
            # BUGS in the pipeline if positions under selection after this block !!!
        }


        # Fix several sites identified at the same locus at the same time (DISTINCT)
        # Start in the descending order to avoid updating a position not yet updated (temporary duplicates)
        my $sth_position = $dbh->prepare('SELECT DISTINCT position FROM position WHERE id = ? ORDER BY position desc');
        $sth_position->execute($id)  or die $sth_position->errstr;
        my $position_ref = $sth_position->fetchall_arrayref;
        $sth_position->finish();


        if ( exists($position_ref->[0]) ){

            my @blocks = split(/,/, $kept_positions);

            POS:
            for my $row2 (@$position_ref){
                my ($position) = @$row2;

                my $fixed_position = shift_position($position, \@blocks);
                die "\n\tERROR: one of the fixed position is out of the whole MSA length: [id:$id] [$position -> $fixed_position] [$msa_length]\n\n"
                    if ( $fixed_position > $msa_length );
                if ( $position < $fixed_position ){
                    my $sth_upPos = $dbh->prepare('UPDATE position SET position = ? WHERE id = ? AND position = ?');
                    $sth_upPos->execute($fixed_position, $id, $position)  or die $sth_upPos->errstr;
                    $sth_upPos->finish();
#                    print "$id: $position -> $fixed_position\n";
                }
            }
        }
    }
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;


sub shift_position {
    my ($position, $blocks) = @_;

    my $shift = 0;
    my ($prev_start, $prev_end) = (0, 0);
    BLOCK:
    for my $block ( @$blocks ){
        my ($start, $end) = split(/:/, $block);

        #FIXME Check if 13:13 works as block (single col block)
        # Required shift
        $shift     += $start - $prev_end - 1;
        $prev_start = $start;
        $prev_end   = $end;

        # When to apply the shift
        my $fixed_position = $position + $shift;
        last BLOCK  if ( $start <= $fixed_position && $fixed_position <= $end );
    }

#    print {*STDERR} "[$position] [$shift] [@$blocks]\n";
    return ($position + $shift);
}

