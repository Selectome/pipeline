#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use Getopt::Long;

use lib '.';
use DB;


# Define arguments & their default value
my ($geneTSV, $dbname) = ('', '');
my ($verbose) = (0);
my %opts = ('gene=s'   => \$geneTSV,   # gene.tsv file
            'db=s'     => \$dbname,    # Selectome database name
            'verbose'  => \$verbose,   # verbose mode, do not insert/update in database
           );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $dbname eq '' || $geneTSV eq '' ){
    print "\n\tInvalid or missing argument:
\t     $0  -db=selectome_v07_euteleostomi98  -gene=gene.tsv
\t-gene     gene.tsv file
\t-db       Selectome database name to update
\t-verbose  verbose mode, do not insert/update in database
\n";
    exit 1;
}


# selected field is not set in gene table for MGP_ ids, so use the update_genes_selected.pl script!
my $dbh      = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $sth_id   = $dbh->prepare('SELECT id FROM id WHERE AC=? AND taxon=? AND subfamily=?');
my $sth_gene = $dbh->prepare('SELECT own_id, gene_name, description, source FROM gene WHERE id=? AND prot_id LIKE ? AND transcript_id LIKE ? AND gene_id LIKE ?');
my $up_gene  = $dbh->prepare('UPDATE gene SET prot_id=?, transcript_id=?, gene_id=?, gene_name=?, description=?, source=? WHERE own_id=?');
my $up_xref  = $dbh->prepare('UPDATE xref SET prot_id=? WHERE id=? AND prot_id=?');
my $ins_gene = $dbh->prepare('INSERT INTO gene (id, prot_id, transcript_id, gene_id, gene_name, description, source, taxid) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');

GENE:
for my $line ( grep { !/^#/ } read_file("$geneTSV", chomp=>1) ){
    #0	ENSGT00390000000002.Euteleostomi.002	MGP_CAROLIEiJ_P0068404	MGP_CAROLIEiJ_T0068404	MGP_CAROLIEiJ_G0027260	Apbb2	amyloid beta precursor protein binding family B member 2	NCBI:110294041	0	0	10089
    my (undef, $fid, $prot_id, $transcript_id, $gene_id, $gene_name, $description, $source, undef, undef, $taxid) = split(/\t/, $line);
    my ($AC, $taxon, $subfamily) = split(/\./, $fid);
    $sth_id->execute($AC, $taxon, $subfamily)  or die $sth_id->errstr;
    my @id_row = $sth_id->fetchrow_array;
    #Should be a single match
    if ( $sth_id->rows() != 1 ){
        die "Several or no ids found for [$fid]\n";
    }

    #NOTE Need to protect id strings because _ is a special LIKE character for MySQL!
    #NOTE Need to shorten id strings because the db had a string limit at 18 character long!
    my $protected_prot_id       = substr($prot_id,       0, 18);
    my $protected_transcript_id = substr($transcript_id, 0, 18);
    my $protected_gene_id       = substr($gene_id,       0, 18);
    $protected_prot_id       =~ s{_}{\\_}g;
    $protected_transcript_id =~ s{_}{\\_}g;
    $protected_gene_id       =~ s{_}{\\_}g;
    $sth_gene->execute($id_row[0], "$protected_prot_id%", "$protected_transcript_id%", "$protected_gene_id%")  or die $sth_gene->errstr;
    my @gene_row = $sth_gene->fetchrow_array;

    $ins_gene->execute($id_row[0], $prot_id, $transcript_id, $gene_id, $gene_name, $description, $source, $taxid)  or $ins_gene->errstr;
    next GENE;
    if ( $sth_gene->rows() > 1 ){
        warn "Too many or no matches for [$id_row[0]] [$protected_prot_id] [$protected_transcript_id] [$protected_gene_id]\n";
        next GENE;
    }
    elsif ( $sth_gene->rows() == 1 ){
        # Update fields
        if ( $verbose ){
            print join('|', @gene_row), "\n";
            print "UPDATE gene SET prot_id=$prot_id, transcript_id=$transcript_id, gene_id=$gene_id, gene_name=$gene_name, description='$description', source=$source WHERE own_id=$gene_row[0]\n";
            print "UPDATE xref SET prot_id=$prot_id WHERE id=$id_row[0] AND prot_id=".substr($prot_id, 0, 18)."\n";
        }
        $up_gene->execute($prot_id, $transcript_id, $gene_id, $gene_name, $description, $source, $gene_row[0])  or $up_gene->errstr;
        $up_xref->execute($prot_id, $id_row[0], substr($prot_id, 0, 18))  or $up_xref->errstr;
    }
    else { # $sth_gene->rows() == 0
        # Missing gene => insert
        $ins_gene->execute($id_row[0], $prot_id, $transcript_id, $gene_id, $gene_name, $description, $source, $taxid)  or $ins_gene->errstr;
        #NOTE xref may be there!
        $up_xref->execute($prot_id, $id_row[0], substr($prot_id, 0, 18))  or $up_xref->errstr;
    }
}

$sth_id->finish;
$sth_gene->finish;
$up_xref->finish;
$up_gene->finish;
$ins_gene->finish;
# Close db connections
$dbh->disconnect;
exit 0;

