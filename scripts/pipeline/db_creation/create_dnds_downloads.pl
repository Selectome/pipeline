#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;

use Bio::Tree::Tree;
use Bio::Taxon;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

&create_download($dbh, $dbname);

$dbh->disconnect  or warn $dbh->errst;
exit 0;

sub create_download {
    my ($dbh, $dbname) = @_;

    my $outfile = $dbname."_dnds.tsv";
    open(ALL, '>', $outfile) or die "Could not create file '$outfile' $!";



    my @taxonomic_groups = ('Homo sapiens','Mus musculus','Danio rerio', 'Aves', 'Sauria', 'Mammalia','Sarcopterygii', 'Actinopterygii');
    my %FH;

    foreach my $group (@taxonomic_groups) {
        my $group_outfile =  $group."_".$dbname."_dnds.tsv";
        open($FH{$group}, '>', $group_outfile) or die "Couldn't create output file $!";
    }

    #get dnds data
    my $sql = '
    SELECT g.gene_id, s.dnds, t.scientific_name as species_name, g.taxid as tax_id, i.AC as tree_id ,i.subfamily
    FROM gene g

    JOIN (
        SELECT taxid, scientific_name FROM taxonomy
        ) AS t
    on g.taxid = t.taxid

    JOIN (
        SELECT id, avg(h0_omega0)*avg(h0_p0) + avg(h0_p1) dnds
        FROM selection
        GROUP BY id
        ) AS s
    ON g.id = s.id

    JOIN (
        SELECT id,AC,subfamily
        FROM id
        ) AS i
    ON i.id = s.id
    ORDER BY g.taxid
    ';


    my $db = Bio::DB::Taxonomy->new(-source => 'entrez');

    my $sth = $dbh->prepare($sql);
    $sth->execute()  or die $sth->errstr;

    while ( my ($gene_id, $dnds, $species_name, $tax_id, $tree_id, $subfamily) = ($sth->fetchrow_array) ){
        print ALL "$gene_id\t$dnds\t$species_name\t$tax_id\t$tree_id\t$subfamily\n";

        # get lineage of taxon
        my $taxon = $db->get_taxon(-taxonid => $tax_id);

        my $tree_functions = Bio::Tree::Tree->new();
        my $lineage = $tree_functions->get_lineage_string($taxon);

        foreach my $group (@taxonomic_groups) {
            #print to file if group in lineage
            if ($lineage =~ /$group/) {
                print {$FH{$group}} "$gene_id\t$dnds\t$species_name\t$tax_id\t$tree_id\t$subfamily\n";
            }
        }
    }
    close ALL;
}

