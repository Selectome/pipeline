**Goal**: create the Selectome database, and insert data.

## Details
See https://intranet.sib.swiss/display/mrr/How+to+update+Selectome

### db schema
Create a new selectome database in MySQL: selectome_vX_ensY, with proper rights (with 'X' the selectome release number, and 'Y' the Ensembl release number Selectome is based on).
```
DROP DATABASE IF EXISTS `selectome_v07_ens98`;
FLUSH TABLES;
CREATE DATABASE `selectome_v07_ens98` CHARACTER SET latin1;
```
Don't forget to GRANT this new database to selectome user(s)!

Run *new_tables.pl* to create selectome specific database and tables.
```
perl new_tables.pl  selectome_v07_ens98
```

Schema generated with SQL::Translator
```
sqlt-diagram --db=MySQL --natural-join-pk -s name,own_id --skip-tables tools,datasource --color -o selectome_v07_ens98.png  selectome_v07_ens98.dump
```

### Create main dumps
...

Then create dumps [create_dumps.pl](create_dumps.pl) and fix ids [create_ids.pl](create_ids.pl).

Then add genes and xrefs [get_geneName.pl](get_geneName.pl) and taxonomy [fill_swissprot_species_code.pl](fill_swissprot_species_code.pl).

Then it will add annotation in trees [add_NHX_tags.pl](add_NHX_tags.pl), generate subfamily symbols [generate_symbols.pl](generate_symbols.pl) and description [generate_family_description.pl](generate_family_description.pl).

Then will fix positions in MSA according to masking [kept_positions_in_MSA.pl](kept_positions_in_MSA.pl) and [fix_positions.pl](fix_positions.pl).

At the end it copies raw data in web site directory [copy_msa_4_website.pl](copy_msa_4_website.pl).

It also applies some fixes [Fix_pseudoroot.pl](Fix_pseudoroot.pl) or [fix_strange_BEB.pl](fix_strange_BEB.pl).


## Data generation
* If it is the first time you execute this step in this pipeline run: `make clean`

* Run Makefile: `make`

* Generate families name: `./generate_symbols.pl selectome_v07_euteleostomi98`

* Update selection for genes (when selection happens in ancestral nodes): `./update_selected_genes.pl selectome_v07_euteleostomi98`

* At the end of the pipeline, update data source information, with version used and types of data pulled into Selectome.


## Other notable Makefile targets
* drop the database: `make dropDatabaseSelectomeRELEASE` (e.g., `make dropDatabaseSelectome7`)

