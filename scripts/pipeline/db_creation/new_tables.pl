#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $engine = 'MyISAM';

#TODO check if character set uft8 database is required. If not keep latin1!

#FIXME msa need to be stored somewhere because it is more and more difficult
#       to get them from ensembl db without re-doing filtering steps
#       => store MSA or path to them !!!!!
#       They can still be gzipped !!!!

# Create the id table in selectome_? db
my $sth_id = $dbh->prepare("CREATE TABLE if not exists id (
id             mediumint     unsigned NOT NULL AUTO_INCREMENT   COMMENT 'Selectome subfamily internal identifier',
AC             varchar(19)            NOT NULL default ''       COMMENT 'Family accession number',
taxon          varchar(30)            NOT NULL default ''       COMMENT 'Taxon name used to build Selectome',
subfamily      tinyint       unsigned NOT NULL default 0        COMMENT 'Subfamily number for this family',
PRIMARY KEY (id),
UNIQUE KEY  (AC, subfamily, taxon))
ENGINE = $engine");
$sth_id->execute();
$sth_id->finish();


# Create the subtree table in selectome_? db
my $sth_tree = $dbh->prepare("CREATE TABLE if not exists subtree (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
tree           mediumtext             NOT NULL default ''       COMMENT 'Tree in NHX format, with selection predictions',
leaf_nbr       smallint      unsigned NOT NULL default 0        COMMENT 'Tree number of leaves',
subname        varchar(120)           NOT NULL default ''       COMMENT 'Subfamily name based on frequency in subtrees',
subdesc        text                   NOT NULL default ''       COMMENT 'Subfamily description',
selected       tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is there positive selection in that subtree ?',
table_name     char(1)                NOT NULL default 's'      COMMENT 'table_name for Sphinx',
PRIMARY KEY        (id))
ENGINE = $engine");
$sth_tree->execute();
$sth_tree->finish();

#TODO Check text/char fields to better fitting
#  TINYTEXT:        255 char
#      TEXT:     65,535 char
#MEDIUMTEXT: 16,777,215 char

# CHAR/VARCHAR


#FIXME Encode parent/child links here ????
# Create the taxonomy table in selectome_? db
my $sth_taxo = $dbh->prepare("CREATE TABLE if not exists taxonomy (
taxid             mediumint  unsigned NOT NULL default 0        COMMENT 'Taxonomic id from NCBI',
scientific_name   varchar(199)        NOT NULL default ''       COMMENT 'Clades or species fullname',
common_name       varchar(99)         NOT NULL default ''       COMMENT 'The comon name as defined by Genbank',
aliases           tinytext            NOT NULL default ''       COMMENT 'Aliases to common or scientific names if any',
is_a_species      tinyint(1) unsigned NOT NULL default 0        COMMENT 'Is a species and not a clade somewhere in the tree',
short_name        varchar(4)          NOT NULL default ''       COMMENT 'Ensembl name of this genome in the Gspe (\"G\"enera \"spe\"cies) format',
ensembl_code      varchar(13)         NOT NULL default ''       COMMENT 'Species code beginning  ensembl  identifiers',
uniprot_code      varchar(5)          NOT NULL default ''       COMMENT 'Species code finishing swissprot identifiers',
table_name        char(1)             NOT NULL default 't'      COMMENT 'table_name for Sphinx',
PRIMARY KEY        (taxid),
KEY                (ensembl_code),
KEY                (uniprot_code))
ENGINE = $engine");
$sth_taxo->execute();
$sth_taxo->finish();


# Create the gene table in selectome_? db
my $sth_gene = $dbh->prepare("CREATE TABLE if not exists gene (
own_id         mediumint     unsigned NOT NULL AUTO_INCREMENT   COMMENT 'gene internal identifier',
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Tree internal id',
prot_id        varchar(23)            NOT NULL default ''       COMMENT 'Ensembl   protein  identifier from the tree',
transcript_id  varchar(23)            NOT NULL default ''       COMMENT 'Ensembl transcript identifier from the tree',
gene_id        varchar(23)            NOT NULL default ''       COMMENT 'Ensembl     gene   identifier from the tree',
gene_name      varchar(35)            NOT NULL default ''       COMMENT 'Official gene name',
description    text                   NOT NULL default ''       COMMENT 'Gene long description',
source         varchar(70)            NOT NULL default ''       COMMENT 'Gene id source',
selected       tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is it a positively selected gene, itself or one ancestor ?',
branch         smallint      unsigned NOT NULL default 0        COMMENT 'Branch localization in the subtree',
taxid          mediumint     unsigned NOT NULL default 0        COMMENT 'Gene species taxid',
table_name     char(1)                NOT NULL default 'g'      COMMENT 'table_name for Sphinx',
PRIMARY KEY                (own_id),
INDEX        `tree_id`     (`id`),
UNIQUE KEY   `prot`        (`id`, `prot_id`),
UNIQUE KEY   `transcript`  (`id`, `transcript_id`),
UNIQUE KEY   `gene`        (`id`, `gene_id`),
INDEX        `gene_symbol` (`gene_name`),
INDEX        `gene_id` (`gene_id`),
INDEX        `prot_id` (`prot_id`),
INDEX        `transcript_id` (`transcript_id`),
INDEX        `taxid`       (`taxid`))
ENGINE = $engine");
$sth_gene->execute();
$sth_gene->finish();


# Create the xref table in selectome_? db
#NOTE add xref description, if any, mostly for GO to have their description
my $sth_xref = $dbh->prepare("CREATE TABLE if not exists xref (
own_id         int           unsigned NOT NULL AUTO_INCREMENT   COMMENT 'xref internal identifier',
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
prot_id        varchar(23)            NOT NULL default ''       COMMENT 'Ensembl protein identifier from the tree',
xref           varchar(120)           NOT NULL default ''       COMMENT 'Ensembl   xref  identifier(s) related to this prot_id',
db_source      varchar(20)            NOT NULL default ''       COMMENT 'Ensembl   xref  db source related to this xref id',
table_name     char(1)                NOT NULL default 'x'      COMMENT 'table_name for Sphinx',
PRIMARY KEY       (own_id),
UNIQUE KEY `UID`  (id, prot_id, xref),
INDEX      `ID`   (`id`),
INDEX      `XREF` (`xref`),
INDEX      `SRC`  (`db_source`),
INDEX             (`prot_id`))
ENGINE = $engine");
$sth_xref->execute();
$sth_xref->finish();


# Create the node2gene table in selectome_? db
my $sth_node = $dbh->prepare("CREATE TABLE if not exists node2gene (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
branch         smallint      unsigned NOT NULL default 0        COMMENT 'Branch localization in the subtree',
is_a_leaf      tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is a leaf, external node',
is_root        tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is root == 2; is a pseudoroot == 1, internal node around the root',
prot_id        varchar(23)            NOT NULL default ''       COMMENT 'Ensembl protein identifier linked to/under this branch',
UNIQUE KEY (id, branch, prot_id),
INDEX      (`prot_id`))
ENGINE = $engine");
$sth_node->execute();
$sth_node->finish();


# Create the msa table in selectome_? db
#FIXME  Store also filtering scoring somewhere ???
my $sth_msa = $dbh->prepare("CREATE TABLE if not exists msa (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
kept_positions text                   NOT NULL default ''       COMMENT 'Positions that have passed pre-processing filters in the alignment',
msa_path       tinytext               NOT NULL default ''       COMMENT 'Path to reach MSA files',
msa_length     smallint      unsigned NOT NULL default 0        COMMENT 'MSA length in amino acids/codons',
used_length    smallint      unsigned NOT NULL default 0        COMMENT 'MSA length really used by positive selection tool after trimming = sum of kept_positions',
PRIMARY KEY (id))
ENGINE = $engine");
$sth_msa->execute();
$sth_msa->finish();


# Create the branch table in selectome_? db
my $sth_branches = $dbh->prepare("CREATE TABLE if not exists branch (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
branch         smallint      unsigned NOT NULL default 0        COMMENT 'Branch localization in the subtree',
branch_name    varchar(23)            NOT NULL DEFAULT ''       COMMENT 'Name of the node in the subtree',
taxid          mediumint     unsigned NOT NULL default 0        COMMENT 'NCBI Taxon id for the branch',
duplication    tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is a duplication event on this branch? 1:yes, 0:no(speciation), 2:ambiguous',
bootstrap      tinyint       unsigned NOT NULL default 0        COMMENT 'Bootstrap value support for this branch (between 0 and 100)',
length         double                 NOT NULL default 0        COMMENT 'Length of this branch',
selected       tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Is it a positively selected branch ?',
PRIMARY KEY          (id, branch),
INDEX       `branch` (`taxid`,`id`,`duplication`))
ENGINE = $engine");
#loss           mediumtext             NOT NULL default ''       COMMENT 'List of lost taxa under this branch',
$sth_branches->execute();
$sth_branches->finish();


# Create the position table in selectome_? db
my $sth_pos = $dbh->prepare("CREATE TABLE if not exists position (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
branch         smallint      unsigned NOT NULL default 0        COMMENT 'Branch localization in the subtree',
position       smallint      unsigned NOT NULL default 0        COMMENT 'Position in the multiple sequence alignment (codon MSA)',
aa             char(1)                NOT NULL default ''       COMMENT 'Amino acid predicted by BEB at position with proba',
proba          decimal(4,3)  unsigned NOT NULL default 0.000    COMMENT 'Probability of the site/position do be under positive selection (BEB)',
validity       tinyint(1)    unsigned NOT NULL default 1        COMMENT 'Posterior validity of the predicted site/position',
PRIMARY KEY            (id, branch, position),
INDEX       `validity` (`id`, `branch`, `validity`))
ENGINE = $engine"); #FIXME Both index seem to share exactly the same root, maybe should use only one index !
$sth_pos->execute();
$sth_pos->finish();


# Create the selection table in selectome_? db
#FIXME Difficult to know the range of float used by lnL, lrt, pvalue, ... Thus, keep double type until better idea
my $sth_selection = $dbh->prepare("CREATE TABLE if not exists selection (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
selection_type varchar(10)            NOT NULL default ''       COMMENT 'Tool used to evaluate selection',
branch         smallint      unsigned NOT NULL default 0        COMMENT 'Branch localization in the subtree',
omega0         double        unsigned          default NULL     COMMENT 'Purifying selection (w0 < 1) on both foreground and background branches',
omega2         double        unsigned          default NULL     COMMENT 'Positive selection (w2 > 1) on the foreground branch',
p0             double        unsigned          default NULL     COMMENT 'Probability of belonging to the first  class of sites with omega < 1',
p1             double        unsigned          default NULL     COMMENT 'Probability of belonging to the second class of sites with omega = 1',
kappa          double        unsigned          default NULL     COMMENT 'Transition to transversion ratio',
alphac         double        unsigned          default NULL     COMMENT 'Alpha (shape) parameter of the Gamma distribution on the codon rate variation',
lnLH0          double                 NOT NULL default 0.0      COMMENT 'LogLikelihood value for the null hypothesis',
timeH0         mediumint     unsigned NOT NULL default 0        COMMENT 'Time used to run H0 in seconds',
lnLH1          double                 NOT NULL default 0.0      COMMENT 'LogLikelihood value for the positive selection hypothesis',
timeH1         mediumint     unsigned NOT NULL default 0        COMMENT 'Time used to run H1 in seconds',
lrt            double        unsigned NOT NULL default 0.0      COMMENT 'Corrected LRT with Iakov s method',
pvalue         double        unsigned NOT NULL default 1.0      COMMENT 'P-value for lrt: Chi2 test with 1 degree of freedom',
qvalue         double        unsigned NOT NULL default 1.0      COMMENT 'Q-value lrt: p-value with correction for multiple testing (FDR: False Discovery rate)',
selected       tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Validity of the predicted branch after FDR correction',
validity       tinyint(1)    unsigned NOT NULL default 1        COMMENT 'Posterior validity of the predicted branch',
PRIMARY KEY                   (id, branch),
INDEX       `selected_branch` (`id`,`branch`,`selected`,`validity`))
ENGINE = $engine");
$sth_selection->execute();
$sth_selection->finish();


# Create the warning table in selectome_? db
my $sth_warning = $dbh->prepare("CREATE TABLE if not exists warning (
id             mediumint     unsigned NOT NULL default 0        COMMENT 'Selectome subfamily internal identifier related to AC+taxon+subtree',
warning        varchar(100)           NOT NULL default ''       COMMENT 'Warning/Error message',
severity       tinyint(1)    unsigned NOT NULL default 0        COMMENT 'Warning/Error level',
PRIMARY KEY (id))
ENGINE = $engine");
$sth_warning->execute();
$sth_warning->finish();


# Create the tools table in selectome_? db
my $sth_tools = $dbh->prepare("CREATE TABLE if not exists tools (
name           varchar(25)           NOT NULL                  COMMENT 'Name of the tool',
version        varchar(14)           NOT NULL default ''       COMMENT 'Version of the tool',
description    varchar(140)          NOT NULL default ''       COMMENT 'Description of the tool',
url            varchar(100)          NOT NULL default ''       COMMENT 'URL of the tool',
PRIMARY KEY (name))
ENGINE = $engine");
$sth_tools->execute();
$sth_tools->finish();


# Create the datasource table in selectome_? db
my $sth_src = $dbh->prepare("CREATE TABLE if not exists datasource (
name           varchar(25)           NOT NULL                  COMMENT 'Name of the datasource',
version        varchar(13)           NOT NULL default ''       COMMENT 'Version of the datasource',
description    varchar(140)          NOT NULL default ''       COMMENT 'Description of the datasource',
url            varchar(100)          NOT NULL default ''       COMMENT 'URL of the datasource',
PRIMARY KEY (name))
ENGINE = $engine");
$sth_src->execute();
$sth_src->finish();

# Create the news table in selectome_?
#my $sth_news = $dbh->prepare("CREATE TABLE if not exists news (
#news           tinytext               NOT NULL default ''       COMMENT 'News message',
#date           timestamp              NOT NULL default CURRENT_TIMESTAMP,
#PRIMARY KEY (date))
#ENGINE = $engine");
#$sth_news->execute();
#$sth_news->finish();

$dbh->disconnect  or warn $dbh->errst;

exit 0;

