#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use Compress::Zlib;
use File::Slurp;

use Statistics::Distributions;

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::Utils;
my $famid_length = $Selectome::Utils::famid_length;


############################## Options management ##############################
my ($subtaxon, $label_leaves) = ('', 0);
GetOptions('subtaxon|taxon=s' => \$subtaxon, # Subtaxon to extract: Euteleostomi, ...
           'label_leaves'     => \$label_leaves,
           );

# Check arguments
error("Invalid options:\n\te.g.: $0 --taxon=Euteleostomi [--label_leaves]")  if ( $subtaxon !~ /[A-Z]/i );
$subtaxon = ucfirst($subtaxon);
my $acc = 'E'; #    Starting wih E*: e.g. EMGT00000000000001 ENSGT00390000017138


############################## Locate families to apply on #####################
# Get families which have successfully done computations
my @families = glob($acc.'*/'.$subtaxon.'/DONE*');
map { s{/DONE\.gz$}{}; s{/DONE$}{} } @families;
error('No data to analyze')  if ( !exists($families[0]) );

#FIXME Remove in dumps ac without mlc, or only H0.mlc. SHOULD BE checked in JOBS2CHECK, isn't it ???
#TODO  Put different dump creation in functions to be able to call them independently

#FIXME Store ctl, mlc, MSA and annotated trees
#FIXME Discard families with LRT significantly lower than 0 ????


############################## Check codeml/godon job completion #####################
print "\tCheck codeml/godon job completion\n";
my @jobs_to_check = glob($acc.'*/'.$subtaxon.'/*/*.H0.ctl');
my $jobs;
JOBS2CHECK:
for my $job (@jobs_to_check){
#    print "[$job]\n";
    my ($H0) = $job =~ /^(.+)\.ctl$/;
    my $H1   = $H0;
    $H1     =~ s{H0$}{H1};


    # H0 and H1 control files are there
    if ( !-e "$H0.ctl" || -z "$H0.ctl" ){
        error("[$H0.ctl] problem with fixed (H0) control file");
    }
    if ( !-e "$H1.ctl" || -z "$H1.ctl" ){
        error("[$H1.ctl] problem with non-fixed (H1) control file");
    }


    # H0 and H1 results files are there
    if ( !-e "$H0.mlc" || -z "$H0.mlc" ){
        error("[$H0.mlc] problem with H0 result file");
    }
    if ( !-e "$H1.mlc" || -z "$H1.mlc" ){
        error("[$H1.mlc] problem with H1 result file");
    }


    # Computations are completed
    my $completed_H0 = `tail -1 $H0.mlc | grep '^Time used:'`;
    error("[$H0.mlc] does not seem to be complete")  if ( !$completed_H0 );

    my $completed_H1 = `tail -1 $H1.mlc | grep '^Time used:'`;
    error("[$H1.mlc] does not seem to be complete")  if ( !$completed_H1 );


    # Fill valid $jobs to check later
    my ($id) = $H0 =~ /([^\/]+?)\.\d+\.H0$/;
    $jobs->{$id} = 1; # e.g. ENSGT00390000007084.Glires.002
}


my %selectome_id;


##############################   CREATE MySQL DUMPS   ##########################
##############################      subtree.txt       ##########################
##############################       branch.txt       ##########################
print "\tCREATE MySQL DUMPS for subtree & branch\n";
open(my $TREES,    '>', 'subtree.txt');
open(my $BRANCHES, '>', 'branch.txt');
for my $fam (@families){
    my @trees = glob($fam.'/*.nhx');
    if ( defined($trees[0]) ){
        TREE:
        for my $tree (@trees){
            my ($tree_name) = $tree =~ /\/([^\/]*)$/;
            my @tree_desc   = split(/\./, $tree_name);

            $selectome_id{"$tree_desc[0].$tree_desc[1].$tree_desc[2]"} = 1;
            next TREE  if ( !exists $jobs->{"$tree_desc[0].$tree_desc[1].$tree_desc[2]"} );

            ################## Read subtree ##################
            my $nhx_tree = read_file("$tree", chomp => 1);
            #TODO Need to fix when several times the same gene name used as label
            # see line 363 in TreeFam/treefam/nhx_plot.pm
            # e.g. EMGT00050000002610.1  or  ENSGT00390000007840.2.Primates
            print {$TREES} "$tree_desc[0].$tree_desc[1].$tree_desc[2]\t", $nhx_tree, "\t0\t\t\t0\ts\n";


            ################## branch ##################
            my @branches;
            if ( $label_leaves ){
                @branches = split (/[\),]/, $nhx_tree);
            }
            else {
                @branches = split (/\)/, $nhx_tree);
            }
            for my $branch__ ( @branches ){ # Split for internal and external branches
                if ( $branch__ =~ /\[&&NHX:/ && $branch__ =~ /===/ ){ # === should always be there, even for labeled leaves if any
                    # Check all fields independently, to get all of them
#                    my ($branch, $taxid, $duplication, $bootstrap, $length, $loss, $selected) = (0, '', 0, 0, 0, 'LLL', 'SSS');
                    my ($branch, $taxid, $duplication, $bootstrap, $length, $selected) = (0, '', 0, 0, 0, 0);

                    # TODO check with nwk2nhx fix for 0 branch length !!!
                    $branch      = $1  if ($branch__ =~ /==(\d+)=/); # Should have only 1 per loop after the split !
                    $taxid       = $1  if ($branch__ =~ /T=(\d+)/);

                    # By default, duplication is set to ambiguous: 1:yes, 0:no(speciation), 2:ambiguous
                    #FIXME ambiguous is linked to a duplication_confidence_score available in protein_tree_tag table as a value of the tag field, ambiguous if duplication_confidence_score==0
                    $duplication = $1  if ($branch__ =~ /D=(\w)/);
                    $duplication = $duplication eq 'Y' ? 1 : 0;
                    $bootstrap   = $1  if ($branch__ =~ /B=(\d+)/);
#                    $loss        = $1  if ($branch__ =~ /E=\$-([^:\]]+)/); #FIXME not directly in ensembl db

                    $length      = $1  if ($branch__ =~ /:([\d\.eE\-]*)\[&&NHX/);
                    $length      = remove_useless_0($length);


                    print {$BRANCHES} "$tree_desc[0].$tree_desc[1].$tree_desc[2]\t$branch\t$taxid\t$duplication\t$bootstrap\t$length\t$selected\n"
;#                        if ($branch ne '0' && $taxid ne '');
                    print "\t\tProblem with $tree_desc[0].$tree_desc[1].$tree_desc[2] tree [branch:$branch][taxon:$taxid]\n"  if ( $branch eq '0' || $taxid eq '' );
                }
            }
        }
    }
}
close $TREES;
close $BRANCHES;
sortUniq('subtree.txt');
sortUniq('branch.txt');



############################## position.txt ########################
##############################    msa.txt   ########################
print "\tCREATE MySQL DUMPS for position & msa\n";
open(my $POSITIONS, '>', 'position.txt');
for my $fam (@families){
    my @nonfixed_mlc = glob($fam.'/*/*[0-9].H1.mlc');
    if ( defined($nonfixed_mlc[0]) ){
        H1:
        for my $mlc (@nonfixed_mlc){
            my ($acc, $taxon, $subtree, $branch) = $mlc =~ /^.+\/(E\w+)\.(\w+)\.(\d+)\.(\d+)\.H1\.mlc$/;

            $selectome_id{"$acc.$taxon.$subtree"} = 1;
            next H1  if ( !exists $jobs->{"$acc.$taxon.$subtree"} );

            my @BEB_positions;
            open(my $BEB, '<', "$mlc");
            my $flag = 0;
            BEB:
            while(<$BEB>){
                last BEB   if ( /^The grid \(see ternary graph for p0-p1\)/ );

                if ( /^Bayes Empirical Bayes \(BEB\) analysis/ ){
                    $flag = 1;
                    next BEB; # Do not need that particular line
                }
                next BEB   if ( $flag == 0 );

                if ( /^\s+(\d+\s+[A-Z\-]\s+\d\.\d+)/ ){
                    push @BEB_positions, $1;
                }
#                else {
#                    print "\t\tProblem with $mlc positions: [$_]\n";
#                }
            }
            close $BEB;


            POS:
            for my $pos (@BEB_positions){
                next POS  if ($pos eq '');

                my ($aa_pos, $aa, $BEB_proba) = $pos =~ /(\d+)\s+([A-Z\-])\s+(\d\.\d+)/;
                $BEB_proba = remove_useless_0($BEB_proba);

                print {$POSITIONS} "$acc.$taxon.$subtree\t$branch\t$aa_pos\t$aa\t$BEB_proba\t1\n";
            }
        }
    }
}
close $POSITIONS;
sortUniq('position.txt');



############################## selection.txt ############################
print "\tCREATE MySQL DUMP  for selection\n";
open(my $SELECTION, '>', 'selection.txt');
for my $fam (@families){
    my @mlc = glob($fam.'/*/*.mlc');
    if ( defined($mlc[0]) ){
        my ($lnL_H0, $lnL_H1) = ('', '');
        my ($timeH0, $timeH1) = (0, 0);
        MLC:
        for my $mlc (@mlc){
            next MLC  if ( -z "$mlc" );

            open(my $MLC, '<', "$mlc");
            if ( $mlc =~ /H0\.mlc$/ ){
                H0:
                while(<$MLC>){
                    if ( /^lnL\(.+?\):\s*(-?[\d\.]+)\s+/ ){
                        $lnL_H0 = $1 || '@';
                        last H0;
                    }
                }

                $timeH0 = clean_time_used(`tail -1 $mlc | grep '^Time used:'`);
            }
            else{ # H1.mlc
                my ($omega0, $omega2, $omega2b) = (1.0, 1.0, 1.0);
                H1:
                while(<$MLC>){
                    if ( /^lnL\(.+?\):\s*(-?[\d\.]+)\s+/ ){
                        $lnL_H1 = $1 || '@';
                    }
                    elsif ( /^foreground w\s+(-?\d+\.\d+)\s+-?\d+\.\d+\s+(-?\d+\.\d+)\s+(-?\d+\.\d+)/ ){
                        $omega0  = $1; # Should not be negative but catch negative values anyway (also not allowed in db)
                        $omega2  = $2;
                        $omega2b = $3;
                        last H1;
                    }
                }

                $timeH1 = clean_time_used(`tail -1 $mlc | grep '^Time used:'`);

                my $lrt    = 2 * ($lnL_H1 - $lnL_H0);
                my $pvalue = Statistics::Distributions::chisqrprob(1, $lrt);
                $pvalue    = 1e-200  if ( $pvalue==0 ); # To avoid NaN qvalue when pvalue too close to 0
#TODO Iakov's lrt correction method
                $pvalue    = remove_useless_0($pvalue);
                $lnL_H0    = remove_useless_0($lnL_H0);
                $lnL_H1    = remove_useless_0($lnL_H1);
                $lrt       = remove_useless_0($lrt);

                my ($acc, $taxon, $subtree, $branch) = $mlc =~ /^.+\/(E\w+)\.(\w+)\.(\d+)\.(\d+)\.H1\.mlc$/;
                $selectome_id{"$acc.$taxon.$subtree"} = 1;
                if ( !exists $jobs->{"$acc.$taxon.$subtree"} ){
                    ($lnL_H0, $lnL_H1) = ('', '');
                    ($timeH0, $timeH1) = (0, 0);
                    next MLC;
                }

                warn "\tw0 has negative value ($omega0) for $acc.$taxon.$subtree $branch\n"                if ( $omega0 < 0 );
                warn "\tw2a & w2b are different ($omega2 != $omega2b) for $acc.$taxon.$subtree $branch\n"  if ( $omega2 != $omega2b );

                if ( $lrt < -2 ) { # == << 0 !?!
                    print "Need to resubmit codeml/godon: $acc.$taxon.$subtree\t$branch\t$omega0\t$omega2\t$lnL_H0\t$lnL_H1\t$lrt\t$pvalue\n";
                    #TODO Store everything required to allow resubmission of these branch hypotheses !!!
                }
    #TODO Add p0, p1 & p2a+p2b proportions  +  kappa
                print {$SELECTION} "$acc.$taxon.$subtree\t$branch\t$omega0\t$omega2\t$lnL_H0\t$timeH0\t$lnL_H1\t$timeH1\t$lrt\t$pvalue\n";

                ($lnL_H0, $lnL_H1) = ('', '');
            }
            close $MLC;
        }
    }
}
close $SELECTION;
sortUniq('selection.txt');


#p-value / q-value
open(my $SELECTIONORDERED, '<', 'selection.txt.sorted.uniq');
open(my $PVALUES,       '>', 'pvalues.txt');
while(<$SELECTIONORDERED>){
    my @fields = split('\t', $_);
    print {$PVALUES} "$fields[9]\n";
}
close $PVALUES;
close $SELECTIONORDERED;

Rscript();
my @qvalues = read_file('qvalues.txt', chomp => 1);
unlink 'pvalues.txt', 'qvalues.txt';

open(my $SELECTIONFINAL, '<', 'selection.txt.sorted.uniq');
open(my $SELECTIONPLUS,  '>', 'selection.plus.sorted.uniq');
while(<$SELECTIONFINAL>){
    if ( $_ =~ /^E\w+/){ # Ensembl gene tree id
        chomp($_);
        $qvalues[0] =~ s{ +}{}g;
        #FIXME Check this !!!!
        my @qval_bool = split(/\t/, $qvalues[0]);
        $qval_bool[0] = remove_useless_0($qval_bool[0]);
        print {$SELECTIONPLUS} $_."\t$qval_bool[0]\t$qval_bool[1]\t1\n"; # Add a field with 1 for the validity
        shift(@qvalues);
    }
}
undef @qvalues;
close $SELECTIONPLUS;
close $SELECTIONFINAL;
unlink 'selection.txt.sorted.uniq';



############################## warning.txt ###########################
if ( -s 'selectome_warning' ){
    print "\tCREATE MySQL DUMP  for warning\n";
    system("sort -k1 -k3 selectome_warning | grep '$subtaxon' | uniq  > warning.pre");
    my @warnings = read_file('warning.pre', chomp => 1);
    open(my $CLEANED,  '>', 'warning.txt');
    my ($family, $taxon, $subtree, $message, $severity) = ('', '', '', '', '');
    #FIXME do not loose highly severe warnings !
    if ( -s 'warning.pre' ){
        WARN:
        for my $warn (@warnings){
            my @fields = split(/\t/, $warn);
#            if ( $fields[0] eq $family && $fields[1] eq $taxon && $fields[2] == $subtree ){
#                next WARN;
#            }
#            elsif ( $fields[0] eq $family && $fields[1] eq $taxon && $fields[2] == 0 && $subtree == 1 ){
#                next WARN;
#            }
#            else {
                my $subfamily = sprintf("%0${famid_length}d", $fields[2]);
                print {$CLEANED} "$fields[0].$fields[1].$subfamily\t$fields[3]\t$fields[4]\n";
#            }
            ($family, $taxon, $subtree, $message, $severity) = @fields;
            $selectome_id{"$family.$taxon.$subfamily"} = 1;
        }
    }
    close $CLEANED;
    unlink 'warning.pre';
    sortUniq('warning.txt');
    unlink 'warning.txt';
}



############################## id.txt ################################
print "\tCREATE MySQL DUMP  for id\n";
open(my $ID, '>', 'id.txt');
ID:
for my $id ( sort keys(%selectome_id) ){
    my ($AC, $taxon, $subfamily) = split(/\./, $id);
    next ID  if ( $taxon ne $subtaxon );
    print {$ID} "$AC\t$taxon\t$subfamily\n";
}
close $ID;
sortUniq('id.txt');


################################################################################
exit 0;




sub sortUniq {
    my ($file) = @_;
    if ( -e "$file" ){
        system("sort $file | uniq -d")  if ( $file !~ /filter/ ); # Check for duplicates
        system("sort $file | uniq > $file.sorted.uniq");
    }
    return;
}

sub Rscript {
    open(my $R, '>', 'script.R');
    print {$R} "library(qvalue)\np<-scan(\"pvalues.txt\")\nqobj<-qvalue(p, pi0.method=\"bootstrap\", fdr.level=0.1, pfdr=TRUE)\nwrite.qvalue(qobj, file=\"qvalues.t\")\n";
    close $R;

    system("/usr/bin/env R --vanilla --quiet --slave --no-save  <script.R 2>&1 | grep -v 'Read'");
    unlink 'script.R';

    system("sed -n -e '6,\$p' qvalues.t | awk '{print \$2\"\\t\"\$3}' >qvalues.txt");
    unlink 'qvalues.t';
    return;
}

sub remove_useless_0 {
    my ($value) = @_;

    if ( $value !~ /[eE]/ ){ #Do not touch 1-e200 nbr
        $value =~ s{([^0])0+$}{$1};
        $value =~ s{\.$}{};
    }
    return $value;
}

sub clean_time_used {
    my ($time_used) = @_;
    chomp $time_used;
    $time_used =~ s{^Time used:\s+}{};

    my @time_fields = split(/:/, $time_used);
    my $time_in_seconds = 0;
    if ( exists $time_fields[2] ){
        $time_in_seconds = $time_fields[2] + ($time_fields[1] * 60) + ($time_fields[0] * 60 * 60);
    }
    elsif ( exists $time_fields[1] ){
        $time_in_seconds = $time_fields[1] + ($time_fields[0] * 60);
    }
    else {
        $time_in_seconds = $time_fields[0];
    }

    return $time_in_seconds;
}

sub error {
    my ($msg) = @_;

    print "\n\t$msg\n\n";
    exit 1;
}

