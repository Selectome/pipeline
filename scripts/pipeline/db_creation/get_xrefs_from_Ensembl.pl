#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use List::MoreUtils qw( uniq );
use LWP::Simple;
#use JSON::XS; # See http://blogs.perl.org/users/e_choroba/2018/03/numbers-and-strings-in-json.html
use Cpanel::JSON::XS;
use File::Slurp;

use Data::Dumper;


# Define arguments & their default value
my ($root, $selectome_raw_file_path) = ('*', '');
my ($prot_id, $fam_pattern)          = ('', '');
my ($verbose)                        = (0);
my %opts = ('prot=s'  => \$prot_id,                 # protein id to search in Ensembl to insert/update in the db
            'fam=s'   => \$fam_pattern,             # family pattern info
            'root=s'  => \$root,                    # tree_root_taxon
            'path=s'  => \$selectome_raw_file_path, # Selectome raw (NHX) root file path
            'verbose' => \$verbose,                 # verbose mode, do not insert/update in database
           );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || ($prot_id eq '' && $selectome_raw_file_path eq '') || ($prot_id ne '' && $selectome_raw_file_path ne '') ){
    print "\n\tInvalid or missing argument:
\te.g. $0  -path=<path where ENSGT folders are>  [-root=<tree root taxon>]
       OR
\t     $0  -prot=MGP_SPRETEiJ_P0029697 -fam=ENSGT00980000198571.Euteleostomi.005

\t-prot     protein id to search in Ensembl to insert/update in the db
\t-fam      family pattern info

\t-path     Selectome raw (NHX) root file path
\t-root     tree root taxon
\t-verbose  verbose mode, do not insert/update in database
\n";
    exit 1;
}


# EnsEMBL REST URLs
#doc: http://rest.ensembl.org/
#e.g. http://rest.ensembl.org/xrefs/id/ENSTGEP00000031995?content-type=application/json
my $ensembl_lookup = 'http://rest.ensembl.org/lookup/id/';
my $ensembl_xrefs  = 'http://rest.ensembl.org/xrefs/id/';
my $ensembl_type   = '?content-type=application/json';

# UniProt REST taxonomy URL
my $uniprot_taxonomy = 'https://www.uniprot.org/taxonomy/';
my $uniprot_type     = '.rdf';


my $family;   # Store family->gene->xrefs info
my $taxonomy; # Store node taxid
if ( $selectome_raw_file_path ){
    FAMILY:
    for my $file ( sort glob("$selectome_raw_file_path/ENSGT*/$root/*.nhx") ){
        my ($fam, $taxon_root, $subfam) = $file =~ /^.*\/(.+?)\.(.+?)\.(\d+).+?$/; #ENSGT00390000000002.Euteleostomi.001.nhx
        print "\t$fam.$taxon_root.$subfam\n";

        my $subfamily = $family->{$fam}->{$taxon_root}->{$subfam};
        NODE:
        for my $node ( grep { /T=/ } split(/[\(\),]/, read_file("$file", chomp=>1)) ){
            my ($peptide, $gene) = ('', '');
            my ($taxon) = $node =~ /T=(\d+)/;

            #Internal node
            if ( $node =~ /^:/ ){
                $taxonomy->{$taxon} = '_';
                next NODE;
            }
            #Terminal branch, i.e. gene/peptide
            elsif ( $node =~ /^(\w+):.*?G=(\w+)/ ){
                $peptide = $1;
                $gene    = $2;
            }
            else {
                die "\tInvalid tree element to process [$node]\n";
            }

            #Re-attach to the external ref
            $family->{$fam}->{$taxon_root}->{$subfam}->{$peptide} = get_info($subfamily, $peptide, $gene, $taxon);
        }
        sleep 2;
    }
}
elsif ( $prot_id ){
    my ($fam, $taxon_root, $subfam) = $fam_pattern =~ /^(.+?)\.(.+?)\.(\d+)$/; #ENSGT00980000198571.Euteleostomi.005
    print "\t$fam.$taxon_root.$subfam\n";
    my $subfamily = $family->{$fam}->{$taxon_root}->{$subfam};
    $family->{$fam}->{$taxon_root}->{$subfam}->{$prot_id} = get_info($subfamily, $prot_id, '', get_taxid_for_Mus_species($prot_id));
}



# Genes & xrefs
my $own_id_g = 0;
my $own_id_x = 0;
my $gene_table = '';#join("\t", '#id', qw(prot_id transcript_id gene_id gene_name description source taxid))."\n";
my $xref_table = '';#join("\t", '#id', qw(prot_id xref db_source))."\n";
for my $fam ( sort keys %$family ){
    for my $tax_root ( sort keys %{ $family->{$fam} } ){
        for my $sub ( sort keys %{ $family->{$fam}->{$tax_root} } ){
            for my $pep ( sort keys %{ $family->{$fam}->{$tax_root}->{$sub} }){
                $gene_table .= join("\t", $own_id_g++,
                                          "$fam.$tax_root.$sub",
                                          $pep,
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'transcript'}->{'id'},
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'id'},
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'name'}   || '',
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'description'},
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'source'} || '',
                                          0, #NOTE selected to be updated later in the db
                                          0, #FIXME branch should be taken from the NHX to ease linking with branch
                                          $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'species'},
                                   )."\n";
                my @xrefs;
                push @xrefs, keys %{ $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'xrefs'} }
                    if ( $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'gene'}->{'xrefs'} );
                push @xrefs, keys %{ $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'transcript'}->{'xrefs'} }
                    if ( $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'transcript'}->{'xrefs'} );
                push @xrefs, keys %{ $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'peptide'}->{'xrefs'} }
                    if ( $family->{$fam}->{$tax_root}->{$sub}->{$pep}->{'peptide'}->{'xrefs'} );
                for my $xf ( uniq sort @xrefs ){
                    my ($db_source, $xref) = split(':', $xf, 2);
                    $xref =~ s{\.\d+$}{}; #Without version number
                    $xref_table .= join("\t", $own_id_x++,
                                              "$fam.$tax_root.$sub",
                                              $pep,
                                              $xref,
                                              $db_source,
                                       )."\n";
                }
            }
        }
    }
}
undef $family;
append_file('gene.tsv', $gene_table);
undef $gene_table;
append_file('xref.tsv', $xref_table);
undef $xref_table;

# Taxonomy
my $taxonomy_table = '';#join("\t", '#taxid', qw(scientific_name common_name aliases is_a_species short_name ensembl_code uniprot_code))."\n";
TAXON:
for my $taxid ( sort keys %$taxonomy ){
    my $content = get("$uniprot_taxonomy$taxid$uniprot_type");
    if ( defined $content ){
        my @aliases;
        my ($scientific_name, $common_name, $is_a_species, $short_name, $uniprot_code) = ('', '', 0, '', '');
        if ( $content =~ m|https?://purl.uniprot.org/core/Species/| || $taxonomy->{$taxid} ne '_' ){
            $is_a_species = 1;
        }
        if ( $content =~ /<scientificName>(.+?)<\/scientificName>/ ){
            $scientific_name = $1;
            if ( $is_a_species ){
                $short_name = $scientific_name;
                $short_name =~ s{^(.)\S*\s+(...).*$}{$1$2};
            }
        }
        if ( $content =~ /<commonName>(.+?)<\/commonName>/ ){
            $common_name = $1;
        }
        if ( $content =~ /<mnemonic>(.+?)<\/mnemonic>/ ){
            $uniprot_code = $1;
        }
        while ( $content =~ /<otherName>(.+?)<\/otherName>/g ){
            push @aliases, $1;
        }
        if ( $taxid == 9598 ){
            push @aliases, 'chimp';
        }

        $taxonomy_table .= join("\t", $taxid,
                                      $scientific_name,
                                      $common_name,
                                      join('  ', uniq sort @aliases),
                                      $is_a_species,
                                      $short_name,
                                      ($taxonomy->{$taxid} ne '_' ? $taxonomy->{$taxid} : ''),
                                      $uniprot_code,
                               )."\n";
    }
    else {
        warn "Cannot get [$taxid] info\n";
        $taxonomy_table .= $taxid."\n";
    }
    sleep 0.12;
}
append_file('taxonomy.tsv', $taxonomy_table);

print "\n\tCheck if any duplicates in 'short_name' 'ensembl_code' 'uniprot_code' fields\n\n";
#NOTE "Canis lupus dingo" got assigned a fake uniprot_code (mnemonic code) to distinct from dog -> DINGO
exit 0;


sub get_info {
    my ($subfamily, $peptide, $gene, $taxon) = @_;

    #TODO will it work for insects?
    if ( $peptide =~ /^(.+?)_*P\d+$/ ){
        # Should work for human       e.g. ENSP00000311135       -> ENS
        # for other Ensembl prot id   e.g. ENSRNOP00000038631    -> ENSRNO
        # and weird Ensembl "strains" e.g. MGP_SPRETEiJ_P0073662 -> MGP_SPRETEiJ
        $taxonomy->{$taxon} = $1;
    }
    else {
        die "Weird protein identifier: [$peptide]\n";
    }

    $subfamily->{$peptide}->{'peptide'}->{'id'}    = $peptide;
    # Get peptide parent: transcript, then gene id
    $subfamily->{$peptide}->{'transcript'}->{'id'} = get_parent($peptide);
    $subfamily->{$peptide}->{'gene'}->{'id'}       = $gene || get_parent($subfamily->{$peptide}->{'transcript'}->{'id'});

    # Get name and description
    ($subfamily->{$peptide}->{'gene'}->{'name'}, $subfamily->{$peptide}->{'gene'}->{'description'}) =
        get_name($subfamily->{$peptide}->{'gene'}->{'id'});

    $subfamily->{$peptide}->{'species'} = $taxon;

    # Get xrefs
    $subfamily->{$peptide}->{'peptide'}->{'xrefs'}    = get_xrefs($peptide);
    $subfamily->{$peptide}->{'transcript'}->{'xrefs'} = get_xrefs($subfamily->{$peptide}->{'transcript'}->{'id'});
    $subfamily->{$peptide}->{'gene'}->{'xrefs'}       = get_xrefs($subfamily->{$peptide}->{'gene'}->{'id'});

    if ( $subfamily->{$peptide}->{'gene'}->{'description'} =~ /^(.+?) +\[Source:(\w+).*?;Acc:(.+?)\]$/ ){
        $subfamily->{$peptide}->{'gene'}->{'description'} = $1;
        $subfamily->{$peptide}->{'gene'}->{'source'}      = "$2:$3";
    }

    #Clean HGNC/MGI ref
    if ( $subfamily->{$peptide}->{'gene'}->{'source'} ){
        $subfamily->{$peptide}->{'gene'}->{'source'} =~ s{^HGNC:HGNC:}{HGNC:};
        $subfamily->{$peptide}->{'gene'}->{'source'} =~ s{^VGNC:VGNC:}{VGNC:};
        $subfamily->{$peptide}->{'gene'}->{'source'} =~ s{^MGI:MGI:}{MGI:};
    }

    return $subfamily->{$peptide};
}


sub get_parent {
    my ($id) = @_;
    my $parent = '';

    return $parent  if ( !$id );

    my $content = get("$ensembl_lookup$id$ensembl_type");
    if ( defined $content ){
        my $json = decode_json( $content );
        $parent = $json->{'Parent'};
    }

    return $parent;
}

sub get_name {
    my ($id) = @_;
    my $name = '';
    my $desc = '';

    return ($name, $desc)  if ( !$id );

    my $content = get("$ensembl_lookup$id$ensembl_type");
    if ( defined $content ){
        my $json = decode_json( $content );
        $name = $json->{'display_name'} // '';
        $desc = $json->{'description'}  // '';
    }

    if ( $name =~ / \[provisional:(.+?)\]$/ ){ #e.g. XB5961369 [provisional:plpp3]
        $name = $1;
    }
    $name =~ s{ +\[provisional\]$}{}; #e.g. XB1010847 [provisional]
    $name =~ s|\[|\{|g; $name =~ s|\]|\}|g; #e.g. su(w[a])

    return ($name, $desc);
}

sub get_xrefs {
    my ($id) = @_;
    my $xrefs;

    return $xrefs  if ( !$id );

    my $content = get("$ensembl_xrefs$id$ensembl_type");
    if ( defined $content ){
        my $json = decode_json( $content );
        XREF:
        for my $xref ( @{ $json }){
            next XREF  if ( $xref->{'dbname'} eq 'ArrayExpress' ); #May refer to another species ensembl id
            next XREF  if ( $xref->{'dbname'} =~ /^Ens_Ga_/ );     #May refer to another species ensembl id
            next XREF  if ( $xref->{'dbname'} eq 'KEGG_Enzyme' );  #Weird EC number syntax
            next XREF  if ( $xref->{'dbname'} eq 'MEROPS' );       #Useful?

            my $id = $xref->{'dbname'} eq 'GO'         ? $xref->{'display_id'}
                   : $xref->{'dbname'} =~ /^ZFIN_ID_/  ? 'ZFIN_ID:'.$xref->{'display_id'}
                   : $xref->{'dbname'} =~ /^Uniprot\// ? 'Uniprot:'.$xref->{'display_id'}
                   : $xref->{'dbname'} =~ /^RefSeq_/   ? 'RefSeq:'.$xref->{'display_id'}
                   : $xref->{'dbname'} =~ /^HGNC_/     ? 'HGNC:'.$xref->{'display_id'}
                   : $xref->{'dbname'} =~ /^Vega_/     ? 'Vega:'.$xref->{'display_id'}
                   :                                     $xref->{'dbname'}.':'.$xref->{'display_id'};
            $xrefs->{ $id } = {};
            if ( scalar @{ $xref->{'synonyms'} } > 0 ){
                $xrefs->{ $id }->{'synonyms'} = join(';', @{ $xref->{'synonyms'} });
            }
        }
    }

    return $xrefs;
}

sub trim {
    my ($string) = @_;

    $string =~ s{^\s+}{};
    $string =~ s{\s+$}{};

    return $string;
}

sub get_taxid_for_Mus_species {
    my ($prot_id) = @_;

    my %species = ('MGP_CAROLIEiJ' => 10089,
                   'MGP_PahariEiJ' => 10093,
                   'MGP_SPRETEiJ'  => 10096,
                  );

    my @match = grep { $prot_id =~ /^$_/ } keys %species;
    if ( scalar @match == 1 ){
        return $species{ $match[0] };
    }
    else {
        die "Issue with the species for [$prot_id]\n";
    }
}

