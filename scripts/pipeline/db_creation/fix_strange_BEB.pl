#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use Statistics::Descriptive;

use lib '.';
use DB;



my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates [debug]\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates [debug]\n\n";
my $debug  = defined $ARGV[2] ? 1 : 0;

use constant THRESHOLD => 0.20; # 20% after looking at nbr_of_BEB_predicted/MSA_used_length distribution


#### Get useful subfamilies that have passed filters
my $dbh     = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_msa = $dbh->prepare('SELECT m.id, m.kept_positions, m.msa_length FROM msa m, id i WHERE i.id=m.id AND i.taxon=? ORDER BY m.id');
$sth_msa->execute($phylum)  or die $sth_msa->errstr;
my $msa_ref = $sth_msa->fetchall_arrayref;
$sth_msa->finish();


####
print "ID\tBEBnbr\tMSAsize\tperc\tmode\tblength\tbootstrap\ttaxon\tisDuplic\tomega0\tomega2\tLRT\tpval\tqval\tlnLH0\tlnLH1\ttimeH0\ttimeH1\n"  if ( $debug );
if ( exists($msa_ref->[0]) ){
    MSA:
    for my $row (@$msa_ref){
        my ($id, $kept_positions, $msa_length) = @$row;
        next MSA  if ( $kept_positions eq ':' );

        my $blocks_length = 0;
        for my $block ( split(/,/, $kept_positions) ){
            my ($start, $end) = $block =~ /^(\d+):(\d+)$/;
            $blocks_length += $end - $start + 1;
        }


        # Get BEB positions (almost) everywhere on branch
        my $sth_beb = $dbh->prepare('SELECT COUNT(branch), branch FROM position WHERE id=? GROUP BY branch');
        $sth_beb->execute($id)  or die $sth_beb->errstr;
        my $beb_ref = $sth_beb->fetchall_arrayref;
        $sth_beb->finish();

        BEB:
        for my $row (@$beb_ref){
            my ($count, $branch) = @$row;
#            print "\t", $branch, "\t", ($count/$blocks_length)*100, "\n";
            next BEB  if ( ($count/$blocks_length) < THRESHOLD );
            warn "\tShort MSA: $id\n"  if ( $msa_length < 50 );

            # Get positions with problematic BEB detection
            my $sth_pos = $dbh->prepare('SELECT branch, position, proba FROM position WHERE id=? AND branch=?');
            $sth_pos->execute($id, $branch)  or die $sth_pos->errstr;
            my $pos_ref = $sth_pos->fetchall_arrayref;
            $sth_pos->finish();

            if ( $debug ){
                my $sth_info = $dbh->prepare('SELECT CONCAT(i.ac, ".", i.taxon, ".", i.subfamily, ".", c.branch), b.length, b.bootstrap, t.scientific_name, b.duplication, c.omega0, c.omega2, c.lrt, c.pvalue, c.qvalue, c.lnLH0, c.lnLH1, c.timeH0, c.timeH1 FROM branch b, id i, selection c, taxonomy t WHERE i.id=c.id AND i.id=b.id AND b.branch=c.branch AND b.taxid=t.taxid AND i.id=? AND c.branch=?');
                $sth_info->execute($id, $branch)  or die $sth_info->errstr;
                my $info_ref = $sth_info->fetchall_arrayref;
                $sth_info->finish();


                print "$info_ref->[0]->[0]\t$count\t$blocks_length\t", ($count/$blocks_length)*100, "\t";
                #TODO Use mode to get peak distribution then remove mode + .1
#                print join(' ', map {$_->[2]} @$pos_ref), "\n";
                my $stat = Statistics::Descriptive::Full->new();
                $stat->add_data( map {$_->[2]} @$pos_ref);
                print '', ($stat->mode() || 'NA'), "\t$info_ref->[0]->[1]\t$info_ref->[0]->[2]\t$info_ref->[0]->[3]\t$info_ref->[0]->[4]\t$info_ref->[0]->[5]\t$info_ref->[0]->[6]\t$info_ref->[0]->[7]\t$info_ref->[0]->[8]\t$info_ref->[0]->[9]\t$info_ref->[0]->[10]\t$info_ref->[0]->[11]\t$info_ref->[0]->[12]\t$info_ref->[0]->[13]\n";
#                print $stat->quantile(3), "\n";
#                print $stat->percentile(THRESHOLD), "\n";
            }
            else {
            #TODO Invalidate all BEB positions for such branch
                my $sth_UPpos = $dbh->prepare('UPDATE position SET validity=0 WHERE id=? AND branch=?');
                $sth_UPpos->execute($id, $branch)  or die $sth_UPpos->errstr;
                $sth_UPpos->finish();
            }
        }
    }
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;

