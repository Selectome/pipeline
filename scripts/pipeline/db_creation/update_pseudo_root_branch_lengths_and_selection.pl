#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use List::Util qw(max);

use lib '.';
use DB;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;

#use Node;

my $dbname = $ARGV[0] || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";

$| = 1;

# db connect
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)
                or die $DBI::errstr;

# get all the trees
my $sql_trees = 'SELECT s.id, s.tree FROM subtree s ORDER BY id';
my $sth_trees = $dbh->prepare($sql_trees);
$sth_trees->execute()  or die $sth_trees->errstr;

#SQL
my $sql                   = 'SELECT s.* FROM selection s WHERE s.id = ? AND s.branch = ?';
my $sth_selection         = $dbh->prepare($sql);
my $selected_insert_sql   = 'INSERT INTO selection VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
my $sth_selection_insert  = $dbh->prepare($selected_insert_sql);
my $sth_branch_update_sql = 'UPDATE branch SET length = ?, selected = ? WHERE id = ? AND branch IN (?, 1)';
my $sth_branch_update     = $dbh->prepare($sth_branch_update_sql);
my $node2gene_update_sql  = 'UPDATE node2gene SET is_root = 1 WHERE id = ? AND branch IN (?, 1)';
my $sth_node2gene_update  = $dbh->prepare($node2gene_update_sql);
my $sth_updateTree        = $dbh->prepare('UPDATE subtree SET tree = ? WHERE id = ?');

while ( my ($st_id, $st_nhx) = ($sth_trees->fetchrow_array) ){
    print "updated tree $st_id\n";
    my $forest = Bio::Phylo::IO->parse(
        -string          => $st_nhx,
        -format          => 'nhx',
        -keep_whitespace => 1,
    );

    TREE:
    for my $tree ( @{ $forest->get_entities } ){
        my $bn = 0;
        # access nodes in $tree
        for my $node ( @{ $tree->get_entities } ){
            if ( $bn == 0 ){ #root
                $node->set_meta_object('nhx:ROOT' => 1);

                # get the pseudo roots
                my @children = @{ $node->get_children };
                my ($branch_length, $pvalue);
                my $selected = 0;

                for ( my $i = 1; $i >= 0; $i-- ){ # get branch with values first
                    my $child = $children[$i];
                    my $child_branch_id = $child->get_meta_object('nhx:ND');

                    if ( $child_branch_id > 1 ){ # the child in selection table
                        $branch_length = $child->get_branch_length;
                        $branch_length = $branch_length / 2;

                        $sth_selection->execute($st_id, $child_branch_id)  or die $sth_selection->errstr;
                        while ( (my @row) = ($sth_selection->fetchrow_array) ){
                            $selected = $row[16];
                            $pvalue   = $row[14];
                            $row[2] = 1; #replace branch id with that of other child which is always 1;

                            unless ( $child->is_Leaf ){ # skip if pseudo root is a leaf
                                $sth_selection_insert->execute(@row)  or warn $sth_selection_insert->errstr;
                            }
                        }
                        # update branch length of children in db
                        $sth_branch_update->execute($branch_length, $selected, $st_id, $child_branch_id)  or die $sth_branch_update->errstr;

                        # update branch lengths in tree
                        $child->set_branch_length($branch_length);

                        # update node2gene
                        $sth_node2gene_update->execute($st_id, $child_branch_id)  or die $sth_node2gene_update->errstr;
                    }
                    elsif ( $child_branch_id == 1 ){   #update tree for other pseudo root
                        $child->set_branch_length($branch_length);
                        $child->set_meta_object('nhx:PVAL' => $pvalue);
                        if ( $selected == 1 ){
                            $child->set_meta_object('nhx:SEL' => 'Y');
                        }
                    }
                }
            }
            $bn++;
        }

        my $returnTree = Bio::Phylo::IO->unparse(
            '-phylo'  => $tree,
            '-format' => 'nhx',
        );
        $sth_updateTree->execute($returnTree, $st_id)  or die $sth_updateTree->errstr;
    }
}

$sth_selection->finish();
$sth_selection_insert->finish();
$sth_branch_update->finish();
$sth_node2gene_update->finish();
$sth_updateTree->finish();
$sth_trees->finish();

$dbh->disconnect;

exit 0;

