#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use Bio::Phylo::IO qw(parse unparse);
use List::Util qw(any);

use lib '.';
use DB;


my $dbname = $ARGV[0]  // die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98  OR  selectome_v07_timema1\n\n";

# Shut off the print buffer to have immediat display of the progress
$| = 1;


# % complete stuff initialization
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)
                or die $DBI::errstr;

my $branch_qr  = 'SELECT b.id, b.branch, b.selected FROM branch b';
my $sth_branch = $dbh->prepare($branch_qr);
$sth_branch->execute()  or die $sth_branch->errstr;


my $tree;
# Get all branch info in a hash
while ( my ($st_id, $st_branch, $st_selected) = ($sth_branch->fetchrow_array) ){
    $tree->{$st_id}->{$st_branch} = $st_selected;
}


my $sth_getGeneBranch  = $dbh->prepare('SELECT g.branch, g.prot_id FROM gene g    WHERE g.id = ? AND g.branch != 0');
my $sth_getTree        = $dbh->prepare('SELECT s.tree   FROM subtree s WHERE s.id = ?');
my $sth_upGeneSelected = $dbh->prepare('UPDATE gene g SET g.selected=1 WHERE g.id = ? AND g.branch = ?');
for my $st_id ( sort { $a <=> $b } keys %$tree ){
    # Get gene info
    my $gene;
    $sth_getGeneBranch->execute($st_id)  or die $sth_getGeneBranch->errstr;
    while ( my ($st_branch, $st_prot_id) = ($sth_getGeneBranch->fetchrow_array) ){
        $gene->{$st_branch} = $st_prot_id;
    }

    $sth_getTree->execute($st_id)  or die $sth_getTree->errstr;
    my @res = $sth_getTree->fetchrow_array;
    my $nhx = Bio::Phylo::IO->parse(
                 '-string' => $res[0],
                 '-format' => 'nhx',
              )->first;
    my $root_id      = 0;#NOTE Fixed for us to 0
    my $pseudo1      = splice(@{ $nhx->get_root->get_internals() }, 0, 1); #Get first pseudoroot
    my @pseudo_roots = @{ $pseudo1->get_sisters }; #Get all pseudoroot1 sisters => all pseudoroots. Should work for more than 2 sisters!
    my @leaves       = @{ $nhx->get_terminals() };
    print scalar @leaves, ' leaves: ', join(';', map { $_->get_meta_object('nhx:ND') } @leaves), "\n";
    print "TREE [$st_id] [pseudo:", join(';', map { $_->get_meta_object('nhx:ND') } @pseudo_roots), "]\n";

    # Reasoning about ancestral selection
    GENE:
    for my $gene_node ( sort { $a <=> $b } @leaves ){
        my $gene_branch = $gene_node->get_meta_object('nhx:ND');
        my $status = 'none';
        # Whole (rooted) tree under selection, with its pseudoroots
        if ( (any { exists $tree->{$st_id}->{$_} && $tree->{$st_id}->{$_} == 1 } map { $_->get_meta_object('nhx:ND') } @pseudo_roots)
            || (exists $tree->{$st_id}->{$root_id} && $tree->{$st_id}->{$root_id} == 1 ) ){
            $sth_upGeneSelected->execute($st_id, $gene_branch);
            $status = 'root';
        }
        # gene is itself under selection
        elsif ( exists $tree->{$st_id}->{$gene_branch} && $tree->{$st_id}->{$gene_branch} == 1 ){
            $sth_upGeneSelected->execute($st_id, $gene_branch);
            $status = 'gene';
        }
        # any parent branch is under selection
        else {
            if ( any { exists $tree->{$st_id}->{$_} && $tree->{$st_id}->{$_} == 1 } map { defined $_->get_meta_object('nhx:ND') && $_->get_meta_object('nhx:ND') } @{ $gene_node->get_ancestors() } ){
                $sth_upGeneSelected->execute($st_id, $gene_branch);
                $status = 'internal';
            }
        }
        print "DONE [$st_id] [$gene_branch] [$status] [".$gene_node->id."]\n";
    }
}
$sth_getGeneBranch->finish;
$sth_getTree->finish;
$sth_upGeneSelected->finish;

$dbh->disconnect;

exit 0;

