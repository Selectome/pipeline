#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use List::Util qw(max);

use lib '.';
use DB;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;

#use Node;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";


$| = 1;


# db connect
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)
                or die $DBI::errstr;

# get all the trees
my $sql_trees = 'SELECT s.id, s.tree FROM subtree s';
my $sth_trees = $dbh->prepare($sql_trees);
$sth_trees->execute()  or die $sth_trees->errstr;

my $sql_insert = 'INSERT INTO node2gene VALUES (?, ?, ?, ?, ?)';
my $insert     = $dbh->prepare($sql_insert);
my $sql_prot   = 'SELECT prot_id FROM gene WHERE gene_id = ?';
my $sth_prot   = $dbh->prepare($sql_prot);


while ( my ($st_id, $st_nhx) = ($sth_trees->fetchrow_array) ){
    my $forest =  Bio::Phylo::IO->parse(
        -string          => $st_nhx,
        -format          => 'nhx',
        -keep_whitespace => 1,
    );
    my $bn = 0;
    for my $tree ( @{ $forest->get_entities } ){
        # access nodes in $tree
        for my $node ( @{ $tree->get_entities } ){
            my $is_root   = 0;
            my $is_a_leaf = 0;
            if ( $node->is_Leaf ){
                $is_a_leaf = 1;
                my $gene = $node->get_meta_object('nhx:G');
                $sth_prot->execute($gene)  or die $sth_prot->errstr;
                while ( my $prot = ($sth_prot->fetchrow_array) ){
                    $insert->execute($st_id, $bn, $is_a_leaf, $is_root, $prot)  or warn $insert->errstr;
                }
            }
            else {
                if ( $bn == 0 ){
                    $is_root = 2;
                }
                if ( $node->get_meta_object('nhx:ND') > 1 ){
                    $is_root = 1;
                }
                for my $child ( $node->get_all_Descendents ){
                    if ( $child->is_Leaf ){
                        my $gene = $child->get_meta_object('nhx:G');
                        my $nd = $child->get_meta_object('nhx:ND');
                        $sth_prot->execute($gene)  or die $sth_prot->errstr;
                        while ( my $prot = ($sth_prot->fetchrow_array) ){
                            $insert->execute($st_id, $bn, $is_a_leaf, $is_root, $prot)  or warn $insert->errstr;
                        }
                    }
                }
            }
            $bn++;
        }
    }
}
$sth_trees->finish();
$insert->finish();
$sth_prot->finish();

$dbh->disconnect;

exit 0;

