#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;

use lib '.';
use DB;



my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $aa_extension        = 'aa.fas.REF';
my $nt_extension        = 'nt.fas.ORI';
my $aa_masked_extension = 'aa.fas.newmafft.ScoreMerged';
my $nt_masked_extension = 'nt.fas.preTrimAl';


#### Get useful subfamilies that have passed filters
my $dbh     = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_msa = $dbh->prepare('SELECT m.id, m.msa_path, m.kept_positions FROM msa m, id i WHERE i.id=m.id AND i.taxon = ? ORDER BY m.id');
$sth_msa->execute($phylum)  or die $sth_msa->errstr;
my $msa_ref = $sth_msa->fetchall_arrayref;
$sth_msa->finish();


#### Temp dir for FTP archives
my ($basename) = $dbname =~ /^(selectome_v\d+)_/;
$basename      = ucfirst $basename;
my @dirs       = ('aa_masked', 'aa_unmasked', 'nt_masked', 'nt_unmasked', 'Trees_Newick', 'Trees_NHX');


####
if ( exists($msa_ref->[0]) ){
    mkdir "${basename}_$phylum-$_"  for ( @dirs );
    MSA:
    for my $row (@$msa_ref){
        my ($id, $msa_path, $kept) = @$row;
        next MSA  if ( $kept eq ':' );

        my @msa;
        push @msa, (glob "$msa_path.$aa_extension*")[0]; # Take only the 1st found
        push @msa, (glob "$msa_path.$nt_extension*")[0];
        push @msa, (glob "$msa_path.$aa_masked_extension*")[0];
        push @msa, (glob "$msa_path.$nt_masked_extension*")[0];

        if ( -s "$msa[0]" && -s "$msa[1]" && -s "$msa[2]" && -s "$msa[3]" ){
#            print "[$msa_path]\n";
#            system("ls -l $msa[0] $msa[1]");

            my ($final_path) = $msa_path =~ /^(.+)\//;
            my ($family)     = $msa_path =~ /^(.+?)\//;
            # Create destination path tree
            system("mkdir -p $DB::wwwtmp/$final_path/");

            # Copy MSAs to destination path
            system("cp -f $msa[0] $msa[1] $msa[2] $msa[3]  $DB::wwwtmp/$final_path/");

            # Gunzip MSAs if required
            my @gzipped = glob "$DB::wwwtmp/$final_path/*.gz";
            if ( exists($gzipped[0]) ){
                GZIPPED:
                for my $gzip ( @gzipped ){
                    system("gunzip --force $gzip");
                }
                $msa[0] =~ s/\.gz$//;
                $msa[1] =~ s/\.gz$//;
                $msa[2] =~ s/\.gz$//;
                $msa[3] =~ s/\.gz$//;
            }


            # Rename files with an unproper extension
            if ( $msa[0] !~ /\.aa.fas$/ ){
                system("mv -f $DB::wwwtmp/$msa[0] $DB::wwwtmp/$msa_path.aa.fas");
            }
            if ( $msa[1] !~ /\.nt.fas$/ ){
                system("mv -f $DB::wwwtmp/$msa[1] $DB::wwwtmp/$msa_path.nt.fas");
            }
            if ( $msa[2] !~ /\.aa_masked.fas$/ ){
                system("mv -f $DB::wwwtmp/$msa[2] $DB::wwwtmp/$msa_path.aa_masked.fas");
            }
            if ( $msa[3] !~ /\.nt_masked.fas$/ ){
                system("mv -f $DB::wwwtmp/$msa[3] $DB::wwwtmp/$msa_path.nt_masked.fas");
            }

            # Chmod for apache user
            system("chmod a+w $DB::wwwtmp/$final_path/");
#            print "$msa_path\n";


            # Copy NHX and Newick trees there also
            my $sth_tree = $dbh->prepare('SELECT tree FROM subtree WHERE id = ?');
            $sth_tree->execute($id)  or die $sth_tree->errstr;
            my $tree_ref = $sth_tree->fetchall_arrayref;
            $sth_tree->finish();


            my %symbols;
            if ( exists($tree_ref->[0]) ){
                open(my $NHX, '>', "$DB::wwwtmp/$msa_path.nhx");
                print {$NHX} $tree_ref->[0]->[0], "\n";
                close $NHX;
                system("cp $DB::wwwtmp/$msa_path.nhx  ${basename}_$phylum-Trees_NHX/");

                my $Newick = $tree_ref->[0]->[0];
                $Newick   =~ s{\[&&NHX:.*?\]([,\);])}{$1}g;
                open(my $NWK, '>', "$DB::wwwtmp/$msa_path.nwk");
                print {$NWK} $Newick, "\n";
                close $NWK;
                system("cp $DB::wwwtmp/$msa_path.nwk  ${basename}_$phylum-Trees_Newick/");

                my @nodes = grep { /&&NHX:/ } split(']', $tree_ref->[0]->[0]);
                for my $node ( @nodes ){
                    # Is a leaf with a gene_id and under selection directly or in the past
                    if ( $node =~ /:PR=(\w+)/ ){ # Because ensembl id only
                        my $prot_id         = $1;
                        my ($transcript_id) = $node =~ /:TR=(\w+)/;
                        my ($gene_id)       = $node =~ /:G=(\w+)/;
                        my ($taxid)         = $node =~ /:T=(\d+)/;
                        if ( $node =~ /[\(,]([^:\)\(\[\]=,]+):/ ){
                            $symbols{$prot_id} = $1." PROTID=$prot_id TRANSID=$transcript_id GENEID=$gene_id TAXID=$taxid";
                        }
                    }
                }
            }
            else {
                print "No tree for $id\n";
                exit 3;
            }


            # Need to substitute leaf labels by geneName_species if they have changed !!!
            my $nhx_file_aa        = read_file( "$DB::wwwtmp/$msa_path.aa.fas" );
            my $nhx_file_nt        = read_file( "$DB::wwwtmp/$msa_path.nt.fas" );
            my $nhx_file_aa_masked = read_file( "$DB::wwwtmp/$msa_path.aa_masked.fas" );
            my $nhx_file_nt_masked = read_file( "$DB::wwwtmp/$msa_path.nt_masked.fas" );
            for my $prot ( keys(%symbols) ){
                $nhx_file_aa        =~ s/>$prot/>$symbols{$prot}/;
                $nhx_file_nt        =~ s/>$prot/>$symbols{$prot}/;
                $nhx_file_aa_masked =~ s/>$prot/>$symbols{$prot}/;
                $nhx_file_nt_masked =~ s/>$prot/>$symbols{$prot}/;
            }
            write_file("$DB::wwwtmp/$msa_path.aa.fas",        $nhx_file_aa);
            system("cp $DB::wwwtmp/$msa_path.aa.fas           ${basename}_$phylum-aa_unmasked/");
            write_file("$DB::wwwtmp/$msa_path.nt.fas",        $nhx_file_nt);
            system("cp $DB::wwwtmp/$msa_path.nt.fas           ${basename}_$phylum-nt_unmasked/");
            write_file("$DB::wwwtmp/$msa_path.aa_masked.fas", $nhx_file_aa_masked);
            system("cp $DB::wwwtmp/$msa_path.aa_masked.fas    ${basename}_$phylum-aa_masked/");
            write_file("$DB::wwwtmp/$msa_path.nt_masked.fas", $nhx_file_nt_masked);
            system("cp $DB::wwwtmp/$msa_path.nt_masked.fas    ${basename}_$phylum-nt_masked/");
        }
        else {
            print "Problem in [$msa_path]\n";
            exit 2;
        }
    }
}
$dbh->disconnect  or warn $dbh->errst;


#### Archive dir for FTP
for ( @dirs ){
    system("zip -r -9  ${basename}_$phylum-$_.zip  ${basename}_$phylum-$_/");
    system("rm -Rf ${basename}_$phylum-$_/")  if ( -s "${basename}_$phylum-$_.zip");
}

exit 0;

