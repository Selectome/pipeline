#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use File::Slurp;
use DBI;
use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome id]\n\n";
my $id     = $ARGV[1]  || 0;
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $outdir = '/var/SELECTOME';
my ($db1, $db2) = ('', '');
if ( $dbname =~ /timema/ ){
    $db1 = 'timema';
    $db2 = $db1;
}
elsif ( $dbname =~ /euteleostomi/ ){
     $db1 = 'euteleostomi';
     $db2 = 'Euteleostomi';
}

# SQL
my $sql_ids = 'SELECT id, AC, subfamily FROM id ORDER BY id';
if ( $id && $id =~ /^\d+$/ ){
    $sql_ids = 'SELECT id, AC, subfamily FROM id WHERE id=?';
}
my $sth_ids = $dbh->prepare($sql_ids);
my $sql_tree = 'SELECT s.tree FROM subtree s WHERE s.id=?';
my $sth_tree = $dbh->prepare($sql_tree);
my $NHX_gene_info      = qq{SELECT CONCAT('G=', g.gene_id), CONCAT('PR=', g.prot_id),
                                CONCAT('GN=', g.gene_name, '_', IF(t.uniprot_code != '', t.uniprot_code, IF(t.ensembl_code != '', t.ensembl_code, t.short_name)))
                            FROM gene g, taxonomy t
                            WHERE g.id=? AND g.branch=? AND g.taxid=t.taxid};
my $NHX_branch_info    = qq{SELECT CONCAT('T=', b.taxid), CONCAT('S=', REPLACE(t.scientific_name, ' ', '_')),
                                CONCAT('B=', b.bootstrap), CONCAT('D=', IF(b.duplication = 1, 'Y', 'N'))
                            FROM branch b, taxonomy t
                            WHERE b.id=? AND b.branch=? AND b.taxid=t.taxid};
my $NHX_selection_info = qq{SELECT CONCAT('PVAL=', s.pvalue), CONCAT('SEL=', IF(s.selected = 1, 'Y', 'N'))
                            FROM selection s
                            WHERE s.id=? AND s.branch=?};
my $sth_gene      = $dbh->prepare($NHX_gene_info);
my $sth_branch    = $dbh->prepare($NHX_branch_info);
my $sth_selection = $dbh->prepare($NHX_selection_info);


# Get all ids
if ( $id && $id =~ /^\d+$/ ){
    $sth_ids->execute($id)  or die $sth_ids->errstr;
}
else {
    $sth_ids->execute()     or die $sth_ids->errstr;
}
while ( my ($st_id, $st_AC, $st_subfamily) = ($sth_ids->fetchrow_array) ){
    # Get NHX tree
    $sth_tree->execute($st_id)  or die $sth_tree->errstr;
    my ($NHX_tree) = $sth_tree->fetchrow_array;

    #NOTE to avoid id without trees (e.g. warnings)
    next  if ( ! $NHX_tree );

    #NOTE some NHX tree don't have the ROOT tag and that breaks the tree stucture at the end
    if ( $NHX_tree =~ /\[([^\[\]]+)\];$/ && $1 !~ /ROOT=1/ ){
        warn "Missing ROOT tag for $st_id\n";
        $NHX_tree =~ s{\];$}{:ROOT=1\];};
    }

    my $new_NHX = '';
    for my $str ( split(/\]/, $NHX_tree) ){
#        print "$str\n";
        if ( $str =~ /ROOT=1/ ){
            $str =~ s{:PR=na}{};
            $str =~ s{:SEL=N}{};
            $str =~ s{:PVAL=na}{};
            $str =~ s{DD}{D};
            $new_NHX .= $str.']';
        }
        elsif ( $str =~ /.*?ND=(\d+).*?$/ ){
            my $branch   = $1;
#            print "$NHX_tags [$branch]\n";
            $sth_gene->execute($st_id, $branch)       or die $sth_gene->errstr;
            $sth_branch->execute($st_id, $branch)     or die $sth_branch->errstr;
            $sth_selection->execute($st_id, $branch)  or die $sth_selection->errstr;
            my @gene      = map { s/:/_/g; $_ } $sth_gene->fetchrow_array; # Replace gene ids/names containing ":" by "_"
            @gene         = map { s| +\[.*?\]||;        $_ } @gene;        # Remove part of gene names like "XB5813854 [provisional:ankrd52]" or "XB1010847 [provisional]"
            @gene         = map { s| +\(1 of many\)||;  $_ } @gene;        # Remove part of gene names like " (1 of many)"
            @gene         = map { s|\(|\{|g; s|\)|\}|g; $_ } @gene;        # Replace "(" and ")" characters left
            die "Gene name contain ']' character: {@gene}\nFix it in the database first => gene_name AND tree!\n"  if ( grep { /\]/ } @gene );
            my @branch    = $sth_branch->fetchrow_array;
            my @selection = $sth_selection->fetchrow_array;

            # Remove empty GN
            #NOTE assuming no gene name start with _
            @gene = grep { !/^GN=_/ } @gene;

            my $is_a_leaf = exists $gene[0] ? 1 : 0;
            # if gene do not show B= (even if B=0)
            if ( $is_a_leaf || $dbname =~ /timema/ ){
                @branch = grep { !/^B=/ } @branch;
            }
            #NOTE if @selection is empty, it means it has not been tested, so no tag!

            # Fix leaf with na or "empty" as label
            if ( $is_a_leaf && (grep { /^PR=/ } @gene) && ($str =~ /[,\(]na:/i || $str =~ /[,\(]:/) ){
#                print "\t$str\n";
                my @PR = grep { /^PR=/ } @gene;
                my ($pr) = $PR[0] =~ /^PR=(.+)$/;
                if ( $pr ){
                    $str =~ s{([,\(])na:}{$1$pr:}i  if ( $str =~ /[,\(]na:/i );
                    $str =~ s{([,\(]):}{$1$pr:}     if ( $str =~ /[,\(]:/ );
                }
            }
            elsif ( $is_a_leaf && ($str =~ /[,\(]na:/i || $str =~ /[,\(]:/) ){
                warn "Leaf without label AND no PR tag in [$st_id]\n";
            }

#            print join(':', "[&&NHX:ND=$branch", @gene, @branch, @selection).']', "\n";
            $str =~ s{\[&&NHX:.*$}{};
            $new_NHX .= $str.join(':', "[&&NHX:ND=$branch", @gene, @branch, @selection).']';
        }
        else {
            $new_NHX .= $str;
        }
    }
#    print $NHX_tree, "\n", $new_NHX, "\n";
    my $subfamily = sprintf('%03i', $st_subfamily);
    write_file("$outdir/$db1/$st_AC/$db2/$st_AC.$db2.$subfamily.NHX", $new_NHX);
    #TODO do it on all families and check for any errors, e.g. na: =: ...
}

$sth_ids->finish;
$sth_tree->finish;
$sth_gene->finish;
$sth_branch->finish;
$sth_selection->finish;

$dbh->disconnect  or warn $dbh->errst;
exit 0;

