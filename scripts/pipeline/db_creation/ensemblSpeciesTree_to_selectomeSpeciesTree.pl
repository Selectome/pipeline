#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use LWP::Simple;
use List::Util qw(first);

use lib './pipeline';
use DB;

my $ncbi_taxo_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&retmode=txt&rettype=xml&tool=selectome&email=selectome@unil.ch&id=';


die "\n\tMissing ensembl species file name: $0 ensembl_species_tree selectome_v07\n\n"  if ( ! $ARGV[0] || !-e "$ARGV[0]" );
# e.g.  /usr/local/ensembl/ensembl-compara/scripts/pipeline/species_tree_blength.nh
my $ensembl_species_tree = read_file("$ARGV[0]", chomp => 1);
$ensembl_species_tree   =~ s{\?}{}g; # Protect against some strange char in file
$ensembl_species_tree   =~ s{\*}{}g;


my $dbname = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 ensembl_species_tree selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


#TODO In the future (API 74?), a meth in the ensembl API should return the annotated species tree directly

# Fill leaf node labels/taxid
my %selectome_taxonomy;
#TODO Need to remove manually unused taxa !!!
my $sth_taxa = $dbh->prepare('SELECT taxid, scientific_name FROM taxonomy');
$sth_taxa->execute()  or die $sth_taxa->errstr;
TAXON:
while ( my ($taxid, $species) = ($sth_taxa->fetchrow_array) ){
    $selectome_taxonomy{$species} = $taxid;
    $species =~ s{ }{_}g;

    $ensembl_species_tree =~ s{([\(,]$species:\d+(\.\d+)?)}{$1\[&&NHX:T=$taxid\]}g;
}
$sth_taxa->finish();
$dbh->disconnect  or warn $dbh->errst;


# Mark all internal nodes
#$ensembl_species_tree =~ s{([\)\(,]:\d+(\.\d+)?)}{$1\[&&NHX:T=\]}g;


# Fill internal node labels/taxid
#TODO Sometimes need to add internal NHX tag + name to force what we expect
# e.g. on Euteleostomi tree, there are several multifurcating nodes that are
#      translated in bifurcating nodes by duplicating the same taxon name (like Sauria or Laurasiatheria)
#      The TreeFam API, to view trees in png, works badly if several nodes have the same name.
# ===> So, prefer multifurcating nodes, with distinct node name (This will also make the tree clearer).
my %stored_lineages;
my @int_node = split(/\):/, $ensembl_species_tree);
shift @int_node; # Remove 1st array element because before the first ): match, before the first internal node
INTNODE:
for my $int_node ( @int_node ){
    my $node = quotemeta $int_node; # Need to protect this variable because contains ()[]...
    my ($from_start) = $ensembl_species_tree =~ /^(.*?\)):$node/;

    my ($left, $right);
    my ($lineage_left, $lineage_right);
    # ^.*?Homo_sapiens:0.0067[&&NHX:T=9606],Pan_troglodytes:0.006667[&&NHX:T=9598])
    # ^.*?Homininae:0.00225[&&NHX:T=207598],Gorilla_gorilla:0.008825[&&NHX:T=9593])
    if ( $from_start =~ /^.*\w+:[\d\.]+\[&&NHX:T=(\d+)\],\w+:[\d\.]+\[&&NHX:T=(\d+)\]\)$/ ){
        ($left, $right) = ($1, $2);
        $lineage_left  = $stored_lineages{$left}  || parse_taxo_xml($left, 1);
        $lineage_right = $stored_lineages{$right} || parse_taxo_xml($right, 1);
    }
    # ^.*(Microcebus_murinus:0.092749[&&NHX:T=30608],Otolemur_garnettii:0.129725[&&NHX:T=30611])Strepsirrhini:0.035463[&&NHX:T=376911])
    elsif ( $from_start =~ /^.*,\w+:[\d\.]+\[&&NHX:T=(\d+)\]\)\w+:[\d\.]+\[&&NHX:T=(\d+)\]\)$/ ){
        ($left, $right) = ($1, $2);
        $lineage_left  = $stored_lineages{$left}  || parse_taxo_xml($left);
        $lineage_right = $stored_lineages{$right} || parse_taxo_xml($right);
    }
    else {
        next INTNODE;
    }

#    print "\t", $left, "|", $right, "\n";

    # Compare lineage lists
    if ( !defined $lineage_left || !defined $lineage_right ){
        next INTNODE;
    }
    my @shared_lineage;
    LIN:
    for my $lin ( @$lineage_left ){
        if ( first { $_ eq $lin } @$lineage_right ){
            next LIN  if ( !exists $selectome_taxonomy{$lin} );
            push @shared_lineage, $lin, $selectome_taxonomy{$lin};
            last LIN;
        }
    }

    # Replace in ensembl tree
    my ($length) = $int_node =~ /^([\d\.]+)/;
    $ensembl_species_tree =~ s{\):$length}{\)$shared_lineage[0]:$length\[&&NHX:T=$shared_lineage[1]\]};
}


#TODO Exclude species out of the taxonomic range we use
# (e.g. remove  Caenorhabditis elegans  for Euteleostomi Selectome build


# Check if not labeled node(s) remains
if ( $ensembl_species_tree =~ /(\w*:[\d\.]+[,\(\)]\w*)/ ){
    warn "\nNot labeled node(s) remains: [$1]\n\n";
}
if ( $ensembl_species_tree =~ /[,\(\)](\w*):[\d\.]+/ ){
    my $missing_name = $1 || '';
    if ( $missing_name eq '' || $missing_name !~ /^\w+$/ ){
        warn "\nNot named node(s) remains: [$1]\n\n";
    }
}


print "$ensembl_species_tree\n";

exit 0;


sub parse_taxo_xml {
    my ($taxid, $dont_exclude_itself) = @_;

    my $content = get($ncbi_taxo_url.$taxid);

    $content =~ s{\s+}{}g; # Remove \n & other spacers !
    my ($sc_name) = $content =~ /^.*?<ScientificName>(.+?)<\/ScientificName>/  if ( $dont_exclude_itself ); # Catch only the 1st ScientificName, the one related to the taxid
    my ($lineage) = $content =~ /^.*<Lineage>(.+?)<\/Lineage>.*$/;
    my @lineages  = reverse split(';', $lineage);
    @lineages     = ($sc_name, @lineages)  if ( $dont_exclude_itself );

    $stored_lineages{$taxid} = \@lineages;

    return \@lineages;
}

