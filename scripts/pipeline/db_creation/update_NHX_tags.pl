#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;
use DBI;
use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

&update_nhx_tags($dbh);

$dbh->disconnect  or warn $dbh->errst;
exit 0;

sub update_nhx_tags {
    my ($dbh) = @_;

    #get all the trees
    my $sql_trees = 'SELECT s.id, s.tree FROM subtree s ORDER BY id';
    my $sth_trees = $dbh->prepare($sql_trees);
    $sth_trees->execute()  or die $sth_trees->errstr;
    my $sth_updateTree = $dbh->prepare('UPDATE subtree SET tree = ? WHERE id = ?');

    while ( my ($st_id, $st_nhx) = ($sth_trees->fetchrow_array) ){
        my $forest =  Bio::Phylo::IO->parse(
            -string => $st_nhx,
            -format => 'nhx',
            -keep_whitespace => 1,
        );

        foreach my $tree ( @{ $forest->get_entities } ) {
        # access nodes in $tree
            my $returnTree;

            foreach my $node ( @{ $tree->get_entities } ){
                my $bn = $node->get_meta_object('nhx:ND');
                # updated SEL and PVAL tags
                my $sql_selected = "SELECT s.selected, s.pvalue FROM selection s WHERE s.branch = ? AND s.id = ?";
                my $sth_selected = $dbh->prepare($sql_selected);
                $sth_selected->execute($bn, $st_id)  or die $sth_selected->errstr;
                while ( my ($selected, $pvalue) = ($sth_selected->fetchrow_array) ){
                    if ($selected == 1) {
                        $node->set_meta_object('nhx:SEL' => 'Y');
                    }
                    $node->set_meta_object('nhx:PVAL'=> $pvalue);
                }
                # update S tag
                my $tax_id    = $node->get_meta_object('nhx:T') || '-1';
                my $sql_tax   = 'SELECT scientific_name FROM taxonomy WHERE taxid = ?';
                my $sth_tax   = $dbh->prepare($sql_tax);
                $sth_tax->execute($tax_id);

                while (my $scientific_name = ($sth_tax->fetchrow_array) ) {
                    $scientific_name =~ tr/ /_/;
                    $node->set_meta_object('nhx:S' =>$scientific_name);
                }

                # update branch numbers in gene table
                my $gene_id = $node->get_meta_object('nhx:G');
                    if ($gene_id){
                        my $sth_updateGene = $dbh->prepare('UPDATE gene SET branch = ? WHERE id = ? AND gene_id = ?');
                        $sth_updateGene->execute($bn, $st_id, $gene_id);

                        # update pr nhx tag in tree
                        my $sql_protein = "SELECT prot_id FROM gene WHERE id = ? AND gene_id = ?";
                        my $sth_protein = $dbh->prepare($sql_protein);
                        $sth_protein->execute($st_id, $gene_id)  or die $sth_protein->errstr;
                        while ( my ($prot_id) = ($sth_protein->fetchrow_array) ){
                            $node->set_meta_object('nhx:PR'  => $prot_id);
                            $node->set_meta_object('nhx:GN'  => $prot_id);
                        }

                        # update GN tag if uniprot code
                        my $sql_gene_name = "SELECT t.uniprot_code, g.gene_name FROM gene g, taxonomy t WHERE g.id = ? AND g.gene_id = ? AND g.taxid = t.taxid";
                        my $sth_gene_name = $dbh->prepare($sql_gene_name);
                        $sth_gene_name->execute($st_id, $gene_id)  or die $sth_gene_name->errstr;
                        while ( my ($uniprot_code, $gene_name) = ($sth_gene_name->fetchrow_array) ) {
                            unless (($uniprot_code eq '') || ($gene_name eq '')) {
                                #TODO fixes for : and [] chars!
                                $node->set_meta_object('nhx:GN'  => $gene_name."_".$uniprot_code);
                            }
                        }
                }

                # update duplication, bootstrap and tax in branch table
                my $dupl = $node->get_meta_object('nhx:D') || 'U';
                my $db_dupl = 0;

                if ($dupl eq 'Y') {
                   $db_dupl = 1;
                }
                my $bootstrap = $node->get_meta_object('nhx:B') || '-1';

                my $sql_branch = "UPDATE branch SET taxid=?, duplication = ?, bootstrap = ? where id = ? and branch = ?";
                my $sth_updateBranch = $dbh->prepare($sql_branch);
                $sth_updateBranch->execute($tax_id, $db_dupl, $bootstrap, $st_id, $bn);

                # add ROOT tag
                if ($node->is_root) {
                      $node->set_meta_object('nhx:ROOT'=> 1);
                }

            }
            $returnTree = Bio::Phylo::IO->unparse(
                '-phylo'  => $tree,
                '-format' => 'nhx',
                );
            $sth_updateTree->execute($returnTree, $st_id);
        }
    }
}

