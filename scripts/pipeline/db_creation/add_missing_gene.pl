#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Bio::EnsEMBL::Utils::Exception;
use Bio::EnsEMBL::Registry;

use DBI;
use lib '.';
use DB;
use Selectome::EnsemblDB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome id]\n\n";
my $id     = $ARGV[1]  || 0;
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $connection_param = Selectome::EnsemblDB::get_connection_parameters('ensembl');
############################## EnsEMBL connection ##############################
local $| = 1; # $OUTPUT_AUTOFLUSH => forces a flush after every write or print

my $reg = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => 1,  # for debug purpose !
);


# SQL
my $sql_ids = 'SELECT id, AC, subfamily FROM id ORDER BY id';
if ( $id && $id =~ /^\d+$/ ){
    $sql_ids = 'SELECT id, AC, subfamily FROM id WHERE id=?';
}
my $sth_ids = $dbh->prepare($sql_ids);
my $missing_gene_info = qq{SELECT b.id, b.branch, b.branch_name, b.taxid
                           FROM branch b
                           WHERE b.branch_name !='' AND b.id=? AND NOT EXISTS (SELECT g.id FROM gene g WHERE g.id=b.id AND b.branch=g.branch)};
my $sth_missing       = $dbh->prepare($missing_gene_info);
my $ins_gene          = qq{INSERT INTO gene (id, prot_id, transcript_id, gene_id, gene_name, description, source, taxid, branch) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)};
my $sth_ins_gene      = $dbh->prepare($ins_gene);
my $ins_xref          = qq{INSERT INTO xref (id, prot_id, xref, db_source) VALUES (?, ?, ?, ?)};
my $sth_ins_xref      = $dbh->prepare($ins_xref);
#|own_id | id | prot_id            | transcript_id      | gene_id            | gene_name | description                                                      | source                  | selected | branch | taxid | table_name |
#|   1   |  1 | ENSACIP00000014826 | ENSACIT00000015222 | ENSACIG00000011492 | apbb2b    | amyloid beta (A4) precursor protein-binding, family B, member 2b | ZFIN:ZDB-GENE-090313-73 |        0 |     50 | 61819 |            |
#| own_id | id | prot_id            | xref    | db_source | table_name |
#|      1 |  1 | ENSACIP00000014826 | 0001540 | GO        |            |


# Get ids
if ( $id && $id =~ /^\d+$/ ){
    $sth_ids->execute($id)  or die $sth_ids->errstr;
}
else {
    $sth_ids->execute()     or die $sth_ids->errstr;
}
while ( my ($st_id, $st_AC, $st_subfamily) = ($sth_ids->fetchrow_array) ){
    $sth_missing->execute($st_id)   or die $sth_missing->errstr;
    while ( my ($st_id, $st_branch, $st_branch_name, $st_taxid) = ($sth_missing->fetchrow_array) ){
#        print "[$st_id][$st_AC][$st_subfamily][$st_branch][$st_branch_name][$st_taxid]\n";
        #NOTE missing genes look to be mostly deprecated ones, so the REST Ensembl API cannot deal with them
        #because only for the last Ensembl release. So need to use the Perl API of the specific version
        # Get gene info based on identifier
        my ( $species, $object_type, $db_type ) = $reg->get_species_and_object_type( $st_branch_name );
        my $prot_adaptor                        = $reg->get_adaptor( $species, $db_type, $object_type );
        my $prot_desc        = $prot_adaptor->fetch_by_stable_id($st_branch_name);

        my $protein_id       = $prot_desc->display_id();
        if ( $protein_id ne $st_branch_name ){
            warn "ERROR with prot ids: [$protein_id] [$st_branch_name]\n";
            next;
        }
        my $transcript_id    = $prot_desc->transcript->display_id();
        my $gene_id          = $prot_desc->transcript->get_Gene->display_id();
        my $gene_description = $prot_desc->transcript->get_Gene->description()   || '';
        my $gene_name        = $prot_desc->transcript->get_Gene->external_name() || '';
#        print "[$protein_id] [$transcript_id] [$gene_id] [$gene_name] [$gene_description] t[$st_taxid] i[$st_id] b[$st_branch]\n";

        # Cleaning
        #Remove HTML tags
        $gene_name =~ s{<[^>]+?>}{}g;
        #NOTE $gene_description may contain source with an xref
        #e.g. lysophosphatidylglycerol acyltransferase 1 [Source:Xenbase;Acc:XB-GENE-985546]
        $gene_description =~ s{  +}{ }g;
        $gene_description =~ s{[\.,]+ *\[Source:}{ \[Source:};
        my $gene_desc = $gene_description;
        $gene_desc   =~ s{ \[Source:.*$}{};
        my ($gene_db_src, $gene_xref) = $gene_description =~ /\[Source:\s*(.+?)\s*;Acc:\s*(.+?)\s*\]/;
        if ( $gene_db_src ){
            $gene_db_src = $gene_db_src eq 'HGNC Symbol'          ? 'HGNC'
                         : $gene_db_src eq 'NCBI gene'            ? 'EntrezGene'
                         : $gene_db_src eq 'UniProtKB/TrEMBL'     ? 'TrEMBL'
                         : $gene_db_src eq 'UniProtKB/Swiss-Prot' ? 'Swiss-Prot'
                         : $gene_db_src eq 'RefSeq DNA'           ? 'RefSeq'
                         : $gene_db_src eq 'RefSeq peptide'       ? 'RefSeq'
                         : $gene_db_src eq 'FlyBase gene name'    ? 'FlyBase'
                         # + miRBase
                         :                                        $gene_db_src;
        }
        my $gene_source = $gene_xref && $gene_db_src ? "$gene_db_src:$gene_xref" : '';
        print "[$protein_id] [$transcript_id] [$gene_id] [$gene_name] [$gene_desc] [$gene_source] t[$st_taxid] i[$st_id] b[$st_branch]\n";

        # DB insertion
        $sth_ins_gene->execute($st_id, $protein_id, $transcript_id, $gene_id, $gene_name, $gene_desc, $gene_source, $st_taxid, $st_branch)  or die $sth_ins_gene->errstr;
        if ( $gene_xref && $gene_db_src ){
            $sth_ins_xref->execute($st_id, $protein_id, $gene_xref, $gene_db_src)  or warn $sth_ins_xref->errstr;
        }
    }
}

$reg->clear();
$sth_ids->finish;
$sth_missing->finish;
$sth_ins_gene->finish;
$sth_ins_xref->finish;

$dbh->disconnect  or warn $dbh->errst;
exit 0;

