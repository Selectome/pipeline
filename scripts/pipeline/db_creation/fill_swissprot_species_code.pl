#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use LWP::Simple;
use List::MoreUtils qw( uniq );

use DBI;
use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";


#### Get species taxid
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_species = $dbh->prepare('SELECT taxid, scientific_name, common_name, aliases  FROM taxonomy WHERE is_a_species=1');
$sth_species->execute()  or die $sth_species->errstr;
my $species_ref = $sth_species->fetchall_arrayref;
$sth_species->finish();


####
if ( exists($species_ref->[0]) ){
    for my $row (@$species_ref){
        my ($taxid, $scientific_name, $common_name, $aliases) = @$row;

        print "$taxid\t$scientific_name\t$common_name\t$aliases\n";
        my $content = get("http://www.uniprot.org/taxonomy/$taxid.rdf");
        die 'Couldn\'t get it!'  unless defined $content;


        # Get SwissProt species code
        my $swissprot_code = '';
        if ( $content =~ /<mnemonic>(\w+)<\/mnemonic>/ ){
            $swissprot_code = $1;
            print "[$swissprot_code]\n";
        }


        # Get species name here also because could have been changed since the ensembl release
        my $scientificName = '';
        if ( $content =~ /<scientificName>(.+?)<\/scientificName>/ ){
            $scientificName = $1;
        }
        die 'Invalid species canonical name'  if ( $scientificName eq '' );


        # Deal with new aliases
        my %aliases = map { $_ => 1 } uc($common_name), split('  ', uc($aliases)), uc($scientific_name);
        my @new_aliases;
        if ( $content =~ /<commonName>(.+?)<\/commonName>/ ){
            my $alias = $1;
            push @new_aliases, $alias  if ( ! exists($aliases{ uc($alias) }) );
        }
        while( $content =~ /<otherName>(.+?)<\/otherName>/g ){
            my $alias = $1;
            # Missing synonyms
            if ( $taxid == 9598 ){
                my $extra = 'chimp';
                if ( ! exists($aliases{ uc($extra) }) ){
                    $aliases{ uc($extra) } = 1;
                    push @new_aliases, $extra;
                }
            }
            elsif ( $taxid == 43179 ){
                my $extra = 'Ictidomys tridecemlineatus';
                if ( ! exists($aliases{ uc($extra) }) ){
                    $aliases{ uc($extra) } = 1;
                    push @new_aliases, $extra;
                }
            }
            push @new_aliases, $alias  if ( ! exists($aliases{ uc($alias) }) );
        }
        while( $content =~ /<synonym>(.+?)<\/synonym>/g ){
            my $alias = $1;
            push @new_aliases, $alias  if ( ! exists($aliases{ uc($alias) }) );
        }
        #TODO Check alpaga: "Vicugna pacos" is the right species name! --> 30538
        #TODO Add missing synonyms for couple Metatheria/Marsupialia   -->  9263

        #TODO Move missing synonyms out of (after) the 2 while loops

        my @all_new_aliases = uniq (split('  ', $aliases), @new_aliases);
        my $all_aliases = join('  ', sort @all_new_aliases);
        $all_aliases    =~ s{^  }{};
        $all_aliases    =~ s{&amp;}{&}g;
        print "\t\t$all_aliases\n";


        # Update taxonomy table
        my $sth_swcode = $dbh->prepare('UPDATE taxonomy SET scientific_name=?, aliases=?, uniprot_code=? WHERE taxid=?');
        $sth_swcode->execute($scientificName, $all_aliases, $swissprot_code, $taxid)  or die $sth_swcode->errstr;
        $sth_swcode->finish();
    }
}


$dbh->disconnect or warn $dbh->errst;
exit 0;

