#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use List::Util qw(sum any);
use Sort::Versions;

use lib '.'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome_id]\n\n";
my $idd    = $ARGV[1]  || 0;

# disable print buffer
$| = 1;

my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $sql_genes = 'SELECT id, gene_name FROM gene';
if ( $idd && $idd =~ /^\d+$/ ){
    $sql_genes = "SELECT id, gene_name FROM gene WHERE id=$idd";
}
my $sth_genes = $dbh->prepare($sql_genes);
$sth_genes->execute()  or die $sth_genes->errstr;
my $genes;
while ( my ($tree_id, $gene_name) = ($sth_genes->fetchrow_array) ){
    push @{ $genes->{$tree_id} }, $gene_name;
}
$sth_genes->finish();


my $up_symbol = $dbh->prepare('UPDATE subtree SET subname = ? WHERE id = ?');
for my $tree_id ( keys %$genes ){
    my %counts    = ();
    my $total_sum = 0;
    my $symbol    = '';
    my @remove_regexps = (qr/^ENS\w+\d+/,          # Ensembl id
                          qr/^[XNAYZ][TPRM]_\d/,   # RefSeq id
                          qr/^[A-Z0-9]{7,}\.\d+$/, # GenBank/EMBL id
                          qr/\@/,                  # Protection for gene anme with : like is ZFIN
                          qr/\d+Rik$/,             # Some Riken id
                          qr/^G[A-Z]\d{5}$/,       # Drosophila basic gene ids
                          qr/^CG\d{4,5}$/,         # Drosophila basic gene ids 2
                         );
    for my $symbol ( @{ $genes->{$tree_id} } ){
        if ( any { $symbol =~ m/$_/ } @remove_regexps ){
            $symbol = 'TOREMOVE';
        }
        # remove last underscore stuff part (useless)
        $symbol =~ s{_.*$}{};
        # remove underscore xxxx-x-of-x
        $symbol =~ s{(.*[^-])-+\d+-of-\d+-+$}{$1}i;
        $symbol = uc($symbol);
        unless ( $symbol eq 'TOREMOVE' || $symbol eq '' ){
            if ( exists($counts{$symbol}) ){
                $counts{$symbol}++;
                $total_sum++;
            }
            else {
                $counts{$symbol} = 1;
                $total_sum++;
            }
        }
    }
    if ( %counts ){
        my $mean_count = sum( values(%counts)) / length(values(%counts) );
        for my $key ( keys(%counts) ){
            my $perc = ($counts{$key} * 100 / $total_sum);
            if ( $perc > 10 && $counts{$key} >= 1 ){
                $symbol .= "$key/";
            }
        }
        chop($symbol);
    }

    $symbol =~ s{(LOC\d+)}{zzzz$1}g; # to put LOC... at the end
    my @all_symbols = split(/\//, $symbol);
    @all_symbols    = sort { versioncmp($a, $b) } @all_symbols;
    $symbol         = join('/', @all_symbols);
    $symbol =~ s{zzzz(LOC\d+)}{$1}g;

    $symbol =~ s{C(\d+)ORF(\d+)}{C$1orf$2}g;


    $up_symbol->execute($symbol, $tree_id)  or die $up_symbol->errstr;
    print "ID: $tree_id\t[$symbol]\tlength: ", length($symbol), "\n";
}
$up_symbol->finish();
$dbh->disconnect  or warn $dbh->errst;

exit 0;

