#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;

my $dbname       = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 warning_file\n\n";
my $warning_file = $ARGV[1]  || die "\n\tWarning file is missing, e.g. $0 selectome_v07 warning_file\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $warning_insert = "INSERT INTO warning VALUES (?, ?, ?)";
my $sth = $dbh->prepare($warning_insert) or die "Prepare failed: " . $dbh->errstr();

my $id_insert  = "INSERT INTO id VALUES (?, ?, ?, ?)";
my $sth_id     = $dbh->prepare($id_insert) or die "Prepare failed: " . $dbh->errstr();

my $max_id_sql     = "SELECT max(id) FROM id";
my $sth_max_id =  $dbh->prepare($max_id_sql) or die "Prepare failed: " . $dbh->errstr();
$sth_max_id->execute();
my $max_id  = ($sth_max_id->fetchrow_array);

open my $fh, "<", $warning_file or die $!;

while (<$fh>) {
    my ($ac, $taxon, $fam, $warning, $severity)  = split /\t/;
    # get id
    my $sql_id = 'SELECT id FROM id WHERE AC = ? and subfamily= ?';
    my $sth_checkid = $dbh->prepare($sql_id);
    $sth_checkid->execute($ac, $fam)  or die $sth_checkid->errstr;

    if ( my ($id) = ($sth_checkid->fetchrow_array) ) {
        print "$ac.$fam already in db. warning - $warning\n";
    }
    else {
        $max_id ++;
        $sth_id->execute($max_id,$ac,$taxon,$fam);
        $sth->execute($max_id,$warning,$severity);
    }
}
close $fh;

exit 0;

