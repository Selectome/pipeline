#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Bio::TreeIO;
use Bio::AlignIO;
use Bio::Phylo::IO;
use Bio::Phylo::Util::CONSTANT;
use DBI;
use lib '.';
use DB;
#use Tree;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07\n\n";
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my @tables = ('subtree', 'branch', 'selection','position','msa');

#my @tables = ('');

foreach my $table (@tables){
    next if ($table eq "");
    my $query;
    if ($table eq 'subtree') {
        $query = "INSERT INTO $table VALUES (?, ?, ?, ?, ?, ?, ?)";
    }
    elsif ($table eq 'branch') {
         $query = "INSERT INTO $table VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    }
    elsif ($table eq 'selection'){
        $query = "INSERT INTO $table VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }
    elsif ($table eq 'position') {
        $query = "INSERT INTO $table VALUES (?, ?, ?, ?, ?, ?)";
    }
    elsif (($table eq 'node2gene')  || ($table eq 'msa')) {
        $query = "INSERT INTO $table VALUES (?, ?, ?, ?, ?)";
    }
    elsif ($table eq 'id') {
        $query = "INSERT INTO $table VALUES (?, ?, ?, ?)";
    }
    my $sth = $dbh->prepare($query) or die "Prepare failed: " . $dbh->errstr();
    open my $fh, "<", "$table.txt" or die $!;

    while (<$fh>)
    {
        my @values = split;
        $sth->execute(@values);
    }
    close $fh;
}
# update selected columns

my @updates = (
    "UPDATE selection set selected = 1 where qvalue <=0.05",
    "UPDATE branch b, selection s set b.selected = 1 where b.id=s.id and b.branch=s.branch and s.selected = 1",
    "UPDATE subtree t, selection s set t.selected = 1 where s.id=t.id and s.selected = 1"
);
foreach my $update (@updates) {
    my $sth = $dbh->prepare($update) or die "Prepare failed: " . $dbh->errstr();
    #$sth->execute();
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;
