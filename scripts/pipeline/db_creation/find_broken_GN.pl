#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98  [selectome id]\n\n";
my $id     = $ARGV[1]  || 0;
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;


my $sql_trees  = 'SELECT s.id, g.prot_id, g.gene_name, s.tree, t.uniprot_code, t.ensembl_code FROM gene g, subtree s, taxonomy t WHERE g.id=s.id AND g.taxid=t.taxid';
if ( $id && $id =~ /^\d+$/ ){
    $sql_trees = 'SELECT s.id, g.prot_id, g.gene_name, s.tree, t.uniprot_code, t.ensembl_code FROM gene g, subtree s, taxonomy t WHERE g.id=s.id AND g.taxid=t.taxid AND s.id=?';
}
my $sth_trees = $dbh->prepare($sql_trees);
if ( $id && $id =~ /^\d+$/ ){
    $sth_trees->execute($id)  or die $sth_trees->errstr;
}
else {
    $sth_trees->execute()  or die $sth_trees->errstr;
}

my $tree;
GENE_WITH_TREE:
while ( my ($st_id, $st_prot, $st_gn, $st_nhx, $st_uniprot, $st_ens) = ($sth_trees->fetchrow_array) ){
    next GENE_WITH_TREE  if ( $st_id !~ /^\d+$/ );
    next GENE_WITH_TREE  if ( $st_gn eq '' );
    $st_gn = uc $st_gn;
    $st_gn =~ s{:}{_}g;     # The change we want to apply
    $st_gn =~ s{ *\[.*$}{}; # The other change we want to apply

    if ( !exists $tree->{$st_id}->{'tree'} ){
        $tree->{$st_id}->{'tree'} = $st_nhx;
    }

    # Find broken GN
    my @leaf = grep { /$st_prot:/ } grep { /GN=/ } split(/,/, $st_nhx); # Only those with a gene_name, and the right prot_id
    #FIXME find also leaves without GN but that should have!
    next GENE_WITH_TREE  if ( !exists $leaf[0] );
    die "Duplicated [$st_prot] in tree\n"  if ( exists $leaf[1] );
    my ($prod_id, $gene_name) = $leaf[0] =~ /^\(*(.+?):.+?GN=([^\]:]+)/;
    die "[$st_prot] ne [$prod_id]\n"  if ( $st_prot ne $prod_id );

    if ( uc $gene_name ne uc "${st_gn}_${st_uniprot}" && uc $gene_name ne uc "${st_gn}_${st_ens}" ){
        print $leaf[0], "\n";
        print "[$prod_id] [$gene_name]\n";
        print "$st_id\t$st_prot\t$st_gn\t$st_uniprot\t$st_ens\n";

        # Apply fix
        $tree->{$st_id}->{'tree'} =~ s{($prod_id.+?:GN)=$gene_name}{$1=$st_gn};
#        print $tree->{$st_id}->{'tree'}, "\n";
    }
}
exit; #TODO update here or not?

my $sth_updateTree = $dbh->prepare('UPDATE subtree SET tree = ? WHERE id = ?');

$sth_trees->finish();
$sth_updateTree->finish();
$dbh->disconnect  or warn $dbh->errst;
exit 0;

