#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;
use DBI;
use lib '.';
use DB;

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07_euteleostomi98/selectome_v07_timema1 [selectome AC]\n\n";
my $AC     = $ARGV[1]  || '';
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $outdir = '/var/SELECTOME';
my ($db1, $db2) = ('', '');
if ( $dbname =~ /timema/ ){
    $db1 = 'timema';
    $db2 = $db1;
}
elsif ( $dbname =~ /euteleostomi/ ){
     $db1 = 'euteleostomi';
     $db2 = 'Euteleostomi';
}


#TODO dump db first !!!
my $sql_up_tree = 'UPDATE subtree SET tree=? WHERE id=(SELECT i.id FROM id i WHERE i.AC=? AND i.subfamily=?)';
my $sth_up_NHX_tree = $dbh->prepare($sql_up_tree);


# Get new NHX trees
NHX:
for my $NHX_file ( sort grep { /$AC/ } glob("$outdir/$db1/*/$db2/*.NHX") ){
    #/var/SELECTOME/euteleostomi/ENSGT00980000199298/Euteleostomi/ENSGT00980000199298.Euteleostomi.001.NHX
    my ($st_AC, $st_subfamily) = $NHX_file =~ /\/(\w+)\.\w+\.(\d+)\.NHX$/;
#    print "[$NHX_file] [$st_AC][$st_subfamily]\n";
    my $NHX = read_file($NHX_file, chomp =>1);

    # Take care of the previous nhx file
    my $nhx_file = $NHX_file;
    $nhx_file =~ s{\.NHX$}{.nhx};
    my $nwk_file = $NHX_file;
    $nwk_file =~ s{\.NHX$}{.nwk};
    if ( !-e "$nhx_file" ){
        warn "Previous NHX file does not look to exist for [$st_AC][$st_subfamily]\n";
        next NHX;
    }
    if ( !-e "$nwk_file" ){
        warn "Previous nwk file does not look to exist for [$st_AC][$st_subfamily]\n";
        next NHX;
    }
    if ( -z "$NHX_file" ){
        warn "Empty NHX file for [$st_AC][$st_subfamily]\n";
        next NHX;
    }
    # Backup previous NHX file
    system("mv -f $nhx_file $nhx_file.backup");
    # Rename .NHX to .nhx
    system("mv -f $NHX_file $nhx_file");
    # Regenerate Newick file
    system("perl -pe 's|\\\[&&NHX.*?\\\]||g' $nhx_file > $nwk_file");
    # Update in the db
    $sth_up_NHX_tree->execute($NHX, $st_AC, $st_subfamily)  or die $sth_up_NHX_tree->errstr;
}

$sth_up_NHX_tree->finish();
$dbh->disconnect  or warn $dbh->errst;

exit 0;

