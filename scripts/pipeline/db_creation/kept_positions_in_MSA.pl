#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;
use Selectome::Utils;

my $famid_length = $Selectome::Utils::famid_length;


my $min_block_size = 15 / 3; # Because min length of 15 nt == 5 codons / 5 AA
#NOTE Block length has to deal with prot or DNA, even for codons

my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates\n\n";


#### Get useful subfamilies that have passed filters
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass) or die $DBI::errstr;

my $sth_success = $dbh->prepare('SELECT DISTINCT i.id, i.AC, i.taxon, i.subfamily FROM id i, subtree t, selection c WHERE i.id=t.id AND i.id=c.id AND i.taxon = ? ORDER BY i.id');
$sth_success->execute($phylum)  or die $sth_success->errstr;
my $success_ref = $sth_success->fetchall_arrayref;
$sth_success->finish();


####
if ( exists($success_ref->[0]) ){
    open (my $DUMP, '>', 'msa.txt');
    for my $row (@$success_ref){
        my ($id, $ac, $taxon, $subfamily) = @$row;

        #FIXME NEED to use the MSA after MaxAlign but without residue removal from MCoffee to get a "biologicaly real" MSA !!!!!
        $subfamily = sprintf("%0${famid_length}d", $subfamily);


        # Nt final MSA, after residues masking BUT before TrimAl filtering !!!
        my $msa_length = get_msa_length("$ac/$taxon/$ac.$taxon.$subfamily.nt.fas.preTrimAl.gz");

        # TrimAl stat file, with column info
        my $col_range  = extract_stat("$ac/$taxon/$ac.$taxon.$subfamily.nt.fas.trimal.stat.gz", $msa_length);

#TODO add used_length column
        print {$DUMP} "$id\t$col_range\t$ac/$taxon/$ac.$taxon.$subfamily\t$msa_length\n";
    }
    close $DUMP;
}

$dbh->disconnect  or warn $dbh->errst;
exit 0;


sub extract_stat {
    my ($stat_file, $msa_length) = @_;

    my $STAT;
    open($STAT, "zcat $stat_file |")  or open($STAT, "cat $stat_file |")  or die "Cannot open the TrimAl stat file '$stat_file'\n";
    my $stat = do{local $/; <$STAT>;};
    chomp $stat;
    close $STAT;

    #NOTE Easy because stat sur nucleotide CDS !
    my @col_kept = map  { $_ / 3 }          # Back to codon position number
                   grep { ($_ % 3 )==0 }    # Keep only 1 position out of 3 to get codon
                   map  { ++$_ }            # Positions start at 0 and not 1, so ++$_ to return $_+1
                   grep { /^\d+$/ }         # Remove header + non-numeric things
                   split(/,?\s+/, $stat);

    die "Prob with MSA length: $stat_file: $col_kept[-1] > $msa_length\n"  if ( $col_kept[-1] > $msa_length ); # $msa_length is in aa/codon


    my $range = "$col_kept[0]:";             # Initialize at the 1st col_kept value
    COL:
    for (my $c = 1; $c <= $#col_kept; $c++){ # so start loop at 2nd index (#2)
        if ( ($col_kept[$c] - 1) != $col_kept[$c-1] ){
            $range .= "$col_kept[$c-1],$col_kept[$c]:";
        }
    }
    #FIXME Does cgi can deal with e.g. 1:25,26:26,100:105 (26:26 single/isolated column) ???
    $range .= $col_kept[-1];

    return $range;
}


sub get_msa_length {
    my ($file) = @_;

    my $is_gz = 0;
    if ( $file =~ /\.gz$/ ){
        system("gunzip $file");
        $file =~ s/\.gz$//;
        $is_gz = 1;
    }

    my $msa_length = `readal -in $file -info | grep '^## Alignment length' | sed -e 's/^## Alignment length\\s\\s*//'`;
    chomp $msa_length;

    system("gzip -9 $file")  if ($is_gz);

    #For codon/aa length ONLY !!!
    die "\n\tProblem with msa length of '$file'\n"  if ( ($msa_length % 3 ) != 0 );

    return ($msa_length / 3);
}

sub extract_columns {
    my ($file) = @_;

#TODO should work with protein vs prot or prot vs nt MSAs

    my @msa;
    my $FILE;
    open($FILE, "zcat $file |") or open($FILE, "cat $file |")  or die "Cannot open the file '$file'\n";
    my $position   = 0;
    my $msa_length = 0;
    SEQ:
    while(<$FILE>){
        if ( $_ =~ /^>/){
            $position   = 0;
            $msa_length = 0;
            next SEQ;
        }

        chomp $_;
        $_ =~ s{ }{}g;
        $msa_length += length($_);

        my @tmp = split(//, $_);
        for(my $pos = 0; $pos <= $#tmp; $pos++){
            $msa[$pos + $position] .= $tmp[$pos];
        }
        $position += ($#tmp + 1);
    }
    close $FILE;

    #For codon/aa length ONLY !!!
    die "\n\tProblem with msa length of '$file'\n"  if ( ($msa_length % 3 ) != 0 );

    return ($msa_length / 3);
}

