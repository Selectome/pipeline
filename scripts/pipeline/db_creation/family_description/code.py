__author__ = 'Loony'

##### library that I need to use SQL commande #####
import MySQLdb as mdb
import sys
import Config

##### import the function that I need to run the code #####
from function import *
from counter import dictionary_create

##### to open the database (db) I need to connect the db and create a cursor to execute SQL commande #####
con = mdb.connect(Config.host, Config.login, Config.password, Config.dbname)
cur = con.cursor()

##### execute() is the function to exectute a commande SQL and fetchall() show what is find with the query #####
cur.execute("SELECT gene.id, gene.description "
            "FROM gene, id "
            "WHERE gene.id = id.id ")       # return the id (of family) and the description (of family) of all primates

primates = cur.fetchall()        # gives all the id and desc. of primates in a variable
print len(primates)              # gives the length of the variable
liste = list(primates)           # change the variable into a list

remove_source(liste)                 # function that removes the source in the list
lower(liste)                         # function that harmonize the size of the letter (all in minus)
remove_parenthesis(liste)            # function that remove the parenthesis from the list
#print 'list=',list
#for row in liste:                    # print the list in order to separate the rows
#    print row

from collections import Counter
count = Counter(liste)
count_order = sorted(list(count),key=str.lower)
print len(count_order), type(count_order)

dic = dictionary_create(liste)
#print 'dictionary=',dic

### transfer the description which means nothing into the description_key '' ###
id = dic.keys()
for i in range(len(id)):
    id_1 = dic[id[i]]
    if id_1.has_key(''):
        if id_1.has_key('uncharacterized protein'):
            id_1['']+= id_1['uncharacterized protein']
            del id_1['uncharacterized protein']
    else:
        id_1[''] = 0
        if id_1.has_key('uncharacterized protein'):
            id_1['']+= id_1['uncharacterized protein']
            del id_1['uncharacterized protein']


save = open('fd','w') # fd = fichier du dictionnaire
import pickle
pickle.dump(dic,save)
save.close()

id_1 = dic['94827']
print id_1

#Commande SQL:
#    select
#    from
#    where
#    limit
#     count
#    group by
#    sort by
#    desc : description des champs


#import re:

