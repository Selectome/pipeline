__author__ = 'Loony'

#liste_fjid= ["94828, 'ubiquitin carboxyl-terminal hydrolase'", "94828, 'ubiquitin specific peptidase 51'", "94828, ''", "94828, ''", "94828, ''", "94828, 'ubiquitin specific peptidase 51'", "94827, 'ubiquitin carboxyl-terminal hydrolase'", "94827, 'ubiquitin carboxyl-terminal hydrolase'", "94827, 'ubiquitin carboxyl-terminal hydrolase'", "94827, 'ubiquitin specific peptidase 22'", "94827, 'ubiquitin specific peptidase 22'", "94827, 'predicted: pan troglodytes ubiquitin carboxyl-terminal hydrolase 22-like usp22, miscrna'", "94827, 'ubiquitin specific peptidase 22'", "94827, 'ubiquitin specific peptidase 22'", "94827, 'ubiquitin specific peptidase 22'", "94826, 'ubiquitin carboxyl-terminal hydrolase'", "94826, 'ubiquitin carboxyl-terminal hydrolase'", "94826, 'ubiquitin carboxyl-terminal hydrolase'", "94826, 'ubiquitin carboxyl-terminal hydrolase 33'", "94826, 'ubiquitin specific peptidase 33'", "94826, ''", "94825, 'ubiquitin specific peptidase 20'", "94825, 'ubiquitin specific peptidase 20'", "94825, 'ubiquitin carboxyl-terminal hydrolase'", "94825, 'ubiquitin carboxyl-terminal hydrolase'", "94825, 'ubiquitin carboxyl-terminal hydrolase 20'", "94825, ''", "94825, 'ubiquitin specific peptidase 20'", "94825, 'ubiquitin specific peptidase 20'", "94824, 'ubiquitin carboxyl-terminal hydrolase'", "94824, 'ubiquitin carboxyl-terminal hydrolase'", "94824, 'ubiquitin specific peptidase 44'", "94824, 'ubiquitin specific peptidase 44'", "94824, ''", "94824, 'ubiquitin specific peptidase 44'", "94823, 'ubiquitin carboxyl-terminal hydrolase'", "94823, 'ubiquitin specific peptidase 49'", "94823, 'ubiquitin carboxyl-terminal hydrolase'", "94823, 'ubiquitin carboxyl-terminal hydrolase'", "94823, 'ubiquitin carboxyl-terminal hydrolase'", "94823, 'ubiquitin specific peptidase 49'", "94823, 'ubiquitin specific peptidase 49'", "94823, ''", "94823, 'ubiquitin specific peptidase 49'", "94822, ''", "94822, 'histone h2a'", "94822, ''", "94822, 'histone cluster 1, h2ag'", "94822, 'histone h2a type 1'", "94822, 'histone cluster 1, h2ag'", "94822, 'histone cluster 1, h2ag'", "94821, 'histone cluster 1, h2ae'", "94821, 'histone h2a'", "94821, 'histone h2a'", "94821, 'histone cluster 1, h2ae'", "94821, ''", "94821, ''", "94821, ''", "94820, 'uncharacterized protein'", "94820, 'g protein-activated inward rectifier potassium channel 1'", "94820, 'uncharacterized protein'", "94820, 'potassium inwardly-rectifying channel, subfamily j, member 3'", "94820, ''", "94820, 'potassium inwardly-rectifying channel, subfamily j, member 3'", "94820, 'potassium inwardly-rectifying channel, subfamily j, member 3'", "96960, 'uncharacterized protein'", "96960, ''", "96960, 'hydroxyacyl-coa dehydrogenase'", "96960, 'hydroxyacyl-coa dehydrogenase'", "96960, 'hydroxyacyl-coa dehydrogenase'", "96955, 'peroxisome proliferator-activated receptor gamma'", "96955, 'peroxisome proliferator-activated receptor gamma'", "96955, 'uncharacterized protein'", "94818, 'atp-sensitive inward rectifier potassium channel 8'", "94818, 'uncharacterized protein'", "94818, 'potassium inwardly-rectifying channel, subfamily j, member 8'", "94818, ''", "94818, 'potassium inwardly-rectifying channel, subfamily j, member 8'", "94818, 'potassium inwardly-rectifying channel, subfamily j, member 8'", "94817, 'potassium inwardly-rectifying channel, subfamily j, member 6'", "94817, 'uncharacterized protein'", "94817, 'g protein-activated inward rectifier potassium channel 2; uncharacterized protein'", "94817, 'uncharacterized protein'", "94817, 'potassium inwardly-rectifying channel, subfamily j, member 6'", "94817, 'potassium inwardly-rectifying channel, subfamily j, member 6'", "94817, ''", "94817, 'potassium inwardly-rectifying channel, subfamily j, member 6'", "94817, 'potassium inwardly-rectifying channel, subfamily j, member 6'", "94816, 'potassium inwardly-rectifying channel, subfamily j, member 5'", "94816, 'uncharacterized protein'", "94816, 'uncharacterized protein'", "94816, 'uncharacterized protein'", "94816, 'potassium inwardly-rectifying channel, subfamily j, member 5'", "94816, 'potassium inwardly-rectifying channel, subfamily j, member 5'", "94816, 'potassium inwardly-rectifying channel, subfamily j, member 5'", "94816, ''", "94815, 'calcium binding protein 1'", "94815, 'calcium binding protein 1'", "94815, 'uncharacterized protein'", "94815, 'uncharacterized protein'"]

def dictionary_create(liste):
    from collections import Counter
    count = Counter(liste)
    count_order = sorted(list(count),key=str.lower)

    dico_description = dict()                  # create a second dictionary with an empty key to introduce the description
    dictionary = dict()                      # create a dictionary with an empty key
    i = 0
    while i < len(count_order):
        element_i_str = str(count_order[i])
        id = element_i_str.find(",")                  # find the first element `"," to know where the id finish
        clef_id = element_i_str[:id]                 # determine the id-key for the dictionary
        j = 0
        for k in count_order:
            if clef_id == k[:k.find(',')]:
                value = count[k]
                clef_desc = str(k)[id+3:-1]               # determine the description-key for the dictionary
                dico_description[clef_desc] = value             # add the description-key and the corresponding value in the second dictionary
                j += 1
        i += j
        dictionary[clef_id] = dico_description                      # add the id-key and the corresponding dictionary (containing the description)
        dico_description = dict()                  # create a second empty dictionary
    return dictionary

#dic = diction(liste_fjid)
#print 'dictionnaire =',dic
#dic = diction(liste_fjid)
