#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use DBI;
use List::MoreUtils qw{uniq any};
use Algorithm::LCSS qw( LCSS CSS );

use lib './'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;


# Define arguments & their default value
my ($dbname, $idd)  = ('', '');
my ($verbose)       = (0);
my %opts = ('db=s'    => \$dbname,   # selectome db name
            'id=i'    => \$idd,      # selectome_id
            'verbose' => \$verbose,  # verbose mode, do not insert/update in database
           );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $dbname eq '' ){
    print "\n\tInvalid or missing argument:
\te.g. $0  -db=<Selectome db name>  [-id=<selectome_id>] [-verbose]

\t-db       Selectome db name
\t-id       selectome_id (db internal id)

\t-verbose  verbose mode, do not insert/update in database
\n";
    exit 1;
}


my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $threshold = 2/3;

# NEED to remove \W around  e.g. Predicted:  (predicted)
my %to_exclude = ('putative'        => 1,
                  'uncharacterized' => 1,
                  'predicted'       => 1,
                  'similar to'      => 1,
                  'ortholog of'     => 1,
                  'potential'       => 1,
                  'probable'        => 1,
                  'hypothetical'    => 1,
                 );


# Get all ids
my $sth_id = $dbh->prepare('SELECT id FROM id ORDER BY id');
if ( $idd ){
    $sth_id = $dbh->prepare("SELECT id FROM id WHERE id=$idd ORDER BY id");;
}
$sth_id->execute()  or die $sth_id->errstr;
my $ids    = $sth_id->fetchall_arrayref;
$sth_id->finish();


#Some desc MUST be empty at the same, if too many different choices and/or too many empty desc
#     need semantic; don't care about putative, undefined, uncharacterized, ...
#                    remove some words like fragment, parentheses, ...
#     Try with the shortest sentence  if contained in others

my $up_desc = $dbh->prepare('UPDATE subtree SET subdesc = ? WHERE id = ?');
ID:
for my $rows (@$ids){
    my ($tree_id) = @$rows;

    # NOTE Not use SELECT DISTINCT because need to know how many times the same descriptions are given !
    my $sth_gene_desc = $dbh->prepare('SELECT g.description, CONCAT(i.AC, ".", i.subfamily) FROM gene g, id i WHERE g.id=? AND i.id=g.id');
    $sth_gene_desc->execute($tree_id)  or die $sth_gene_desc->errstr;
    my $desc = $sth_gene_desc->fetchall_arrayref;
    $sth_gene_desc->finish();

    next ID  if ( !exists ${$desc}[0] );
    my $ac = $desc->[0][1];


    # Remove source tag at the end of gene description
    my @descriptions             = map  { s/\s{2,}/ /g; $_ }
                                   map  { s/\s*\(?Fragment\)?\s*//i; $_ }
                                   map  { $_->[0] =~ s/\s*\.*\s*\[Source:.+$//; $_->[0]}   @$desc;

    # Use not empty descriptions
    my @not_empty_desc           = grep { $_ ne '' }                                       @descriptions;

    # Same case for all descriptions
    my @same_case_desc           = map { uc $_ }                                           @descriptions;



    my $tree_desc = '';
    my $category  = 0;
    # Test if gene descriptions are 100% identical
    if ( scalar uniq(@descriptions) == 1 ){
        $tree_desc = $descriptions[0];
        $category  = 1;
    }

    # Identical but mix of up and lowercases => need to choose which one to take
    elsif ( scalar uniq(@same_case_desc) == 1 ){
        ($tree_desc) = desc_occurence(@descriptions);
        $category = 2;
    }

    # More than 50% of empty desc, so should be empty fam desc
    elsif ( (scalar @not_empty_desc)/(scalar @descriptions) < 0.5 ){
        $tree_desc = '';
        $category  = 3;
    }

    # Identical some are empty               => take it if more than x% of not empty
    # FIXME need to choose more appropriate threshold !
    elsif ( (scalar @not_empty_desc           / scalar @descriptions) >= $threshold ){
        ($tree_desc) = desc_occurence(@not_empty_desc);
        $category = 4;
    }

    else {
        $category  = 9;

        my $css_ref =  CSS($not_empty_desc[0], $not_empty_desc[1]);
        #my $lcss    = LCSS($not_empty_desc[0], $not_empty_desc[1]);
        my @css     = map { s/  +/ /g; s/^ +//; s/ +$//; $_ } @$css_ref;
        #Skip if several isolated characters e.g. [nu e r d o n protein 1]  => CSS() bug???
        my %tmp;
        map { $tmp { length($_) }++ } grep { /[a-z]/i } @css;
        @css = ()  if ( exists $tmp{'1'} && $tmp{'1'} >= 1 );
        my $rebuilt_from_css = join(' ', @css);


        # My own code
        my @own0 = split(/\W+/, $not_empty_desc[0]);
        my @own1 = split(/\W+/, $not_empty_desc[1]);
        my @own;
        for my $word ( @own0 ){
            if ( any { lc $word eq lc $_ } @own1 ){
                push @own, $word;
            }
        }
        my $rebuilt_from_own = join(' ', @own);


        $tree_desc = length($rebuilt_from_own) > length($rebuilt_from_css) ? $rebuilt_from_own : $rebuilt_from_css;
    }
    print "\t=> [$tree_desc]\n"  if ( $verbose );

#TODO threshold must be the most frequent/sum  AND  most frequent/2nd most freq
#     if not big diff between 1st and 2nd most freq, try to get a substring of both
#     e.g. gremlin 1 (6) gremlin 2 (5)    => gremlin

    # FIXME More complex cases
    # 1 outlier => consensus of the rest
    # no outlier BUT some common words in descriptions (skip common english+bio words)
    # Remove Fragment / (Fragment)

    my $ori_tree_desc = $tree_desc;
    $tree_desc =~ s{ +$}{};
    $tree_desc =~ s{^ +}{};
    #NOTE must be longer than 2 characters, and more than a word to avoid false positive (the least common between most freq descriptions)
    $tree_desc = ''  if ( length($tree_desc) <=2 || $tree_desc !~ /\W/ );
    #$tree_desc = ''  if ( any { lc($tree_desc) =~ /$_/ } keys %to_exclude );
    $tree_desc = ''  if ( $tree_desc eq 'NA' );

    $category .= 'm'  if ( $ori_tree_desc ne $tree_desc );
    printf "%-33s %s\n", "$tree_id $ac $category", "[$tree_desc]";
    if ( !$verbose && $tree_desc ne '' ){
        $up_desc->execute($tree_desc, $tree_id)  or die $up_desc->errstr;
    }
}
$up_desc->finish();


$dbh->disconnect  or warn $dbh->errst;
exit;


sub desc_occurence {
    my (@desc) = @_;

    my %count;
    my $max_occurence = '';
    my $max           = 0;
    DESC:
    for my $desc (@desc){
        $count{$desc}++;
        if ( $count{$desc} > $max ){
            $max_occurence = $desc;
            $max           = $count{$desc};
        }
    }

    my $sum = 0;
    $sum += $_  for (values(%count));
    for my $desc ( keys(%count) ){
        print "\t\t[$desc] -> $count{$desc}/$sum\n"  if ( $verbose );
    }

    return ($max_occurence, $sum, \%count);
}

