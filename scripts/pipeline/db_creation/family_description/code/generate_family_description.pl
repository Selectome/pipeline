#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use List::Util qw(sum first);
use List::MoreUtils qw{uniq any};
use Data::Dumper;
use Term::ANSIColor;

use String::Similarity::Group ':all';
use Roman;
#use Text::Median;
#use String::Trigram;
#use String::LCSS_XS qw(lcss);
#use Algorithm::LCSS qw( LCSS );

use lib '../../'; # To use DB.pm module in pipeline directory with update enabled mysql account
use DB;

my $VERSION = 0.0.1;
my $debug   = 1;

my $dbname = $ARGV[0] || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 Primates [selectome_id]\n\n";
my $phylum = $ARGV[1] || die "\n\tPhylum is missing, e.g. $0 selectome_v07 Primates [selectome_id]\n\n";
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $idd    = $ARGV[2] || 0;


#TODO give model organisms higher priority if available (and no other info) !

# Exclusions
# NEED to remove \W around  e.g. Predicted:  (predicted)
my %to_exclude = ('hypothetical'            => 1,
                  'open reading frame'      => 1,
                  'ortholog of'             => 1,
                  'potential'               => 1,
                  'predicted'               => 1,
                  'predicted gene'          => 1,
                  'predicted pseudogene'    => 1,
#                  'predicted loc'           => 1, # && LOC\d+
                  'probable'                => 1,
                  'putative'                => 1,
                  'riken cdna'              => 1,
                  'shotgun'                 => 1,
                  'similar to'              => 1,
#                  'uncharacterized'         => 1,
                  'uncharacterized protein' => 1,
#                  'uncharacterized'         => 1, # && LOC\d+
                 );

#FIXME remove species in description e.g. PREDICTED: Pan troglodytes heterogeneous nuclear ribonucleoprotein A1-like 2-like (LOC100609303), miscRNA   OR in 1304, 25175, ...
my %specific   = ('PREDICTED: Pongo abelii'     => 1,
                  'PREDICTED: Pan troglodytes'  => 1,
                  '(Drosophila)'                => 1,
                  '(yeast)'                     => 1,
                  '(mouse)'                     => 1,
                  '(human)'                     => 1,
                  '(C.elegans)'                 => 1,
                 );

#FIXME what to do with:  "Putative uncharacterized protein RGD1309519_predictedUncharacterized protein"

# Finish by ", xxx" e.g. , partial miscRNA   , mRNA
my %to_minor   = ('fragment'                => 1,
                  'gene'                    => 1,
                  'miscRNA'                 => 1,
                  'mRNA'                    => 1,
                  'partial miscRNA'         => 1,
                  'precursor'               => 1,
                  'preproprotein'           => 1,
                  'protein'                 => 1,
                  'pseudogene'              => 1,
                 );



# Get all ids
my $sth_id = $dbh->prepare('SELECT i.id FROM id i, subtree s WHERE i.taxon = ? AND i.id=s.id ORDER BY i.id');
if ( $idd && $idd =~ /^\d+$/ ){
    $sth_id = $dbh->prepare("SELECT i.id FROM id i, subtree s WHERE i.id=$idd AND i.taxon = ? AND i.id=s.id ORDER BY i.id");
}
$sth_id->execute($phylum)  or die $sth_id->errstr;
my $ids    = $sth_id->fetchall_arrayref;
$sth_id->finish();


#TODO some desc MUST be empty at the same, if too many different choices and/or too many empty desc
#     need semantic; don't care about putative, undefined, uncharacterized, ...
#                    remove some words like fragment, parentheses, ...
#     Try with the shortest sentence  if contained in others
#TODO Avoid gene id as description !

my $up_desc = $dbh->prepare('UPDATE subtree SET subdesc = ? WHERE id = ?');

my $done      = 0;
my $not_empty = 0;
my $vote      = 0;
my ($identical, $identical_case, $fully_empty, $become_empty, $partially_excluded, $fully_excluded, $unchanged, $changed) = (0, 0, 0, 0, 0, 0, 0, 0);
ID:
for my $rows (@$ids){
    my ($tree_id) = @$rows;

    # BINARY is for case sensitive queries
    # Order by most frequent 1st
    my $sth_gene_desc = $dbh->prepare('SELECT g.description, COUNT(g.description) FROM gene g WHERE g.id=? GROUP BY BINARY g.description ORDER BY COUNT(g.description) desc');
    $sth_gene_desc->execute($tree_id)  or die $sth_gene_desc->errstr;
    my $desc     = $sth_gene_desc->fetchall_arrayref;
    my $gene_nbr = sum map { $_->[1] } @$desc;
    $sth_gene_desc->finish();

    if ( !exists ${$desc}[0] ){
        warn "Strange id [$tree_id]\n";
        next ID;
    }
    my $tree_desc = '';


    #############################################################################
    # Create a dictionary/hash to store description and their frequency after
    # case checks, exclusion, ... So to store description merging !
    # Clean multiple spaces
    my %hash     = map { $_->[0] =~ s/\s{2,}/ /g;
                         $_->[0] => $_->[1];
                       }
                   @$desc; # And dereference them !
    my @fam_desc = keys %hash;


    # Merge descriptions
    my $hash_case         = check_identical_case_insensitive(\%hash);
    my $hash_full_exclude = exclude_fully(\%hash);
    my @weird_ids         = grep { !/^si:.+\d+$/ } #NOTE Weird ZFIN gene id, without really description meaning
                            grep { !/^zgc:\d+$/ }  #NOTE Weird ZFIN gene id, without really description meaning
                            @fam_desc;
    my @group             = groups( 0.9, \@weird_ids);
    #TODO
    my $tmp = exclude_species(\%hash);
    exclude_minor($tmp);
    #############################################################################


    my $flag = 0;
    $done++;

## Identical
# Identical descriptions for all genes in @$desc
    if (    scalar @fam_desc == 1 && $fam_desc[0] ne '' && !exists $to_exclude{ lc $fam_desc[0] } ){
        $identical++;
        $unchanged++;
        $not_empty++;
        $tree_desc = $fam_desc[0];
    }
# Empty description among them
    elsif ( scalar @fam_desc == 1 && $fam_desc[0] eq '' ){
        $identical++;
        $unchanged++;
        $fully_empty++;
        $tree_desc = '';
    }
# Identical descriptions for all genes but case
    elsif ( scalar keys %$hash_case == 1 && scalar @fam_desc > 1 ){
        $identical_case++;
        $unchanged++;
        $not_empty++  if ( get_most_frequent(\%hash) ne '' );
        $tree_desc = get_most_frequent(\%hash);
    }


## Remove the non-sense description. The value of the non-sense description is transferred to the empty one
# description == a exclusion term
#FIXME lot more to do when excluded term is NOT the whole description !!!!
    elsif ( scalar keys %$hash_full_exclude == 1 && exists $hash_full_exclude->{''} ){ # NOTE exclusion case is tested !!!
        $become_empty++;
        $changed++;
        $fully_excluded++;
        $tree_desc = '';
    }
#    elsif ( scalar keys %$hash_full_exclude == 1 ){
#        warn "\t\t\t$tree_id\n";
#        next ID;
#        $changed++;
#        $partially_excluded++;
#    }

## VOTE
    # Original hash
    elsif ( vote($gene_nbr) <= (get_most_frequent(\%hash, 1)/$gene_nbr) ){
        $vote++;
        if ( get_most_frequent(\%hash) eq '' ){
            $unchanged++;
            $become_empty++;
            $tree_desc = '';
        }
        elsif ( exists $to_exclude{ lc get_most_frequent(\%hash) } ){
            $changed++;
            $become_empty++;
            $fully_excluded++;
            $tree_desc = '';
        }
        else {
            $unchanged++;
            $not_empty++;
            $tree_desc = get_most_frequent(\%hash);
        }
    }
    # Merge description case insensitivily
    elsif ( vote($gene_nbr) <= (get_most_frequent($hash_case, 1)/$gene_nbr) ){
        $vote++;
        if ( get_most_frequent($hash_case) eq '' ){
            $unchanged++;
            $become_empty++;
            $tree_desc = '';
        }
        elsif ( exists $to_exclude{ lc get_most_frequent($hash_case) } ){
            $changed++;
            $become_empty++;
            $fully_excluded++;
            $tree_desc = '';
        }
        else {
            $unchanged++;
            $not_empty++;
            $tree_desc = get_most_frequent($hash_case);
        }
    }
    # Merge description after full exclusion
    elsif ( vote($gene_nbr) <= (get_most_frequent($hash_full_exclude, 1)/$gene_nbr) ){
        $vote++;
        if ( get_most_frequent($hash_full_exclude) eq '' ){
            $unchanged++;
            $become_empty++;
            $tree_desc = '';
        }
        elsif ( exists $to_exclude{ lc get_most_frequent($hash_full_exclude) } ){
            $changed++;
            $become_empty++;
            $fully_excluded++;
            $tree_desc = '';
        }
        else {
            $unchanged++;
            $not_empty++;
            $tree_desc = get_most_frequent($hash_full_exclude);
        }
    }


## Similarity between descriptions
    elsif ( scalar @group > 0 && scalar @group < scalar @fam_desc ){
        my $sim_merged = merge_by_similarity(\%hash, \@group);
        # Group on full description list
        if ( scalar keys $sim_merged == 1 ){
            $changed++;
            $not_empty++;
            $tree_desc = get_most_frequent($sim_merged);
        }
        elsif ( vote($gene_nbr) <= (get_most_frequent($sim_merged, 1)/$gene_nbr) ){
            $vote++;
            $changed++;
            $not_empty++;
            $tree_desc = get_most_frequent($sim_merged);
#            $flag = 1;
        }

        # TODO group after exclusion
    }

#TODO remove \W (punctuation) char to help + parentheses and their containt
#TODO partial exclusion
#TODO %to_minor
#TODO one description is contained in others (longer)


    else {
        $flag = 1;
#        next ID;
    }

    next  if ( $flag == 0 );
    print "$tree_id\t$gene_nbr\t[$tree_desc]";
    if ( $debug ){
        print "\t";
        map  { print color 'green';
               print $_;
               print color 'reset';
               print '__';
             }
        sort { $hash{$b} <=> $hash{$a} }
        keys %hash;
        print "\b\b  ";
    }
    print "\n";
#    $up_desc->execute($tree_desc, $tree_id)  or die $up_desc->errstr;
}
$up_desc->finish();

printf("\n\tStrictly identical: %9d/%d\n\tIdentical but case: %9d/%d\n\tFully empty: %16d/%d\n\tBecome empty: %15d\n\tWith some exclusions: %7d\n\tAll excluded: %15d\n\tAfter vote: %17d/%d\n\tNot empty:%19d/%d\n\n\tUnchanged: %18d/%d\n\tChanged: %20d/%d\n",
        $identical, scalar @$ids, $identical_case, scalar @$ids, $fully_empty, $identical, $become_empty, $partially_excluded, $fully_excluded, $vote, scalar @$ids, $not_empty, scalar @$ids, $unchanged, scalar @$ids, $changed, scalar @$ids);
printf("\n\tDONE: %23d/%d\n", ($unchanged+$changed), scalar @$ids);


$dbh->disconnect  or warn $dbh->errst;
exit 0;


sub check_identical {
    my ($desc) = @_;

    my @list = uniq
               @$desc;

    return \@list;
}

sub check_identical_case_insensitive {
    my ($dict) = @_;

    my %hash = %$dict;
    for my $desc1 ( keys %$dict ){
        for my $desc2 ( keys %$dict ){
            next  if ( $desc1 eq $desc2 ); # Skip same key

            if ( lc $desc1 eq lc $desc2 ){ # Deal with key with different cases only
                next  if ( !exists $hash{$desc1} || !exists $hash{$desc2} );
                if ( $hash{$desc1} > $hash{$desc2} ){
                    $hash{$desc1} += $hash{$desc2};
                    delete $hash{$desc2};
                }
                else {
                    $hash{$desc2} += $hash{$desc1};
                    delete $hash{$desc1};
                }
            }
        }
    }

    return \%hash;
}

sub get_most_frequent {
    my ($dict, $freq_only) = @_;
    $freq_only = $freq_only || 0;

    my @ordered = @{ order_dict($dict) };

    return $dict->{$ordered[0]}  if ( $freq_only );
    return $ordered[0];
}

sub order_dict {
    my ($dict) = @_;

    my @ordered = # Order by frequency, then by alphanumeric order if same frequency
                  sort { $dict->{$b} <=> $dict->{$a} || $a cmp $b }
                  keys %$dict;

    return \@ordered;
}

sub exclude_fully {
    my ($dict) = @_;

    my %hash = %$dict;
    for my $desc1 ( keys %$dict ){ # Whole description is to exclude !
        if ( exists $to_exclude{ lc $desc1 } ){
            $hash{''} += $hash{$desc1};
            delete $hash{$desc1};
        }
    }

    return \%hash;
}

sub exclude_species {
    my ($dict) = @_;

    my %hash = %$dict;
    for my $desc ( keys %$dict ){ # Exclude only species specific info from description
        my $match = first { $desc =~ /\Q$_\E/ } keys %specific;
        if ( $match ){
            my $new_desc = $desc;
            $new_desc   =~ s{ *\Q$match\E *}{ };
            $new_desc   =~ s{(^ +| +$)}{};
            $hash{$new_desc} = $hash{$desc};
            delete $hash{$desc};
            print "[$desc] [$new_desc]\n";
        }
    }

    return \%hash;
}

sub exclude_minor {
    my ($dict) = @_;

    my %hash = %$dict;
    for my $desc ( keys %$dict ){ # Exclude only minor words from description
        my $match = first { $desc =~ /\Q$_\E/ } keys %to_minor;
#TODO Need to loop over multiple to_minor matches e.g. Primates 97395
        if ( $match ){
            my $new_desc = $desc;
            $new_desc   =~ s{,? *\Q$match\E *}{ };
            $new_desc   =~ s{(^ +| +$)}{};
            $hash{$new_desc} = $hash{$desc};
            delete $hash{$desc};
            print "[$desc] [$new_desc]\n";
        }
    }

    return \%hash;
}

sub vote {
    my ($gene_nbr) = @_;

    my $threshold = 0.75;
    if ( $gene_nbr <= 12 ){
        $threshold = 0.0104 * $gene_nbr + 0.5417; # Ad-hoc function that feets our needs
    }

    return $threshold;
}

sub merge_by_similarity {
    my ($dict, $group) = @_;

    if ( scalar @$group == 0 || scalar @$group >= scalar keys %$dict ){
        return $dict;
    }

    my @ordered = @{ order_dict($dict) };

    my %hash = %$dict;
    GROUP:
    for my $grp ( @$group ){
        my %hash_subgroup = map { $_ => $dict->{$_} }
                            @$grp;
        %hash_subgroup = %{ check_paralog(\%hash_subgroup) };
        my @ordered_subgroup = @{ order_dict(\%hash_subgroup) };
        my $best = shift @ordered_subgroup;
        for my $subgroup ( @ordered_subgroup ){
            $hash{$best} += $hash{$subgroup}  if ( exists $hash{$subgroup} );
            delete $hash{$subgroup};
        }
    }

    return \%hash;
}

# Check close descriptions e.g. truc A with truc B. Should give truc A/B
sub check_paralog {
    my ($dict) = @_;

    # test case: 95369
    my %hash  = %$dict;
    my @order = @{ order_dict($dict) };
    my @uniq  = uniq
                map { $_ =~s / kDa$/kDa/; #NOTE Fix for few cases with kDa not pasted to its numerical value
                      my @tmp = split(/\s+/, $_);
                      pop @tmp;
                      my $tmp = join(' ', @tmp);
                      $tmp;
                    }
                @order;
#FIXME id=64036 mix cases and suffix
#FIXME id=22224 " roman" & "-arabic" value
    if ( scalar @uniq > 1 || ( scalar @uniq == 1 && 1 == scalar split(/\s+/, $uniq[0]) ) ){
        #NOTE Do no look to be paralogous descriptions or too short descriptions
        return $dict;
    }

    # SO a single (long) main description now (sub-group) !!!
    my $main = $order[0];
    $main   =~ s/^(.+?)\s+\S+$/$1/;
    my @ends = map { my @tmp = split(/\s+/, $_);
                     $tmp[-1];
                   }
               @order;
    #NOTE Mix of with & without kDa (e.g. 64254 and 62135: 164/164kDa and 250/250kDa)
    for my $end ( grep {/^[\d\.]+kDa$/} @ends ){
        if ( $end =~ /([\d\.]+)kDa$/ && exists $hash{$main." $1"} ){
            $hash{$main." $end"} += $hash{$main." $1"};
            $hash{$main." $1"}    = 0;
        }
    }
    #NOTE Mix of Romand & Arabic numbers (e.g. 88492 and 50211: III/3 and 7/VII)
    for my $end ( grep {/^\d+$/} @ends ){
        next  if ( $end >= 4000 ); # Domain of valid Roman numerals is limited to less than 4000, since proper Roman digits for the rest are not available in ASCII.
        if ( $end =~ /^(\d+)$/ && exists $hash{$main.' '.Roman($1)} ){
            $hash{$main.' '.Roman($1)} += $hash{$main." $end"};
            $hash{$main." $end"}        = 0;
        }
        elsif ( $end =~ /^(\d+)$/ && exists $hash{$main.' '.roman($1)} ){
            $hash{$main.' '.Roman($1)} += $hash{$main." $end"} + $hash{$main.' '.roman($1)};
            $hash{$main.' '.roman($1)}  = 0;
            $hash{$main." $end"}        = 0;
        }
    }


    @order = @{ order_dict(\%hash) };
    my $paralogous_description = $order[0];
    my $paralogous_count       = $hash{$order[0]};
    shift @order;
    for my $desc ( @order ){
        my @tmp = split(/\s+/, $desc);
        $paralogous_description .= '/'.$tmp[-1];
        $paralogous_count       += $hash{$desc};
#        delete $hash{$desc}; # Does not work as expected here because sub-hash
        $hash{$desc}             = 0;
    }
    $hash{$paralogous_description} = $paralogous_count;

    return \%hash;
}

