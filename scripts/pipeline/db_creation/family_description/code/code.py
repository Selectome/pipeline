__author__ = 'Loony'


# to create a dictionary containing the followed format:
import pickle
from copy import deepcopy
from function import *
import re

# to allow modification of the dictionary
save = open('fd', 'r')              # can open the dictionary from the computer's memory
dictionary = pickle.load(save)
save.close()
dic = deepcopy(dictionary)
tries = 0                           # count the number of called the percentage_solved() function. It is used to create the file that store the annotated family


percentage_solve(dic, tries)
tries += 1

compare_short_desc(dic)
percentage_solve(dic, tries)
tries += 1

# remove the non-sense description. The value of the non-sense description is transferred to the empty one
id_family = dic.keys()
for family_id in id_family:
    id_element = dic[family_id]
    key_desc = id_element.keys()
    for key in key_desc:
        if key.find('predicted gene') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
        elif key.find('uncharacterized') > -1 and key.find('loc') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
        elif key.find('predicted') > -1 and key.find('loc') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
        elif key.find('open reading frame') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
        elif key.find('riken cdna') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
        elif key.find('shotgun') > -1:
            id_element[''] += id_element[key]
            del id_element[key]
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()

compare_short_desc(dic)
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()

# remove the unwanted typography from the description
for family_id in id_family:
    id_element = dic[family_id]
    key_desc = id_element.keys()
    for key in key_desc:
        if key.find(',') > -1 or key.find('-') > -1:        # check if the description contains a coma or an hyphen and
        # replace it then with the condition I try to add the count of the same description without typography.
            first_new_key = key.replace('-', ' ')
            sec_new_key = first_new_key.replace(',', '')
            if key != first_new_key and first_new_key != sec_new_key:
                try:
                    id_element[sec_new_key] += id_element[key]
                    del id_element[key]
                except KeyError:
                    id_element[sec_new_key] = id_element[key]
                    del id_element[key]
            elif key != first_new_key and first_new_key == sec_new_key:
                try:
                    id_element[sec_new_key] += id_element[key]
                    del id_element[key]
                except KeyError:
                    id_element[sec_new_key] = id_element[key]
                    del id_element[key]
            elif key == first_new_key and first_new_key != sec_new_key:
                try:
                    id_element[sec_new_key] += id_element[key]
                    del id_element[key]
                except KeyError:
                    id_element[sec_new_key] = id_element[key]
                    del id_element[key]
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()

compare_short_desc(dic)
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()

# remove the parenthesis and their content
for family_id in id_family:
    id_element = dic[family_id]
    key_desc = id_element.keys()
    for key in key_desc:
        if key.find('(') > -1:
            # determined if we have parenthesis in the description. If yes we search the first '(' with the find function
            # and the last ')' with the rfind function (rfind does the same that the find one but start looking at the end of the string)
            first_new_key = key[:key.find('(')] + key[key.rfind(')') + 2:]
            if id_element.has_key(first_new_key):
                id_element[first_new_key] += id_element[key]
                del id_element[key]
            else:
                id_element[first_new_key] = id_element[key]
                del id_element[key]
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()


compare_short_desc(dic)
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()

# remove the frequent words "protein" and "precursor"
for family_id in id_family:
    id_element = dic[family_id]
    key_desc = id_element.keys()
    # the following regular expression allow the verify if the description do not contain only 2 words
    regexp = r'^\w*\s\w*$'
    for key in key_desc:
        match = re.search(regexp,key)
        if key.find('protein ') > -1 and match is None:
            first_new_key = key.replace('protein ', '')
            if id_element.has_key(first_new_key):
                id_element[first_new_key] += id_element[key]
                del id_element[key]
            else:
                id_element[first_new_key] = id_element[key]
                del id_element[key]
        elif key.find(' precursor') > -1 and match is None:
            first_new_key = key.replace(' precursor', '')
            if id_element.has_key(first_new_key):
                id_element[first_new_key] += id_element[key]
                del id_element[key]
            else:
                id_element[first_new_key] = id_element[key]
                del id_element[key]
percentage_solve(dic, tries)
tries += 1
id_family = dic.keys()


compare_short_desc(dic)
percentage_solve(dic, tries)

# remove the unsolvable families
unsolved_dic = unsolved_dictionary(dic)
id_family = dic.keys()


### regroup family solved ###
final_file = open("family_solved.txt",'w')
final_file.write('id_family_gene\tdescription_family \t nb_of_gene_in_family \n')
for i in range(tries):
    name = 'family_solved_%d.txt' %i
    part_of_file = open(name,'r')
    lines_of_file = part_of_file.readlines()
    for line in lines_of_file:
        regexp = r'^\d.'
        match = re.search(regexp,line)
        if match:
            final_file.write(line)
