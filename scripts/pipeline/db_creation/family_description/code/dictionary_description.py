__author__ = 'Loony'


# library that I need to use SQL commend
import MySQLdb as my_DB
import Config

# import the function that I need to run the code
from function import *

# to open the database (db) I need to connect the db and create a cursor to execute SQL commend
con = my_DB.connect(Config.host, Config.login, Config.password, Config.dbname)
cur = con.cursor()

# execute() is the function to execute a commend SQL and fetchall() show what is find with the query
cur.execute("SELECT gene.id, gene.description "
            "FROM gene, id "
            "WHERE gene.id = id.id ")   # return the id (of family) and the description (of family) of all organisms from the database

organisms = cur.fetchall()              # gives all the id and desc. of primates in a variable
list_org = list(organisms)              # change the variable into a list

# homogenize the information from the database
remove_source(list_org)                 # function that removes the source in the list
lower(list_org)                         # function that harmonize the size of the letter (all in minus)
remove_parenthesis(list_org)            # function that remove the parenthesis from the list

# create the dictionary
dic = dictionary_create(list_org)

# transfer the description which means nothing into the description_key ''
id_key = dic.keys()
for i in range(len(id_key)):
    id_element = dic[id_key[i]]
    try:                               # try if we can add the value of the description 'uncharacterized protein' in the empty one
        id_element['']+= id_element['uncharacterized protein']
        del id_element['uncharacterized protein']
    except KeyError:                   # if the key do not exist we verify if we have the key 'uncharacterized protein'
        if id_element.has_key('uncharacterized protein'):
            id_element[''] = id_element['uncharacterized protein']
            del id_element['uncharacterized protein']
        elif id_element.has_key(''):
            continue
        else:
            id_element[''] = 0

# save the dictionary into the computer's memory
save = open('fd','w')  # fd = file of dictionary
import pickle
pickle.dump(dic,save)
save.close()
