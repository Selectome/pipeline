__author__ = 'Loony'


def remove_source(description):
# this function allows to suppress the source of an information in a list
    import re
    if type(description) == list:                       # check if the type of description is a list
        for i in range(len(description)):               # we go through the list to remove all sources
            desc_string = str(description[i])           # change the i element to allow the removing
            regexp = r'(.*)\s\[Source'
            match = re.search(regexp,desc_string)       # search for the regular expression in the description
            if match:
                desc_new_string = match.group(1)            # take the expression in parenthesis for the new description
                desc_new_string += ')'                      # add ')' to restore the usefulness of the description for the main program
                description[i] = desc_new_string            # replace the change in the list at the place of the i element
    return description


def lower(description):
# this function allows to change letter in lower letter
    if type(description) == list:               # check if the type of description is a list
        for i in range(len(description)):       # we go through the list to remove the source
            desc_string = str(description[i])   # change the i element in string to allow the removing
            new_desc = desc_string.lower()      # change the letter of the i element in lower size
            description[i] = new_desc           # replace the change in the list at the place of the i element
    return description


def remove_parenthesis(description):
# this function remove the first and the last parenthesis from the list
    if type(description) == list:                    # check if the type of description is a list
        for i in range(len(description)):            # we go through the list to remove the source
            desc_string = str(description[i])        # change the i element into string to allow the removing
            desc_without_1st_parenthesis = desc_string.replace('(', '', 1)            # remove the first parenthesis of the i element
            desc_without_external_parenthesis = desc_without_1st_parenthesis[:-1]   # remove the last parenthesis
            description[i] = desc_without_external_parenthesis                      # replace the change in the list at the place of the i element
    return description


def dictionary_create(liste_data):
# this function allow to create a dictionary in the followed form :
# {clef1 = id:{clef2 = description:value = number of description in the correspondent id}}
    from collections import Counter
    count = Counter(liste_data)                                 # create a dictionary that count all the same description for the corresponding family's ID
    count_order = sorted(list(count),key=str.lower)             # sort the count dictionary
    dic_description = dict()                                    # create a second dictionary with an empty key to introduce the description
    dictionary = dict()                                         # create a dictionary with an empty key
    i = 0
    while i < len(count_order):
        element_str = str(count_order[i])
        position = element_str.find(",")                        # find the first element `"," to know where the id finish
        key_id = element_str[:position]                         # determine the id-key for the dictionary
        j = 0
        for description in count_order:
            if key_id == description[:description.find(',')]:   # compare if the ID of the first description is the same as the other one
                value = count[description]
                clef_desc = str(description)[position+3:-1]     # determine the description-key for the dictionary
                dic_description[clef_desc] = value              # add the description-key and the corresponding value in the second dictionary
                j += 1
        i += j
        dictionary[key_id] = dic_description                    # add the id-key and the corresponding dictionary (containing the description)
        dic_description = dict()                                # empty the second dictionary
    return dictionary


def percentage_solve(dic, tries):
# this function allow to annotate a description to a family's ID
    nb_of_family_annotated = 0
    total_gene = 0
    nb_more_frequent_desc = 0
    id_family = dic.keys()                                                                  # store all the keys of the dictionary into a list
    name_file = 'family_solved_%d.txt' % tries                                              # create the name where the annotated families are going to be store
    family_solved = open(name_file, 'w')
    family_solved.write("Number of family in start is %d\n" % len(id_family))               # write information about the file's content
    family_solved.write('id_family_gene\tdescription_family\tnb_of_gene_in_family\n')
    for i in range(len(id_family)):
        description = dic[id_family[i]].keys()                                              # store the description of the corresponding ID into a list
        for j in range(len(description)):
            total_gene += dic[id_family[i]][description[j]]                                 # count the number of gene into the family

            if nb_more_frequent_desc < dic[id_family[i]][description[j]]:                   # find the more frequent gene's description into the family
                nb_more_frequent_desc = dic[id_family[i]][description[j]]
        print total_gene, id_family[i]
        if total_gene <= 12:                                                                # first case the family contain <= 12 genes
            percentage = 0.0104 * total_gene + 0.5417                                       # calculate the percentage need as threshold depending of the size of the family
            if percentage <= (float(nb_more_frequent_desc) / total_gene):                   # determine if the ratio is higher than the threshold
                for j in range(len(description)):
                    if nb_more_frequent_desc == dic[id_family[i]][description[j]] and description[j] == '':     # verify if the description to annotated is an empty description
                        nb_of_family_annotated += 1
                        family_solved.write(str(id_family[i])+"\t"+'keep_empty'+"\t"+str(total_gene) + "\n")    # write into the file the ID, the description, the size of the family
                    elif nb_more_frequent_desc == dic[id_family[i]][description[j]] and description[j] != '':   # verify that the description is not an empty one
                        nb_of_family_annotated += 1
                        family_solved.write(str(id_family[i])+"\t"+description[j]+"\t"+str(total_gene)+"\n")    # write into the file the ID, the description, the size of the family
                del dic[id_family[i]]                                                       # remove from the dictionary the family's ID annotated
        else:                                                                               # second case; the family contains more than 12 genes
            percentage = 0.75                                                               # fix the threshold at 75%
            if percentage <= (float(nb_more_frequent_desc) / total_gene):                   # determine the ratio of the more frequent description
                for j in range(len(description)):
                    if nb_more_frequent_desc == dic[id_family[i]][description[j]] and description[j] == '':     # verify if the description to annotated is an empty description
                        nb_of_family_annotated += 1
                        family_solved.write(str(id_family[i])+"\t"+'keep_empty'+"\t"+str(total_gene)+"\n")      # write into the file the ID, the description, the size of the family
                    elif nb_more_frequent_desc == dic[id_family[i]][description[j]] and description[j] != '':   # verify that the description is not an empty one
                        nb_of_family_annotated += 1
                        family_solved.write(str(id_family[i])+"\t"+description[j]+"\t"+str(total_gene)+"\n")    # write into the file the ID, the description, the size of the family
                del dic[id_family[i]]                                                       # remove from the dictionary the family's ID annotated
        total_gene = 0
        nb_more_frequent_desc = 0
    family_solved.write('number of family already solved is %d' % nb_of_family_annotated)   # write the last information in the file
    return dic


def compare_short_desc(dic):
#this function allow to find the smallest definition and compare it to the other. If the small definition is in the bigger one and the smallest has not one word add the value of the biggest to the smallest.
    id_family = dic.keys()
    for family_id in id_family:
        id_desc = dic[family_id]
        key_desc = id_desc.keys()
        short_desc = ''
        for key in key_desc:    # go through the list of key_desc to find the shortest description
            if short_desc == '' and key != '' and key.find(' ') > -1 and len(key) > 1:
                short_desc = key
            elif short_desc != '' and len(key) < len(short_desc) and key.find(' ') > -1:
                short_desc = key
        for key in key_desc:    # go through the list of key_desc to compare the shortest description with the other
            if short_desc != key and short_desc != '' and short_desc in key:
                id_desc[short_desc] += id_desc[key]     # transfer the value of the key to the shortest description
                del id_desc[key]                        # remove the long description to keep the shortest
    return dic


def unsolved_dictionary(dic):
# this function allow to remove all the family that can be solved by the percentage_solved() function
    unsolved_dic = dict()
    familiy_id = dic.keys()
    for i in range(len(familiy_id)):
        if len(dic[familiy_id[i]]) == 2:                # verify if the number of description is equal to 2
            unsolved_dic[familiy_id[i]] = dic[familiy_id[i]]    # transfer the family from the dictionary to the unsolved one
            del dic[familiy_id[i]]
    return unsolved_dic