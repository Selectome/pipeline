__author__ = 'Loony'

#s = ["('uidhffdkjpfs [Sourcefndkjnfd fndjnfd]')", "('uidhfer [Sgmlfdkgpodourcefndkjnfd fndjnfd]')", "uidhfer [Sourcefnddnklkskjnfd fndjnfd])", "('uidhfer [Sourcefndkjnfd fndjnfd]')"]

def remove_source(description):                 # this function allows to suppress the source of an information in a list
    if type(description) == list:               # check if the type of description is a list
        for i in range(0,len(description)):     # we go through the list to remove the source
            string = str(description[i])        # change the i element to allow the removing
            x = string.find(" [Source")              # search the start of the source with the symbol " [S"
            string = string[:x]
            string+=')'                   # add ') to restore the usefulness of the description for the main program
            description[i] = string             # replace the change in the list at the place of the i element
    return description


def lower(description):                         # this function allows to change letter in minus letter
    if type(description) == list:               # check if the type of description is a list
        for i in range(0,len(description)):     # we go through the list to remove the source
            string = str(description[i])        # change the i element to allow the removing
            x = string.lower()                  # change the letter of the i element in lower size
            description[i] = x                  # replace the change in the list at the place of the i element
    return description

def remove_parenthesis(description):            # this function remove the parenthesis from the list
    if type(description) == list:               # check if the type of description is a list
        for i in range(0,len(description)):     # we go through the list to remove the source
            string = str(description[i])        # change the i element to allow the removing
            x = string.replace('(','')          # remove the parenthesis of the element
            y = x.replace(')','')
            description[i] = y                  # replace the change in the list at the place of the i element
    return description

def percentage_solve(dic,tries):
    family_name = 0
    total_gene = 0
    number = 0
    id = dic.keys()

    name_file = 'family_solved_%d.txt' %tries
    family_solved= open(name_file,'w')
    family_solved.write("Number of family in start is %d \n" %len(id))
    family_solved.write('id_family_gene\tdescription_family')
    for i in range(len(id)):
        description = dic[id[i]].keys()
        for j in range(len(description)):
            total_gene += dic[id[i]][description[j]]
            if number < dic[id[i]][description[j]]:
                number = dic[id[i]][description[j]]
        if total_gene <= 12:
            percentage = 0.0261*total_gene + 0.3533
            if percentage <= (float(number)/total_gene):
                for j in range(len(description)):
                    if number == dic[id[i]][description[j]]:
                        family_name += 1
                        family_solved.write(str(id[i])+"\t"+description[j]+"\n")
                del dic[id[i]]
        else:
            percentage = 0.75
            if percentage <= (float(number)/total_gene):
                for j in range(len(description)):
                    if number == dic[id[i]][description[j]]:
                        family_name += 1
                        family_solved.write(str(id[i])+"\t"+description[j]+"\n")
                del dic[id[i]]

        total_gene = 0
        number = 0
    family_solved.write('number of family already solved is %d' %family_name)
    print family_name, 'family solved'
    return dic

def unsolved_dictionary(dic):
    unsolved_dic = {}
    id = dic.keys()
    for i in range(len(id)):
        if len(dic[id[i]]) == 2:
            unsolved_dic[id[i]] = dic[id[i]]
            del dic[id[i]]
    return unsolved_dic

