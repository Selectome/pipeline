#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use lib '.';
use DB;

my $dbname = $ARGV[0]  or die "\n\tNo db provided: $0 <db name> <taxon>\n\tE.g. $0 selectome_v07 Primates\n\n";
my $taxon  = $ARGV[1]  or die "\n\tNo db provided: $0 <db name> <taxon>\n\tE.g. $0 selectome_v07 Primates\n\n";

my $pval_file = 'pvalues.txt';
my $qval_file = 'qvalues.txt';

# Get p-values from db per clade/taxon
mysql($dbname, $taxon);
# Compute q-values
Rscript();

# Insert/Update q-values in db
my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $upQval = $dbh->prepare('UPDATE selection SET qvalue=?, selected=? WHERE id=? AND branch=?');
QVAL:
for my $line ( read_file("$qval_file", chomp=>1) ){
    my ($id, $branch, $pval, $qval, $selected) = split(/\t/, $line);
    # For Drosophila, qvalue distribution is shifted to the right because lots of selection
    # some p-value are higher than q-value e.g. 1 > 0.9806856
    # so consider this test only for p-value < 0.95
    die "p-value higher than q-value: [$line]\n"  if ( $pval > $qval && $pval < 0.95 );

    $upQval->execute($qval, $selected, $id, $branch)  or die $upQval->errstr;
}
$upQval->finish();
$dbh->disconnect  or warn $dbh->errst;

exit 0;

sub mysql {
    my ($dbname, $taxon) = @_;

    system("mysql -u $DB::user -p$DB::pass $dbname -e \"SELECT c.id, c.branch, c.pvalue FROM selection c, id i WHERE i.id=c.id AND i.taxon='$taxon' ORDER BY c.id, c.branch\" | grep -v 'pvalue' > $pval_file")==0
        or die "MySQL failed\n";
    die "MySQL failed\n"  if ( -z $pval_file );

    return;
}

sub Rscript {
    open(my $R, '>', 'script.R');
    print {$R} "library(qvalue)\np<-read.table(\"$pval_file\", header=F)\nqobj<-qvalue(unlist(p[3]), pi0.method=\"bootstrap\", fdr.level=0.1, pfdr=TRUE)\nwrite.qvalue(qobj, file=\"qvalues.t\")\n";
    close $R;

    system("/usr/bin/env R --vanilla --quiet --slave --no-save  <script.R 2>&1 | grep -v 'Read'");
    unlink 'script.R';

    system("sed -n -e '6,\$p' qvalues.t | awk '{print \$2\"\\t\"\$3}' | paste $pval_file - >$qval_file");
    unlink 'qvalues.t', $pval_file;
    return;
}

