#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use DBI;
use lib '.';
use DB;

my $dbname    = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 root_path\n\n";
my $root_path = $ARGV[1];
my $dbh    = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

#get all the trees
my $sql_trees = 'SELECT s.tree, m.msa_path FROM subtree s, msa m WHERE m.id=s.id';
my $sth_trees = $dbh->prepare($sql_trees);
$sth_trees->execute()  or die $sth_trees->errstr;

while (my ($st_nhx, $st_path) = ($sth_trees->fetchrow_array) ){
    open (my $fh, '>', "$root_path/$st_path.nhx") || die "Can't create nhx $root_path/$st_path.nhx\n";
    print $fh "$st_nhx";
    close $fh || warn "can't close nhx $st_path.nhx\n";

    open (my $newick, '>', "$root_path/$st_path.nwk") || die "Can't create newick $root_path/$st_path.nwk\n";
    my $nwk = $st_nhx;
    $nwk =~ s{\[&&NHX:.*?\]([,\);])}{$1}g; # Remove NHX tags
    $nwk =~ s{===\d+=}{}g;                 # Remove branch labels

    print $newick "$nwk";
    close $newick || warn "can't close $st_path.nwk\n";;
}
$dbh->disconnect  or warn $dbh->errst;
exit 0;

