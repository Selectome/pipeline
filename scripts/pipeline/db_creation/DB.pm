package DB;
#File DB.pm

use strict;
use warnings;
use diagnostics;

use Selectome::Config;

our $host = $Selectome::Config::host;
our $user = $Selectome::Config::user;
our $pass = $Selectome::Config::pass;
our $port = $Selectome::Config::port;

our $wwwtmp = '/var/SELECTOME';

1;

