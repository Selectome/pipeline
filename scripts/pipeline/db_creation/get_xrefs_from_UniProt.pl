#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
#use File::Basename;
#use File::Slurp;
use List::MoreUtils qw{uniq any};
use LWP::Simple;
use XML::Fast;
use Data::Dumper;

use DBI;
use lib '.';
use DB;


# Define arguments & their default value
my ($prot_id, $dbname) = ('', '');
my ($verbose) = (0);
my %opts = ('prot=s'   => \$prot_id,   # protein id to search in UniProt to insert/update in the db
            'db=s'     => \$dbname,    # Selectome database name
            'verbose'  => \$verbose,   # verbose mode, do not insert/update in database
           );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $dbname eq '' ){
    print "\n\tInvalid or missing argument:
\te.g. $0  -db=selectome_v07_euteleostomi98
\t     OR
\t     $0  -db=selectome_v07_euteleostomi98  -prot=ENSRBIP00000031228
\t-prot     protein id to search in UniProt to insert/update in the db
\t-db       Selectome database name to search in UniProt all missing prot info
\t-verbose  verbose mode, do not insert/update in database
\n";
    exit 1;
}


my $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $up_xref  = $dbh->prepare('INSERT INTO xref (id, prot_id, xref, db_source) VALUES (?, ?, ?, ?)');
my $up_gene1 = $dbh->prepare('UPDATE gene SET gene_id=?       WHERE own_id=?');
my $up_gene2 = $dbh->prepare('UPDATE gene SET transcript_id=? WHERE own_id=?');
my $up_gene3 = $dbh->prepare('UPDATE gene SET gene_name=?     WHERE own_id=?');
my $up_gene4 = $dbh->prepare('UPDATE gene SET description=?   WHERE own_id=?');

#Test gene table for empty fields
#NOTE 454019 rows! => 454019 requests to UniProt
my $sth_genes = $dbh->prepare('SELECT own_id, id, prot_id, gene_id, transcript_id, gene_name, description FROM gene WHERE gene_id="" OR transcript_id="" OR gene_name="" OR description=""');
if ( $prot_id ne '' ){
    $sth_genes = $dbh->prepare("SELECT own_id, id, prot_id, gene_id, transcript_id, gene_name, description FROM gene WHERE prot_id='$prot_id'");
}
$sth_genes->execute()  or die $sth_genes->errstr;
GENE:
while ( my ($st_own_id, $st_id, $st_prot_id, $st_gene_id, $st_transcript_id, $st_gene_name, $st_description) = ($sth_genes->fetchrow_array) ){
    sleep 1;
    # Get info from UniProt
    my $annotations;
    #my $content = get('https://www.uniprot.org/uniprot/?query='.$st_prot_id.'&format=xml&force=true'); #General
    my $content = get('https://www.uniprot.org/uniprot/?query=database%3A(type%3Aensembl*+'.$st_prot_id.')&sort=score&format=xml&force=true'); #with Ensembl/EnsemblMetazoa xref
    #WARNING some cases with one ensembl -> several ensembl xrefs: ENSG00000139618
    if ( defined $content && $content ne '' && $content =~ /<\/uniprot>/ ){
        my $hash = xml2hash $content;
        #NOTE not easy to test if exists an array in hash ref. It works with eval!
        #NOTE may return several entries, keep the first one (the best one?)
        my $root = eval { exists $hash->{'uniprot'}->{'entry'}->[0] } ? $hash->{'uniprot'}->{'entry'}->[0] : $hash->{'uniprot'}->{'entry'};

        # Check the UniProt entry contains the xref used to query it, and is for the right species
        print "ProtID:$st_prot_id\n"  if ( $verbose );
        my @ensembl_xref = grep { $_->{'-type'} =~ /^Ensembl/ } @{ $root->{'dbReference'} }; #Ensembl, EnsemblMetazoa, ...
        if ( grep { $_->{'-value'} eq "$st_prot_id" && $_->{'-type'} eq 'protein sequence ID' } map { @{ $_->{'property'} } } @ensembl_xref ){
            $annotations->{'prot_id'} = $st_prot_id;
#            print Dumper $root->{'dbReference'};
            #Get Ensembl gene ID
            ($annotations->{'gene_id'}) = map { $_->{'-value'} } grep { $_->{'-type'} eq 'gene ID'} map { @{ $_->{'property'} } } @ensembl_xref;
            #Get Ensembl transcript ID
            ($annotations->{'transcript_id'}) = map { $_->{'-id'} } @ensembl_xref;

            #Get protein name
#            print Dumper $root->{'gene'};
            $annotations->{'prot_name'} = '';
            if ( ref $root->{'gene'} ne 'ARRAY' ){
                #NOTE to avoid some weird syntax such as GeneID:386601
                my @gene_names = eval { exists $root->{'gene'}->{'name'}->[0] } ? @{ $root->{'gene'}->{'name'} } : ($root->{'gene'}->{'name'});
                for my $gene_name ( sort @gene_names ){
                    next  if ( ref $gene_name eq 'ARRAY' );
                    if ( $gene_name->{'-type'} eq 'primary' ){
                        $annotations->{'prot_name'} = $gene_name->{'#text'};
                    }
                    else {
                        push @{ $annotations->{'synonyms'} }, $gene_name->{'#text'};
                    }
                }
            }
            @{ $annotations->{'synonyms'} } = grep { $_ ne $annotations->{'prot_name'} } uniq @{ $annotations->{'synonyms'} };
            #Get protein description if any
            my @prot_desc_type = sort keys %{ $root->{'protein'} };
            my $prot_root;
            if ( scalar @prot_desc_type >= 1 ){
                $prot_root = $root->{'protein'}->{'recommendedName'} || $root->{'protein'}->{ $prot_desc_type[0] };
            }
            if ( $prot_root ){
                my $prot_name = eval { exists $prot_root->[0] } ? $prot_root->[0] : $prot_root;
                my $prot_desc = eval { exists $prot_name->{'fullName'}->{'#text'} } ? $prot_name->{'fullName'}->{'#text'} : $prot_name->{'fullName'};
                $annotations->{'prot_desc'} = $prot_desc  if ( $prot_desc !~ /LOC\d+/ );
            }

            #Get xrefs
#            print Dumper $root->{'dbReference'};
            push @{ $annotations->{'xrefs'} }, 'uniprotID:'.$root->{'name'};
            my @uniprotAC = eval { exists $root->{'accession'}->[0] } ? @{ $root->{'accession'} } : ($root->{'accession'});
            push @{ $annotations->{'xrefs'} }, map { 'uniprotAC:'.$_ } @uniprotAC;
            # Xrefs
            my @used_xref_db = ('EMBL', 'CCDS', 'RefSeq', 'FlyBase', 'MGI', 'RGD', 'WormBase', 'Xenbase', 'ZFIN', 'GeneTree');
            for my $dbref ( sort @{ $root->{'dbReference'} }){
                if ( any { $dbref->{'-type'} eq $_ } @used_xref_db ){
                    push @{ $annotations->{'xrefs'} }, $dbref->{'-type'}.':'.$dbref->{'-id'};
                    if ( exists $dbref->{'property'} ){
                        my @properties = eval { exists $dbref->{'property'}->[0] } ? @{ $dbref->{'property'} } : ($dbref->{'property'});
                        for my $property ( sort @properties ){
                            if ( $property->{'-type'} =~ /sequence ID$/ ){
                                push @{ $annotations->{'xrefs'} }, $dbref->{'-type'}.':'.$property->{'-value'};
                            }
                        }
                    }
                }
                elsif ( $dbref->{'-type'} eq 'GO' ){
                    for my $property ( sort @{ $dbref->{'property'} } ){
                        if ( $property->{'-type'} eq 'evidence' ){
                            push @{ $annotations->{'go'} }, $dbref->{'-id'};#.'___'.$property->{'-value'};
                            last;
                        }
                    }
                }
            }
            @{ $annotations->{'xrefs'} } = map { s/GeneID:/GenBank:/; s/MGI:MGI:/MGI:/; $_ } uniq @{ $annotations->{'xrefs'} };
            @{ $annotations->{'go'} }    = uniq map { uc($_) }                                    @{ $annotations->{'go'} };
        }
        else {
            #FIXME Try to get info if the ensembl match is not in the main entry (check in all entries OR better query ???)
            warn "No info in the main entry [$st_prot_id]\n";
            next GENE;
        }
    }
    else {
        warn "No info found for [$st_prot_id]\n";
        next GENE;
    }

    warn Dumper $annotations  if ( $verbose );
    # Update gene table
    if ( $st_gene_id       eq '' && exists $annotations->{'gene_id'}       && $annotations->{'gene_id'}       ne '' ){
        $up_gene1->execute($annotations->{'gene_id'},       $st_own_id)  or die $up_gene1->errstr;
    }
    if ( $st_transcript_id eq '' && exists $annotations->{'transcript_id'} && $annotations->{'transcript_id'} ne '' ){
        $up_gene2->execute($annotations->{'transcript_id'}, $st_own_id)  or die $up_gene2->errstr;
    }
    if ( $st_gene_name     eq '' && exists $annotations->{'prot_name'}     && $annotations->{'prot_name'}     ne '' ){
        $up_gene3->execute($annotations->{'prot_name'},     $st_own_id)  or die $up_gene3->errstr;
    }
    if ( $st_description   eq '' && exists $annotations->{'prot_desc'}     && $annotations->{'prot_desc'}     ne '' && $annotations->{'prot_desc'} ne 'Uncharacterized protein' ){
        $up_gene4->execute($annotations->{'prot_desc'},     $st_own_id)  or die $up_gene4->errstr;
    }

    # Update xref table
    if ( exists $annotations->{'go'} ){
        push @{ $annotations->{'xrefs'} }, @{ $annotations->{'go'} };
    }
    if ( scalar @{ $annotations->{'xrefs'} } > 0 ){
        for my $xf ( uniq sort @{ $annotations->{'xrefs'} } ){
            my ($db_source, $xref) = split(/:/, $xf, 2);
            $up_xref->execute($st_id, $st_prot_id, $xref, $db_source);#  or die $up_xref->errstr;
        }
    }
}

$sth_genes->finish;
$up_gene1->finish;
$up_gene2->finish;
$up_gene3->finish;
$up_gene4->finish;
$up_xref->finish;
# Close db connections
$dbh->disconnect;

exit 0;

