#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use List::MoreUtils qw( indexes uniq );

use FindBin qw($Bin);
use lib "$Bin";
use Selectome::EnsemblDB;
use Bio::EnsEMBL::Registry;

use DBI;
use lib '.';
use DB;


my $dbname = $ARGV[0]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 taxon_name\n\n";
my $phylum = $ARGV[1]  || die "\n\tDatabase name is missing, e.g. $0 selectome_v07 taxon_name\n\n";

#### Initialize remote ensembl connection
#my $connection_param = Selectome::EnsemblDB::get_connection_parameters('ensembl'); # Remote connection to official  ensembl db
#my $connection_param = Selectome::EnsemblDB::get_connection_parameters('annot_e'); # Remote connection to our local ensembl copy
my $connection_param = Selectome::EnsemblDB::get_connection_parameters('annot_m'); # Remote connection to our local ensembl metazoa copy
my $debug = 1;
my $reg   = 'Bio::EnsEMBL::Registry';
$reg->load_registry_from_db(
            -host    => $connection_param->{'host'},
            -port    => $connection_param->{'port'},
            -user    => $connection_param->{'user'},
            -pass    => $connection_param->{'pass'},
            -verbose => $debug,
);



#### Get ensembl gene id / protein id for subtree table
my $dbh      = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;

my $sth_tree = $dbh->prepare('SELECT s.id, s.tree FROM subtree s, id i WHERE i.id=s.id AND i.taxon=? ORDER BY s.id');
$sth_tree->execute($phylum)  or die $sth_tree->errstr;
my $tree_ref = $sth_tree->fetchall_arrayref;
$sth_tree->finish();


#### Taxonomy data structure
my $taxonomy; # 1st level keys are Taxonomic id from NCBI
# $taxonomy = { taxid => { 'taxid'           => '',     # Taxonomic id from NCBI
#                          'scientific_name' => '',     # Clades or species fullname
#                          'common_name'     => '',     # The comon name as defined by Genbank
#                          'aliases'         => '',     # Aliases to common or scientific names if any
#                          'is_a_species'    => 0,      # Is a species and not a clade somewhere in the tree
#                          'short_name'      => '',     # Ensembl name of this genome in the Gspe ("G"enera "spe"cies) format
#                          'ensembl_code'    => '',     # Species code beginning  ensembl  identifiers
#                        },
#             };


#### Tree parsing ####
if ( exists($tree_ref->[0]) ){
    for my $row (@$tree_ref){
        my ($id, $tree) = @$row;
#        print "$id\t\t$tree\n";

        my @nodes = grep { /&&NHX:/ } split(']', $tree);
        for my $node (@nodes){
#            print "$node\n";

            if ( $node =~ /:T=(\d+)/ ){
                my $taxid = $1;
                $taxonomy->{$taxid}->{'taxid'} = $taxid;

                if ( $node =~ /[\(,](ENS\w+).+:G=(ENS\w+)/ || $node =~ /[\(,](FBpp\d+).+:G=(FBgn\d+)/ ){ # For Ensembl ids + Droso ids
                    my $protein = $1;
                    my $gene    = $2;

                    # Ensembl regular ids
                    if ( $protein =~ /^ENS\w+$/ && $gene =~ /^ENS\w+$/ ){
                        # length - 12 to get only beginning of the identifier for species matching purpose !!!
                        die "Mix between species protein and gene list\n"  if ( substr($protein, 0, length($protein)-12) ne substr($gene, 0, length($gene)-12) );

                        $taxonomy->{$taxid}->{'ensembl_code'} = substr($protein, 0, length($protein)-12);
                    }
                    # Flybase ids
                    elsif ( $protein =~ /^FBpp\d+$/ && $gene =~ /^FBgn\d+$/ ){
                        $taxonomy->{$taxid}->{'ensembl_code'} = '';
                        # Cannot solve this because does not exist in Ensembl Genomes because ids are imported from db sources!!!
                    }
                    else {
                        die "Unproper ids [$protein] [$gene]\n";
                    }
                    $taxonomy->{$taxid}->{'is_a_species'} = 1;


                    # Check if the protein_id is already in the database to save time due to remote connection to ensembl !!!!
                    my $sth_id = $dbh->prepare('SELECT COUNT(p.prot_id) FROM gene p, id i WHERE p.id=i.id AND i.taxon=? AND p.prot_id=?');
                    $sth_id->execute($phylum, $protein)  or die $sth_id->errstr;
                    my $row = $sth_id->fetchall_arrayref;
                    $sth_id->finish();

                    if ( $row->[0]->[0] > 0 ){
                        printf {*STDERR} "\t%-20s already inserted!\n", $protein;
                    }
                    else {
                        print "\t\tGetting data from Ensembl for $protein\n";
                        # Get gene info based on identifier
                        my ( $species, $object_type, $db_type ) = $reg->get_species_and_object_type( $gene );
                        my $gene_adaptor                        = $reg->get_adaptor( $species, $db_type, $object_type );

                        my $gene_desc     = $gene_adaptor->fetch_by_stable_id($gene);

                        my $display_id    = $gene_desc->display_id();
                        my $stable_id     = $gene_desc->stable_id();
                        my $external_name = $gene_desc->external_name() || '';
                        my $description   = $gene_desc->description()   || '';
                        #TODO check these subst
                        $description     =~ s{  +}{ }g;
                        $description     =~ s{[\.,]+ *\[Source:}{ \[Source:};
                        # Remove HTML tags
                        $external_name   =~ s{<[^>]+?>}{}g;
                        # Old problems
                        warn "\t[$description]\n"  if ( $description =~ /formTruncated/ );
                        warn "\t[$description]\n"  if ( $description =~ /\[Source:.*?\[Source:/ );


                        my $transcript    = '';
                        my @xrefs;
                        # Get all splice forms
                        for my $mrna ( @{ $gene_desc->get_all_Transcripts } ){
                            if ( defined $mrna->stable_id() ){
                                push @xrefs, $mrna->stable_id();
                            }

                            my $translation = $mrna->translation();
                            if ( defined $translation && defined $translation->stable_id() ){
                                push @xrefs, $translation->stable_id();

                                if ( $translation->stable_id() eq $protein ){
                                    $transcript = $mrna->stable_id();
                                }
                            }
                        }
                        # Remove transcript and protein ids used in the tree
                        @xrefs = grep { $_ ne $transcript && $_ ne $protein } @xrefs;


                        # Get extra Xref from gene description line
                        my %extra_xref;
                        if ( $description ne '' ){
                            #EC number
                            while ( $description =~ /E\.?C\.?\s*([1-7]\.[\d\-]+\.[\d\-]+\.[\d\-]+)/g ){
                                $extra_xref{$1} = 'EC';
                            }

                            #Entry source
                            while ( $description =~ /\[Source:\s*(.+?)\s*;Acc:\s*(.+?)\s*\]/g ){
                                my $db_source = $1;
                                my $acc       = $2;

                                $db_source    = $db_source eq 'HGNC Symbol'          ? 'HGNC'
                                              : $db_source eq 'UniProtKB/TrEMBL'     ? 'TrEMBL'
                                              : $db_source eq 'UniProtKB/Swiss-Prot' ? 'Swiss-Prot'
                                              : $db_source eq 'RefSeq DNA'           ? 'RefSeq'
                                              : $db_source eq 'RefSeq peptide'       ? 'RefSeq'
                                              : $db_source eq 'FlyBase gene name'    ? 'FlyBase'
                                              # + miRBase
                                              :                                        $db_source;
                                $extra_xref{$acc} = $db_source;
                            }
                        }
                        push @xrefs, keys(%extra_xref);


                        # Get Xref
                        # Xref for gene, transcripts + proteins with $gene_desc->get_all_xrefs()
                        push @xrefs, map { $_->display_id() }
                                     # Remove what is already in gene table: ids + symbol
                                     # + all homologous references between ensembl ids of different species
                                     grep { $_->display_id() !~ /^ENS/ && $_->display_id() ne $external_name && $_->display_id() =~ /\w/ }
                                     @{ $gene_desc->get_all_xrefs() };
                               # Avoid duplications with representative ids from trees, between geen and xref tables
                        @xrefs = grep { $_ ne $protein && $_ ne $transcript  && $_ ne $gene }
                                 @xrefs;
                        @xrefs = uniq sort @xrefs; # Need to put uniq here for redundancy with extra_xref


                        # Db insertions
                        if ( $gene eq $display_id && $gene eq $stable_id ){
                            my $source   = $description;
                            $source      =~ s{^.*? \[Source:(.+?)\].*$}{$1};
                            $description =~ s{ \[Source:.*$}{};
                            $description =~ s{(\S)(Uncharacterized protein)}{$1 $2}i; #Correct for wrongly concatenated text
                            warn "\t[$description]\n"  if ( $description =~ /Source:/ || $description =~ /Acc:/ );
                            #TODO Check taxid insertion next time
                            my $sth_gene = $dbh->prepare('INSERT INTO gene (id, prot_id, transcript_id, gene_id, gene_name, description, source, taxid) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
                            $sth_gene->execute($id, $protein, $transcript, $gene, $external_name, $description, $source, $taxid)  or die $sth_gene->errstr;
                            $sth_gene->finish();

                            my $sth_xref = $dbh->prepare('INSERT INTO xref (id, prot_id, xref, db_source) VALUES (?, ?, ?, ?)');
                            my %uniq_xref_case_unsensitive;
                            XREF:
                            for my $xref (@xrefs){
                                $xref =~ s{^\s+}{};
                                $xref =~ s{\s+$}{};
                                #TODO How to get db_source from the API???
                                if ( !exists($uniq_xref_case_unsensitive{uc $xref}) ){
                                    # Remove duplicates with different cases this way to keep the original case for xrefs !
                                    $uniq_xref_case_unsensitive{uc $xref} = 1;

                                    my $db_source = $extra_xref{$xref} || '';
                                    if ( length($xref) > 120 ){# Same limit as defined in MySQL schema for xref table, xref field
                                        warn "\t[$xref] too long\n";
                                        $sth_xref->execute($id, $protein, $xref, $db_source)  or warn $sth_xref->errstr;
                                    }
                                    else {
                                        $sth_xref->execute($id, $protein, $xref, $db_source)  or die  $sth_xref->errstr;
                                    }
                                }
                            }
                            $sth_xref->finish();
                        }
                        else {
                            die "Something wrong with gene identifiers: $gene|$display_id|$stable_id\n";
                        }
                    }
                }
                elsif ( $node =~ /\)(\w+)===\d+=/ || $node =~ /\)===\d+=(\w+)/ ){
                    my $clade = $1;
                    if ( !exists($taxonomy->{$taxid}->{'is_a_species'}) ){
                        # NEED to do that because a duplication at a leaf will create in internal node with a species name !!!
                        $taxonomy->{$taxid}->{'is_a_species'}    = 0;
                        $taxonomy->{$taxid}->{'scientific_name'} = $clade;
                    }
                }
                else {
                    print "\t$id\t[$node]\n";
                }
            }
        }
    }
}


#### Taxonomy filling ####
if ( %$taxonomy ne 0 ){
    open(my $TAXA, '>', 'taxonomy.txt')  or die "\n\tCannot open 'taxonomy.txt' file\n\n";
    for my $taxid ( keys(%$taxonomy) ){
        my $ncbi_taxon_adaptor = $reg->get_adaptor($connection_param->{'multi'}, 'compara', 'NCBITaxon');
        my $taxon              = $ncbi_taxon_adaptor->fetch_node_by_taxon_id($taxid);


        my $scientific_name = '';
        my @aliases;
        my $short_name      = '';
        my $ensembl_code    = '';
        if ( $taxonomy->{$taxid}->{'is_a_species'} ){
            $scientific_name = $taxon->binomial()        || '';
            push @aliases, ($taxon->ensembl_alias()      || '');
            push @aliases, ($taxon->ensembl_alias_name() || '');
            $short_name      = $taxon->get_short_name()      || '';
            $ensembl_code    = $taxonomy->{$taxid}->{'ensembl_code'};
        }
        else {
            $scientific_name = $taxonomy->{$taxid}->{'scientific_name'};
        }
        my $genbank_name    = $taxon->common_name() || '';


        # Remove duplicated aliases
        @aliases = uniq @aliases;
        my @new_aliases;
        ALIAS:
        for my $alias ( @aliases ){
            next ALIAS                 if ( $alias eq '' );
            push @new_aliases, $alias  if ( $genbank_name !~ /$alias/i );
        }


        my $aliases = join('__', @new_aliases);
        $aliases    =~ s{&amp;}{&}g;
        print {$TAXA} "$taxid\t$scientific_name\t$genbank_name\t$aliases\t$taxonomy->{$taxid}->{'is_a_species'}\t$short_name\t$ensembl_code\t\tt\n";
    }
    close $TAXA;
}


$dbh->disconnect  or warn $dbh->errst;
exit 0;

