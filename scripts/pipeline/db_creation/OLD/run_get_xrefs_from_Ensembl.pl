#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use File::Slurp;
use FindBin;

my $file = $ARGV[0]  or die "\n\t$0 LIST_MGP_TRUNCATED.tsv\n\n";

for my $line ( read_file("$file", chomp => 1) ){
    my ($prot, $fam) = split(/\t/, $line);
    system("$FindBin::Bin/get_xrefs_from_Ensembl.pl  -prot=$prot -fam=$fam\n");
    sleep 1;
}

exit 0;


