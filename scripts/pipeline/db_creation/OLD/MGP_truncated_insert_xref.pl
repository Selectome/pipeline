#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use DBI;
use File::Slurp;
use Getopt::Long;

use lib '.';
use DB;


# Define arguments & their default value
my ($xrefTSV, $dbname) = ('', '');
my ($verbose) = (0);
my %opts = ('xref=s'   => \$xrefTSV,   # xref.tsv file
            'db=s'     => \$dbname,    # Selectome database name
            'verbose'  => \$verbose,   # verbose mode, do not insert/update in database
           );

# Check arguments
my $test_options = Getopt::Long::GetOptions(%opts);
if ( !$test_options || $dbname eq '' || $xrefTSV eq '' ){
    print "\n\tInvalid or missing argument:
\t     $0  -db=selectome_v07_euteleostomi98  -xref=xref.tsv
\t-xref     xref.tsv file
\t-db       Selectome database name to update
\t-verbose  verbose mode, do not insert/update in database
\n";
    exit 1;
}


#FIXME  node2gene table was not filled with truncated prot_id ids !!!! Need to be updated later!
#FIXME  branch field in gene table is always 0 for MGP_ truncated ids !!!
my $dbh      = DBI->connect("dbi:mysql:database=$dbname;host=$DB::host;port=$DB::port", $DB::user, $DB::pass)  or die $DBI::errstr;
my $sth_id   = $dbh->prepare('SELECT id FROM id WHERE AC=? AND taxon=? AND subfamily=?');
my $ins_xref = $dbh->prepare('INSERT INTO xref (id, prot_id, xref, db_source) VALUES (?, ?, ?, ?)');

XREF:
for my $line ( grep { !/^#/ } read_file("$xrefTSV", chomp=>1) ){
    #   id                                      prot_id                   xref     db_source
    #0  ENSGT00390000000002.Euteleostomi.002    MGP_CAROLIEiJ_P0068404    Apbb2    EntrezGene
    my (undef, $fid, $prot_id, $xref, $db_source) = split(/\t/, $line);
    my ($AC, $taxon, $subfamily) = split(/\./, $fid);
    $sth_id->execute($AC, $taxon, $subfamily)  or die $sth_id->errstr;
    my @id_row = $sth_id->fetchrow_array;
    #Should be a single match
    if ( $sth_id->rows() != 1 ){
        die "Several or no ids found for [$fid]\n";
    }

    # Insert missing xref
    $ins_xref->execute($id_row[0], $prot_id, $xref, $db_source);#  or $ins_xref->errstr;
}

$sth_id->finish;
$ins_xref->finish;
# Close db connections
$dbh->disconnect;
exit 0;

