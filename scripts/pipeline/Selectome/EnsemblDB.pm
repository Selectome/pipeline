package Selectome::EnsemblDB;
#File Selectome/EnsemblDB.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Selectome::Config;

my $default_mysql_host = $Selectome::Config::default_mysql_host;
my $default_mysql_port = $Selectome::Config::default_mysql_port;
my $default_mysql_user = $Selectome::Config::default_mysql_user;
my $default_mysql_pass = $Selectome::Config::default_mysql_pass;

my $local_mysql_user   = $Selectome::Config::local_mysql_user;
my $local_mysql_pass   = $Selectome::Config::local_mysql_pass;

my $annot_mysql_user   = $Selectome::Config::annot_mysql_user;
my $annot_mysql_pass   = $Selectome::Config::annot_mysql_pass;
my $annot_mysql_host   = $Selectome::Config::annot_mysql_host;
my $annot_mysql_port   = $Selectome::Config::annot_mysql_port;

my $default_ensembl_multi = 'Multi';

my $connection_parameters = {
         # Remote connections
         'ensembl'  => { 'host'  => 'ensembldb.ensembl.org',
                         'port'  => 5306,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => $default_ensembl_multi,
                       },
         'metazoa'  => { 'host'  => 'mysql-eg-publicsql.ebi.ac.uk',
                         'port'  => 4157,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => 'metazoa',
                       },
         'bacteria' => {
                         'host'  => 'mysql-eg-publicsql.ebi.ac.uk',
                         'port'  => 4157,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => 'bacteria',
                       },
         'protists' => {
                         'host'  => 'mysql-eg-publicsql.ebi.ac.uk',
                         'port'  => 4157,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => 'protists',
                       },
         'fungi'    => {
                         'host'  => 'mysql-eg-publicsql.ebi.ac.uk',
                         'port'  => 4157,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => 'fungi',
                       },
         'plants'   => {
                         'host'  => 'mysql-eg-publicsql.ebi.ac.uk',
                         'port'  => 4157,
                         'user'  => $default_mysql_user,
                         'pass'  => $default_mysql_pass,
                         'multi' => 'plants',
                       },

         # Local connections
         'local_e'  => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => $default_ensembl_multi,
                       },
         'local_m'  => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => 'metazoa',
                       },
         'local_b'  => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => 'bacteria',
                       },
         'local_p'  => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => 'protists',
                       },
         'local_f'  => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => 'fungi',
                       },
         'local_pl' => { 'host'  => $default_mysql_host,
                         'port'  => $default_mysql_port,
                         'user'  => $local_mysql_user,
                         'pass'  => $local_mysql_pass,
                         'multi' => 'plants',
                       },


         # Local - annotbioinfo - connections
         'annot_e'  => { 'host'  => $annot_mysql_host,
                         'port'  => $annot_mysql_port,
                         'user'  => $annot_mysql_user,
                         'pass'  => $annot_mysql_pass,
                         'multi' => $default_ensembl_multi,
                       },
         'annot_m'  => { 'host'  => $annot_mysql_host,
                         'port'  => $annot_mysql_port,
                         'user'  => $annot_mysql_user,
                         'pass'  => $annot_mysql_pass,
                         'multi' => 'metazoa',
                       },
};



sub get_connection_parameters {
    my ($param) = @_;

    # Test if $param is a defined connection type
    if ( ! exists($connection_parameters->{$param}) ){
        warn "\tFalse connection type\n\tonly -- ", join(', ', sort keys %$connection_parameters), " -- are valid\n";
        exit 1;
    }

    # Set selected_connection_parameters to return
    my $selected_connection_parameters = {
            'host'  => $connection_parameters->{$param}->{'host'},
            'port'  => $connection_parameters->{$param}->{'port'},
            'user'  => $connection_parameters->{$param}->{'user'},
            'pass'  => $connection_parameters->{$param}->{'pass'},
            'multi' => $connection_parameters->{$param}->{'multi'},
    };

    return $selected_connection_parameters;
}


1;

