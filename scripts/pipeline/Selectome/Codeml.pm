package Selectome::Codeml;
#File Selectome/Codeml.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Selectome::Utils;
use Selectome::Tree;

use File::Slurp;
#use File::Which;
use FindBin qw($Bin);
BEGIN {
    # Test if required executables are in the path
    #warn('Cannot reach "codeml"')     if ( ! which('codeml') );
}


my $ctl_template = '     seqfile = __PHY__.phy  * sequence data file name
    treefile = __TREE__.nwk  * tree structure file name
     outfile = __OUT__.mlc  * main result file name

       noisy = 0   * 0,1,2,3,9: how much rubbish on the screen
     verbose = 1   * 1: detailed output, 0: concise output
     runmode = 0   * 0: user tree;  1: semi-automatic;  2: automatic
                   * 3: StepwiseAddition; (4,5):PerturbationNNI; -2: pairwise

     seqtype = 1   * 1:codons; 2:AAs; 3:codons-->AAs
   CodonFreq = 2   * 0:1/61 each, 1:F1X4, 2:F3X4, 3:codon table
       ndata = 1   * specifies the number of separate data sets in the file
       clock = 0   * 0: no clock, unrooted tree, 1: clock, rooted tree

      aaDist = 0   * 0:equal, +:geometric; -:linear, 1-6:G1974,Miyata,c,p,v,a
                   * 7:AAClasses

       model = 2   * models for codons:
                        * 0:one, 1:b, 2:2 or more dN/dS ratios for branches
                   * models for AAs or codon-translated AAs:
                        * 0:poisson, 1:proportional, 2:Empirical, 3:Empirical+F
                        * 6:FromCodon, 8:REVaa_0, 9:REVaa(nr=189)

     NSsites = 2   * 0:one w; 1:neutral; 2:positive selection; 3:discrete; 4:freqs;
                   * 5:gamma; 6:2gamma; 7:beta; 8:beta&w; 9:beta&gamma;
                   * 10:beta&gamma+1; 11:beta&normal>1; 12:0&2normal>1;
                   * 13:3normal>0

       icode = 0   * 0:universal/standard code; 1:mammalian/vertebrate mt;
                   * 2:yeast mt; 3:mold, protozoan, coelenterate mt + mycoplasma/spiroplasma code;
                   * 4:invertebrate mt; 5:ciliate, dasycladacean and hexamita nuclear code;
                   * 6:echinoderm and flatworm mt; 7:euplotid mt; 8:alternative yeast nuclear;
                   * 9:ascidian mt; 10:blepharisma nuclear; 11:Yang’s regularized code
                   * See NCBI genetic code
       Mgene = 0   * 0:rates, 1:separate;

   fix_kappa = 0   * 1: kappa fixed, 0: kappa to be estimated
       kappa = 2   * initial or fixed kappa

   fix_omega = 0   * 1: omega or omega_1 fixed, 0: estimate
       omega = 1   * initial or fixed omega, for codons or codon-based AAs

       getSE = 0   * 0: don\'t want them, 1: want S.E.s of estimates
RateAncestor = 0   * (0,1,2): rates (alpha>0) or ancestral states (1 or 2)
  Small_Diff = .5e-6 * small value used in the difference approximation of derivatives

   cleandata = 0   * remove sites with ambiguity data (1:yes, 0:no)?
 fix_blength = 0   * 0: ignore, -1: random, 1: initial, 2: fixed
      method = 0   * 0: simultaneous; 1: one branch at a time
';


my @codeml_tmp_file = ('rub', 'rst', 'rst1', '4fold.nuc', 'lnf', '2NG.t', '2NG.dS', '2NG.dN');


sub run_M0_model {
    my ($nwk, $family_info, $subtree, $chr) = @_;
    my $prefix = $family_info->{'id'}.'.'.$family_info->{'subtaxon'}.'.'.$subtree;
    unlink @codeml_tmp_file;


    # Create ctl file for M0 based on template
    my $ctl = $ctl_template;
    $ctl =~ s{seqfile = __PHY__.phy }{seqfile = $prefix.phy };
    $ctl =~ s{treefile = __TREE__.nwk }{treefile = $prefix.nwk };
    $ctl =~ s{outfile = __OUT__.mlc }{outfile = $prefix.MLC };

    $ctl =~ s{verbose = 1 }{verbose = 0 };
    $ctl =~ s{model = 2 }{model = 0 };
    $ctl =~ s{NSsites = 2 }{NSsites = 0 }; # M0
    $ctl =~ s{method = 0 }{method = 1 };
    $ctl =~ s{fix_blength = 0 }{fix_blength = 1 };

    $ctl =~ s{icode = 0 }{icode = 1 }  if ( $chr eq 'MT' );


    write_file("$prefix.CTL", $ctl);
    write_file("$prefix.nwk", $nwk);


    # Run codeml with M0 model
    # NOTE Need a sandbox to prevent conflicts between temporary codeml files with the same name (e.g. rst) ???
    system("codeml $prefix.CTL >$prefix.codemlM0.log")==0
        or Selectome::Utils::error("Cannot run M0 model in codeml on '$prefix'", $family_info->{'id'});
    unlink @codeml_tmp_file;#, "$prefix.nwk";
    Selectome::Utils::error('Empty M0 model results', $family_info->{'id'}) if ( -z "$prefix.MLC" );


    # Get M0 estimations
    my $mlc = read_file( "$prefix.MLC" )  or  Selectome::Utils::error('Cannot open M0 model results', $family_info->{'id'});
    Selectome::Utils::error('Truncated M0 results', $family_info->{'id'}) if ( $mlc !~ /Time used: / );

    my $kappa = 2; #Default (?) value in codeml doc
    ($kappa)  = $mlc =~ /kappa\s+\(ts\/tv\)\s+=\s+([\d\.eE\-]+)/;

#    my $omega = 0.4; #Default (?) value in codeml doc
#    ($omega)  = $mlc =~ /omega\s+\(dN\/dS\)\s+=\s+([\d\.eE\-]+)/;

    my $estimated_nwk_tree = '';
    ($estimated_nwk_tree)  = $mlc =~ /(\(.+;)\s+Detailed output identifying parameters/;
    $estimated_nwk_tree   =~ s{ }{}g;
    $estimated_nwk_tree   =~ s{;}{:0.0;};


    return($estimated_nwk_tree, $kappa);
}


sub run_M1_model {
    my ($nwk, $family_info, $subtree, $chr) = @_;
    my $prefix = $family_info->{'id'}.'.'.$family_info->{'subtaxon'}.'.'.$subtree;
    unlink @codeml_tmp_file;


    # Create ctl file for M1 based on template
    my $ctl = $ctl_template;
    $ctl =~ s{seqfile = __PHY__.phy }{seqfile = $prefix.phy };
    $ctl =~ s{treefile = __TREE__.nwk }{treefile = $prefix.nwk };
    $ctl =~ s{outfile = __OUT__.mlc }{outfile = $prefix.MLC };

    $ctl =~ s{verbose = 1 }{verbose = 0 };
    $ctl =~ s{model = 2 }{model = 0 };
    $ctl =~ s{NSsites = 2 }{NSsites = 1 }; # M1a
    $ctl =~ s{method = 0 }{method = 1 };
    $ctl =~ s{fix_blength = 0 }{fix_blength = 1 };

    $ctl =~ s{icode = 0 }{icode = 1 }  if ( $chr eq 'MT' );


    write_file("$prefix.CTL", $ctl);
    write_file("$prefix.nwk", $nwk);


    # Run codeml with M1 model
    # NOTE Need a sandbox to prevent conflicts between temporary codeml files with the same name (e.g. rst) ???
    system("codeml $prefix.CTL >$prefix.codemlM1.log")==0
        or Selectome::Utils::error("Cannot run M1 model in codeml on '$prefix'", $family_info->{'id'});
    unlink @codeml_tmp_file;#, "$prefix.nwk";
    Selectome::Utils::error('Empty M1 model results', $family_info->{'id'}) if ( -z "$prefix.MLC" );


    # Get M1 estimations
    my $mlc = read_file( "$prefix.MLC" )  or  Selectome::Utils::error('Cannot open M1 model results', $family_info->{'id'});
    Selectome::Utils::error('Truncated M1 results', $family_info->{'id'}) if ( $mlc !~ /Time used: / );

    my $kappa = 2; #Default (?) value in codeml doc
    ($kappa)  = $mlc =~ /kappa\s+\(ts\/tv\)\s+=\s+([\d\.eE\-]+)/;

#    my $omega = 0.4; #Default (?) value in codeml doc
#    ($omega)  = $mlc =~ /omega\s+\(dN\/dS\)\s+=\s+([\d\.eE\-]+)/;

    my $estimated_nwk_tree = '';
    ($estimated_nwk_tree)  = $mlc =~ /(\(.+;)\s+Detailed output identifying parameters/;
    $estimated_nwk_tree   =~ s{ }{}g;
    $estimated_nwk_tree   =~ s{;}{:0.0;}  if ( $estimated_nwk_tree !~ /\d;$/ );


    return($estimated_nwk_tree, $kappa);
}


sub move_sharp1_in_tree {
    my ($nwk, $family_info, $subtree, $kappa, $chr) = @_;
    my $prefix = $family_info->{'id'}.'.'.$family_info->{'subtaxon'}.'.'.$subtree;

    # )===$branch_nbr=:
    my @nodes = split('==', $nwk); #split on == to keep 1 = to make parsing easier

    $Tree::debug = 0; # Is not verbose anymore
    my $tree = Tree->new($nwk);
    my $is_pseudoroot_a_leaf = 0;
    my @pseudoroot;
    NODE:
    for my $node ( @{$tree->all_nodes} ){
        if ( defined $node->is_pseudo_root && $node->is_pseudo_root ){
            push @pseudoroot, $node->branch_number  if ( defined $node->branch_number );
            $is_pseudoroot_a_leaf = 1  if ( $node->is_leaf eq 'leaf' );
        }
        last NODE  if ( scalar(@pseudoroot) > 1 );
    }
    #TODO test it on a tree without pseudoroot-leaf !
    pop @pseudoroot  if ( $is_pseudoroot_a_leaf==0 && scalar(@pseudoroot) > 1 );
    undef $tree;


    NODE:
    for my $node (@nodes){
        next NODE  if ( $node !~ /=\d+=:/ );
        next NODE  if ( $node =~ /;$/ ); # Root branch useless for codeml in an unrooted-like tree
#        print "[$node]\n";
        my ($branch_length) = $node =~ /:([\d\.eE\-]+)/;
        next NODE  if ( $branch_length == 0 ); #NOTE Exclude branch with a length of 0 (already estimated by codeml in M0/M1 models)


        my ($node_nbr) = $node =~ /=(\d+)=:/;
        # Skip 1 pseudoroot node to avoid computation duplication
        # Skip both pseudoroot nodes if one of them is a leaf, because in an unrooted tree, both are the same leaf !!!
        for my $node_to_exclude ( @pseudoroot ){
            next NODE  if ( $node_to_exclude == $node_nbr );
        }

        # Create tree files with #1 per node
        my $branchfile = '';
        my @noeuds = @nodes;
        for my $diese (@noeuds){
            if ( $node eq $diese ){
                $diese =~ s{=\d+=:}{#1:};
            }
            else {
                $diese =~ s{=\d+=:}{:};
            }
            $branchfile .= $diese;
        }
        $branchfile .= "\n";
        write_file("$prefix.$node_nbr.nwk", $branchfile);


        create_CTL_file($prefix, $node_nbr, 0, $kappa, $chr); #For non-fixed parameters
        create_CTL_file($prefix, $node_nbr, 1, $kappa, $chr); #For     fixed parameters
    }

    return;
}


sub create_CTL_file {
    my ($prefix, $node_nbr, $is_fixed, $kappa, $chr) = @_;
    my $prefix0 = $prefix;
    $prefix    .= '.'.$node_nbr;

    my $ctlfile    = "$prefix.H1.ctl";
    my $mlcfile    = "$prefix.H1.mlc";
    my $fix_omega  = 0;
    if ( $is_fixed ){
        $ctlfile   = "$prefix.H0.ctl";
        $mlcfile   = "$prefix.H0.mlc";
        $fix_omega = 1;
    }


    my $ctl = $ctl_template;
    $ctl =~ s{seqfile = __PHY__.phy }{seqfile = $prefix0.phy };
    $ctl =~ s{treefile = __TREE__.nwk }{treefile = $prefix.nwk };
    $ctl =~ s{outfile = __OUT__.mlc }{outfile = $mlcfile };

    $ctl =~ s{kappa = 2 }{kappa = $kappa };

    $ctl =~ s{fix_omega = 0 }{fix_omega = $fix_omega };
#    $ctl =~ s{omega = .4 }{omega = $omega };

    $ctl =~ s{fix_blength = 0 }{fix_blength = 1 };

    # Change genet code if MT genes
    $ctl =~ s{icode = 0 }{icode = 1 }  if ( $chr eq 'MT' );
    #TODO Need to adapt genetic code for non-vertebrate species ...

    write_file("$ctlfile", $ctl);

    #TODO Best Small_Diff value ?

    return;
}



sub get_chromosomal_localization {
    my ($tree, $aln) = @_;

    # Get chromosomal localization
    CHR:
    for my $seq_in_aln ( $aln->each_seq ){
        my $leaf = $tree->find_leaf_by_name($seq_in_aln->display_id);

#        print "\t", $leaf->dnafrag()->name(), "\n";
        return 'MT'  if ( $leaf->dnafrag()->name() eq 'MT' ); #TODO check new method returns what expected
    }

    return '';
}

1;

