package Selectome::MSA;
#File Selectome/MSA.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Selectome::Utils;
use Selectome::Tree;

use Bio::AlignIO;
use Bio::SimpleAlign;

use File::Copy;
use File::Slurp;



use File::Which;
BEGIN {
    $ENV{'PATH'} = '/usr/local/guidance/www/Guidance:'.$ENV{'PATH'};
    # Test if required executables are in the path
    warn('Cannot reach "t_coffee"')     if ( ! which('t_coffee') );
    warn('Cannot reach "trimal"')       if ( ! which('trimal') );
    warn('Cannot reach "maxalign.pl"')  if ( ! which('maxalign.pl') );
    warn('Cannot reach "mafft"')        if ( ! which('mafft') );
    warn('Cannot reach "muscle"')       if ( ! which('muscle') );
    warn('Cannot reach "clustalo"')     if ( ! which('clustalo') );
    warn('Cannot reach "clustalw2"')    if ( ! which('clustalw2') );
    warn('Cannot reach "kalign"')       if ( ! which('kalign') );
    warn('Cannot reach "guidance"')     if ( ! which('guidance.pl') );

    #Clear Tcoffee tmp cache directory
    system('rm -Rf /var/tmp/t_coffee.tmp.*/*');
    system('rm -f *_TCtmp');
}


$ENV{'NO_ERROR_REPORT_4_TCOFFEE'} = 1;
#$ENV{'MAFFT_BINARIES'}            = '/opt/mine/bin/_bioinfo/_MSA/mafft/libexec/mafft/'; #pcdee3220 settings !
#$ENV{'MAFFT_BINARIES'}            = '/usr/local/mafft/libexec/mafft/'; #for devbioinfo

my $MCoffee_threshold  = '0-5';
my $Guidance_aligner   = 'MAFFT';
my $Guidance_threshold = 0.93; # Or even more !
my $min_leaf_number    = $Selectome::Utils::min_leaf_number;
my $branchid_length    = $Selectome::Utils::branchid_length;


# FIXME Mask non-amino acids by X ?.
# Do mask selenocysteine (U aa) because it is not yet properly managed by codeml (a codon stop is used to code for U, e.g. in human) and MSA programs.
# The same for Pyrrolysine (O aa) !!!!
#my @aa = qq(A R N D C E Q G H I L K M F P S T W Y V);
#my $aa = join('', @aa);


###############  MSA building  ###############

sub align_by_MAFFT {
    my ($family_info, $msa_filename) = @_;

    system("mafft  --auto --reorder  --quiet  --anysymbol  $msa_filename > $msa_filename.mafft")==0  #NOTE --anysymbol is used to score U (selenocysteine) and any other special chars as X is
        or Selectome::Utils::error("Cannot run MAFFT MSA building on '$msa_filename'", $family_info->{'id'});
    #TODO Enable multi-threading in tools using MAFFT (MCoffee/Guidance) ???
    #NOTE Use our own binary tree as guide tree in MAFFT (http://mafft.cbrc.jp/alignment/software/treein.html) ???

    return 0;
}


sub align_by_newMAFFT {
    my ($family_info, $msa_filename) = @_;

    system("mafft  --globalpair  --reorder  --quiet  --anysymbol  --allowshift  $msa_filename > $msa_filename.newmafft")==0  #NOTE --anysymbol is used to score U (selenocysteine) and any other special chars as X is
        or Selectome::Utils::error("Cannot run new MAFFT MSA building on '$msa_filename'", $family_info->{'id'});
    #TODO Enable multi-threading in tools using MAFFT (MCoffee/Guidance) ???
    #NOTE Use our own binary tree as guide tree in MAFFT (http://mafft.cbrc.jp/alignment/software/treein.html) ???

    return 0;
}


###############  MSA format convertion  ###############

sub write_MSA {# From aln Object to fasta file
    my ($aln, $filename_prefix, $suffix_1, $suffix_2) = @_;

    # Write it in fasta format
    my $aln_out = Bio::AlignIO->new(
        -file             => "> $filename_prefix.$suffix_1",
        -format           => 'fasta',
        -displayname_flat => 1,
    );

    $aln_out->write_aln($aln);

    #FIXME ensembl or bioperl API problem ?????
    # Sometimes, ensembl API returns sequences with length info, e.g.: ENSCJAP00000009838/1-575
    # This breaks everything because names in sequence files does not match names in tree object anymore.
    #                         add some non-alphanumeric char for ensembl metazoa id
#    system("perl -i -pe 's/>([\\w:\\.\\-]+).*\$/>\$1/' $filename_prefix.$suffix_1"); NOTE Should be useless with -displayname_flat

    #Remove columns 100% gapped
    remove_completely_gapped_columns("$filename_prefix", $suffix_1, $suffix_2);

    return;
}


sub backTranslate_mask {
    my ($nt_filename, $aa_masked_filename, $chr) = @_;

    sub read_MSA {
        my ($inputMSA, $is_ref_msa) = @_;

        $/ = '>'; # Local changes
        my @seq = read_file("$inputMSA");
        my ($MSA, $order);
        my $count = 0;
        SEQ:
        for my $seq ( @seq ){
            next SEQ if ( $seq eq '>' );

            my ($header) = $seq =~ /^(.+)\n/;

            my $sequence = $seq;
            $sequence =~ s{>?$header}{};
            $sequence =~ s{[\n\r>]}{}g;
            $sequence =~ s{[\-\.]}{}g   if ( ! $is_ref_msa );# Remove only that because AA MSA may contain special char, e.g. * (stop codons)

            $count++;
            $order->{$count} = $header;
            $MSA->{$header}  = $sequence;
        }

        return ($MSA, $order);
    }


    # Read AA MSA
    my ($AA, $order)  = read_MSA("$aa_masked_filename", 1);
    # Read nt MSA
    my ($nt, undef) = read_MSA("$nt_filename", 0);
    $/ = "\n";


    # AA MSA gaps mapped on nt MSA
    my $nt_MSA       = '';
    my $seleno_there = 0;
    AA_MSA:
    for my $header ( map { $order->{$_} } sort {$a <=> $b} keys(%$order) ){
        if ( exists($nt->{$header}) ){
            my $aa_seq = $AA->{$header};
            my $nt_seq = $nt->{$header};

            my $nt_remapped_seq = '';
            my $nt_position     = 0;
            POS:
            for ( my $pos = 0; $pos < length($aa_seq); $pos++ ){
                # Replace gaps
                if ( substr($aa_seq, $pos, 1) eq '-' ){
                    $nt_remapped_seq .= '---';
                }
                # Masking
                elsif ( substr($aa_seq, $pos, 1) eq 'X' ){ # Original undefined base
                    $nt_remapped_seq .= 'NNN';
                    $nt_position += 3;
                }
                elsif ( substr($aa_seq, $pos, 1) eq 'x' ){ # Our masking
                    $nt_remapped_seq .= 'nnn';
                    $nt_position += 3;
                }
                else {
                    my $codon = substr($nt_seq, $nt_position, 3);
                    # ACHTUNG Only for Vertebrates !!!
                    if ( $chr ne 'MT' && (uc $codon eq 'TGA' || uc $codon eq 'TAA' || uc $codon eq 'TAG') ){
                        $codon = 'nnn'; # Replace selenocystein and stop codons by undefined
                        $seleno_there++;
                    }
        # Replace selenocystein and stop codons by undefined
                    $nt_remapped_seq .= $codon;
                    $nt_position += 3;
                }
            }

            $nt_MSA .= ">$header\n$nt_remapped_seq\n";
        }
        else {
            print {*STDERR} "\t$header does NOT exist in the nucleotide alignment you have provided\n";
        }
    }

    print "\t\t$seleno_there Selenocystein or Stop codon(s) found\n"  if ( $seleno_there );
    write_file("$nt_filename.preTrimAl", $nt_MSA);

    return 0;
}


sub write_MCoffee_MSA {
    my ($mcoffee_msa, $filename_prefix, $family_info, $chr) = @_;

    open(my $AA_SEQ, '>', "$filename_prefix.aa_f.fas") or Selectome::Utils::error("Cannot run write final filtered MSA", $family_info->{'id'});
    open(my $NT_SEQ, '>', "$filename_prefix.nt_f.fas") or Selectome::Utils::error("Cannot run write final filtered MSA", $family_info->{'id'});
    my $seleno_there = 0;
    LEAF:
    for my $leaf_stable_id ( keys(%$mcoffee_msa) ){
        my $aa_seq = $mcoffee_msa->{$leaf_stable_id}->{'seq_aa'};
        $aa_seq    =~ s{U}{x}ig;  # Replace selenocystein AA by undefined
        $aa_seq    =~ s{\*}{x}ig; # Replace stop codon by undefined
        print {$AA_SEQ} '>', $leaf_stable_id, "\n", $aa_seq, "\n";

        my $NT_seq = $mcoffee_msa->{$leaf_stable_id}->{'seq_nt'};
        my $nt_seq = '';
        while( $NT_seq =~ /(...)/g ){
            # ACHTUNG Only for Vertebrates !!!
            if ( $chr ne 'MT' && ($1 =~ /^TGA$/i || $1 =~ /^TA[AG]$/i) ){
                $nt_seq .= 'nnn'; # Replace selenocystein and stop codons by undefined
                $seleno_there++;
            }
            else {
                $nt_seq .= $1;
            }
        }
        print {$NT_SEQ} '>', $leaf_stable_id, "\n", $nt_seq, "\n";
    }
    close $AA_SEQ;
    close $NT_SEQ;
    print "\t\t$seleno_there Selenocystein or Stop codon(s) found\n"  if ( $seleno_there );


    #Remove columns 100% gapped
    remove_completely_gapped_columns("$filename_prefix", 'aa_f.fas', 'aa.fas');
    remove_completely_gapped_columns("$filename_prefix", 'nt_f.fas', 'nt.fas');

    return;
}

sub convert_fasta_to_phylip {
    my ($infile, $outfile) = @_;

    $/ = "\n";
    my $in  = Bio::AlignIO->new(
                -file             => "$infile",
                -format           => 'fasta',
                -alphabet         => 'dna',
              );
    my $out = Bio::AlignIO->new(
                -file             => "> $outfile",
                -interleaved      => 0,
                -format           => 'phylip',
                -idlength         => 30, #WARN: seq name + spaces before seq -> 30 of length
              );

    while ( my $aln = $in->next_aln() ){
        $out->write_aln($aln);
    }

    return;
}


sub protect_phylip_msa {
    my ($nwk, $filename_prefix) = @_;

    # Get codon MSA in phylip
    my $msa = read_file( "$filename_prefix.phy" );
    copy("$filename_prefix.phy", "$filename_prefix.phy.ORI");


    my $species_nbr = 0;
    my $map         = '';
    # Mapping file to recover protected gene names
    while( $nwk =~ m/[\(,]([A-Z0-9][\w\.\-]+).*?:/ ){
        my $seq_name = $1;
        $species_nbr = sprintf("%0${branchid_length}d", ++$species_nbr);
        $nwk =~ s{([\(,])[A-Z0-9][\w\.\-]+(.*?):}{$1a$species_nbr$2:};

        #10: old phylip seq start place
        Selectome::Utils::error("Too long branchid_length variable !")  if ( (10 - $branchid_length - 1) < 1 );
        my $spacer = ' ' x (10 - $branchid_length - 1);
        #NEED this for seq to start all at the same place in phylip
        # because some seq names are shorter (human ones) !
        $msa =~ s{$seq_name *}{a$species_nbr$spacer}g;
        $map .= "$seq_name\ta$species_nbr\n";
    }
    write_file("$filename_prefix.map", $map);


    # Write protected codon MSA in phylip
    check_protected_msa($msa);
    write_file("$filename_prefix.phy", $msa);


    # Check if no remaining old seq names still in newick tree
    Selectome::Tree::check_protected_nwk($nwk);

    return ($nwk);
}


sub check_protected_msa {
    my ($msa) = @_;

    MSA:
    while( $msa   =~ m/^(\w+\s+)/ ){
        my $match = $1;
        my ($seq_id ) = $match =~ /^([^ ]+)/;
        if ( $seq_id !~ /^a\d{$branchid_length}$/ ){
            Selectome::Utils::error("Some taxon names have not been replaced properly in this phylip alignment: [$seq_id]");
        }
        $msa =~ s{$match}{};
    }

    return 0;
}


###############  MSA cleaning           ###############

sub remove_completely_gapped_columns {
    # Remove columns 100% gapped
    my ($filename_prefix, $suffix_1, $suffix_2) = @_;

    #FIXME Or better with trimAl because exit code trapping problem with the sed after tcoffee ?!?
    #FIXME TrimAl problem: it adds seq_length bp after the seq_name !!!

    # Could be replaced by other tools like TrimAL for this !!!!
    #FIXME Some ensembl transcripts have stop codons (* in AA seq) that break trimAl 1.2
    system("perl -i -pe 's/\\\*/x/g' $filename_prefix.$suffix_1");
    #                                                                                   add some non-alphanumeric char for ensembl metazoa id
    system("trimal -in $filename_prefix.$suffix_1 -noallgaps -keepseqs | perl -pe 's/>([\\w:\\.\\-]+).*\$/>\$1/' > $filename_prefix.$suffix_2")==0
        or Selectome::Utils::error("Cannot remove 100% gapped columns for '$filename_prefix.$suffix_1'", $filename_prefix);
    unlink "$filename_prefix.$suffix_1";

    Selectome::Utils::error("Removing 100% gapped columns failed for '$filename_prefix.$suffix_1'", $filename_prefix)  if ( ! -e "$filename_prefix.$suffix_2" || -z "$filename_prefix.$suffix_2" );

    return;
}




###############  MSA information        ###############

sub get_seq_length {
    my ($tree, $aln) = @_;

    my %seq_length_info;
    # Get seq length in the MSA $aln
    SEQ:
    for my $seq_in_aln ( $aln->each_seq ){
        # print Dumper($seq_in_aln);

        my $seq  = $seq_in_aln->seq;
        $seq     =~ s{-}{}g; # Remove gaps
        my $Seq  = $seq;
        $seq     =~ s{X}{}ig;
        printf "%s%5s\t%5d\t%5d\n", $seq_in_aln->display_id, ' ', length($Seq), length($seq);
        $seq_length_info{$seq_in_aln->display_id} = length($seq);
    }

    return \%seq_length_info;
}


sub get_msa_length {
    #FIXME Get this from ensembl API ?????
    my ($msa_filename) = @_;

    my $MSA_length = `t_coffee -other_pg seq_reformat -in $msa_filename -output statistics -cache=no 2>/dev/null  | tail -1 | awk '{print \$5}'`;
    Selectome::Utils::error("Cannot get MSA length info for '$msa_filename'", $msa_filename)  if ( $? != 0 );
    chomp $MSA_length;

    return $MSA_length;
}



###############  MSA filtering          ###############

sub filter_by_maxAlign {
    my ($family_info, $msa_filename, $tree) = @_;

    Selectome::Utils::error("Empty file '$msa_filename'", $family_info->{'id'})  if ( ! -e "$msa_filename" || -z "$msa_filename" );

    replace_X_by_gap("$msa_filename");
    remove_completely_gapped_columns("$msa_filename", 'Xless', 'Xless_cleaned');

    Selectome::Utils::error("Empty file '$msa_filename'", $family_info->{'id'})  if ( ! -e "$msa_filename.Xless_cleaned" || -z "$msa_filename.Xless_cleaned" );

    system('maxalign.pl', "$msa_filename.Xless_cleaned")==0  or Selectome::Utils::error("Cannot run MaxAlign filtering on '$msa_filename'", $family_info->{'id'});
    unlink("$msa_filename.Xless");

    my @exclusion;
    if ( -s 'heuristic_exclude_headers.txt' ){
        $/ = "\n";
        @exclusion = read_file('heuristic_exclude_headers.txt', chomp => 1);

        my $excluded_nbr = 0;
        for my $excluded_seq ( @exclusion ){
            my $leaf = $tree->find_leaf_by_name($excluded_seq);
            # Code more robust for non-protein id in $excluded_seq
            if ( ! defined $leaf ){
                my $member_adaptor = $family_info->{'registry'}->get_adaptor($family_info->{'multi'}, 'compara', 'Member');
                my $gene_member    = $member_adaptor->fetch_by_source_stable_id('ENSEMBLGENE', $excluded_seq);
                my $peptide_member = $gene_member->get_canonical_Member();
                $leaf = $tree->find_leaf_by_name($peptide_member->stable_id);
            }
            printf "excluding: %8s\t%s\n", $leaf->node_id, $excluded_seq;
            $tree    = $tree->remove_nodes([$leaf]);
            $excluded_nbr++;
        }
        print "\t$excluded_nbr sequences excluded\n";
    }

    unlink('report.txt', 'heuristic_include_headers.txt', 'heuristic_exclude_headers.txt', 'heuristic.fsa', "$msa_filename.Xless_cleaned");

    return ($tree, \@exclusion);
}


sub replace_X_by_gap {
    my ($msa_filename) = @_;

    system("perl -pe 'if ( /^[^>]/ ){s/X/-/ig;}' $msa_filename > $msa_filename.Xless");

    return;
}

sub check_masking {
    my ($family_info, $msa_filename) = @_;

    replace_X_by_gap("$msa_filename");
    remove_completely_gapped_columns("$msa_filename", 'Xless', 'Xless_cleaned');

    my $status = filter_by_trimal($family_info, "$msa_filename.Xless_cleaned");
    unlink "$msa_filename.Xless_cleaned";

    return $status;
}


sub filter_by_trimal {
    my ($family_info, $msa_filename) = @_;

    #NOTE Must be TrimAl >1.4 ???
    #TODO check exception with Tribolium names

    # Keep only columns with at least 3 sequences (no columns with 1 or 2 residues alone)
    my $seq_nbr = `grep '>' $msa_filename | wc -l`;
    chomp $seq_nbr;
#    my $gapthreshold = ( $min_leaf_number / $seq_nbr ) + 0.0001;
    my $gapthreshold = ( 4 / $seq_nbr ) + 0.0001; # 4 residues per column to get an internal branch
    $gapthreshold    = 1  if ( $gapthreshold > 1 );


    my ($output_name) = $msa_filename =~ /^(.+?)\.\w+$/;

    # Filtering by TrimAl
    system("trimal -in $msa_filename  -out $output_name.trimal  -keepseqs -gapthreshold $gapthreshold  -colnumbering  > $output_name.trimal.stat")==0
        or Selectome::Utils::error("Cannot run TrimAl filtering on '$msa_filename'", $family_info->{'id'});

    if ( !-e "$output_name.trimal" || -z "$output_name.trimal" ){
        return 1;
    }
    return 0;
}


sub score_by_Guidance {
    my ($family_info, $msa_filename) = @_;
    #NOTE Guidance requires an unaligned input dataset if used without a seed MSA!
    #     but problems in release 1.4.1 and 1.5 with --msaFile argument !
    system("perl -pe 's/-//g  if ( !/^>/ )' $msa_filename > $msa_filename.unaligned");


    # Compute guidance scores
    #TODO Try to use Guidance locally on Vital-IT nodes
    #TODO Adjust proc_num value!
#    system("guidance.pl  --seqFile $msa_filename.unaligned  --program GUIDANCE  --msaProgram $Guidance_aligner  --seqType aa  --outOrder as_input  --proc_num 1  --outdir /tmp/$msa_filename.GUIDANCE")==0
#        or Selectome::Utils::error("Cannot run Guidance-$Guidance_aligner score filtering on '$msa_filename'", $family_info->{'id'});

    system("guidance.pl  --seqFile $msa_filename.unaligned  --program GUIDANCE2  --msaProgram $Guidance_aligner  --seqType aa  --MSA_Param \\\\-\\\\-globalpair \\\\-\\\\-allowshift  --outOrder as_input  --proc_num 1  --outDir /tmp/$msa_filename.GUIDANCE")==0
        or Selectome::Utils::error("Cannot run Guidance-new$Guidance_aligner score filtering on '$msa_filename'", $family_info->{'id'});
    unlink "$msa_filename.unaligned";

    Selectome::Utils::error("Applying Guidance-$Guidance_aligner score filtering crashed")  if ( -z "/tmp/$msa_filename.GUIDANCE/MSA.$Guidance_aligner.Guidance2_res_pair_res.scr" );


    # Parse results
    my @scores = grep { $_ !~ /^#/ } read_file("/tmp/$msa_filename.GUIDANCE/MSA.$Guidance_aligner.Guidance2_res_pair_res.scr", chomp => 1);
    my ($row_scores, $col_scores);
    SCORES:
    for my $score (@scores){ #COL_NUMBER  #ROW_NUMBER  #RES_PAIR_RESIDUE_SCORE
        if ( $score =~ /^\s*(\d+)\s+(\d+)\s+(\S+)$/ ){
            my ($col, $row, $val) = ($1, $2, $3);
            $val = 1  if ( $val eq 'nan' || $val eq '-nan'); # Because a single residue per column, so properly excluded by MAFFT AND not used by codeml after triaml cleaning

            $row_scores->{$row}->{$col} = $val;
#            $col_scores->{$col}->{$row} = $val;
        }
        else {
            print {*STDERR} "\tGUIDANCE problem: $score\n";
        }
    }


    # Apply scoring on MSA
    {# Local changes only !!!
        $/ = '>';
        my @MSA_seq   = read_file("$msa_filename");
        my @clean_MSA = ('>'); #First fasta delimiter
        my $seq_count = 0;
        SEQ:
        for my $seq ( @MSA_seq ){
            next SEQ  if ( $seq eq '>' );

            $seq_count++;
            my ($header) = $seq =~ /^>?(.+)\n/;
            my $sequence = $seq;
            $sequence =~ s{\n}{}g;
            $sequence =~ s{^>?$header}{};
            $sequence =~ s{>$}{};


            # Mask low scores for each sequence
            RES:
            for (my $j=0; $j < length($sequence); $j++){
                next RES  if ( substr($sequence, $j, 1) eq '-' ); # Skip gaps

                if ( exists $row_scores->{$seq_count}->{$j+1} && $row_scores->{$seq_count}->{$j+1} <= $Guidance_threshold ){ # Low score
                    substr($sequence, $j, 1, 'x');
                }
#               elsif ( ! exists $row_scores->{$seq_count}->{$j+1} ){
#                   print {*STDERR} "[$seq_count][$j+1]\n";
#               }
            }


            push @clean_MSA, ">$header\n$sequence\n";
        }
        shift @clean_MSA;
        write_file("$msa_filename.Guidance", @clean_MSA);
#       print @clean_MSA;
    }
    $/ = "\n";


    # Cleaning
    move("/tmp/$msa_filename.GUIDANCE/MSA.$Guidance_aligner.Guidance2_res_pair_res.scr", "$msa_filename.Guidance.score");
#    unlink "$msa_filename.clean"; # If Guidance without seed MSA is used !!!
    system("rm  -Rf  /tmp/$msa_filename.GUIDANCE/");

    return 0;
}


sub score_by_MCoffee {
    my ($family_info, $msa_filename) = @_;

    #Compute MCoffee score
    #with different methods if mcoffee fails following Ensembl methodology: http://www.ensembl.org/info/genome/compara/homology_method.html (note 3)
    system("t_coffee -infile $msa_filename -method mafft_msa, muscle_msa, clustalo_msa, t_coffee_msa -score -output score_ascii -cache=no -outorder input  >/dev/null 2>&1")==0
    ||
    #Mafft only cannot produce scores in MCoffee !!!
    Selectome::Utils::error('MCoffee crashed, no way to get scores', $family_info->{'id'});

    my $scores = $msa_filename;
    $scores   =~ s{\.\w+$}{\.score_ascii};
    move("$scores", "$msa_filename.mcf.score");

    # Replace lower scores by x in AA
    system("t_coffee -other_pg seq_reformat -in $msa_filename -struc_in $msa_filename.mcf.score -struc_in_f number_aln -action +convert '[$MCoffee_threshold]' '#x' -cache=no > $msa_filename.mcf 2>/dev/null")==0
        or Selectome::Utils::error('Applying MCoffee score filtering crashed on AA', $family_info->{'id'});
    unlink ( glob('*.dnd') );
    Selectome::Utils::error('Applying MCoffee score filtering crashed on AA!', $family_info->{'id'}) if ( -z "$msa_filename.mcf" );#File existence is tested with the open afterwards.

    return 0;
}


sub merge_scores {
    my ($ref_filename, @other_filenames) = @_;

    #TODO Produce mask map file for Jalview feature file later, on the web site !
    my $merged_scores = $ref_filename;
    $merged_scores   =~ s{\.\w+$}{\.ScoreMerged};
    copy("$ref_filename", "$merged_scores");

    for my $scored_MSA (@other_filenames){
        #system ("touch $scored_MSA");
        #NOTE Assume score files have the same length, with residues at the same position !!!
        my @ref_MSA = msa_on_1_line("$merged_scores");
        my @scores  = msa_on_1_line("$scored_MSA");


        my @merged_scores;
        REF:
        for (my $u = 0; $u <= $#ref_MSA; $u++ ){
            # Fasta header
            if ( $ref_MSA[$u] =~ />/ ){
                push @merged_scores, $ref_MSA[$u];
                next REF;
            }

            SEQ:
            for (my $v = 0; $v < length($ref_MSA[$u]); $v++){
                # Apply masking on the ref MSA
                if ( substr($scores[$u], $v, 1) eq 'x' ){ # 'X' should be masked in all score files because all come from MAFFT !!!
                    substr($ref_MSA[$u], $v, 1)  = 'x';
                }
            }

            push @merged_scores, $ref_MSA[$u];
        }

        write_file("$merged_scores", join("\n", @merged_scores) );
    }


    return 0;
}


#############################################

sub msa_on_1_line {
    my ($file) = @_;

    my $infile = read_file("$file");
    $infile    =~ s{(>[^\n]+)}{Z_Z$1Z_Z}g;
    $infile    =~ s{\n}{}g;
    my @temp   = split /Z_Z/, $infile;
    shift @temp;

    return @temp;
}

#FIXME Should do a general module to clean temporary files, gzipping, ...


1;

