package Selectome::Tree;
#File Selectome/Tree.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

use Selectome::Utils;
#use Selectome::Codeml;

#use File::Which;
BEGIN {
    # Test if required executables are in the path
    #warn('Cannot reach "nw_reroot" from newick-utils')  if ( ! which('nw_reroot') );
}


my $branchid_length = $Selectome::Utils::branchid_length;


sub get_leaf_number {# For a tree Object from ensembl API !
    my ($tree) = @_;

    my @leaves;
    if ( defined $tree ){
        @leaves = @{$tree->get_all_leaves};
    }

    return $#leaves+1;
}


sub label_newick_branches {
    my ($nwk, $family_info, $label_leaves, $subtree, $chr) = @_;


    # Estimate tree branch length / kappa with codeml M1 model
    $nwk =~ s{:[\d\.eE-]*;}{:0.0;}; #Remove root length
    my $kappa      = 2;
    #($nwk, $kappa) = Selectome::Codeml::run_M1_model($nwk, $family_info, $subtree, $chr);


    my $branch_nbr = 0;
    if ( $label_leaves ){# Label internal and external (leaves) branches
        while( $nwk =~ m/[^=]:/ ){# Cannot use /g modifier because positions change with substitution so infinite loop expected
            $branch_nbr = sprintf("%0${branchid_length}d", ++$branch_nbr);
            $nwk =~ s{([^=]):}{$1===$branch_nbr=:};
        }
    }
    else {# Default: Label only internal branches
        #NOTE do all nodes from ensembl have this syntax:   ):\d   ???
        while( $nwk =~ m/\):[\d\.eE\-]*/ ){
            $branch_nbr = sprintf("%0${branchid_length}d", ++$branch_nbr);
            $nwk =~ s{\):([\d\.eE\-]*)}{)===$branch_nbr=:$1};
        }
    }
    Selectome::Utils::error('Wrong Newick tree structure', $family_info->{'id'})  if ( $nwk =~ /(\)...)/ && $1 !~ /^\)===$/ );

    $nwk =~ s{:[\d\.eE-]*;}{:0.0;}; #Remove root length

    return($nwk, $kappa);
}


sub map_nwk_to_nhx {
    my ($nhx, $nwk) = @_;

    my @nwk_nodes = split(/\)+/, $nwk);
    my @nhx_nodes = split(/\)+/, $nhx);
    NODE:
    for (my $i=0; $i < scalar(@nwk_nodes); $i++){
        next NODE  if ( $nwk_nodes[$i] !~ /^===\d+=:/ );

        # ===025=:0.176310
        my ($node_nbr, $branch_lengh)  = $nwk_nodes[$i] =~ /^(===\d+=):([\d\.eE\-]*)/;
        # Clupeocephala:0.1659[&&NHX:D=N:B=97:T=186625]
        my ($nhx_tag, $nhx_to_replace) = $nhx_nodes[$i] =~  /^(([^:]*):[\d\.eE\-]*)/;

        # Should always replace the right one because processed in the same order for both trees !
        # ===025=Clupeocephala:0.176310[&&NHX:D=N:B=97:T=186625]
        $nhx =~ s{\)$nhx_tag\[}{\)$node_nbr$nhx_to_replace:$branch_lengh\[};
        # Put ) & [ as anchors to avoid as much as it can substitutions at the same place for similar taxon:branch_length in the same tree
        # e.g. )Pelodiscus_sinensis:0[
    }

    my @nwk_branches = split(/[\(,]+/, $nwk);
    my @nhx_branches = split(/[\(,]+/, $nhx);
    LEAF:
    for (my $j=0; $j < scalar(@nwk_branches); $j++){
        next LEAF  if ( $nwk_branches[$j] !~ /^a\d+:/ );

        # a027:0.130397
        my ($leaf_name, $branch_lengh)  = $nwk_branches[$j] =~ /^(a\d+):([\d\.eE\-]*)/;
        # ENSTRUP00000004123:0.0583[&&NHX:D=N:G=ENSTRUG00000001786:T=31033]
        my ($nhx_to_replace, $nhx_name) = $nhx_branches[$j] =~ /^(([^:]*):[\d\.eE\-]*)/;

        # Do 2 replacements because branch length only could be ambiguous
        $nhx =~ s{$nhx_to_replace}{$leaf_name:$branch_lengh};
        $nhx =~ s{$leaf_name:}{$nhx_name:};
    }

    return $nhx;
}


sub check_protected_nwk {
    my ($nwk) = @_;

    #FIXME Could be implemented in a better way with \G in regexp

    #MUST not stay some non-a\d+ taxon names inside !
    TREE:
    while( $nwk  =~ m/[\(,]([^:\(]+):/ ){
        my $match = $1;
        if ( $match !~ /^a\d{$branchid_length}$/ && $match !~ /^a\d{$branchid_length}===\d{$branchid_length}=$/ ){
            Selectome::Utils::error("Some taxon names have not been replaced properly in this Newick tree: [$match]");
        }
        $nwk =~ s{$match}{};
    }

    return 0;
}

1;

