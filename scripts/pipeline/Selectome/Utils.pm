package Selectome::Utils;
#File Selectome/Utils.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;


# Parameters
our $min_leaf_number    = 10;
our $min_species_number = 6;

# Display parameters
our $famid_length       = 3;
our $branchid_length    = 3;


sub warnings {
    my ($family_info, $msg, $subtree, $severity) = @_;

    open(my $WARN, '>>', 'selectome_warning');
    print {$WARN} "$family_info->{'id'}\t$family_info->{'subtaxon'}\t$subtree\t$msg\t$severity\n";
    close $WARN;

#    $msg .= "\n    -----" if ( $msg =~ /^Subtree (too large|with less) / );
    print "\n\t$msg\n\n";

    return;
}


sub error {
    my ($msg, $id) = @_;

    print "\n\tERROR: $msg\n\n";
    system("mv $id.* error/")  if ( defined($id) ); #Not easy to remove this system call !
    exit 1;
}


1;

