package Selectome::Taxa;
#File Selectome/Taxa.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;


our $valid_taxa = {#'vertebrata'         => 7742, #Vertebrata and descents
                   #'vertebrates'        => 7742,
                   #'vertebrate'         => 7742,
                   #'7742'               => 7742,

                   'bilateria'          => 33213,

                   'euteleostomi'       => 117571,
                   'vertebrates'        => 117571,
                   'vertebrate'         => 117571,
                   '117571'             => 117571,

                   'amniota'            => 32524,
                   'amniote'            => 32524,
                   'amniotes'           => 32524,
                   '32524'              => 32524,

                   'theria'             => 32525,
                   'mammals'            => 32525,
                   '32525'              => 32525,

                   'eutheria'           => 9347,
                   'placentals'         => 9347,
                   '9347'               => 9347,

                   'sauropsida'         => 8457,
                   'sauropsids'         => 8457,
                   'birds+reptiles'     => 8457,
                   '8457'               => 8457,

                   'glires'             => 314147,
                   'rodents+rabbits'    => 314147,
                   '314147'             => 314147,

                   'clupeocephala'      => 186625, # coelacanth is not included
                   'bony fishes'        => 186625,
                   '186625'             => 186625,

                   # Primates large meaning not Catarrhini (9526) only !
                   'primates'           => 9443,
                   'primate'            => 9443,
                   'primata'            => 9443,
                   '9443'               => 9443,

                   'euarchontoglires'   => 314146,
                   'primates+rodents'   => 314146,
                   '314146'             => 314146,

                   'arthropoda'         => 6656, #Arthropoda and descents
                   'arthropods'         => 6656,
                   'arthropod'          => 6656,
                   '6656'               => 6656,

                   'hexapoda'           => 6960,
                   'insects'            => 6960,
                   '6960'               => 6960,

                   'drosophila'         => 7215, # Drosophila genera
                   'fruit flies'        => 7215,
                   '7215'               => 7215,

                   'diptera'            => 7147, # Flies
                   'flies'              => 7147,
                   '7147'               => 7147,

                   'nematoda'           => 6231, #Nematoda and descents
                   'roundworms'         => 6231,
                   'roundworm'          => 6231,
                   'nematodes'          => 6231,
                   'nematode'           => 6231,
                   'nemata'             => 6231,
                   '6231'               => 6231,

                  # Taxonomic ranges for them ?
                   #protists
                   #fungi
                   #bacteria
                   #plants
};

1;

