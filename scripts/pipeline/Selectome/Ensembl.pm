package Selectome::Ensembl;
#File Selectome/Ensembl.pm

# Perl embedded modules
use strict;
use warnings;
use diagnostics;

sub get_chromosomal_localization {
    my ($tree, $aln) = @_;

    # Get chromosomal localization
    CHR:
    for my $seq_in_aln ( $aln->each_seq ){
        my $leaf = $tree->find_leaf_by_name($seq_in_aln->display_id);

    #print "\t", $leaf->dnafrag()->name(), "\n";
        return 'MT'  if ( $leaf->dnafrag()->name() eq 'MT' ); #TODO check new method returns what expected
    }
    return '';
}

sub get_species_number {
    my ($tree, $aln) = @_;
    my %species_list;

    for my $seq_in_aln ( $aln->each_seq ) {
        my $leaf = $tree->find_leaf_by_name($seq_in_aln->display_id);

        if ($leaf) {
            $species_list{$leaf->taxon()->name()}=0;
        }
    }
    my $species_number = scalar keys %species_list;
    print "\t Species number: $species_number\n ";
    return $species_number;
}

sub write_MSA {# From aln Object to fasta file
    my ($aln, $filename_prefix, $suffix_1, $suffix_2) = @_;

    # Write it in fasta format
    my $aln_out = Bio::AlignIO->new(
        -file             => "> $filename_prefix.$suffix_1",
        -format           => 'fasta',
        -displayname_flat => 1,
    );

    $aln_out->write_aln($aln);

    #FIXME ensembl or bioperl API problem ?????
    # Sometimes, ensembl API returns sequences with length info, e.g.: ENSCJAP00000009838/1-575
    # This breaks everything because names in sequence files does not match names in tree object anymore.
    #                         add some non-alphanumeric char for ensembl metazoa id
#    system("perl -i -pe 's/>([\\w:\\.\\-]+).*\$/>\$1/' $filename_prefix.$suffix_1"); NOTE Should be useless with -displayname_flat

    #Remove columns 100% gapped
    remove_completely_gapped_columns("$filename_prefix", $suffix_1, $suffix_2);

    return;
}

sub remove_completely_gapped_columns {
    # Remove columns 100% gapped
    my ($filename_prefix, $suffix_1, $suffix_2) = @_;

    #FIXME Or better with trimAl because exit code trapping problem with the sed after tcoffee ?!?
    #FIXME TrimAl problem: it adds seq_length bp after the seq_name !!!

    # Could be replaced by other tools like TrimAL for this !!!!
    #FIXME Some ensembl transcripts have stop codons (* in AA seq) that break trimAl 1.2
    system("perl -i -pe 's/\\\*/x/g' $filename_prefix.$suffix_1");
    #                                                                                   add some non-alphanumeric char for ensembl metazoa id
    system("trimal -in $filename_prefix.$suffix_1 -noallgaps | perl -pe 's/>([\\w:\\.\\-]+).*\$/>\$1/' > $filename_prefix.$suffix_2")==0
        or Selectome::Utils::error("Cannot remove 100% gapped columns for '$filename_prefix.$suffix_1'", $filename_prefix);
    unlink "$filename_prefix.$suffix_1";

    Selectome::Utils::error("Removing 100% gapped columns failed for '$filename_prefix.$suffix_1'", $filename_prefix)  if ( ! -e "$filename_prefix.$suffix_2" || -z "$filename_prefix.$suffix_2" );

    return;
}

1;
