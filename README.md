Set of scripts for the Selectome pipeline:
- Prepare data
- Filter MSAs and trees
- Compute positive selection predictions
- Fill the database
- ...